# taken from https://github.com/brainhubeu/react-native-opencv-tutorial/blob/master/downloadAndInsertOpenCV.sh

# ios

git_url=https://deepak7855@bitbucket.org/deepak7855/vumi.git

commit_msg=firstinit


git init
git add . 
git commit -m commit_msg
git remote add origin git_url
git push origin master --force


# wget ${base_url}/opencv-${version}-ios-framework.zip
# unzip -a opencv-${version}-ios-framework.zip
# cd ios
# cp -r ./../opencv2.framework ./
# cd ..
# rm -rf opencv-${version}-ios-framework.zip
# rm -rf opencv2.framework/

# # android

# wget ${base_url}/opencv-${version}-android-sdk.zip
# unzip opencv-${version}-android-sdk.zip
# cd android/app/src/main
# mkdir jniLibs
# cp -r ./../../../../OpenCV-android-sdk/sdk/native/libs/ ./jniLibs
# cd ../../../../
# rm -rf opencv-${version}-android-sdk.zip
# rm -rf OpenCV-android-sdk/

 