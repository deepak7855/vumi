import ImagePicker from 'react-native-image-picker';
import { Platform } from "react-native";
import Helper from '../lib/Helper';
import { request, PERMISSIONS, RESULTS, openSettings } from 'react-native-permissions';

export default class CameraController {
    static async open(cb) {
        Platform.OS == 'android' ?
            request(PERMISSIONS.ANDROID.CAMERA).then(result => {
                if (result == 'granted') {
                    request(PERMISSIONS.ANDROID.READ_EXTERNAL_STORAGE).then(resultStorage => {
                        if (resultStorage == 'granted') {
                            this.selecteImage(cb);
                        } else {
                            Helper.confirm("Allow " + 'Vumi' + " access to your device's photo,media and camera.", (status) => {
                                if (status) {
                                    openSettings().catch(() => console.warn('cannot open settings'));
                                }
                            })
                        }
                    })
                } else {
                    Helper.confirm("Allow " + 'Vumi' + " access to your device's photo,media and camera.", (status) => {
                        if (status) {
                            openSettings().catch(() => console.warn('cannot open settings'));
                        }
                    })
                }
            })
            :
            this.selecteImage(cb)
    }

    static selecteImage(cb) {
        var options = {
            title: 'Select type',
            quality: 0.2,
            // storageOptions: {
            //     skipBackup: true,
            //     path: 'images',
            // },
        };
        ImagePicker.showImagePicker(options, async (response) => {
            console.log(response, "responseresponse");
            if (response.didCancel) {
                console.log('-----User cancelled image picker');
            } else if (response.error) {
                console.log('-------ImagePicker Error: ' + response.error);
            } else {
                response.uri = Platform.OS === "android" ? response.uri : response.uri.replace("file://", "");
                cb(response);
            }
        });
    }

}
