export const Colors = {
  appGreen: '#45eaa4',
  lightBlack: '#202026',
  cloudyBlue: "#c0bed1",
  purple: '#7494ff',
  purplishGrey: "#6a697b",
  charcoalGrey: "#4b4b52",
  coolGrey: "#b5b5bc",
  white: "#ffffff",
  blackgrey: '#707070',
  white: '#fff',
  red: '#E57D81',
  button: '#036675',
  primeryGreen: "#79c2b8",
  cloudy_white: "#cbcbcb",
  yellow: "#E88746",
  concrete: "#f2f2f2",
  lightgrey: '#ACAFB3',
  royalGreen: '#6dd400',
  txtColor: 'rgba(153,138,242,1.5)',
  btxtColor: 'rgba(104 ,83 ,224 ,3.3)',
  abuttonColor: 'rgb(30, 206, 199)',
  dblue: '#5e5ce6',
  lightBackgroundColor: 'rgb(245, 245, 245)',
  lineBorderColor: 'rgb(235, 235, 237)',
  headerPurple: 'rgb(111, 87, 255)',
  orange: 'rgb(240, 134, 55)',
  black: 'black',
  bluedark: 'rgb(94,92,230)',
  bluelight: 'rgb(77,75,209)',




  ///Use Color New Screens
  blue: 'rgba(94,92,230,255)',

}