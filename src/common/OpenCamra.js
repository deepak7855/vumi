import React, { Component } from 'react'
import { Text, View, TextInput, StyleSheet, TouchableOpacity, Image, Alert, Dimensions, Platform, TouchableWithoutFeedback } from 'react-native'
import ImageZoom from 'react-native-image-pan-zoom';
import { RNCamera } from 'react-native-camera';
import { Images } from './Images';
import ViewShot from "react-native-view-shot";
import fonts from './fonts';
import CameraRoll from "@react-native-community/cameraroll";
import Toast from 'react-native-root-toast';
import { Stopwatch } from 'react-native-stopwatch-timer';
import Video from 'react-native-video';
import ProgressBar from "react-native-progress/Bar";


const screen_width = Dimensions.get('window').width;
const screen_height = Dimensions.get('window').height;

export default class OpenCamra extends Component {
    constructor(props) {
        super(props);
        this.state = {
            torchOn: false,
            backCamera: true,
            recording: false,
            cameraImage: '',
            croppedImage: '',
            shwoImageforCrop: false,
            showPhoneCamera: true,
            selectedImagePreviewIndex: '',
            stopwatchReset: false,
            videoUri: '',
            previewVideo: false,
            pauseVideo: false,
            timer: null,
            minutes_Counter: '00',
            seconds_Counter: '00',
        }
    }

    handleLoad = (meta) => {
        this.setState({
            duration: meta.duration
        })
    }

  
    handleProgress = progress => {
        this.setState({
          progress: parseFloat(progress.currentTime / progress.playableDuration),
        });
      };

    takePicture = async () => {
        if (this.camera) {
            const options = { quality: 0.5, base64: true };
            const data = await this.camera.takePictureAsync(options);
            this.setState({ cameraImage: data.uri })
        }
    };


    async startRecording() {
        this.startTimer();
        const { uri, codec = "mp4" } = await this.camera.recordAsync();

        if (this.state.minutes_Counter <= '00' && this.state.seconds_Counter < '03') {
            Toast.show('Video must be of minimum 3 seconds!', { position: 0, duration: 2000 })
            this.setState({
                videoUri: '',
                previewVideo: false,
                showPhoneCamera: true,
                recording: false,
                timer: null,
                minutes_Counter: '00',
                seconds_Counter: '00'
            })
        } else {
            this.setState({
                videoUri: uri,
                previewVideo: true,
                showPhoneCamera: false,
                recording: true
            })
        }
    }

    stopRecording() {
        this.camera.stopRecording();
        clearInterval(this.state.timer);
        this.setState({ recording: false });
    }

    startTimer = () => {
        let timer;
        clearInterval(timer)
        timer = setInterval(() => {

            var num = (Number(this.state.seconds_Counter) + 1).toString(),
                count = this.state.minutes_Counter;

            if (Number(this.state.seconds_Counter) == 59) {
                count = (Number(this.state.minutes_Counter) + 1).toString();
                num = '00';
            }

            this.setState({
                minutes_Counter: count.length == 1 ? '0' + count : count,
                seconds_Counter: num.length == 1 ? '0' + num : num
            });
        }, 1000);
        this.setState({ timer });
    }

    onCrop = () => {
        this.refs.viewShot.capture().then(uri => {
            if (this.props.cameraImageArray.length < 3) {
                this.setState({ croppedImage: uri, cameraImage: '' })

            }
            this.props.capturePicture(uri)
        });
    }

    onDownload = (params) => {
        if (this.state.previewVideo) {
            CameraRoll.saveToCameraRoll(this.state.videoUri, "video").then((success) => {
                Toast.show('Video Saved in Gallery!', { position: 0 })
            });
        } else {
            CameraRoll.saveToCameraRoll(this.state.cameraImage, "photo").then((success) => {
                Toast.show('Image Saved in Gallery!', { position: 0 })
            });
        }
    }

    onCubePress = (index) => {
        if (this.props.selectedCubes == index) {
            this.setState({ showPhoneCamera: true, selectedImagePreviewIndex: '' })
        } else {
            this.setState({ showPhoneCamera: false, selectedImagePreviewIndex: index })
        }
    }

    onImagePreviewNextPress = () => {
        this.refs.viewShot.capture().then(uri => {
            this.props.onImagePreviewNextPress(this.state.selectedImagePreviewIndex, uri)
        });
    }

    onVideoPreviewNextPress = () => {
        // alert('p')
        this.setState({ pauseVideo: true }, () => {
            this.props.captureVideo(this.state.videoUri)
        })
    }

    methodRetake() {
        this.props.onImagePreviewRetakePress(this.state.selectedImagePreviewIndex)
    }
methodRetakeVideo(){
    this.setState({
        videoUri: '',
        previewVideo: false,
        showPhoneCamera: true,
        recording: false,
        timer: null,
        minutes_Counter: '00',
        seconds_Counter: '00'
    })
    this.props.onVideoPreviewRetakePress()

    
}
    render() {
        return (
            <>
                <View style={styles.container}>

                    <View style={{ height: screen_height / 10, backgroundColor: "#232323", width: "100%", justifyContent: "center", alignItems: "center" }}>
                        {
                            this.props.cameraType == 'Picture' ?

                                <View style={{ flexDirection: "row" }}>
                                    {
                                        [0, 1, 2, 3].map((item, index) => (
                                            <TouchableOpacity
                                                onPress={() => this.onCubePress(index)}
                                                disabled={this.props.selectedCubes < index ? true : false}
                                                style={{ backgroundColor: this.props.selectedCubes > index ? '#cbcbcb' : this.props.selectedCubes == index ? '#323334' : 'black', marginHorizontal: 5, height: 22, width: 22, borderRadius: 5, justifyContent: "center", alignItems: 'center' }}>
                                                {this.props.selectedCubes == index && <Text style={{ color: '#444646', fontSize: 16 }}>+</Text>}
                                            </TouchableOpacity>
                                        ))
                                    }
                                </View>
                                :
                                <View style={{ flex: 1, alignItems: "center", width: '100%' }}>
                                    <View style={{ flex: 1, justifyContent: 'center', alignItems: "center", width: '100%', flexDirection: 'row' }}>
                                        <View style={{ height: 8, width: 8, borderRadius: 4, backgroundColor: '#ea0054' }} />
                                        <Text style={styles.counterText}>{this.state.minutes_Counter} : {this.state.seconds_Counter}</Text>
                                    </View>
                                    <Text style={{ color: '#707070', fontSize: 12, marginBottom: 15 }}>Tap and hold anywhere to record a clip</Text>
                                </View>
                        }
                    </View>

                    {
                        this.state.showPhoneCamera ?

                            <View style={{ flex: 1 }}>
                                {
                                    this.state.cameraImage ?

                                        <ViewShot ref="viewShot" options={{ format: "jpg", quality: 0.9 }}>
                                            <ImageZoom cropWidth={screen_width}
                                                cropHeight={screen_width}
                                                imageWidth={screen_width}
                                                imageHeight={screen_width}>
                                                <Image resizeMode='cover' style={styles.preview}
                                                    source={{ uri: this.state.cameraImage }} />
                                            </ImageZoom>
                                        </ViewShot>
                                        :
                                        <RNCamera
                                            ref={ref => {
                                                this.camera = ref;
                                            }}
                                            style={styles.preview}
                                            type={this.state.backCamera ? RNCamera.Constants.Type.back : RNCamera.Constants.Type.front}
                                            flashMode={this.state.torchOn ? RNCamera.Constants.FlashMode.on : RNCamera.Constants.FlashMode.off}
                                            ratio={'1:1'}
                                            androidCameraPermissionOptions={{
                                                title: 'Permission to use camera',
                                                message: 'We need your permission to use your camera',
                                                buttonPositive: 'Ok',
                                                buttonNegative: 'Cancel',
                                            }}
                                            androidRecordAudioPermissionOptions={{
                                                title: 'Permission to use audio recording',
                                                message: 'We need your permission to use your audio',
                                                buttonPositive: 'Ok',
                                                buttonNegative: 'Cancel',
                                            }}
                                        />
                                }

                                <View style={{ height: screen_height / 5, backgroundColor: "#232323", width: "100%" }}>

                                    <View style={{ flexDirection: "row", justifyContent: "space-between", marginTop: 5, marginHorizontal: 20 }}>

                                        <TouchableOpacity onPress={() => this.onDownload()}>
                                            <Image
                                                style={{ height: 22, width: 22, resizeMode: "contain" }}
                                                source={this.state.cameraImage ? Images.download : ""}
                                            />
                                        </TouchableOpacity>
                                        {
                                            this.state.cameraImage ?
                                                <TouchableOpacity onPress={() => { this.setState({ cameraImage: '' }) }}>
                                                    <Text style={{ color: '#2eb7ff' }}>Retake</Text>
                                                </TouchableOpacity>
                                                :
                                                <View style={{ flexDirection: "row", justifyContent: "flex-end" }}>
                                                    <TouchableOpacity onPress={() => this.setState({ torchOn: !this.state.torchOn })}>
                                                        <Image
                                                            style={{ height: 22, width: 22, resizeMode: "contain", marginRight: 20, }}
                                                            source={Images.flash}
                                                        />
                                                    </TouchableOpacity>
                                                    <TouchableOpacity onPress={() => this.setState({ backCamera: !this.state.backCamera })}>
                                                        <Image
                                                            style={{ height: 22, width: 22, resizeMode: "contain" }}
                                                            source={Images.camera}
                                                        />
                                                    </TouchableOpacity>
                                                </View>
                                        }
                                    </View>
                                    <View style={{ width: '100%', justifyContent: "center", alignItems: "center", flexDirection: "row", flex: 1 }}>

                                        {
                                            this.props.cameraType == 'Picture' ?
                                                <View style={{ width: '100%', flex: 1, justifyContent: "center", alignItems: "center", }}>
                                                    {
                                                        this.state.cameraImage ?
                                                            <View style={{ flex: 1, alignItems: "center", justifyContent: "center", width: '100%', }}>
                                                                <Text style={{ color: '#b9b9b9', fontSize: 12, marginBottom: 10 }}>Drag and pinch photo to crop.</Text>
                                                                <TouchableOpacity onPress={() => this.onCrop()} style={{ paddingVertical: 10, backgroundColor: '#48a2ff', width: '60%', borderRadius: 30, alignItems: 'center' }}>
                                                                    <Text style={{ fontSize: 16, color: 'white', fontFamily: fonts.Poppins_Bold }}>Next</Text>
                                                                </TouchableOpacity>
                                                            </View>
                                                            :
                                                            <TouchableOpacity onPress={this.takePicture.bind(this)} style={styles.capture} />

                                                    }
                                                </View>
                                                :
                                                <TouchableOpacity onPressIn={() => this.startRecording()} onPressOut={() => this.stopRecording()} style={styles.capture} >
                                                    {/* <Text style={{ fontSize: 12, color: "white" }}>{this.state.recording ? "Stop" : "Start"}</Text> */}
                                                </TouchableOpacity>
                                        }

                                    </View>
                                </View>
                            </View>
                            :

                            // Preview Section Begins
                            <View style={{ flex: 1, }}>
                                {
                                    this.state.previewVideo ?
                                        <View style={{ flex: 1, alignItems: "center" }}>
                                            <Video
                                                source={{ uri: this.state.videoUri }}
                                                // source={require('../assets/video.mp4')}
                                                ref={(ref) => {
                                                    this.player = ref
                                                }}
                                                onProgress={this.handleProgress}
                                                onLoad={this.handleLoad}
                                                repeat
                                                resizeMode="cover"
                                                paused={this.state.pauseVideo}
                                                style={styles.video}
                                            />
                                            <ProgressBar
                                                progress={this.state.progress}
                                                color="#00cd8e"
                                                unfilledColor="#282828"
                                                borderColor="transparent"
                                                width={screen_width}
                                                borderRadius={0}
                                                height={18}
                                            />

                                            <View style={{ height: screen_height / 5, backgroundColor: "#232323", width: "100%" }}>

                                                <View style={{ flexDirection: "row", justifyContent: "space-between", marginTop: 5, marginHorizontal: 20 }}>
                                                    <TouchableOpacity onPress={() => this.onDownload()}>
                                                        <Image
                                                            style={{ height: 22, width: 22, resizeMode: "contain" }}
                                                            source={Images.download}
                                                        />
                                                    </TouchableOpacity>

                                                    <TouchableOpacity onPress={() => this.methodRetakeVideo()}>
                                                        <Text style={{ color: '#2eb7ff' }}>Retake</Text>
                                                    </TouchableOpacity>
                                                </View>

                                                <View style={{ flex: 1, alignItems: "center", justifyContent: "center", width: '100%', }}>
                                                    <TouchableOpacity onPress={() => this.onVideoPreviewNextPress()} style={{ paddingVertical: 10, backgroundColor: '#48a2ff', width: '60%', borderRadius: 30, alignItems: 'center' }}>
                                                        <Text style={{ fontSize: 16, color: 'white', fontFamily: fonts.Poppins_Bold }}>Next</Text>
                                                    </TouchableOpacity>
                                                </View>
                                            </View>
                                        </View>
                                        :

                                        //Preview Image Section Begins
                                        <View style={{ flex: 1, }}>
                                            <View style={{ flex: 1, backgroundColor: '#232323', alignItems: "center", justifyContent: "center" }}>

                                                <ViewShot ref="viewShot" options={{ format: "jpg", quality: 0.9 }}>
                                                    <ImageZoom
                                                        reset={this.state.resetZoom}
                                                        cropWidth={screen_width}
                                                        cropHeight={screen_width}
                                                        imageWidth={screen_width}
                                                        imageHeight={screen_width}>
                                                        <Image resizeMode='cover' style={styles.preview}
                                                            source={{ uri: this.props.cameraImageArray[this.state.selectedImagePreviewIndex] }} />
                                                    </ImageZoom>
                                                </ViewShot>
                                            </View>
                                            <View style={{ height: screen_height / 5, backgroundColor: "#232323", width: "100%" }}>

                                                <View style={{ flexDirection: "row", justifyContent: "space-between", marginTop: 5, marginHorizontal: 20 }}>
                                                    <TouchableOpacity onPress={() => this.onDownload()}>
                                                        <Image
                                                            style={{ height: 22, width: 22, resizeMode: "contain" }}
                                                            source={Images.download}
                                                        />
                                                    </TouchableOpacity>

                                                    <TouchableOpacity onPress={() => this.methodRetake()}>
                                                        <Text style={{ color: '#2eb7ff' }}>Retake</Text>
                                                    </TouchableOpacity>
                                                </View>

                                                <View style={{ flex: 1, alignItems: "center", justifyContent: "center", width: '100%', }}>
                                                    <Text style={{ color: '#b9b9b9', fontSize: 12, marginBottom: 10 }}>Drag and pinch photo to crop.</Text>
                                                    <TouchableOpacity onPress={() => this.onImagePreviewNextPress()} style={{ paddingVertical: 10, backgroundColor: '#48a2ff', width: '60%', borderRadius: 30, alignItems: 'center' }}>
                                                        <Text style={{ fontSize: 16, color: 'white', fontFamily: fonts.Poppins_Bold }}>Next</Text>
                                                    </TouchableOpacity>
                                                </View>
                                            </View>
                                        </View>
                                }
                            </View>
                    }
                </View>
            </>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        flex: 1, backgroundColor: "#232323"
    },
    preview: {
        width: screen_width, aspectRatio: 1,
    },
    capture: {

        backgroundColor: "#ff2e56",
        height: 60, width: 60,
        borderRadius: 30,
        borderColor: "white",
        borderWidth: 2,
        justifyContent: "center",
        alignItems: "center"
    },
    video: {
        width: screen_width,
        aspectRatio: 1,
        marginBottom: 5
    },
    counterText: {
        fontSize: 16,
        color: '#FFF',
        marginLeft: 7,
    }
});




const options = {
    container: {
        backgroundColor: 'transparent',
        marginVertical: 0,
    },
    text: {
        fontSize: 16,
        color: '#FFF',
        marginLeft: 7,
    },

};