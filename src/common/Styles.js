
import React, { Component } from 'react';
import { Text, View, StyleSheet, Dimensions, Image } from 'react-native';

export const { height, width } = Dimensions.get('window')


export const Fontfamily = {
  MEB: 'Montserrat-ExtraBold',
  MSB: 'Montserrat-SemiBold',
  MB: 'Montserrat-Bold',
  PB: "proximanova-black",
  PoPB: "Poppins-Bold",
  // SFProDisplayHeavy:'FontsFree-Net-SFProDisplay-Regular'
}

export const colors = {
  blackgrey: '#707070',
  white: '#fff',
  red: '#E57D81',
  button: '#036675',
  primeryGreen: "#79c2b8",
  cloudy_white: "#cbcbcb",
  yellow: "#E88746",
  concrete: "#f2f2f2",
  lightgrey: '#ACAFB3',
  royalGreen: '#6dd400',

  txtColor: 'rgba(153,138,242,1.5)',
  btxtColor: 'rgba(104 ,83 ,224 ,3.3)',
  abuttonColor: 'rgb(30, 206, 199)',
  dblue: '#5e5ce6'


}


export const CustomeImage = props => <Image
  {...props}
  resizeMode='contain'
  source={props.source}
  style={[{ height: props.size, width: props.size, }, props.style]}

/>

export const TxtW = props => <Text {...props} style={[{ color: 'white', fontSize: 20, fontWeight: 'bold', }, props.style]}>{props.children}</Text>
export const TxtB = props => <Text {...props} style={[{ color: '#707070', fontSize: 16, fontWeight: 'bold' }, props.style]}>{props.children}</Text>
export const TxtG = props => <Text {...props} style={[{ color: '#66C565', fontSize: 16, fontWeight: 'bold' }, props.style]}>{props.children}</Text>


export const Roe1by3 = (props) => {
  return (
    <View style={{ flex: 1, flexDirection: 'row', backgroundColor: props.bgColor }}>
      <View style={{ flex: props.children1flexSize, flexDirection: props.direction, alignItems: 'center', }}>
        {props.children1 && <>{props.children}</>}
      </View>

      <View style={{ flex: props.children2flexSize, flexDirection: props.direction, alignItems: 'center' }}>
        {props.children2 && <>{props.children}</>}
      </View>

      <View style={{ flex: props.children3flexSize, flexDirection: props.direction }}>
        {props.children3 && <>{props.children}</>}
      </View>
    </View>
  )
}

export const RowSpaceBetween = (props) => {
  return (
    <View style={styles.container}>
      {props.children}
    </View>
  )
}


// export const RowSpaceBetween = (props) => {
//   return (
//     <View style={styles.container}>
//       {props.children}
//     </View>
//   )
// }











export const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'space-between', padding: 10,
    marginVertical: 5, flexDirection: 'row',
    alignItems: 'center', marginVertical: 5,
  },
  homecontainer: {
    flex: 1,
  },
  homeHeader: {
    marginHorizontal: 20,
    height: 50,
    flexDirection: 'row',
    borderBottomWidth: 1,
    borderColor: '#ddd',
    // backgroundColor:'red',
    justifyContent: 'space-between',
    alignItems: 'center'

  },
  homeHeadercon: {

  },

  homeHeadertest: {

    fontSize: 25,
    // fontFamily: "Metropolis-Medium"
  },
  homeAddHeadertest: {
    fontSize: 14,
    marginTop: 10,
    //fontFamily: "Metropolis-Medium"
  },

  homeaddtickets: {
    alignItems: 'center'

  },
  containerslider: {
    width: 375,
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'transparent',
  },

  homeContaninarTab: {
    flexDirection: 'row',
    position: 'absolute',
    backgroundColor: '#fff',
    bottom: 0,
    left: 0,
    right: 0,
  },
  homeContaninarTabTicket: {
    flex: 1,
    paddingVertical: 5,
    alignItems: 'center',
    justifyContent: 'center',
  },
  homeContaninarTabGuide: {
    flex: 1,
    paddingVertical: 5,
    alignItems: 'center',
    justifyContent: 'center',
  },
  homeContaninarTabprofile: {
    flex: 1,
    paddingVertical: 5,
    alignItems: 'center',
    justifyContent: 'center',
  },
  homeContaninarTabvive: {
    alignItems: 'center',
    justifyContent: 'center',
  },

  homeCon_Text_Log: {
    fontSize: 14,
    // fontFamily: "Metropolis-Medium"
  },

  homeImageTick: {
    height: 30,
    width: 30,
  },
  homeImagepuls: {
    height: 20,
    width: 20, marginHorizontal: 5

  },
  homeImageUserImage: {
    height: 50,
    width: 50,
  },
  homeImageUserView: {
    height: 50,
    width: 50,
    borderColor: 'red',
    borderWidth: 1,
    borderRadius: 30
  },
  homeImageUserView1: {
    height: 50,
    width: 50,

  },
  homeview: {
    top: -5, height: 1.2, backgroundColor: '#A2A1A1', marginLeft: 30, marginRight: 42,
  },
  homeviewprofile: {
    top: -5, height: 1.2, backgroundColor: '#A2A1A1', marginLeft: 30, marginRight: 30,
  },
  homeaddress: {
    marginVertical: 10,
    alignItems: 'center',
    flexDirection: 'row',

  },
  homeImaProfile: {
    height: 100,
    width: 100,
    borderWidth: 1,
    borderRadius: 100 / 2
  },
  homeProfileview: {
    marginVertical: 20,
    justifyContent: 'center', alignItems: 'center'
  },

  homeImageSetting: {
    height: 30,
    width: 30,

  },
  homeprofilesetting: {
    position: 'absolute', right: 20, bottom: 0,
  },
  homeprofilefilde: {
    backgroundColor: '#E4E7E7', paddingVertical: 15, paddingHorizontal: 20, marginVertical: 5,
  },

  homeImaGuide: {
    height: 50,
    width: 50,

  },
  homeCon_TextGuide: {
    fontSize: 16,
    // fontFamily: "Metropolis-Medium"
  },
  homeCon_TextGuide_Loc: {
    fontSize: 20,
    //  fontFamily: "Metropolis-Medium"
  },
  homeGuideLoc: {
    backgroundColor: '#5DDCC0', marginHorizontal: 50, marginVertical: 15, paddingVertical: 10, alignItems: 'center', justifyContent: 'center', borderRadius: 20,
  },
  homeGuideOR: {
    marginHorizontal: 50, marginVertical: 15, paddingVertical: 10, alignItems: 'center', justifyContent: 'center',
  },

  homeGuideSearch: {
    backgroundColor: '#7A7A7A', marginHorizontal: 50, marginVertical: 15, paddingVertical: 10, alignItems: 'center', justifyContent: 'center', borderRadius: 20,
  },
  homealignItems: {
    alignItems: 'center', justifyContent: 'center',
    paddingVertical: 10,
  },
  input: {
    marginHorizontal: 15,
    marginVertical: 10, borderWidth: 0.5, borderColor: '#ddd',
    alignItems: 'center',
    justifyContent: 'center',
    textAlign: 'center',
    fontSize: 18, paddingHorizontal: 5,
    height: 45,
    //fontFamily: "Metropolis-Medium"
  },
  input1: {
    marginHorizontal: 25,
    alignItems: 'center',
    justifyContent: 'center',
    textAlign: 'center',
    fontSize: 20,
    // fontFamily: "Metropolis-Medium"
  },
  marginTopTtwinty: {
    marginTop: 20,
  },
  homeImaSEAT: {
    height: 35,
    width: 35,

  },
  homeHeaderteSEAT: {
    // marginTop: 7,
    fontSize: 20, paddingVertical: 3,
    // fontFamily: "Metropolis-SemiBold"
  },
  SEATbuttonView: {
    width: 100, height: 100, alignItems: 'center', justifyContent: 'center', paddingHorizontal: 10, paddingVertical: 10, borderRadius: 50,
  },
  homeHeadertether: {
    fontSize: 15,
    // fontFamily: "Metropolis-SemiBold"
  },
  Addcontainer: {
    flex: 1,
  },
  AddHeadercon: {
    alignItems: 'center', justifyContent: 'center'
  },
  AddHeadertest: {
    marginTop: 15,
    fontSize: 22,
    //fontFamily: "Metropolis-Medium"
  },
  AddalignItems: {
    alignItems: 'center', justifyContent: 'center',
    paddingVertical: 10,
  },
  AddHeadertest1: {
    marginTop: 15,
    fontSize: 25,
    // fontFamily: "Metropolis-Medium"
  },
  Addaddress: {
    marginVertical: 10,
    justifyContent: 'center',
    flexDirection: 'row',

  },
  AAddHeadertest: {
    fontSize: 14,
    // fontFamily: "Metropolis-Medium"
  },
  AddSEAtbuttonView: {
    paddingHorizontal: 100, paddingVertical: 7, borderRadius: 30, alignItems: 'center'
  },
  AddalignItems: {
    alignItems: 'center', justifyContent: 'center',
    paddingVertical: 10,
  },
  ADDcapture_ticket_info: {
    height: 150,
    width: 150, marginHorizontal: 10

  },
  AddTicketsaddress: {
    //fontFamily: "Metropolis-Medium"
  },


});

