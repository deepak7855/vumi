import React from 'react';
import { TextInput, View, Text, Image, TouchableOpacity } from 'react-native';
import { Fontfamily, Colors } from './Styles';
TextInput.defaultProps.selectionColor = 'black'

const Input = (props) => {
    const { inputStyle, labelStyle, containerStyle } = styles;
    return (
        <TextInput
            //  tintColor={'black'}
            secureTextEntry={props.secureTextEntry}
            placeholder={props.placeholder}
            autoCorrect={false}
            placeholderTextColor="rgb(129,129,129)"
            style={[inputStyle, { borderColor: props.borderColor, borderBottomWidth: 1, width: 320, marginVertical: 10 }]}
            value={props.value}
            onChangeText={props.onChangeText}
            keyboardType={props.keyboardType}
            maxLength={props.maxLength}
            returnKeyType={props.returnKeyType}
            blurOnSubmit={props.blurOnSubmit}
            onSubmitEditing={props.setFocus}
            ref={props.getFocus}
            editable={props.inputedit}
        />
    );
};

export { Input };


export const IconInput = (props) => {
    const { inputStyle, labelStyle, containerStyle } = styles;
    return (

        <View style={{
            flexDirection: 'row', alignItems: 'center', backgroundColor: '#f8f8fa',
            borderRadius: 32.5, width: props.width ? props.width : 320, marginVertical: 10,
        }}>
            <View style={{ paddingHorizontal: 10 }}>
                {props.limagePath && <Image
                    source={props.limagePath}
                    resizeMode='contain'
                    style={props.leftImagetyle ? props.leftImagetyle : { height: 18, width: 18, marginVertical: 10, marginLeft: 10 }}
                />}
            </View>
            <View style={{ alignItems: 'center' }}>
                <TextInput
                    secureTextEntry={props.secureTextEntry}
                    placeholder={props.placeholder}
                    autoCorrect={false}
                    placeholderTextColor={props.placeholderTextColor}
                    selectionColor={'green'}
                    style={[inputStyle, props.style]}
                    value={props.value}
                    onChangeText={props.onChangeText}
                    keyboardType={props.keyboardType}
                    multiline={props.multiline}
                    maxLength={props.maxLength}
                    returnKeyType={props.returnKeyType}
                    blurOnSubmit={props.blurOnSubmit}
                    onSubmitEditing={props.setFocus}
                    ref={props.getFocus}
                    editable={props.inputedit}
                    textContentType={props.textContentType}
                />
            </View>

            {props.rimagePath && <TouchableOpacity
                disabled={props.rightIconDisable}
                onPress={() => props.onClick()}
                style={{ width: 50, right: 40, marginHorizontal: 20, borderWidth: 0 }}>
                <Image
                    source={props.rimagePath}
                    resizeMode='contain'
                    style={{
                        height: 24, width: 24, left: 10,
                        marginVertical: 10, alignSelf: 'center',
                        tintColor: props.activeIcon
                    }}
                />
            </TouchableOpacity>}


        </View>
    )
}


const styles = {
    inputStyle: {
        // color:'#000',
        paddingLeft: 5,
        fontSize: 18,
        fontFamily: Fontfamily.MB,
        // backgroundColor: '#dddd',
        width: 250,
        alignItems: 'center',
        borderRadius: 32.5,
        // elevation:3,

        height: 65,
    },
    containerStyle: {
        height: 40,
        borderWidth: 1,
        flexDirection: 'row',
        // alignItems: 'center',
        // flex:1
    },

}