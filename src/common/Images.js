export const Images = {


    bg: require('../assets/icons/vumiIcons/bg.png'),
    userName: require('../assets/icons/vumiIcons/lock_02.png'),
    pass: require('../assets/icons/vumiIcons/pass.png'),
    Loginbg: require('../assets/icons/vumiIcons/bg.png'),


    Header: {
        leftArrow: require('../assets/icons/vumiIcons/back.png'),
        back_grey: require('../assets/icons/vumiIcons/back_grey.png'),
    },


    getStart: {
        usericon: require('../assets/icons/vumiIcons/profile.png'),
    },

    signUp: {
        chikuIcon: require('../assets/icons/vumiIcons/setting.png'),
    },

    changePasword: {
        eye: require('../assets/icons/vumiIcons/eye_grey.png'),
        smile: require('../assets/icons/vumiIcons/lock.png'),
    },


    notification: {
        bg: require('../assets/icons/vumiIcons/bg.png'),
        turn_noti: require('../assets/icons/vumiIcons/turn_noti.png'),
    },
    referral: require('../assets/icons/vumiIcons/ticks.png'),

    profile: {
        coin: require('../assets/icons/vumiIcons/coin.png'),
        heart: require('../assets/icons/vumiIcons/heart.png'),
        user1: require('../assets/icons/vumiIcons/f569dc33964384f06838856f079fb4fa.png'),
        user2: require('../assets/icons/vumiIcons/4a1fc7fe129c5f0202067916da4bae53.png'),
        user3: require('../assets/icons/vumiIcons/6e10ebfe4a5044088d8e2bf7220d317e.png'),
        info: require('../assets/icons/vumiIcons/info.png'),
        tree: require('../assets/icons/vumiIcons/tree.png'),
        borderview: require('../assets/icons/vumiIcons/borderview.png'),

        
    },
    coins1: {
        search: require('../assets/icons/vumiIcons/search.png'),
    },
    overView: {
        heart_grey: require('../assets/icons/vumiIcons/heart_grey.png'),
        eye_grey: require('../assets/icons/vumiIcons/eye.png'),
    },

    uploadpost1: {
        video: require('../assets/icons/vumiIcons/videos.png'),
        photos: require('../assets/icons/vumiIcons/photos.png'),
        music: require('../assets/icons/vumiIcons/music.png'),

        videonew: require('../assets/icons/vumiIcons/videonew.png'),
        photosnew: require('../assets/icons/vumiIcons/cameranew.png'),
        musicnew: require('../assets/icons/vumiIcons/musicnew.png'),
    },


    coin: require('../assets/icons/vumiIcons/coin.png'),
    coinsbg: require('../assets/icons/vumiIcons/coinsbg.png'),

    
    dummy_user: require('../assets/icons/user5.png'),
    watch_video: require('../assets/icons/watch_Video.png'),
    heart_grey: require('../assets/icons/vumiIcons/heart_grey.png'),
    heart_grey1:require('../assets/icons/vumiIcons/heart_grey1.png'),
    lock: require('../assets/icons/vumiIcons/pass.png'),
    lockicon:require('../assets/icons/vumiIcons/lockicon.png'),
    brdr_heart: require('../assets/icons/vumiIcons/brdr_heart.png'),
    send: require('../assets/icons/vumiIcons/send.png'),
    bloak_usr: require('../assets/icons/vumiIcons/bloak_usr.png'),
    brdr_msg: require('../assets/icons/vumiIcons/brdr_msg.png'),
    close: require('../assets/icons/vumiIcons/close.png'),
    all: require('../assets/icons/vumiIcons/no-coins.png'),
    mind: require('../assets/icons/vumiIcons/transactin.png'),
    eye_grey: require('../assets/icons/vumiIcons/eye.png'),

    y_star: require('../assets/icons/vumiIcons/y_star.png'),
    strom: require('../assets/icons/vumiIcons/strom.png'),
    dummy1: require('../assets/icons/vumiIcons/dummy.png'),
    user_dummy:require('../assets/icons/user_dummy.png'),
    tv: require('../assets/icons/vumiIcons/tv.png'),
    games: require('../assets/icons/vumiIcons/games.png'),

    k3: require('../assets/icons/vumiIcons/3k.png'),
    k5: require('../assets/icons/vumiIcons/5k.png'),
    k7: require('../assets/icons/vumiIcons/7k.png'),
    k9: require('../assets/icons/vumiIcons/9k.png'),

    active_coin: require('../assets/icons/vumiIcons/active_coin.png'),
    inactive_coin: require('../assets/icons/vumiIcons/inactive_coin.png'),
    globs_bg: require('../assets/icons/vumiIcons/globs_bg.png'),

    yellow_star: require('../assets/icons/vumiIcons/yellow_star.png'),
    chats: require('../assets/icons/vumiIcons/chats.png'),
    blew_eye: require('../assets/icons/vumiIcons/bluw_eye.png'),
    heart: require('../assets/icons/vumiIcons/heart.png'),
    emails: require('../assets/icons/vumiIcons/emails.png'),
    eye: require('../assets/icons/vumiIcons/eye_grey.png'),
    ticks: require('../assets/icons/vumiIcons/ticks.png'),
    eye_grey_color: require('../assets/icons/vumiIcons/eye_grey.png'),
    right_arrow: require('../assets/icons/vumiIcons/right_arrow.png'),
    star: require('../assets/icons/vumiIcons/star.png'),
    add_grey: require('../assets/icons/vumiIcons/add_grey.png'),
    addpost: require('../assets/icons/vumiIcons/addpost.png'),

    three_dot: require('../assets/icons/vumiIcons/three_dot.png'),
    monitor: require('../assets/icons/vumiIcons/monitor.png'),
    dashed: require('../assets/icons/vumiIcons/dashed.png'),
    active_watch: require('../assets/icons/vumiIcons/active_watch.png'),
    active_overview: require('../assets/icons/vumiIcons/active_overview.png'),
    inactive_overview: require('../assets/icons/vumiIcons/inactive_overview.png'),
    active_games: require('../assets/icons/vumiIcons/active_games.png'),
    grey_close: require('../assets/icons/vumiIcons/grey_close.png'),
    setting: require('../assets/icons/vumiIcons/setting.png'),
    blue_box: require('../assets/icons/vumiIcons/blue_box.png'),
    one: require('../assets/icons/vumiIcons/one.png'),
    two: require('../assets/icons/vumiIcons/two.png'),
    three: require('../assets/icons/vumiIcons/three.png'),
    cat: require('../assets/icons/vumiIcons/cat.png'),
    green_plus: require('../assets/icons/vumiIcons/green_plus.png'),
    star_grey: require('../assets/icons/vumiIcons/star_grey.png'),
    fast_pass: require('../assets/icons/vumiIcons/fast_pass.png'),
    round_close: require('../assets/icons/vumiIcons/grey_close.png'),
    cross_pink: require('../assets/icons/vumiIcons/cross_pink.png'),
    eye_hidden: require('../assets/icons/vumiIcons/eye_hidden.png'),
    vumi: require('../assets/icons/vumiIcons/vumi.png'),
    splash: require('../assets/icons/vumiIcons/splash.png'),
    edit: require('../assets/icons/vumiIcons/edit.png'),
    more: require('../assets/icons/vumiIcons/more.png'),
    User_profile_icon: require('../assets/icons/vumiIcons/User_profile_icon.png'),
    Coinpurple: require('../assets/icons/vumiIcons/coinpurple.png'),

    
    download: require('../assets/icons/vumiIcons/download.png'),
    down_grey: require('../assets/icons/vumiIcons/down_grey.png'),
    up_grey: require('../assets/icons/vumiIcons/up-arrow.png'),

    slide1: require('../assets/icons/vumiIcons/slide1.png'),
    slide2: require('../assets/icons/vumiIcons/slide2.png'),
    slide3: require('../assets/icons/vumiIcons/slide3.png'),
    slide4: require('../assets/icons/vumiIcons/slide4.png'),
    camera: require('../assets/icons/vumiIcons/camera.png'),
    flash: require('../assets/icons/vumiIcons/flash.png'),


    White_5k: require('../assets/icons/vumiIcons/White_5k.png'),
    White_cube: require('../assets/icons/vumiIcons/White_cube.png'),
    White_cube_big: require('../assets/icons/vumiIcons/White_cube_big.png'),
    White_heart: require('../assets/icons/vumiIcons/White_heart.png'),

    packs: [
        green = require('../assets/icons/vumiIcons/green.png'),
        orange = require('../assets/icons/vumiIcons/orange.png'),
        purple = require('../assets/icons/vumiIcons/purple.png'),
        sky_blue = require('../assets/icons/vumiIcons/sky_blue.png')
    ],
    warning: require('../assets/icons/vumiIcons/warning.png'),
    UploadPost: {
        close: require('../assets/icons/UploadPost/close.png'),
        crop: require('../assets/icons/UploadPost/crop.png'),
        eye: require('../assets/icons/UploadPost/eye.png'),
        plus: require('../assets/icons/UploadPost/plus.png'),
    },

    flag : require('../assets/icons/vumiIcons/flag.png'),
    flag_colors : require('../assets/icons/vumiIcons/flag_colors.png'),

    starGrey : require('../assets/icons/vumiIcons/falged_star.png'),
    starColor : require('../assets/icons/vumiIcons/y_b_star.png'),

    X2 : require('../assets/icons/X2.png'),
    X3 : require('../assets/icons/X3.png'),
    K100 : require('../assets/icons/100K.png'),


    SendingCoins : require('../assets/JsonFiles/SendingCoins.json'),
    Coin_animation : require('../assets/JsonFiles/Coin_animation.json'),
    Vumi_LOAD : require('../assets/JsonFiles/Vumi_LOAD.json'),
    splashGif : require('../assets/JsonFiles/vumi.gif'),
    coinbg : require('../assets/JsonFiles/bg.gif'),

    
    

}



