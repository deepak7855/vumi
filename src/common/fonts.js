export default fonts = {
    SFProRegular: 'SFProDisplay-Regular',
    SFProMedium: 'SFProDisplay-Medium',
    SFProDisplay_Bold: 'SFProDisplay-Bold',
    SFProDisplay_Heavy: 'SFProDisplay-Heavy',
    Montserrat_ExtraBold: 'Montserrat-ExtraBold',
    Montserrat_SemiBold: 'Montserrat-SemiBold',
    Montserrat_Bold: 'Montserrat-Bold',
    proximanova_black: "proximanova-black",
    Poppins_Bold: "Poppins-Bold",
    Poppins_Medium: 'Poppins-Medium',
    SFProDisplay_Light:"SFProDisplay-Light",
    SFProDisplay_Semibold:'SFProDisplay-Semibold'
}

