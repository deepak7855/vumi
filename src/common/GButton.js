import React from 'react';
import { Text, View, TouchableOpacity, onPress, StyleSheet, Dimensions } from 'react-native';
import { Fontfamily } from './Styles';
import { tsPropertySignature } from '@babel/types';

const { height, width } = Dimensions.get('window')


export const GButton = (params) => (
  <TouchableOpacity onPress={params.onPress}
    disabled={params.disabled}
    style={[params.style, {
      height: params.height,
      borderRadius: params.radius,
      backgroundColor: params.bgColor,
    }, style.buttonStyle]}>
    <Text style={[style.txtstyle, { fontSize: params.fontSize ? params.fontSize : 22, color: params.txtcolor }]}>
      {params.bText}</Text>
  </TouchableOpacity>
);


const style = StyleSheet.create({
  txtstyle: {
    //fontSize: 22,
    // fontFamily: 'Metropolis-Thin',
    fontWeight: 'bold',
  },
  buttonStyle: {
    elevation: 0.5,
    width: 320,
    margin: 5,
    alignItems: 'center',
    justifyContent: 'center'
  }
})


export const GSmallButton = (params) => (
  <TouchableOpacity onPress={params.onPress}
    disabled={params.disabled}
    style={{
      height: params.height, borderRadius: 30,
      backgroundColor: params.bgColor,
      width: params.width, alignItems: 'center', justifyContent: 'center'
    }}>
    <Text style={{
      fontSize: 18,
      fontFamily: Fontfamily.PB,
      fontWeight: '600', color: '#fff', fontSize: 21,
    }}>{params.bText}</Text>
  </TouchableOpacity>
);

