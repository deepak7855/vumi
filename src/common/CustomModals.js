import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, TouchableOpacity, Modal, Button, 
  Image } from 'react-native';
// import { colors } from './colors'
 import {Images,string,Colors} from '../common'

export default class CustomModals extends Component {
  render() {
    return (
      <Modal
        animationType={"fade"}
        transparent={true}
        visible={this.props.visible}
        onRequestClose={() => { this.props.hidemodal() }}>
        {/*All views of Modal*/}
        <View style={styles.modalmainView}>
          <View style={styles.modal}>
            <Text style={styles.text}>{this.props.tilte}</Text>


            <View style={{ alignItems: 'center', marginTop: 10 }}>
              <Text style={[styles.AddTicketsaddress, { color: Colors.yellow, fontSize: 25, }]}>{this.props.addrees}</Text>
              <Text style={[styles.AddTicketsaddress, { color: Colors.yellow, fontSize: 16, }]}>{this.props.addrees1}</Text>
              <Text style={[styles.AddTicketsaddress, { color: Colors.yellow, fontSize: 16, }]}>{this.props.addrees2}</Text>
            </View>

            <View style={{ marginTop: 30, alignItems: 'center', flexDirection: 'row', justifyContent: 'center' }}>
              <TouchableOpacity onPress={() => this.props.onYes()}>
                <View style={styles.modalView}>
                  <Text style={[styles.AddTickets, { color: Colors.green, fontSize: 15, }]}>{this.props.yes}</Text>
                  <Image
                    resizeMode='contain'
                    tintColor={Colors.green}
                    source={Images.Home.check}
                    style={styles.homeImaGuide}
                  />
                </View>
              </TouchableOpacity>
              <TouchableOpacity onPress={() => this.props.onNo()}>
                <View style={styles.modalView}>
                  <Text style={[styles.AddTickets, { color: Colors.red, fontSize: 15, }]}>{this.props.NO}</Text>
                  <Image
                    resizeMode='contain'
                    tintColor={Colors.red}
                    source={Images.Home.X}
                    style={styles.homeImaGuideX}
                  />
                </View>
              </TouchableOpacity>

            </View>
          </View>
        </View>
      </Modal>


    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,


  },

  modalmainView:{
    flex: 1, backgroundColor: '#99999995', alignItems: 'center', justifyContent: 'center',
  },
  modal: {

    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: "#fff",
    paddingHorizontal: 30,
    paddingVertical: 20,
    borderRadius: 20,
    borderColor:'#fff',
    borderWidth: 1,


  },
  text: {
    color: '#3f2949',
    fontSize: 22,
    //fontFamily: "Metropolis-Medium",
    marginVertical: 20,
  },
  AddTicketsaddress: {
   // fontFamily: "Metropolis-Regular"
  },
  AddTickets: {
    //fontFamily: "Metropolis-SemiBold"
  },
  homeImaGuide: {
    height: 20,
    width: 20,
    top: -5,
  },
  homeImaGuideX: {
    height: 15,
    width: 15,
    top: -2,
  },
  modalView: {
    width: 70, height: 70, alignItems: 'center', justifyContent: 'center', backgroundColor: '#EBECED', paddingHorizontal: 10, paddingVertical: 10, borderRadius: 70 / 2, marginHorizontal: 15,
  }
});  