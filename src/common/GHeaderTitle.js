import React from 'react';
import { Text, View } from 'react-native';
import {
    Images, Strings , styles, Colors,
     GHeader,Input,GSmallButton
} from '../common'

export const GHeaderTitle = (params) => (
    <View style={{marginHorizontal:20,height:40,borderBottomWidth:1,borderColor:'#ddd'}}>
        <Text style={{color:Colors.red,fontSize:30,fontWeight:'400'}}>{params.leftText}</Text>
    </View>
);


