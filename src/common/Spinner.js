import React from 'react';
import { View, ActivityIndicator } from 'react-native';
import Spinner from 'react-native-loading-spinner-overlay';

const Loader = (props) => {

  return (
    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center' }}>
      <Spinner visible={props.visible} />
    </View>

  );

}

export { Loader };