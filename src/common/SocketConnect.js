import * as React from 'react';
import SocketIOClient from 'socket.io-client';
import { DeviceEventEmitter } from 'react-native'

import NetInfo from '@react-native-community/netinfo';
import Config from '../lib/Config';
import Helper from '../lib/Helper';

export default class SocketConnect extends React.Component {
  static isSocketConnected = false;
  static isAppBackground = false;

  constructor() { }

  static checkConnection(type, cb) {
    NetInfo.fetch()
      .then(isConnected => {
        if (isConnected) {
          if (SocketConnect.isSocketConnected) {
            console.log('********************** chat Already connected ********************');

            if (this.socket.connected == false) {
              this.socket.connect()
            }
            cb(true);
            return;
          } else {
            SocketConnect.connectUser(type);
            cb(false);
          }
        } else {
          alert(
            'Oops! It seems that you are not connected to the internet. Please check your internet connection and try again.',
          );
        }
      })
      .catch(error => {
        Helper.isNetConnected = false;
      });
  }

  static connectUser(type) {
    console.log('connectUser------------', Helper.userInfo.id);
    this.socket = SocketIOClient(Config.chat_url, {
      forceNew: true,
      reconnection: true,
      query: {
        user_id: Helper.userInfo.id,
        device_token: Helper.device_token,
      },
    });
    this.reConnect();
    this.socket.on('connect', data => {
      SocketConnect.isSocketConnected = true;
      console.log('********************** socket connected ********************');
      let formdata = {
        limit: "3",
        user_id: Helper.userInfo.id,
        start: "-1",
        keyword: ''

    }
      SocketConnect.socketDataEvent('get_message_history', formdata);
    });
    this.socket.on('disconnect', data => {
      // SocketConnect.isSocketConnected = false;
      console.log('********************** socket disconnect ********************');
      if (SocketConnect.isAppBackground == false && Helper.userInfo && Helper.userInfo.id) {
        this.reConnect();
      }
    });

    this.socket.on('view_post_response', data => {
   
      let payload = JSON.parse(data);

      if(payload.status == 'true'){
        Helper.userSettings.user_coins = payload.data.earned_coins;
        // DeviceEventEmitter.emit("view_post_response", payload.data.earned_coins)
        DeviceEventEmitter.emit("view_post_response", payload.data)

      }
    });

    this.socket.on('get_message_history_response', data => {
      Helper.recent_chat_history = data.responseData;
      // console.log('get_message_history_response---------------------  ',data)
      DeviceEventEmitter.emit("get_message_history_response", data)
    });

    this.socket.on('get_user_thread_response', data => {
      // console.log("get_user_thread_response---------", data)

      DeviceEventEmitter.emit("get_user_thread_response", data)

    });
    this.socket.on('multi_user', data => {
      // console.log("multi_user---------", data)

      DeviceEventEmitter.emit("multi_user", data)

    });
    this.socket.on('receive_message', data => {
      console.log("receive_message---------", data)

      DeviceEventEmitter.emit("receive_message", data)

    });
    this.socket.on('check_user_status_response', data => {
      // console.log("check_user_status_response---------", data)

      DeviceEventEmitter.emit("check_user_status_response", data)

    });
    this.socket.on('user_block_unblock_response', data => {
      // console.log("user_block_unblock_response---------", data)

      DeviceEventEmitter.emit("user_block_unblock_response", data)

    });


  }

  static reConnect() {
    this.socket.connect();
  }
  static checkSocketConnected() {
    return this.socket.connected
  }
  static closeConnection() {
    this.socket.close();
  }

  static disconnectUser() {
    this.socket.disconnect();
  }

  static socketDataEvent(eventName, formData) {
    // console.log(eventName, formData);
    this.socket.emit(eventName, formData);
  }

  static socketReconnectDisconnect(eventName) {
    NetInfo.fetch()
      .then(isConnected => {
        if (isConnected) {
          if (eventName == 'connect') {
            if (this.socket != null) {
              this.socket.connect();
              console.log('Old connected');
            } else {
              //  Helper.showLoader();
              SocketConnect.checkConnection('');
              console.log('New connected');
            }
          } else {
            if (this.socket != null) {
              this.socket.disconnect();
              console.log('disconnect--------');
            }
          }
        } else {
          alert(
            'Oops! It seems that you are not connected to the internet. Please check your internet connection and try again.',
          );
        }
      })
      .catch(error => {
        Helper.isNetConnected = false;
      });

  }
}
