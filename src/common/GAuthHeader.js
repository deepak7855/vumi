import React, { Component } from "react";
import { Text, View, Image, TouchableOpacity, SafeAreaView, StyleSheet, Platform } from "react-native";
import { hasNotch } from 'react-native-device-info';
import { Actions } from "react-native-router-flux";

import {
    GButton, Input, IconInput, TxtW, Images,
    KeyboardShift, ScrollEnable, Colors, GHeader,
} from '../common';
import { Fontfamily } from "./Styles";


export class GAuthHeader extends Component {
    constructor(props) {
        super(props);
        this.state = {
            useremail: ''
        }
    }

    render() {
        return (
            <SafeAreaView style={[style.safeArea, { backgroundColor: Colors.btxtColor }]}>

                <View style={{
                    flex: 1, alignItems: 'center',
                    marginTop: 0, flexDirection: 'row',
                    // paddingBottom: Platform.OS == 'ios' ? 0 : 0
                }}>

                    <View style={{ flex: 1 }}>
                        {this.props.bArrow && <TouchableOpacity onPress={() => Actions.pop()} >
                            <Image
                                resizeMode='contain'
                                source={Images.Header.leftArrow}
                                style={{ height: 20, width: 20,marginLeft : 20 }} />
                        </TouchableOpacity>}
                    </View>
                    <View style={{ flex: 2, alignItems: 'center' }}>
                        {this.props.title1 && <Text style={{ fontSize: 17, color: 'white', fontFamily: Fontfamily.MEB }}> {this.props.title1} </Text>}
                    </View>

                    <View style={{ flex: 1, borderWidth: 0, }}>

                    { this.props.rightItem && <>
                                    {this.props.children}
                                 </>
                    }
                    
                    </View>
                </View>
            </SafeAreaView>
        );
    }
}

const style = StyleSheet.create({
    container: {
        flex: 1,


    },
    safeArea: {
        // paddingTop: 2, paddingBottom: 10,
        paddingHorizontal: 10,
        flexDirection: 'row',
        height: hasNotch() ? 80 : 60,
        elevation: 3,
        borderWidth: 0.5, borderColor: '#6249f8'
    },
    Header_Crose: {
        resizeMode: 'contain',
        width: 22,
        height: 25,

    },
    headertext_view: {
        flex: 8,
        alignItems: 'center'
    },
    header_text: {
        fontSize: 20,
        fontFamily: "Metropolis-Medium"
    },



}); 