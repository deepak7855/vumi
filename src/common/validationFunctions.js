import Toast from 'react-native-root-toast';


const VALIDATE = {
    EMAIL: /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/,
    ALPHABET_ONLY: /^[a-zA-Z \s]*$/,
    Name_WITH_SOME_SPECIAL_CHARA: /^[a-zA-Z '-.\s]*$/,
    MOBILE: /[0-9]$/,
}

export const validationFunction = {


    checkAlphabet: (name, min, max, value) => {
        var min = min || 5;
        var max = max || 40;
        if (value) {
            if (!VALIDATE.ALPHABET_ONLY.test(value)) { Toast.show(name + ' is Invalid'); return false }
            else if (value.length < min || value.length > max) { Toast.show(`${name} must be entered between ${min} to ${max} Characters`); return false }
            return true
        }
        else { Toast.show(name + " is Required!"); return false }
    },

    checkName: (name, min, max, value) => {
        var min = min || 5;
        var max = max || 40;
        if (value) {
            if (!VALIDATE.Name_WITH_SOME_SPECIAL_CHARA.test(value)) { Toast.show(name + ' is Invalid'); return false }
            else if (value.length < min || value.length > max) { Toast.show(`${name} must be entered between ${min} to ${max} Characters`); return false }
            return true
        }
        else { Toast.show(name + " is Required!"); return false }
    },


    checkEmail: (name, value) => {
        if (value) {
            if (!VALIDATE.EMAIL.test(value)) { Toast.show(`${name} is Invalid!`); return false }
        } else { Toast.show(`${name} is Required!`); return false }
        return true
    },

    checkNumber: (name, min, max, value) => {
        var min = min || 7;
        var max = max || 15;
        if (value) {
            if (!VALIDATE.MOBILE.test(value) || !Number.isInteger(Number(value))) { Toast.show(`${name} is Invalid!`); return false }
            else if (value.length < min || value.length > max) { Toast.show(`${name} must be entered between ${min} to ${max} Characters`); return false }
            return true
        }
        else { Toast.show(`${name} is Required!`); return false }
    },

    checkNotNull: (name, min, max, value) => {
        var min = min || 5;
        var max = max || 40;
        if (value) {
            if (value.length < min || value.length > max) { Toast.show(`${name} must be entered between ${min} to ${max} Characters`); return false }
            return true
        }
        else { Toast.show(`${name} is Required!`); return false }
    }

}