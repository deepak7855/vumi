

import React, { Component, Children } from 'react';
import { View, Text, Dimensions, ScrollView, KeyboardAvoidingView, Platform } from 'react-native';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
const { height } = Dimensions.get('window');


export class ScrollEnable extends Component {
    constructor(props) {
        super(props);
        this.state = {
            contentHeight: ''
        };
    }

    onContentSizeChange = (contentWidth, contentHeight) => {
        // Save the content height in state
        this.setState({ screenHeight: contentHeight });
    };

    render() {
        const scrollEnabled = this.state.screenHeight > height;
        return (
            <KeyboardAwareScrollView
                 style={[this.props.style,{  flex: 1, }] }
                //  contentContainerStyle={{flex:1}}
                 contentContainerStyle={[this.props.containerStyle,,{flex:1}]}
                 scrollEnabled={scrollEnabled}
                // extraHeight={60}
                // extraScrollHeight={60}
                
                onContentSizeChange={this.onContentSizeChange}
            >
                {this.props.children}

            </KeyboardAwareScrollView>
        );
    }
}



export class ScrollContainer extends Component {
    render() {
        return (
            <ScrollView keyboardShouldPersistTaps="handled"
            contentContainerStyle={[this.props.containerStyle,]}
            style={[this.props.style,{ flex: 1, }]}>
                {this.props.children}
            </ScrollView>
        )
    }
}






export class ScrollButtonTop extends Component {
    render() {
        return (
            <KeyboardAvoidingView
                behavior={Platform.OS == 'ios' ? 'padding' : ''}
                keyboardVerticalOffset={50}
                style={[this.props.style,{marginBottom:10}]}>
                {this.props.children}
            </KeyboardAvoidingView>

        )
    }
}



