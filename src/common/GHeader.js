import React, { Component } from "react";
import { Text, View, Image, TouchableOpacity, SafeAreaView, StyleSheet, Platform } from "react-native";
import { hasNotch } from 'react-native-device-info';
import { Actions } from "react-native-router-flux";

import {
    Images,
    Colors,
} from '../common';
import { Fontfamily } from "./Styles";
import fonts from "./fonts";
import Helper from "../lib/Helper";


export class GHeader extends Component {
    constructor(props) {
        super(props);
        this.state = {
            useremail: ''
        }
    }

    render() {
        return (
            <SafeAreaView style={[style.safeArea, { backgroundColor: this.props.backgroundColor ? this.props.backgroundColor : Colors.btxtColor }]}>

                <View style={{
                    flex: 1, alignItems: 'center',
                    marginTop: 0, flexDirection: 'row', justifyContent: 'space-between'
                    // paddingBottom: Platform.OS == 'ios' ? 15 : 0,
                    // backgroundColor: colors.red
                }}>

                    <View style={{ flex: 1 }}>
                        {this.props.bArrow &&
                            <TouchableOpacity hitSlop={{ top: 20, bottom: 20, left: 20, right: 20 }} onPress={() => Actions.pop()} >
                                <Image
                                    resizeMode='contain'
                                    source={this.props.greyBackButton ? Images.Header.back_grey : Images.Header.leftArrow}
                                    style={{ height: 20, width: 20, marginLeft: 15, marginRight: 5 }} />
                            </TouchableOpacity>
                        }
                        {this.props.backArrowWithCallback &&
                            <TouchableOpacity hitSlop={{ top: 20, bottom: 20, left: 20, right: 20 }} onPress={() => this.props.onBackPress()} >
                                <Image
                                    resizeMode='contain'
                                    source={this.props.greyBackButton ? Images.Header.back_grey : Images.Header.leftArrow}
                                    style={{ height: 20, width: 20, marginLeft: 15, marginRight: 5 }} />
                            </TouchableOpacity>
                        }
                        {this.props.ProfileBack &&
                            <TouchableOpacity hitSlop={{ top: 20, bottom: 20, left: 20, right: 20 }} onPress={() => this.props.gotoBackMessage()} >
                                <Image
                                    resizeMode='contain'
                                    source={this.props.greyBackButton ? Images.Header.back_grey : Images.Header.leftArrow}
                                    style={{ height: 20, width: 20, marginLeft: 15, marginRight: 5 }} />
                            </TouchableOpacity>
                        }
                    </View>
                    <View style={{ flex: 2, alignItems: 'center' }}>
                        {this.props.title1 && <Text style={{ fontSize: this.props.title1FontSize || 17, color: this.props.color ? this.props.color : 'white', fontFamily: Fontfamily.MEB }}> {this.props.title1} </Text>}
                    </View>

                    <View style={{ flex: 1, borderWidth: 0, alignItems: "flex-end", marginRight: 15, marginLeft: 5 }}>

                        {this.props.rightItem && <>
                            {this.props.children}
                        </>
                        }

                        {
                            this.props.showCoinsAtRight &&
                            <View style={{ flexDirection: "row", alignItems: "center" }}>
                                <Image style={{ height: 17, width: 17, resizeMode: "contain" }}
                                    source={Images.coin} />
                                <Text style={{ color: "#626262", fontSize: 13, fontFamily: fonts.Montserrat_SemiBold, marginLeft: 3 }}>{Helper.userSettings.user_coins}</Text>
                            </View>
                        }
                        {
                            this.props.showJoinBtn && <TouchableOpacity onPress={() => this.props.onJoinHeaderPress()} style={{ backgroundColor: "#28ddc1", height: 34, width: 82, borderRadius: 20, justifyContent: 'center', alignItems: 'center', }}>
                                <Text style={{ color: "white", fontSize: 14, fontFamily: fonts.Montserrat_SemiBold }}>Join</Text>
                            </TouchableOpacity>
                        }
                        {
                            this.props.showAddPostBtn && <TouchableOpacity hitSlop={{ top: 20, bottom: 20, left: 20, right: 20 }} onPress={() => this.props.onPostHeaderPress()} style={{}}>
                                <Image
                                    source={Images.add_grey}
                                    style={{ height: 27, width: 27, resizeMode: "contain" }} />
                            </TouchableOpacity>
                        }
                        {
                            this.props.showPhotoBtn && <TouchableOpacity onPress={() => this.props.onPhotoButtonPress()} disabled={this.props.photoBtnDisabled} style={{ height: 34, justifyContent: 'center', alignItems: 'flex-end' }}>
                                <Text style={{ color: this.props.photoBtnDisabled ? "white" : "#2eb7ff", fontSize: 14, fontFamily: fonts.SFProDisplay_Semibold }}>{this.props.photoBtnText}</Text>
                            </TouchableOpacity>
                        }
                        {this.props.showDotButton &&
                            <TouchableOpacity onPress={() => this.props.gotoClickDot()} style={{ height: 30, width: 30, justifyContent: 'center', alignItems: 'center', }}>
                                <Image
                                    source={this.props.rightIcon}
                                    style={{ height: 30, width: 30, resizeMode: "contain" }} />
                            </TouchableOpacity>
                        }
                    </View>
                </View>
            </SafeAreaView>
        );
    }
}

const style = StyleSheet.create({
    container: {
        flex: 1,


    },
    safeArea: {
        flexDirection: 'row',
        height: hasNotch() ? 80 : 60,
        marginVertical: Platform.OS == 'ios' ? 0 : 0,
    },
    Header_Crose: {

        resizeMode: 'contain',
        width: 22,
        height: 25,

    },
    headertext_view: {
        flex: 8,
        alignItems: 'center'
    },
    header_text: {
        fontSize: 20,
        fontFamily: "Metropolis-Medium"
    },



}); 