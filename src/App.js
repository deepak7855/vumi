import React, { Component } from 'react';
import { View, AsyncStorage, Text, Platform, Alert, Clipboard } from 'react-native';
import Helper from './lib/Helper';
import { Routes } from './navigation/Navigation'
import firebase from '@react-native-firebase/app'
import messaging from '@react-native-firebase/messaging';
import SplashScreen from 'react-native-splash-screen';



var firebaseConfig = {
  apiKey: "AIzaSyAkTBUHtNnDdqvtyUDVwalznatwtRmx8Gw",
  authDomain: "vumi-58d2f.firebaseapp.com",
  databaseURL: "https://vumi-58d2f.firebaseio.com",
  projectId: "vumi-58d2f",
  storageBucket: "vumi-58d2f.appspot.com",
  messagingSenderId: "983886382894",
  appId: "vumi-58d2f",
  // measurementId: "G-measurement-id",
};
export default class App extends Component {

  constructor(props) {
    super(props);
    this.state = {
    }
    console.disableYellowBox = true;

    // if (!firebase.apps.length) {
    //   firebase.initializeApp(firebaseConfig);
    // }

  }


  async getToken() {
    let fcmToken = await AsyncStorage.getItem('fcmToken');
    if (!fcmToken) {
      if (Platform.OS == 'ios') {
        if (!messaging().isDeviceRegisteredForRemoteMessages) {
          await messaging().registerDeviceForRemoteMessages();
        }
        let fcmTokenKK = await firebase.messaging().getAPNSToken()
        console.log("fcmTokenKK----------", fcmTokenKK)
      }

      fcmToken = await firebase.messaging().getToken();
      if (fcmToken) {
        await AsyncStorage.setItem('fcmToken', fcmToken);
      }
    }

    Helper.device_token = fcmToken
    console.log(Helper.device_token, "Helper.device_token ");
    console.log("Firebase Token-------------------------------------------------", fcmToken)
    // setTimeout(() => {
    //   Alert.alert(
    //     '',
    //     fcmToken,
    //     [
    //       {
    //         text: "OK", onPress: () => {
    //           Clipboard.setString(fcmToken)
    //         }
    //       },
    //     ],
    //     { cancelable: false }
    //   )
    // }, 2000);
  }

  async checkPermission() {
    const enabled = await firebase.messaging().hasPermission();
    if (enabled) {
      this.getToken();
    } else {
      this.requestPermission();
    }
  }

  async requestPermission() {
    try {
      await messaging().requestPermission();
      this.getToken();
    } catch (error) {
      console.log('permission rejected');
    }
  }

  async componentDidMount() {
    if (Platform.OS === 'android') {
      await this.getToken();

    }
    else {
      this.checkPermission()
    }
    setTimeout(() => {
      SplashScreen.hide()
    }, 1000)


    // firebase
    // const channel = new firebase.notifications.Android.Channel('insider', 'insider channel', firebase.notifications.Android.Importance.Max)
    // firebase.notifications().android.createChannel(channel);
    this.createNotificationListeners()
  }

  async createNotificationListeners() {


    await messaging().setBackgroundMessageHandler(remoteMessage => {
      console.log('Notification 1--------------', remoteMessage)
    });

    await messaging().onNotificationOpenedApp(notification => {
      console.log('Notification 2--------------', notification)
    })
    // notification resive after app kill
    await messaging()
      .getInitialNotification()
      .then(remoteMessage => {
        console.log('Notification 3--------------', remoteMessage)

      });

  }

  render() {
    return (
      <View
        style={{ flex: 1, backgroundColor: 'black' }}>

        <Routes />
      </View>
    )
  }

}