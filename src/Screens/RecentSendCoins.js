import React, { Component } from 'react';
import { View, Text, StyleSheet, FlatList, ActivityIndicator } from 'react-native';
import { SearchInputBarCompo } from '../components/Coins/SearchInputBarCompo';
import { GHeader } from '../common';
import { CoinListRow, RecentCoinListRow } from '../components/Coins/CoinListRow';
import { SearchConditionsCompo } from '../components/Coins/SearchConditionsCompo';
import { Actions } from 'react-native-router-flux';
import pushToScreen from '../navigation/NavigationActions';
import Helper from '../lib/Helper';
import Spinner from 'react-native-loading-spinner-overlay';

import SocketConnect from '../common/SocketConnect';
import { InfoModelCompo } from '../components/Watch/InfoModelCompo';
import { BlockModelCompo } from '../components/Watch/BlockModelCompo';


export default class RecentSendCoins extends Component {
  constructor(props) {
    super(props);
    this.state = {
      SendCoinsData: [],
      searchtype: "",
      keyword: '',
      showSpinner: false,
      arrOutCoins: [],
      arrINCoins: [],
      first_id_OutCoins: '',
      first_id_InCoins: '',
      currentPage: 1,
      totalPage: 1,
      totalRecord: 0,
      showLoaderPage: false,
      isRefresh: false,
      selectedRowDetail: {},
      showBlockModel: false,
      showInfoModel: false,

    };
  }

  onSearchTypeChange = (type, loader) => {
    // console.log(searchtype,">>>>>>>>>")
    // this.setState({ searchtype })

    this.state.searchtype = type


    // if (this.flatListRef) {
    //   this.flatListRef.scrollToIndex({ index: 0 })
    // }

    if (this.state.searchtype == "") {
      if (this.state.SendCoinsData.length == 0) {
        this.state.currentPage = 1
        this._onSerach(this.state.keyword, true)
      }
    }
    else if (this.state.searchtype == "IN") {
      if (this.state.arrINCoins.length == 0) {
        this.getSendCoinHistory(this.state.keyword, this.state.searchtype, "", loader)
      }

    }
    else if (this.state.searchtype == "OUT") {
      if (this.state.arrOutCoins.length == 0) {
        this.getSendCoinHistory(this.state.keyword, this.state.searchtype, "", loader)
      }
    }

    this.setState({ showLoaderPage: false })

  }
  componentDidMount = () => {
    this.state.currentPage = 1
    this._onSerach(this.state.keyword, true)
  };
  _onSerach = (keyword, loader) => {
    this.setState({ keyword: keyword })
    let data = {
      keyword: keyword,
      page: this.state.currentPage
    }
    this.setState({ showSpinner: loader })
    Helper.makeRequest({ url: 'search', method: "POST", data: data }).then((resp) => {
      this.state.showLoaderPage = false

      if (this.state.currentPage == 1) {
        this.state.SendCoinsData = []
      }
      if (resp.status == 'true') {
        // console.log(resp, '<<<<<<')
        if (resp.data.data && resp.data.data.length > 0) {
          this.state.SendCoinsData = [...this.state.SendCoinsData, ...resp.data.data]
        }
        this.state.totalPage = resp.data.last_page
        this.state.totalRecord = resp.data.total
        console.log("SendCoinsData------", this.state.SendCoinsData.length)

        console.log("totalPage------", this.state.totalPage)
        this.setState({ isRefresh: !this.state.isRefresh });

      } else {
        // Helper.showToast(resp.message)
      }

      this.state.showSpinner = false
      this.setState({ isRefresh: !this.state.isRefresh });

    })
  }
  getSendCoinHistory(keyword, type, last_id, loader) {

    this.setState({ showSpinner: loader })

    let data = {
      type: type,
      last_id: last_id,
      keyword: keyword
    }
    Helper.makeRequest({ url: "send-coin-history", method: "POST", data: data }).then((resp) => {

      this.state.showLoaderPage = false

      if (resp.status == 'true') {

        if (data.type == 'OUT') {
          if (last_id == "") {
            this.state.arrOutCoins = []
          }
          if (resp.data && resp.data.data && resp.data.data.length > 0) {
            this.state.arrOutCoins = [...this.state.arrOutCoins, ...resp.data.data]
          }
          this.state.first_id_OutCoins = resp.data.first_id
        }
        else if (data.type == 'IN') {
          if (last_id == "") {
            this.state.arrINCoins = []
          }
          if (resp.data && resp.data.data && resp.data.data.length > 0) {
            this.state.arrINCoins = [...this.state.arrINCoins, ...resp.data.data]
          }
          this.state.first_id_InCoins = resp.data.first_id
        }
        this.setState({ showSpinner: false, isRefresh: !this.state.isRefresh });
      } else {
        // Toast.show(resp.message)
        if (data.type == 'OUT') {
          if (last_id == "") {
            this.state.arrOutCoins = []
          }
        }
        else if (data.type == 'IN') {
          if (last_id == "") {
            this.state.arrINCoins = []
          }
        }
        this.setState({ showSpinner: false });
      }

    })
  }
  methodSearch(txt) {
    this.state.keyword = txt

    if (this.state.searchtype == "") {

      this.state.arrINCoins = []
      this.state.arrOutCoins = []
      this.state.first_id_InCoins = ""
      this.state.first_id_OutCoins = ""

      this.setState({})

      this.state.currentPage = 1
      this._onSerach(this.state.keyword, false)


    }
    else {
      this.state.currentPage = 1
      this.state.SendCoinsData = []

      if (this.state.searchtype == "IN") {
        this.state.arrOutCoins = []
        this.state.first_id_OutCoins = ""
      }
      else if (this.state.searchtype == "OUT") {
        this.state.arrINCoins = []
        this.state.first_id_InCoins = ""
      }

      this.setState({})

      this.getSendCoinHistory(this.state.keyword, this.state.searchtype, "", false)
    }
  }
  render_Activity_footer = () => {
    var footer_View = (
      <View style={{ flex: 1, backgroundColor: '#ffffff', alignItems: 'center', justifyContent: 'center', padding: 15 }}>
        <ActivityIndicator size="large" color="#000000" />
      </View>
    );
    return footer_View;
  };
  onScroll = () => {
    console.log("onScroll-------")

    if (this.state.searchtype == "") {
      if (!this.state.showLoaderPage && this.state.totalRecord > this.state.SendCoinsData.length && this.state.totalPage > this.state.currentPage) {
        this.setState({ showLoaderPage: true })
        this.state.currentPage = this.state.currentPage + 1
        this._onSerach(this.state.keyword, false)
      }
    }
    else if (this.state.searchtype == "IN") {
      if (!this.state.showLoaderPage && this.state.arrINCoins.length > 0 && this.state.arrINCoins[this.state.arrINCoins.length - 1].id != this.state.first_id_InCoins && this.state.first_id_InCoins != '') {
        this.setState({ showLoaderPage: true })
        this.getSendCoinHistory(this.state.keyword, this.state.searchtype, this.state.arrINCoins[this.state.arrINCoins.length - 1].id, false);
      }
    }
    else if (this.state.searchtype == "OUT") {
      if (!this.state.showLoaderPage && this.state.arrOutCoins.length > 0 && this.state.arrOutCoins[this.state.arrOutCoins.length - 1].id != this.state.first_id_OutCoins && this.state.first_id_OutCoins != '') {
        this.setState({ showLoaderPage: true })
        this.getSendCoinHistory(this.state.keyword, this.state.searchtype, this.state.arrOutCoins[this.state.arrOutCoins.length - 1].id, false);
      }
    }

  }


  onUserImagePress = (item) => {
    item['is_blocked'] = 0;
    let data = {
      user:item
    }
    this.setState({ showInfoModel: true, selectedRowDetail: data })
  }

  onBlock = (item) => {
    this.setState({ showBlockModel: false, selectedRowDetail: {} })
    let formdata = {
      user_id: Helper.userInfo.id,
      other_user_id: item.user.id,
    }
    if (SocketConnect.checkSocketConnected()) {

      this.methodSearch(this.state.keyword)
      SocketConnect.socketDataEvent('user_block_unblock', formdata);

    } else {
      SocketConnect.checkConnection('connect', cb => {
        console.log('SocketConnect------------', cb);
      });
    }
  }

  goToChat = (item) => {
    this.setState({ showInfoModel: false }, () => {
      pushToScreen('Chat', { OtherUserId: item.user.id, OtherUserName: item.user.username });
    })
  }

  onShowBlockModel = () => {
    this.setState({ showInfoModel: false }, () => {
      this.setState({ showBlockModel: true })
    })
  }


  render() {
    // console.log('render', this.state.searchtype)
    // console.log('this.state.arrINCoins', this.state.arrINCoins.length)
    // console.log('this.state.arrOutCoins', this.state.arrOutCoins.length)
    // console.log('this.state.arrSend', this.state.SendCoinsData.length)

    return (
      <View style={{ flex: 1 }}>
        <GHeader
          backgroundColor="white"
          title1="Send Coins"
          bArrow
          greyBackButton
          color="#606076"
        />

        {/* <View style={{ paddingTop: 15 }}>
          <SearchConditionsCompo selectedType={this.state.searchtype}onSearchTypeChange={(val) => { this.onSearchTypeChange(val, true) }} />
        </View> */}

        <View style={{ paddingHorizontal: 15, paddingBottom: 10 }}>
          <SearchInputBarCompo
            placeholder="Search users"
            width={'100%'}
            onChangeText={(keyword) => this.methodSearch(keyword)}
            value={this.state.keyword}
          />
        </View>
        <Text style={{ color: '#dcdce2', marginBottom: 15, marginLeft: 20 }} >Recents</Text>

        <FlatList
          showsHorizontalScrollIndicator={false}
          ref={(ref) => this.flatListRef = ref}
          data={this.state.searchtype == "" ? this.state.SendCoinsData : this.state.searchtype == "IN" ? this.state.arrINCoins : this.state.arrOutCoins}
          renderItem={({ item }) =>
            this.state.searchtype == "" ? <RecentCoinListRow item={item}
              title={item.title} img={item.img}
              onUserImagePress={(item) => this.onUserImagePress(item)}
              onRowPress={() => pushToScreen('SendCoin', { preData: item })} />
              :
              <CoinListRow item={item}
                title={item.title} img={item.img}
                onUserImagePress={(item) => this.onUserImagePress(item)}
                onRowPress={() => pushToScreen('SendCoin', { preData: item })} />

          }
          ItemSeparatorComponent={() => (<View style={styles.separater} />)}
          keyExtractor={(item, index) => index.toString()}
          onEndReached={this.onScroll}
          onEndReachedThreshold={0.5}
          ListFooterComponent={this.state.showLoaderPage && this.render_Activity_footer}
          extraData={this.state}
        />

        {
          this.state.showBlockModel &&
          <BlockModelCompo
            showBlockModel={this.state.showBlockModel}
            onBlock={(item) => this.onBlock(item)}
            hideBlockModel={() => this.setState({ showBlockModel: false })}
            selectedRowDetail={this.state.selectedRowDetail}
          />
        }
        {
          this.state.showInfoModel &&
          <InfoModelCompo
            showInfoModel={this.state.showInfoModel}
            hideInfoModel={() => this.setState({ showInfoModel: false })}
            onShowBlockModel={() => this.onShowBlockModel()}
            goToChat={(item) => this.goToChat(item)}
            selectedRowDetail={this.state.selectedRowDetail}
          />
        }
        <Spinner visible={this.state.showSpinner} />

      </View>
    );
  }
}


const styles = StyleSheet.create({

  separater: {
    height: 2,
    backgroundColor: "#F0F0F0",
    width: "100%"
  }
});