import React, { Component } from 'react';
import { View, Text, StyleSheet, DeviceEventEmitter, Image, Animated, Platform, FlatList, StatusBar, SafeAreaView, Dimensions, RefreshControl } from 'react-native';
import { Images, GSmallButton, Fontfamily, colors } from '../common';
import { SearchConditionsCompo } from '../components/Coins/SearchConditionsCompo';
import { Actions } from 'react-native-router-flux';
import { CoinListRow } from '../components/Coins/CoinListRow';
import { SearchInputBarCompo } from '../components/Coins/SearchInputBarCompo';
import fonts from '../common/fonts';
import { CashInNoCoinsModel } from '../components/Coins/CashInNoCoinsModel';
import { WhatIsTrollModelCompo } from '../components/common/WhatIsTrollModelCompo';
import pushToScreen from '../navigation/NavigationActions';
import Helper from '../lib/Helper';
import Toast from 'react-native-root-toast';
import { TouchableOpacity } from 'react-native';
import { Easing } from 'react-native';
import { BlockModelCompo } from '../components/Watch/BlockModelCompo';
import Spinner from 'react-native-loading-spinner-overlay';
import moment from 'moment';
import SocketConnect from '../common/SocketConnect';
import { InfoModelCompo } from '../components/Watch/InfoModelCompo';
import { ImageBackground } from 'react-native';
import { Modalize } from 'react-native-modalize';
import ImageLoadView from '../components/ImageLoadView';

HEADER_MAX_HEIGHT = 320;
HEADER_MIN_HEIGHT = 100;
PROFILE_IMAGE_MAX_HEIGHT = 80;
PROFILE_IMAGE_MIN_HEIGHT = 40;
class CoinOld extends Component {
  constructor(props) {
    super(props);
    this.state = {
      arrCoinHistory: [],
      scrollY: new Animated.Value(0),
      searchType: "",
      modelVisible: false,
      trollModel: false,
      notFound: false,
      first_data_id: '',
      keyword: '',
      showLoaderPage: false,
      temp_post_count: 0,
      selectedRowDetail: {},
      showBlockModel: false,
      showInfoModel: false,
      modalizeRef: true,
      showLoaderRefresh: false,
      arrMindData: [],
      Position: 'initial'

    };
  }

  componentDidMount = () => {
    console.log('Helper.userInfo----------------   ', Helper.userInfo)
    this.functionevevnt = this.props.navigation.addListener('didFocus', () => {
      if (Platform.OS == 'android') {
        StatusBar.setBackgroundColor('white');
      }
      StatusBar.setBarStyle('dark-content')
      this.getSettingApi()
      this.getSendCoinHistory("", "")
    });

    this.UserBlockUnBlock = DeviceEventEmitter.addListener('user_block_unblock_response', response => this.UserBlockUnBlockResponse(response));
  };
  componentWillUnmount = () => {
    this.functionevevnt.remove();
    if (this.UserBlockUnBlock) {
      this.UserBlockUnBlock.remove();
    }
  };
  _pullToRefresh = () => {
    alert('yes')
    this.setState({ showLoaderRefresh: true })
    this.getSendCoinHistory("", "")
  }
  onSearchTypeChange = (searchType) => {
    console.log(searchType, "searchTypesearchTypesearchTypesearchType");
    if (this.state.searchType == searchType) {
      return
    }
    this.state.searchType = searchType
    {
      this.state.searchType == '' || this.state.searchType == 'IN' || this.state.searchType == 'OUT' ?
        this.getSendCoinHistory(searchType, "")
        :
        this.getMindSendCoinHistory()
    }
    this.setState({})
  }

  showNoCoinsModel = (val) => {
    this.setState({ modelVisible: val })
  }

  showTrollModel = (val) => {
    this.setState({ trollModel: val })
  }

  getMindSendCoinHistory = () => {
    this.setState({ showSpinner: true })
    Helper.makeRequest({ url: "mined-coin-history", method: "POST", data: '' }).then((resp) => {
      console.log(resp, 'resprespresprespresprespresp');
      this.setState({ showSpinner: false, showLoaderPage: false, showLoaderRefresh: false });
      if (resp.status == 'true') {
        this.setState({ arrMindData: resp.data.data.data })
        console.log(resp.data.data.data, 'resp.data.data');

      } else {
        this.setState({ arrMindData: [] })

        // Toast.show(resp.message)

      }
    })
  }

  getSendCoinHistory(type, last_id, loader = true) {
    if (last_id == "" && loader) {
      this.setState({ showSpinner: true })
    }
    let data = {
      type: type,
      last_id: last_id,
      keyword: this.state.keyword
    }
    Helper.makeRequest({ url: "send-coin-history", method: "POST", data: data }).then((resp) => {
      if (resp.status == 'true') {
        if (data.type == this.state.searchType && data.keyword == this.state.keyword) {
          if (last_id == "") {
            this.state.arrCoinHistory = []
          }
          if (resp.data && resp.data.data && resp.data.data.length > 0) {
            this.state.arrCoinHistory = [...this.state.arrCoinHistory, ...resp.data.data]
          }
          this.state.first_data_id = resp.data.first_id
        }
        this.setState({ showSpinner: false, showLoaderPage: false, showLoaderRefresh: false });
      } else {
        // Toast.show(resp.message)
        this.setState({ arrCoinHistory: [] })

        if (data.type == this.state.searchType && data.keyword == this.state.keyword) {

          if (last_id == "") {
            this.state.arrCoinHistory = []
          }
        }
        this.setState({ showSpinner: false, showLoaderPage: false, showLoaderRefresh: false });
      }
    })
  }

  getSettingApi = () => {
    this.setState({ showSpinner: true })
    Helper.makeRequest({ url: 'settings', method: "POST", data: '' }).then((resp) => {
      if (resp.status == 'true') {
        console.log("settings------------------------------", resp)
        Helper.userSettings = resp.data
        this.setState({ temp_post_count: resp.data.temp_post_count })

      } else {
        Helper.userSettings = {}
        this.setState({})

      }
      this.setState({ showSpinner: false })
    })
  }

  _onSerchHistory(keyword) {
    this.setState({ keyword }, () => {
      this.getSendCoinHistory(this.state.searchType, "", false)
    })
  }

  methodCashInClick() {
    if (this.state.temp_post_count > 0) {
      pushToScreen('CashIn', { fromCoins: true })
    } else {
      Toast.show("Please create post first.")
    }
  }

  onEndReached = () => {
    // alert('yes')
    if (!this.state.showLoaderPage && this.state.arrCoinHistory.length > 0 && this.state.arrCoinHistory[this.state.arrCoinHistory.length - 1].id != this.state.first_data_id && this.state.first_data_id != '') {
      this.setState({ showLoaderPage: true })
      this.getSendCoinHistory(this.state.searchType, this.state.arrCoinHistory[this.state.arrCoinHistory.length - 1].id, false)
    }
  }

  onUserImagePress = (item) => {
    let dicUser = {}

    if (item.user.id == Helper.userInfo.id) {
      dicUser['user'] = item.otheruser
    }
    else {
      dicUser['user'] = item.user
    }

    console.log(dicUser)
    this.setState({ showInfoModel: true, selectedRowDetail: dicUser })
  }

  onBlock = (item) => {
    this.setState({ showBlockModel: false, selectedRowDetail: {} })
    let formdata = {
      user_id: Helper.userInfo.id,
      other_user_id: item.user.id,
    }
    if (SocketConnect.checkSocketConnected()) {
      SocketConnect.socketDataEvent('user_block_unblock', formdata);
    } else {
      SocketConnect.checkConnection('connect', cb => {
        console.log('SocketConnect------------', cb);
      });
    }
  }

  renderListView() {
    return (
      <View style={Style.scrollViewContent}>
        {/* {this.state.searchType == '' || this.state.searchType == 'IN' || this.state.searchType == 'OUT' || this.state.searchType == 'MIND' ? */}
        <View style={{ flex: 1, backgroundColor: '#FFF' }}>
          <FlatList
            bounces={false}
            contentContainerStyle={{ flexGrow: 1 }}
            showsHorizontalScrollIndicator={false}
            data={this.state.searchType == 'MIND' ? this.state.arrMindData : this.state.arrCoinHistory}
            extraData={this.state.searchType == 'MIND' ? this.state.arrMindData : this.state.arrCoinHistory}
            showsVerticalScrollIndicator={false}
            onEndReached={this.onEndReached}
            onEndReachedThreshold={0.5}
            renderItem={({ item }) => this.state.searchType == 'MIND' ? <CoinListRow
              item={item}
              title={item.title} img={item.img}
              onRowPress={() => pushToScreen('SendCoin', { OtherUserData: item })}
              onUserImagePress={(item) => this.onUserImagePress(item)}
            />
              : alert('')

            }
            ItemSeparatorComponent={() => (<View style={Style.separater} />)}
            keyExtractor={(item, index) => index.toString()}
            ListEmptyComponent={() => (
              <View>
                {this.state.searchType == 'MIND' ?
                  <View style={{ flex: 1, backgroundColor: "#FFF", alignItems: "center", justifyContent: "center", }} >
                    <Text style={{ fontSize: 16, fontFamily: fonts.SFProMedium, color: "#a2a2b2" }}>You haven’t mined any coins.</Text>
                    <Text style={{ fontSize: 14, fontFamily: fonts.SFProMedium, color: "#c7cbd8", marginBottom: 15 }}>Watch content to earn view coins!</Text>
                    <Image
                      style={{ height: 100, width: 100, borderRadius: 50, resizeMode: "contain" }}
                      source={Images.all}

                    />
                  </View>
                  : <View style={{ flex: 1, backgroundColor: "#FFF", alignItems: "center", justifyContent: "center" }} >
                    <Text style={{ fontSize: 16, fontFamily: fonts.SFProMedium, color: "#a2a2b2" }}>You have no transfers.</Text>
                    <Text style={{ fontSize: 14, fontFamily: fonts.SFProMedium, color: "#c7cbd8", marginBottom: 15 }}>Send and receive views, for free!</Text>
                    <Image
                      style={{ height: 100, width: 100, borderRadius: 50, resizeMode: "contain" }}
                      source={Images.mind}

                    />
                  </View>
                }
              </View>

            )}
          />
        </View>
        {/* :
          <View style={{ flex: 1, backgroundColor: '#fff' }}>
            {
              Helper.userSettings?.today_points > 0 ?
                <View style={{ flexDirection: 'row', width: '100%', paddingHorizontal: 20, height: 55, alignItems: 'center', justifyContent: 'space-between', }}>
                  <Text style={{ fontSize: 16 }}>You mined <Text style={{ fontFamily: fonts.Montserrat_Bold, fontSize: 20 }}>{Helper.userSettings?.today_points}vc</Text></Text>
                  <Text style={{ fontSize: 16 }}>{moment(new Date()).utc().format('LL')}</Text>
                </View>
                :
                <View style={{ flex: 1, backgroundColor: "#FFF", alignItems: "center", justifyContent: "center", }} >
                  <Text style={{ fontSize: 16, fontFamily: fonts.SFProMedium, color: "#a2a2b2" }}>You haven’t mined any coins.</Text>
                  <Text style={{ fontSize: 14, fontFamily: fonts.SFProMedium, color: "#c7cbd8", marginBottom: 15 }}>Watch content to earn view coins!</Text>
                  <Image
                    style={{ height: 100, width: 100, borderRadius: 50, resizeMode: "contain" }}
                    source={Images.all}

                  />
                </View>
            }

          </View>
        } */}

      </View>
    )
  }

  UserBlockUnBlockResponse(response) {
    if (response) {
      this.setState({
        arrCoinHistory: [],
      }, () => {
        this.getSendCoinHistory('', '');
      })
    }
  }

  onShowBlockModel = () => {
    this.setState({ showInfoModel: false }, () => {
      this.setState({ showBlockModel: true })
    })
  }

  goToChat = (item) => {
    this.setState({ showInfoModel: false }, () => {
      pushToScreen('Chat', { OtherUserId: item.user.id, OtherUserName: item.user.username });
    })
  }

  renderItem = ({ item }) => (
    <View>
      {this.state.searchType == 'MIND' ?
        <View style={{
          backgroundColor: 'white',
          flexDirection: 'row',
          flex: 1,
          alignItems: 'center',
          paddingVertical: 7,
          paddingLeft: 20
        }}>
          <View style={{
            height: 50,
            width: 50,
            borderRadius: 50 / 2,
            resizeMode: 'cover',
          }} >
            <ImageLoadView
              resizeMode='cover'
              source={Images.Coinpurple}
              style={{
                height: 50,
                width: 50,
                borderRadius: 50 / 2,
                resizeMode: 'cover'
              }}
            />
          </View>


          <TouchableOpacity style={{ flex: 1, marginHorizontal: 9, }}>
            <View style={{ flexDirection: "row" }}>
              < Text style={{
                color: "#4c4c55",
                fontSize: 15,
                fontFamily: fonts.SFProMedium
              }}>{item.total_coins ? 'You mined ' + item.total_coins : ""}</Text>

              <Text style={{
                fontSize: 9,
                color: "#4c4c55"
              }} > vc</Text>
            </View>
            <View style={{ flex: 1, flexDirection: "row", alignItems: "center", }}>

              <Text style={{
                color: colors.dblue,
                fontSize: 12,
                fontFamily: fonts.SFProRegular
              }} >{moment.utc(item.date).local().fromNow()}</Text>
              <View style={{
                height: 3,
                width: 3,
                borderRadius: 1.5,
                backgroundColor: "#4c4c55",
                opacity: 0.3,
                marginHorizontal: 5
              }} />
            </View>
          </TouchableOpacity>
        </View>
        :
        <CoinListRow
          item={item}
          title={item.title} img={item.img}
          onRowPress={() => pushToScreen('SendCoin', { OtherUserData: item })}
          onUserImagePress={(item) => this.onUserImagePress(item)}
        />
      }
    </View >
  );

  separetview = ({ }) => (
    <View style={Style.separater} />
  )

  EmptyComponent = ({ }) => (
    <View style={{ flex: 1, backgroundColor: "#FFF", alignItems: "center", justifyContent: "center" }} >
      <Text style={{ fontSize: 16, fontFamily: fonts.SFProMedium, color: "#a2a2b2" }}>You have no transfers.</Text>
      <Text style={{ fontSize: 14, fontFamily: fonts.SFProMedium, color: "#c7cbd8", marginBottom: 15 }}>Send and receive views, for free!</Text>
      <Image
        style={{ height: 100, width: 100, borderRadius: 50, resizeMode: "contain" }}
        source={Images.mind} />
    </View>
  )

  changePos = position => {
    this.setState({ Position: position })
    console.log(position, "hhhhhh>>>>>>>>>>>>>>>>>>>>>")
  }

  headerView(searchType) {
    console.log(searchType, "ttttttt searcvh");

    return (
      <View >
        <SearchConditionsCompo selectedType={searchType} fromCoins onSearchTypeChange={(val) => this.onSearchTypeChange(val)} />
        <View style={{ backgroundColor: '#f5f6fa', height: 4 }}></View>
        {searchType == 'MIND' ? null :
          <View style={{ backgroundColor: '#fff', paddingHorizontal: 18, }}>
            <SearchInputBarCompo placeholder="Search transfers..."
              width={'100%'}
              value={this.state.keyword}
              onChangeText={(txt) => this._onSerchHistory(txt)}
            />
          </View>

        }
      </View>
    )
  }

  CoinGetView() {
    return (
      <View style={{ flexDirection: 'row', width: '100%', paddingHorizontal: 20, height: 55, alignItems: 'center', justifyContent: 'space-between', }}>
        <Text style={{ fontSize: 16 }}>You mined <Text style={{ fontFamily: fonts.Montserrat_Bold, fontSize: 20 }}>{Helper.userSettings?.today_points}vc</Text></Text>
        <Text style={{ fontSize: 16 }}>{moment(new Date()).utc().format('LL')}</Text>
      </View>
    )
  }

  AllInOutEmptyView() {
    return (
      <View style={{ flex: 1, backgroundColor: "#FFF", alignItems: "center", justifyContent: "center" }} >
        <Text style={{ fontSize: 16, fontFamily: fonts.SFProMedium, color: "#a2a2b2" }}>You have no transfers.</Text>
        <Text style={{ fontSize: 14, fontFamily: fonts.SFProMedium, color: "#c7cbd8", marginBottom: 15 }}>Send and receive views, for free!</Text>
        <Image
          style={{ height: 100, width: 100, borderRadius: 50, resizeMode: "contain" }}
          source={Images.mind} />
      </View>
    )
  }

  MindEmptyView() {
    return (
      <View style={{ flex: 1, backgroundColor: "#FFF", alignItems: "center", justifyContent: "center", }} >
        <Text style={{ fontSize: 16, fontFamily: fonts.SFProMedium, color: "#a2a2b2" }}>You haven’t mined any coins.</Text>
        <Text style={{ fontSize: 14, fontFamily: fonts.SFProMedium, color: "#c7cbd8", marginBottom: 15 }}>Watch content to earn view coins!</Text>
        <Image
          style={{ height: 100, width: 100, borderRadius: 50, resizeMode: "contain" }}
          source={Images.all}

        />
      </View>
    )
  }

  render() {

    return (
      <View style={{ flex: 1, backgroundColor: "#f5f6fa" }}>
        <ImageBackground style={{ width: '100%', height: 330 }}
          // source={Images.coinsbg}
          source={Images.coinbg} >
          <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', marginTop: 50 }}>
            <View style={{ alignItems: "center", }}>
              <View style={{ flexDirection: "row", }}>
                <Text style={{ fontSize: 50, fontFamily: Fontfamily.PB, }}>{Helper.userSettings.user_coins}</Text>
                <Text style={{ fontSize: 14, marginTop: 10, color: '#acacbc', fontFamily: Fontfamily.PB, }}> VC</Text>
              </View>

              <View style={{ width: 190, height: 45, borderRadius: 28, justifyContent: 'center', alignItems: 'center', backgroundColor: 'rgb(106,231,152)', shadowOpacity: 0.4, shadowColor: 'rgb(106,231,152)', shadowRadius: 28, marginTop: 15 }} >
                <TouchableOpacity style={{ width: '100%', height: '100%', borderRadius: 28, justifyContent: 'center', alignItems: 'center', backgroundColor: 'rgb(106,231,152)', }} onPress={() => this.methodCashInClick()}>
                  <Text style={{ fontSize: 14, fontFamily: Fontfamily.MSB, color: '#fff', }}>Cash-In</Text>
                </TouchableOpacity>
              </View>

              <View style={{ width: 190, height: 45, borderRadius: 28, justifyContent: 'center', alignItems: 'center', backgroundColor: 'rgb(73,186,243)', marginTop: 8, shadowOpacity: 0.4, shadowColor: 'rgb(73,186,243)', shadowRadius: 28 }} >
                <TouchableOpacity style={{ width: '100%', height: '100%', borderRadius: 28, justifyContent: 'center', alignItems: 'center', backgroundColor: 'rgb(73,186,243)' }} onPress={() => pushToScreen('RecentSendCoins')}>
                  <Text style={{ fontSize: 14, fontFamily: Fontfamily.MSB, color: '#fff', }}>Send View Coins</Text>
                </TouchableOpacity>
              </View>

            </View>
          </View>

        </ImageBackground>

        <View style={{ flex: 1 }}
          refreshControl={
            <RefreshControl
              refreshing={this.state.showLoaderRefresh}
              onRefresh={this._pullToRefresh}
            />
          }>
          {(this.state.searchType == '' || this.state.searchType == 'IN' || this.state.searchType == 'OUT' || this.state.searchType == 'MIND') &&
            <Modalize
              modalHeight={Dimensions.get('window').height / 1.2}
              alwaysOpen={Dimensions.get('window').height - 400}
              withHandle={false}
              HeaderComponent={this.headerView(this.state.searchType)}
              onPositionChange={this.changePos}
              openAnimationConfig={{
                timing: { duration: 1, delay: 0 },
                spring: { speed: 10000, bounciness: 0 }
              }}

              flatListProps={{
                data: this.state.searchType == 'MIND' ? this.state.arrMindData : this.state.arrCoinHistory,
                extraData: this.state.searchType == 'MIND' ? this.state.arrMindData : this.state.arrCoinHistory,
                renderItem: this.renderItem,
                keyExtractor: (item, index) => index.toString(),
                showsVerticalScrollIndicator: false,
                ItemSeparatorComponent: this.separetview,
                onEndReached: this.onEndReached,
                onEndReachedThreshold: 0.5,
                contentContainerStyle: { flexGrow: this.state.Position == 'top' ? 1 : 0.5},

                ListEmptyComponent: () => (
                  this.state.searchType == 'MIND' ?
                    this.MindEmptyView()
                    :
                    this.AllInOutEmptyView()

                )
                // ListEmptyComponent: this.EmptyComponent,
              }}

            />
            // :
            // <View style={{ flex: 1, backgroundColor: '#fff' }}>
            //   {this.headerView(this.state.searchType)}
            //   {this.state.arrMindData.length > 0 == 'MIND' ?
            //     // Helper.userSettings?.today_points > 0 ?
            //     // this.CoinGetView()

            //     this.MindEmptyView()
            //     :
            //     this.AllInOutEmptyView()
            //   }
            // </View>
          }
        </View>

        <CashInNoCoinsModel
          modelVisible={this.state.modelVisible}
          showNoCoinsModel={(val) => this.showNoCoinsModel(val)} />

        <WhatIsTrollModelCompo
          trollModel={this.state.trollModel}
          showTrollModel={(val) => this.showTrollModel(val)} />

        <Spinner visible={this.state.showSpinner} />
        {this.state.showBlockModel &&
          <BlockModelCompo
            showBlockModel={this.state.showBlockModel}
            onBlock={(item) => this.onBlock(item)}
            hideBlockModel={() => this.setState({ showBlockModel: false })}
            selectedRowDetail={this.state.selectedRowDetail}
          /> }
        {this.state.showInfoModel &&
          <InfoModelCompo
            showInfoModel={this.state.showInfoModel}
            hideInfoModel={() => this.setState({ showInfoModel: false })}
            onShowBlockModel={() => this.onShowBlockModel()}
            goToChat={(item) => this.goToChat(item)}
            selectedRowDetail={this.state.selectedRowDetail}
          /> }
      </View>
    );
  }


}
export default CoinOld;
const Style = StyleSheet.create({
  tabtxtstyle: {
    fontSize: 15,
    color: 'rgb(216,217,219)',
    fontFamily: Fontfamily.PB

  },
  separater: {
    height: 2,
    backgroundColor: "#F0F0F0",
    width: "100%"
  },
  scrollViewContent: {
    flex: 1,
    backgroundColor: "#FFF",
  }
})