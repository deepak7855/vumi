import React, { Component } from 'react'
import { Text, View, TextInput, TouchableOpacity, FlatList, Image, StyleSheet } from 'react-native'
import { GHeader, GHeaderTitle } from '../common'



import { Images, string, Colors } from '../common'
import { Actions } from 'react-native-router-flux';


export class SearchScreen extends Component {
    constructor(props) {
        super(props);
        this.state = {
            SearchText: '',
            data: ['AMC Camp Creek 14'],
            notFound: false,
            maincontactList: [
                { name: 'Steve ', },
                { name: 'Molly Freeman', },
                { name: 'Jillian Matthews ', },
                { name: 'Grece Kim', },
                { name: 'Sarah Dodson', },
                { name: 'Center Stations', },
                { name: 'Grece Kim', },
                { name: 'Center Stations', },
            ],
        }
    }
    setValues(key, value) {
        this.setState({ SearchText: value })
        this.SearchApICall(value)
    }

    SearchApICall = (keyword) => {
        if (this.state.maincontactList.length > 0) {
            if (keyword != '') {
                const newData = this.state.maincontactList.filter(item => {

                    const itemData = `${item.name}`
                    const textData = keyword.toUpperCase();
                    return itemData.indexOf(textData) > -1;
                });
                if (newData.length > 0) {
                    console.log("--------notFound false: ");
                    this.setState({ data: newData, notFound: false, });
                }
                else {
                    console.log("--------notFound true: ");
                    this.setState({ data: newData, notFound: true, });
                }
            } else {

                this.setState({ data: [], notFound: false, });
                console.log("--------data: " + this.state.notFound);

            }
        }
        else {
            this.setState({ notFound: true, });
        }

    };

    _renderItem = ({ item, index }) => {
        return (
            <TouchableOpacity onPress={() => Actions.pop()}
            >
                <View style={[styles.list_view,]}>
                    <View style={styles.listView}>
                        <Text style={[styles.text_list, { color: '#919191' }]}>{item}</Text>
                    </View>

                </View>
            </TouchableOpacity>
        );
    }

    render() {
        return (
            <View style={styles.Addcontainer}>
                <GHeader
                    Title={string.AddTickets.search}
                    backgroundColor='#fff'
                    bArrow
                />
                <View style={styles.AddHeadercon}>
                    <View style={styles.marginTopTtwinty}>
                        <View style={{ justifyContent: 'center', }}>
                            <Image
                                resizeMode='contain'
                                source={Images.Home.search}
                                style={styles.homeImaSEAT}
                            />
                        </View>
                        <TextInput
                            ref={(r) => { this._textInputRef = r; }}
                            style={styles.input}
                            underlineColorAndroid='transparent'
                            placeholder="Search"
                            placeholderTextColor="#ACAFB3"
                            autoCapitalize="none"
                            value={this.state.SearchText}

                            onChangeText={(SearchText) => this.setValues('SearchText', SearchText)} />
                    </View>

                    <View>
                        <Text style={[styles.Searchrecent, { color: Colors.lightgrey, marginLeft: 17, }]}>{string.Search.Recent}</Text>

                        <View style={{ marginTop: 10, }}>
                            <FlatList
                                data={this.state.data}
                                extraData={this.state}
                                renderItem={this._renderItem}
                            />
                        </View>


                    </View>


                </View>
                {this.state.notFound ?
                    <View style={styles.NotfoundView}>
                        <Text style={styles.NotfoundText}>No data found</Text>
                    </View> : null}

            </View>
        )
    }
}
const styles = StyleSheet.create({
    Addcontainer: {
        flex: 1,

    },
    input: {
        marginHorizontal: 10,
        alignItems: 'center',
        justifyContent: 'center',
        flex: 1,
        fontSize: 20,
        color: '#ACAFB3',
       //fontFamily: "Metropolis-Medium"
    },
    marginTopTtwinty: {
        paddingHorizontal: 15,
        paddingVertical: 10,
        flexDirection: 'row'
    },
    AddalignItems: {
        justifyContent: 'center',
        paddingVertical: 10,

    },
    homeImaSEAT: {
        height: 25,
        width: 25,

    },
    Searchrecent: {
        fontSize: 16,
      //  fontFamily: "Metropolis-Medium"
    },
    ListImage: {
        height: 50,
        width: 50,
        borderRadius: 25,
        resizeMode: "cover",

    },
    text_list: {
        fontSize: 20, 
        //fontFamily: "Metropolis-Medium",
    },
    listView: {
        flex: 1, marginLeft: 40, paddingVertical: 10,
    },
    NotfoundView: {
        flex: 1, justifyContent: 'center', alignItems: 'center',
    },
    NotfoundText: {
        fontSize: 16, color: "#ACAFB3", 
        //fontFamily: "Metropolis-Medium",
    }

});