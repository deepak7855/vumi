import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, TouchableOpacity, ActivityIndicator, FlatList, TextInput, SafeAreaView, Keyboard, StatusBar, Platform, BackHandler } from 'react-native';
import { GHeader, Images } from '../common';
import { Colors } from '../common/Colors';
import fonts from '../common/fonts';
import { InfoModelCompo } from '../components/Watch/InfoModelCompo';
import { BlockModelCompo } from '../components/Watch/BlockModelCompo';
import { Actions } from 'react-native-router-flux';
import pushToScreen from '../navigation/NavigationActions';
import Helper from '../lib/Helper';
import Spinner from 'react-native-loading-spinner-overlay';
import SocketConnect from '../common/SocketConnect';
import ImageLoadView from '../common/ImageLoadView';

export default class Comments extends Component {
    constructor(props) {
        super(props);
        this.state = {
            comment: '',
            showInfoModel: false,
            showBlockModel: false,
            showSpinner: false,
            current_page: 1,
            keyboardHeight: 0,
            page: 1,
            next_page_url: null, isFooterLoading: false,
            selectedRowDetail: {},
            total_comments: null
        };
    }


    _keyboardDidShow = (e) => {

        this.setState({ keyboardHeight: e.endCoordinates.height })

    }

    _keyboardDidHide = () => {
        this.setState({ keyboardHeight: 0 })
    }

    onComment = () => {
        Keyboard.dismiss()
        this.setState({ showSpinner: true })

        let data = {
            post_id: this.props.post_id,
            comment: this.state.comment,
            listdataArr: []
        }
        Helper.makeRequest({ url: "add-comment", method: "POST", data: data }).then((resp) => {
            if (resp.status == 'true') {
                this.setState({ showSpinner: false, total_comments: resp.data.total_comment })
                this.state.page = 1
                this._getAllcomment(true)
                // this.setState({ watchList: list })
            } else {
                this.setState({ showSpinner: false })

                Helper.show(resp.message)

            }

        })
        this.setState({ comment: '' })
    }

    onShowBlockModel = () => {
        this.setState({ showInfoModel: false }, () => {
            this.setState({ showBlockModel: true })
        })
    }

    goToChat = (item) => {
        this.setState({ showInfoModel: false }, () => {
            pushToScreen('Chat', { OtherUserId: item.user.id, OtherUserName: item.user.username });
        })
    }

    _onPressLike = (id, is_like, index) => {
        this.setState({ showSpinner: true })

        // console.log(is_like,"????")
        let data = {
            comment_id: id,
        }
        Helper.makeRequest({ url: is_like ? 'comment-unlike' : 'comment-like', method: "POST", data: data }).then((resp) => {
            if (resp.status == 'true') {
                this.setState({ showSpinner: false })
                // this.state.page = 1
                // this._getAllcomment(true)
                // listdataArr: [...this.state.listdataArr, ...resp.data.data],
                let likePostdata = this.state.listdataArr[index]
                likePostdata.is_like = is_like ? 0 : 1
                this.state.listdataArr.splice(index, 1, likePostdata)
                this.setState({})
            } else {
                this.setState({ showSpinner: false })

                Helper.show(resp.message)
            }

        })
    }

    _renderItem({ item, index }) {
        // console.log(item, "???//////")
        return (
            <View style={styles.rowContainer}>
                <View style={{ flexDirection: "row" }}>
                    <TouchableOpacity onPress={() => this.setState({ showInfoModel: true, selectedRowDetail: item })}>
                        {/* <Image
                            source={{ uri: Helper.getImageUrl(item.profile,) }}
                            resizeMode={'cover'}
                            style={styles.userImg}
                        /> */}
                        <ImageLoadView
                            source={{ uri: Helper.getImageUrl(item.profile,) }}
                            resizeMode={'cover'}
                            style={styles.userImg} />
                    </TouchableOpacity>
                    <View style={{ width: '80%', marginLeft: 10 }}>
                        <Text style={styles.unserName}>{item.user_name}</Text>
                        <Text style={styles.userComment}>{item.comment}</Text>
                    </View>
                </View>
                <TouchableOpacity onPress={() => this._onPressLike(item.id, item.is_like, index)}
                    style={styles.likeView}>
                    {/* <Text style={styles.unserName}>{item.likes.length}</Text> */}
                    <Image
                        source={item.is_like == 1 ? Images.heart : Images.heart_grey} // Images.heart_grey Images.brdr_heart
                        style={styles.likeImg}
                    />
                </TouchableOpacity>
            </View>
        )
    }

    viewabilityConfig = {
        viewAreaCoveragePercentThreshold: 80
    }

    onEndReached = () => {
        // alert("f")
        // if (this.state.current_page < this.state.last_page) {
        //     this.setState({
        //         current_page: this.state.current_page + 1,
        //     }, () => {
        //         this._getAllcomment();
        //     })
        // }
    }
    onScroll = () => {
        if (this.state.isFooterLoading == false && this.state.next_page_url) {
            this.setState({ isFooterLoading: true })
            this.state.page = this.state.page + 1
            this._getAllcomment(false);
        }
    }
    render_Activity_footer = () => {
        var footer_View = (
            <View style={{ flex: 1, backgroundColor: '#000', alignItems: 'center', justifyContent: 'center', padding: 15 }}>
                <ActivityIndicator size="large" color="#fff" />
            </View>
        );
        return footer_View;
    };

    onBlock = (item) => {
        this.setState({ showBlockModel: false })
        let formdata = {
            user_id: Helper.userInfo.id,
            other_user_id: item.user.id,
        }
        if (SocketConnect.checkSocketConnected()) {
            SocketConnect.socketDataEvent('user_block_unblock', formdata);
            this._getAllcomment(true)
        } else {
            SocketConnect.checkConnection('connect', cb => {
                console.log('SocketConnect------------', cb);
            });
        }
    }

    onBackPress = () => {
         if(this.state.total_comments)
        this.props.navigation.state.params.onBackFromComments(this.state.total_comments)
        Actions.pop();
    }


    render() {
        return (
            <SafeAreaView style={styles.container}>
                <GHeader
                    backArrowWithCallback={true}
                    backgroundColor={"#1b1b21"}
                    title1={"Comments"}
                    onBackPress={() => this.onBackPress()}
                />
                <View style={styles.flatlist}>
                    <FlatList
                        keyExtractor={(item, index) => index}
                        renderItem={(item) => this._renderItem(item)}
                        keyboardShouldPersistTaps="handled"
                        showsVerticalScrollIndicator={false}
                        data={this.state.listdataArr}
                        onEndReached={this.onScroll}
                        onEndReachedThreshold={0.5}
                        ListFooterComponent={this.state.isFooterLoading && this.render_Activity_footer}
                    />
                </View>
                <View style={[styles.commentContainer, { marginBottom: Platform.OS == 'ios' ? this.state.keyboardHeight : 0 }]}>
                    <View style={styles.commentHolder}>
                        <TextInput
                            placeholder="Write a Comment..."
                            placeholderTextColor="#818090"
                            underlineColorAndroid="transparent"
                            style={styles.comment}
                            value={this.state.comment}
                            onChangeText={(text) => this.setState({ comment: text })}
                        />
                        <TouchableOpacity disabled={this.state.comment.trim() ? false : true} onPress={() => this.onComment()}>
                            <Image
                                source={Images.send}
                                style={styles.send}
                            />
                        </TouchableOpacity>
                    </View>
                </View>
                {
                    this.state.showInfoModel &&
                    <InfoModelCompo
                        showInfoModel={this.state.showInfoModel}
                        hideInfoModel={() => this.setState({ showInfoModel: false })}
                        onShowBlockModel={() => this.onShowBlockModel()}
                        goToChat={(item) => this.goToChat(item)}
                        selectedRowDetail={this.state.selectedRowDetail}
                    />
                }

                {
                    this.state.showBlockModel &&
                    <BlockModelCompo
                        showBlockModel={this.state.showBlockModel}
                        onBlock={(item) => this.onBlock(item)}
                        hideBlockModel={() => this.setState({ showBlockModel: false })}
                        selectedRowDetail={this.state.selectedRowDetail}
                    />
                }

                <Spinner visible={this.state.showSpinner} />

            </SafeAreaView>
        );
    }

    _getAllcomment(loader) {
        loader == true ?
            this.setState({ showSpinner: true })
            :
            this.setState({ showSpinner: false })

        let data = {
            post_id: this.props.post_id,
            is_unique: 0
        }
        Helper.makeRequest({ url: "comment-list" + "?page=" + this.state.page, method: "POST", data: data }).then((resp) => {
            this.setState({
                showSpinner: false,
                isFooterLoading: false
            });
            if (this.state.page == 1) {
                this.state.listdataArr = []
            }
            if (resp.status == 'true') {
                // console.log(resp, '???????')
                // this.setState({ listdataArr: resp.data.data })
                // this.setState({ watchList: list })
                if (resp.data.data && resp.data.data.length > 0) {
                    this.setState({
                        listdataArr: [...this.state.listdataArr, ...resp.data.data],
                        next_page_url: resp.data.next_page_url,
                        showSpinner: false,
                        isFooterLoading: false
                    })
                }
                else {
                    this.setState({
                        next_page_url: resp.data.next_page_url,
                        isFooterLoading: false,
                        showSpinner: false
                    })
                }

            } else {
                Helper.show(resp.message)
                this.setState({
                    showSpinner: false,
                    listdataArr: [],
                    isFooterLoading: false,
                    next_page_url: null
                })
            }


        })

    }

    componentDidMount = () => {
        this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this._keyboardDidShow);
        this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this._keyboardDidHide);
        this.backHandler = BackHandler.addEventListener(
            "hardwareBackPress",
            this.backAction
        );
        this.state.page = 1
        this._getAllcomment(true)
        this.functionevevnt = this.props.navigation.addListener('didFocus', () => {
            // this.updateStatusBar();
        });
    };

    backAction = () => {
        if (Actions.currentScene == "Comments") {
            this.onBackPress()
        }

    };

    componentWillUnmount() {
        this.backHandler.remove();
        this.functionevevnt.remove();
        this.keyboardDidShowListener.remove();
        this.keyboardDidHideListener.remove();
    }
    updateStatusBar = () => {

        StatusBar.setBackgroundColor('black');
        StatusBar.setBarStyle('light-content');
    }

}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "rgb(27,27,30)",


    },
    flatlist: {
        flex: 1,
        backgroundColor: "rgb(20,20,20)",
        paddingVertical: 10
    },

    rowContainer: {
        flexDirection: 'row',
        marginHorizontal: 15,
        marginVertical: 15,
        alignItems: "flex-start"
    },

    userImg: {
        height: 40,
        width: 40,
        borderRadius: 20,
    },

    unserName: {
        fontSize: 15,
        color: Colors.charcoalGrey,
        fontFamily: fonts.SFProMedium
    },

    userComment: {
        fontSize: 15,
        color: Colors.coolGrey,
        fontFamily: fonts.SFProRegular
    },

    likeView: {
        height: 40,
        justifyContent: "space-between",
        alignItems: "center"
    },

    likeImg: {
        height: 15,
        width: 15,
        resizeMode: "contain"
    },

    commentContainer: {
        backgroundColor: "#1f1f26",
        padding: 10
    },

    commentHolder: {
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "space-between",
        backgroundColor: "#1f1f26",
        borderRadius: 25,
    },

    comment: {
        paddingVertical: 7,
        paddingHorizontal: 10,
        color: "#818090",
        backgroundColor: "#1f1f26",
        width: '88%',
        borderRadius: 25,
    },

    send: {
        height: 30,
        width: 30,
        resizeMode: "contain",
        marginRight: 10
    }


});