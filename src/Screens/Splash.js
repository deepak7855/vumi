import React, { Component } from 'react'
import { Text, View, Image, Modal, SafeAreaView, ImageBackground } from 'react-native'

import { Actions } from 'react-native-router-flux';
import Helper from '../lib/Helper';
import { Images } from '../common';
import NetInfo from "@react-native-community/netinfo";
import fonts from '../common/fonts';
import pushToScreen from '../navigation/NavigationActions';
// import messaging from '@react-native-firebase/messaging';
// import firebase from '@react-native-firebase/app'
export default class Splash extends Component {


    constructor(props) {
        super(props);
        this.state = {
            isInternetReachable: false,

        }
        
        // this.checkPermission();
        // this.requestUserPermission()
    }



  // requestUserPermission = async ()=> {
  //   const settings = await messaging().requestPermission();
  
  //   if (settings) {
  //     console.log('Permission settings:', settings);
  //   }
  // }



  // checkPermission = async () => {
  //   console.log('checkPermission')
  //   const enabled = await firebase.messaging().hasPermission();
  //   if (enabled) {
  //     console.log('getFcmToken')
  //       this.getFcmToken();
  //   } else {
  //     console.log('requestPermission')
  //       this.requestPermission();
  //   }
  // }
  // getFcmToken = async () => {
  //   const fcmToken = await firebase.messaging().getToken();
  //   if (fcmToken) {
  //     console.log('fcmToken',fcmToken)
      
  //     //this.showAlert('Your Firebase Token is:', fcmToken);
  //   } else {
  //     //this.showAlert('Failed', 'No token received');
  //   }
  // }
  // requestPermission = async () => {
  //   try {
  //     await firebase.messaging().requestPermission();
  //     // User has authorised
  //   } catch (error) {
  //       // User has rejected permissions
  //   }
  // }
  // messageListener = async () => {
  //   this.notificationListener = firebase.notifications().onNotification((notification) => {
  //       const { title, body } = notification;
  //       this.showAlert(title, body);
  //   });
  
  //   this.notificationOpenedListener = firebase.notifications().onNotificationOpened((notificationOpen) => {
  //       const { title, body } = notificationOpen.notification;
  //       this.showAlert(title, body);
  //   });
  
  //   const notificationOpen = await firebase.notifications().getInitialNotification();
  //   if (notificationOpen) {
  //       const { title, body } = notificationOpen.notification;
  //       this.showAlert(title, body);
  //   }
  
  //   this.messageListener = firebase.messaging().onMessage((message) => {
  //     console.log(JSON.stringify(message));
  //   });
  // }


    render() {
        return (
            <>
                <ImageBackground
                    resizeMode = {'contain'}
                    source={Images.splash}
                    style={{ flex: 1, height: '100%', width: '100%', backgroundColor: 'black' }}
                />
                <Modal
                    animationType="slide"
                    transparent={false}
                    visible={false}//this.state.isInternetReachable
                    onRequestClose={() => { }}>
                    <ImageBackground
                        source={Images.bg}
                        style={{ flex: 1, alignItems: "center", padding: '10%', justifyContent: "space-around" }}>

                        <Image
                            source={Images.cat}
                            resizeMode= {"contain"} 
                            style={{ height: 100, width: 100 }}
                        />


                        <View>
                            <Text style={{ color: 'white', fontSize: 25, textAlign: "center", marginBottom: 100, fontFamily: fonts.bold }}>No Internet Connection</Text>
                            <Text style={{ color: 'white', fontSize: 18, textAlign: "center", }}>You are not connected to the internet.</Text>
                            <Text style={{ color: 'white', fontSize: 18, textAlign: "center", }}>Make sure Wi-Fi is on, Airplane Mode is off or Mobile Data is on.</Text>
                        </View>

                    </ImageBackground>
                </Modal>
            </>
        )
    }

    componentDidMount = async () => {
        Helper.getDataFromAsync('token').then((data) => {
            setTimeout(() => {
            if (data) {
                // Actions.TabBar()
                pushToScreen('TabBar')
            } else {
                // Actions.GetStarted()
                pushToScreen('GetStarted')
            }
            }, 500);
            
        })


        NetInfo.addEventListener(state => {
            if (state.isInternetReachable !== null) {
                this.setState({ isInternetReachable: !state.isInternetReachable })
            }
        });
    }
}
