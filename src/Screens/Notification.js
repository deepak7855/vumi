import React, { useState } from 'react'
import { View, ImageBackground, Image, StyleSheet, Text } from 'react-native'
import {
    GButton, Images, ScrollEnable,
    Colors, ScrollButtonTop, Fontfamily,
} from '../common';

import { Actions } from 'react-native-router-flux';
import Helper from '../lib/Helper';
import Toast from 'react-native-root-toast';
import Spinner from 'react-native-loading-spinner-overlay';

export const Notification = (props) => {

    const [showSpinner, setSpinner] = useState(false)

    const onNext = () => {

        var data = {
            user_id: Helper.userInfo.id,
            device_type: Helper.device_type,
            device_id: Helper.device_token
        }
        setSpinner(true)
        Helper.makeRequest({ url: "set-device-token", method: "POST", data: data }).then((resp) => {

            if (resp.status == 'true') {
                Actions.reset('TabBar')
            } else {
                Toast.show(resp.message)
            }
            setSpinner(false)
        })



    }


    {
        return (
            <ImageBackground resizeMode='cover'
                source={Images.Loginbg}
                style={{ flex: 1, alignItems: 'center' }}>

                <ScrollEnable style={{ borderWidth: 0 }} containerStyle={{ alignItems: 'center', flex: 1 }}>

                    <View style={{ marginVertical: 40, borderWidth: 0, marginTop: 60 }}>
                        <View style={Style.bigO}>
                            <Image
                                resizeMode='contain'
                                source={Images.notification.turn_noti}
                                style={{ height: 198, width: 198, }}
                            />
                        </View>

                    </View>

                    <View style={{ width: 300, alignItems: 'center' }}>
                        <Text style={Style.whiteTxt}>Turn on Notifications</Text>
                        <Text style={Style.notiInfo}>We’ll send push notifications to let you know when you get more engagement.</Text>
                    </View>
                </ScrollEnable>

                <ScrollButtonTop>
                    <GButton
                        height={65}
                        radius={32.5}
                        bText='Next'
                        txtcolor='#fff'
                        bgColor={Colors.abuttonColor}
                        // onPress={() => {Actions.TabBar() }}
                        onPress={() => onNext()}
                    />
                </ScrollButtonTop>

                <Spinner visible={showSpinner} />
            </ImageBackground>
        )
    }
}
const Style = StyleSheet.create({



    bigO: {
        height: 200, width: 200, borderWidth: 0,
        borderRadius: 100, borderColor: 'rgb(112,105,255)',
        justifyContent: 'center',
        alignItems: 'center',


    },
    smallO: {
        position: 'absolute', bottom: 5, right: 5,
        borderWidth: 5, borderColor: 'rgb(112,105,255)',
        backgroundColor: Colors.abuttonColor,
        height: 45, width: 45, borderRadius: 23.5,
        justifyContent: 'center', alignItems: 'center'

    },
    whiteTxt: {
        color: '#fff',
        fontFamily: Fontfamily.MEB,
        fontSize: 30,
        textAlign: 'center'
    },

    notiInfo: {
        marginVertical: 20, fontSize: 20,
        fontFamily: Fontfamily.MSB,
        color: 'rgb(137,121,255)', textAlign: 'center'
    }

})