import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, ActivityIndicator, FlatList, TouchableOpacity, ScrollView, Switch, Dimensions, SafeAreaView, ImageBackground, Platform } from 'react-native';
import { Images, Colors, colors } from '../common';
import fonts from '../common/fonts';
import { WhatIsTrollModelCompo } from '../components/common/WhatIsTrollModelCompo';
import { FastPassModelCompo } from '../components/common/FastPassModelCompo';
import { Actions } from 'react-native-router-flux';
import Helper from '../lib/Helper';
import Toast from 'react-native-root-toast';
import Spinner from 'react-native-loading-spinner-overlay';
import pushToScreen from '../navigation/NavigationActions';
import FastImage from 'react-native-fast-image';
import Config from '../lib/Config';

// import InappPurchase from '../lib/InappPurchase';
const itemSubs = Platform.select({
    ios: ['com.vumi.fastpass', 'com.vumi.onemonth.nonnewing', 'com.vumi.sixmonth.nonnewing', 'com.vumi.twelvemonth.nonnewing'],
    android: ['com.vumi.fastpass.android', 'com.vumi.onemonth.nonnewing.android', 'com.vumi.sixmonth.nonnewing.android', 'com.vumi.twelvemonth.nonnewing.android'],
});

import RNIap, {
    Product,
    ProductPurchase,
    PurchaseError,
    acknowledgePurchaseAndroid,
    purchaseErrorListener,
    purchaseUpdatedListener,
} from 'react-native-iap';
import { QuestionMarkModel } from '../components/common/QuestionMarkModel';
import ImageLoadView from '../components/ImageLoadView';

const ScreenWidth = Dimensions.get('window').width;
const postSliderWidth = (Dimensions.get('window').width - 30)
export default class CashIn extends Component {
    constructor(props) {
        super(props);
        this.state = {
            splitSwitchValue: false,
            pickyourPostData: [0, 1, 2],
            activepage: 0,
            showFastPassModel: false,
            showTrollModel: false,
            showSpinner: false,
            view_packs: [],
            selected_pack: '',
            selected_pack_index: -1,
            arrDraftPostList: [], first_post_id: '',
            showLoaderPageing: false,
            showLoaderRefresh: false, arrSplitIds: [],
            is_fast_pass: false,
            transaction_id: '',
            fastpass_available: false,
        };

    }
    async componentDidMount() {
        try {
            const result = await RNIap.initConnection();
            await RNIap.consumeAllItemsAndroid();
        } catch (err) {
            console.log('error in cdm => ', err);
        }


        this.purchaseUpdateSubscription = purchaseUpdatedListener(async (purchase: SubscriptionPurchase) => {
            console.log('purchaseUpdatedListener', purchase);
            const receipt = purchase.transactionReceipt;
            // console.log('receipt', receipt);
            if (receipt) {
                if (Platform.OS === 'ios') {
                    await RNIap.finishTransactionIOS(purchase.transactionId);

                } else if (Platform.OS === 'android') {
                    // If consumable (can be purchased again)
                    await RNIap.consumePurchaseAndroid(purchase.purchaseToken);
                    // If not consumable
                    await RNIap.acknowledgePurchaseAndroid(purchase.purchaseToken);
                }

                this.setState({ showFastPassModel: false, is_fast_pass: true, transaction_id: purchase.transactionId, showIapIndicator: false })

            }
        });

        this.purchaseErrorSubscription = purchaseErrorListener((error: PurchaseError) => {
            this.setState({ showIapIndicator: false })
            console.warn('purchaseErrorListener', error);
        });

        this.functionevevnt = this.props.navigation.addListener('didFocus', () => {
            // StatusBar.setBackgroundColor('black');
            // StatusBar.setBarStyle('light-content')
        });
        this.getSettingApi()
        this.setState({ showSpinner: true })
        this.getDraftPostListData(false, "");

        Helper.makeRequest({ url: "get-post-package", method: "POST", data: '' }).then((resp) => {
            if (resp.status == 'true') {
                this.setState({ view_packs: resp.data, selected_pack: resp.data[0].id })
            } else {
                Toast.show(resp.message)
            }
            this.setState({ showSpinner: false })
        })
    }
    handlePageChange = (e) => {
        var offset = e.nativeEvent.contentOffset;
        if (offset) {
            var page = parseInt(Math.round(offset.x / postSliderWidth));
            console.log("page", page)
            this.setState({ activepage: page });
        }
    }

    onHideFastPassModel = (value) => {
        if (value == "buy") {
            // this.setState({ showFastPassModel: false, is_fast_pass: true, transaction_id: new Date() })
            this.setState({ showFastPassModel: false, showIapIndicator: true })
            //  InappPurchase.requestSubscription("com.vumi.fastpass");

            this.requestSubscription(itemSubs[0])
        }
        else {
            this.setState({ showFastPassModel: false })
        }
    }

    onGetViewsPress() {
        if (this.state.arrSplitIds.length == 0) {
            alert('Please Select Post.')
        }
        else if (this.state.selected_pack == '') {
            alert('Please select View Pack')
        }
        else {
            let dicSend = {}
            dicSend.post_id = this.state.arrSplitIds.toString()
            dicSend.points = this.state.selected_pack
            dicSend.is_split = this.state.splitSwitchValue ? 1 : 0
            dicSend.transaction_id = this.state.transaction_id
            dicSend.is_fast_pass = this.state.is_fast_pass ? 1 : 0
            this.setState({ showSpinner: true })
            Helper.makeRequest({ url: "publish-post", method: "POST", data: dicSend, isImage: false }).then((resp) => {
                if (resp.status == 'true') {
                    this.setState({ showSpinner: false })
                    // Actions.OverView();

                    Actions.pop()

                    pushToScreen('OverView');
                    Toast.show(resp.message);
                } else {
                    Toast.show(resp.message)
                }
                this.setState({ showSpinner: false })
            })
        }

    }


    getDraftPostListData = (spinner, last_id) => {
        this.setState({ showSpinner: spinner }, () => {
            Helper.makeRequest({ url: "get-temp-post", method: "POST", data: { last_id: last_id } }).then((resp) => {
                this.state.showLoaderPageing = false
                this.state.showLoaderRefresh = false
                this.state.showSpinner = false
                if (last_id == "") {
                    this.state.arrDraftPostList = []
                }
                if (resp.status == 'true') {
                    if (resp.data && resp.data.data && resp.data.data.length > 0) {
                        this.state.arrDraftPostList = [...this.state.arrDraftPostList, ...resp.data.data]
                    }
                    this.state.first_post_id = resp.data.first_id ? resp.data.first_id : ""
                } else {
                    Toast.show(resp.message)
                }
                this.setState({})
            })
        })

    }
    onScroll = () => {
        if (!this.state.showLoaderPageing && this.state.arrDraftPostList.length > 0 && this.state.first_post_id != '' && this.state.arrDraftPostList[this.state.arrDraftPostList.length - 1].id != this.state.first_post_id) {
            this.setState({ showLoaderPageing: true })
            this.getDraftPostListData(false, this.state.arrDraftPostList[this.state.arrDraftPostList.length - 1].id);
        }
    }
    getSettingApi = () => {
        // this.setState({ showSpinner: true })
        Helper.makeRequest({ url: 'settings', method: "POST", data: '' }).then((resp) => {
            if (resp.status == 'true') {
                console.log("settings------------------------------", resp)
                Helper.userSettings = resp.data
                this.setState({ fastpass_available: resp.data.fastpass_available })

            } else {
                Helper.userSettings = {}
                this.setState({})
            }
            // this.setState({ showSpinner: false })
        })
    }
    methodAddRemovePostId(item) {
        let index = this.state.arrSplitIds.indexOf(item.id)
        if (index == -1) {
            if (!this.state.splitSwitchValue) {
                this.state.arrSplitIds = []
            }
            this.state.arrSplitIds.push(item.id)
        }
        else {
            this.state.arrSplitIds.splice(index, 1)
        }

        this.setState({})
    }
    methodSplitViewClick() {
        if (this.state.arrDraftPostList.length == 1) {
            return
        }

        if (this.state.arrSplitIds.length > 0 && this.state.splitSwitchValue) {
            this.state.arrSplitIds = []
        }
        this.setState({ splitSwitchValue: !this.state.splitSwitchValue })
    }

    getFastPass = () => {
        if (this.state.fastpass_available) {
            this.setState({ is_fast_pass: true })
        } else {
            this.setState({ showFastPassModel: true })
        }
    }


    render() {
        let dots = this.props.media && this.props.media.length > 0 ? this.props.media : this.state.pickyourPostData
        return (
            <SafeAreaView style={{ backgroundColor: "black", flex: 1, justifyContent: "center" }}>
                <View style={styles.topContainer}>
                    <TouchableOpacity onPress={() => Actions.pop()}>
                        <Image
                            source={Images.Header.leftArrow}
                            // source={Images.right_arrow}
                            style={{ height: 18, width: 18, resizeMode: "contain" }}
                        />
                    </TouchableOpacity>
                    <View style={{ flex: 1 }}>
                        <Text style={styles.cashIn}> Cash-in </Text>
                    </View>
                    <View style={styles.subContainer}>
                        <Image
                            source={Images.coin}
                            style={styles.coin}
                        />
                        <Text style={styles.coinText}>{Helper.userSettings.user_coins}</Text>


                    </View>
                </View>
                <ScrollView style={styles.whiteContainer}>
                    <View style={styles.card}>
                        <View style={styles.rowCenter}>
                            <View style={styles.dotView}>
                                <Text style={styles.textOne}>1</Text>
                            </View>
                            <Text style={styles.textOne}>Pick your post</Text>

                        </View>


                        {this.state.arrDraftPostList.length > 1 && <View style={{ width: '50%', alignItems: 'flex-end', justifyContent: 'center', paddingVertical: 10 }}>
                            <ScrollView horizontal>
                                {this.state.arrDraftPostList.map((item, index) => {
                                    let postDetails = (item && item.post_detail && item.post_detail.length > 0) ? item : ""
                                    // console.log("index " + index + " " + "activepage" + this.state.activepage + " " + postDetails)
                                    if (postDetails == "") {
                                        return (<View />)
                                    }
                                    return (
                                        <View style={{ height: 10, width: 10, borderRadius: 5, marginLeft: 5, backgroundColor: this.state.activepage == index ? '#38bfff' : "#e9eef0" }} />
                                    )
                                })
                                }
                            </ScrollView>
                        </View>}

                    </View>

                    <View style={{ height: 70, marginBottom: 10, }}>
                        <FlatList
                            horizontal
                            data={this.state.arrDraftPostList}
                            pagingEnabled
                            keyExtractor={(item, index) => index.toString()}
                            showsHorizontalScrollIndicator={false}
                            scrollEventThrottle={16}
                            onMomentumScrollEnd={this.handlePageChange}
                            ListFooterComponent={this.state.showLoaderPageing && this.render_Activity_footer}
                            onEndReached={this.onScroll}
                            onEndReachedThreshold={0.5}

                            renderItem={({ item, index }) => {
                                let postDetails = (item && item.post_detail && item.post_detail.length > 0) ? item : ""
                                console.log(postDetails, "postDetails");
                                if (postDetails == "") {
                                    return (<View />)
                                }
                                return (
                                    <TouchableOpacity onPress={() => this.methodAddRemovePostId(item)} style={{ height: 70, borderColor: "#836cff", borderWidth: this.state.arrSplitIds.indexOf(item.id) == -1 ? 0 : 1, justifyContent: 'center', width: postSliderWidth, marginHorizontal: 15, padding: 10 }}>
                                        <View style={{ flexDirection: "row", alignItems: "center" }}>
                                            {/* <FastImage style={styles.userImg}
                                                source={postDetails.post_detail[0].thumbnail_media ? {
                                                    uri: Config.imageUrl + postDetails.post_detail[0].thumbnail_media,
                                                    priority: FastImage.priority.normal,
                                                } : Images.User_profile_icon}
                                                resizeMode={FastImage.resizeMode.stretch} /> */}
                                            <ImageLoadView
                                                resizeMode={"cover"}
                                                style={styles.userImg}
                                                source={postDetails.post_detail[0].thumbnail_media ? {
                                                    uri: Config.imageUrl + postDetails.post_detail[0].thumbnail_media
                                                } : Images.User_profile_icon}
                                            />
                                            <View style={{ width: ScreenWidth - 100, paddingLeft: 10 }}>
                                                <Text numberOfLines={1} style={{ color: '#606076', fontFamily: fonts.SFProDisplay_Heavy, fontSize: 16 }}>{postDetails.display_name}</Text>
                                                <Text numberOfLines={1} style={{ color: '#9e9eb4', fontFamily: fonts.SFProDisplay_Heavy, fontSize: 16 }}>No Views</Text>

                                            </View>

                                        </View>
                                    </TouchableOpacity>
                                )
                            }}
                        />



                        {/* {
                            this.props.media && this.props.media.length > 0 ?
                                <FlatList
                                    horizontal
                                    data={this.props.media}
                                    pagingEnabled
                                    keyExtractor={(item, index) => index}
                                    showsHorizontalScrollIndicator={false}
                                    scrollEventThrottle={16}
                                    onMomentumScrollEnd={this.handlePageChange}
                                    renderItem={({ item }) => (
                                        <View style={{ height: 70, width: ScreenWidth - 40, backgroundColor: 'red' }}>
                                            <View style={{ flexDirection: "row", alignItems: "center" }}>

                                                <Image
                                                    style={{ height: 70, width: 70, resizeMode: "contain", marginLeft: 25, marginRight: 10 }}
                                                    source={{ uri: item }}
                                                />

                                                <View style={{ width: '60%' }}>
                                                    <Text numberOfLines={1} style={{ color: '#606076', fontFamily: fonts.SFProDisplay_Heavy, fontSize: 16 }}>{Helper.userInfo.username}</Text>
                                                    <Text numberOfLines={1} style={{ color: '#9e9eb4', fontFamily: fonts.SFProDisplay_Heavy, fontSize: 16 }}>No Views</Text>

                                                </View>

                                            </View>
                                        </View>
                                    )}
                                />
                                :
                                <FlatList
                                    style={{ width: ScreenWidth - 40, marginHorizontal: 20 }}
                                    horizontal
                                    data={this.state.pickyourPostData}
                                    pagingEnabled
                                    keyExtractor={(item, index) => index}
                                    showsHorizontalScrollIndicator={false}
                                    scrollEventThrottle={16}
                                    onMomentumScrollEnd={this.handlePageChange}
                                    renderItem={() => (
                                        <View style={{ height: 70, width: ScreenWidth - 40, backgroundColor: 'red' }}>
                                            <View style={{ flexDirection: "row", alignItems: "center" }}>
                                                <Image
                                                    style={{ height: 70, width: 70, resizeMode: "contain", paddingRight: 10 }}
                                                    source={Images.dummy1}
                                                />
                                                <View style={{ width: '60%' }}>
                                                    <Text numberOfLines={1} style={{ color: '#606076', fontFamily: fonts.SFProDisplay_Heavy, fontSize: 16 }}>Davontae - Switch Up…  </Text>
                                                    <Text numberOfLines={1} style={{ color: '#9e9eb4', fontFamily: fonts.SFProDisplay_Heavy, fontSize: 16 }}>12, 649 Views</Text>

                                                </View>

                                            </View>
                                        </View>
                                    )}
                                />
                        } */}
                    </View>


                    <View style={{ height: 5, width: '100%', backgroundColor: "#f0f2f7" }} />


                    <View style={styles.card}>
                        <View style={styles.rowCenter}>
                            <View style={styles.dotView}>
                                <Text style={styles.textOne}>2</Text>
                            </View>
                            <Text style={styles.textOne}>Pick a view pack</Text>

                        </View>
                    </View>
                    <ScrollView
                        horizontal
                        showsHorizontalScrollIndicator={false}
                        style={{ height: 50, marginBottom: 10, paddingHorizontal: 10 }}>
                        {
                            this.state.view_packs.map((item, index) => {
                                return (
                                    // <TouchableOpacity style={{ backgroundColor: this.state.selected_pack_index == index ? '#836cff' : 'transparent', justifyContent: 'center', alignItems: 'center', borderRadius: 2, marginHorizontal: 10, minHeight: 50, minWidth: 50, padding: 5 }} onPress={() => this.methodSelectPackage(item, index)}>
                                    //     <ImageBackground source={Images.packs[index % Images.packs.length]} style={styles.kIcon}>
                                    //         <Text style={{ fontSize: 18, fontFamily: fonts.Montserrat_ExtraBold, color: 'white', margin: 5 }}>{Helper.convertValueBillions(item.value)}</Text>
                                    //     </ImageBackground>
                                    // </TouchableOpacity>
                                    <TouchableOpacity style={{ backgroundColor: this.state.selected_pack_index == index ? '#836cff' : 'transparent', justifyContent: 'center', alignItems: 'center', borderRadius: 2, marginHorizontal: 10, minHeight: 50, minWidth: 50, padding: 5 }} onPress={() => this.methodSelectPackage(item, index)}>
                                        <View style={[styles.kIcon, { backgroundColor: item.color_code }]}>
                                            <Text style={{ fontSize: 18, fontFamily: fonts.Montserrat_ExtraBold, color: 'white', margin: 5 }}>{Helper.convertValueBillions(item.value)}</Text>
                                        </View>
                                    </TouchableOpacity>
                                )
                            })
                        }
                    </ScrollView>
                    <View style={{ height: 5, width: '100%', backgroundColor: "#f0f2f7" }} />

                    <View style={styles.card}>
                        <View style={styles.rowCenter}>
                            <View style={styles.dotView}>
                                <Text style={styles.textOne}>3</Text>
                            </View>
                            <Text style={styles.textOne}>Powerups</Text>

                        </View>
                        <TouchableOpacity disabled onPress={() => this.setState({ showQuestionModel: true })} style={[styles.dotView, { marginRight: 0 }]}>
                            <Text style={[styles.textOne, { color: "white" }]}>?</Text>
                        </TouchableOpacity>
                    </View>

                    <Image
                        style={{ height: 7, width: 7, resizeMode: "contain", marginLeft: 50 }}
                        source={Images.y_star}
                    />
                    <Image
                        style={{ height: 11, width: 11, resizeMode: "contain", marginLeft: 59 }}
                        source={Images.y_star}
                    />


                    <View style={{ flexDirection: "row", justifyContent: "space-between", alignItems: "center", paddingHorizontal: 25 }}>
                        <View style={{ flexDirection: "row", width: "70%" }}>
                            <Image
                                style={{ height: 50, width: 50, resizeMode: "contain", marginRight: 10 }}
                                source={Images.strom}
                            />
                            <View>
                                <Text style={{ fontSize: 16, fontFamily: fonts.SFProDisplay_Heavy, color: "#616169" }}>Fast Pass</Text>
                                <Text style={{ fontSize: 14, fontFamily: fonts.SFProMedium, color: "#8488a0", width: "60%" }}>Skip the wait. Get your views faster.</Text>

                            </View>
                        </View>
                        {!this.state.is_fast_pass ?
                            <TouchableOpacity disabled={this.state.showIapIndicator} onPress={() => this.getFastPass()} style={{ height: 30, width: 65, borderRadius: 30, alignItems: "center", justifyContent: "center", backgroundColor: "#836cff" }}>
                                {
                                    this.state.showIapIndicator ?
                                        <ActivityIndicator color={colors.white} />
                                        :
                                        <Text style={{ color: "white", fontFamily: fonts.SFProDisplay_Heavy }}>GET</Text>
                                }
                            </TouchableOpacity> :
                            <View style={{ height: 40, width: 40, borderRadius: 20, alignItems: "center", justifyContent: "center", backgroundColor: "#836cff" }}>
                                <Image style={{ height: 20, width: 20, tintColor: '#FFF' }} source={Images.referral}></Image>
                            </View>
                        }
                    </View>




                    <View style={{ backgroundColor: "#f0f2f7", padding: 20, flexDirection: "row", alignItems: "center", justifyContent: "space-between", marginTop: 15 }}>

                        <View>
                            <Text style={{ color: '#606076', fontFamily: fonts.Montserrat_SemiBold, fontSize: 13 }}>Split Views</Text>
                            <Text numberOfLines={1} style={{ color: '#c3c3d3', fontFamily: fonts.SFProMedium, fontSize: 13 }}>Show repeat viewers your other posts</Text>
                        </View>

                        <Switch
                            // trackColor={{ false: "#767577", true: "#81b0ff" }}
                            thumbColor={this.state.splitSwitchValue ? "white" : "white"}
                            // ios_backgroundColor="#c7cdd5"    
                            onChange={() => this.methodSplitViewClick()}
                            value={this.state.splitSwitchValue}
                        />
                    </View>

                    <TouchableOpacity disabled={this.state.showIapIndicator} onPress={() => this.onGetViewsPress()} style={{ backgroundColor: "#1fe3a7", padding: 20, alignItems: "center", justifyContent: "center", borderBottomLeftRadius: 25, borderBottomRightRadius: 25 }}>
                        <Text style={{ fontFamily: fonts.Montserrat_ExtraBold, fontSize: 20, color: "white" }}>GET VIEWS</Text>
                    </TouchableOpacity>



                </ScrollView>


                <View style={{ alignItems: "center", justifyContent: "center", height: 50, flexDirection: "row" }}>
                    <Text style={{ color: "#cacbdb", marginRight: 5 }}>Community Toll: 5%</Text>
                    <TouchableOpacity onPress={() => { this.setState({ showTrollModel: true }) }} style={[styles.dotView, { height: 20, width: 20, borderRadius: 10, backgroundColor: "#a3a4b1" }]}>
                        <Text style={[styles.textOne, { color: "white", fontSize: 13 }]}>?</Text>
                    </TouchableOpacity>
                </View>


                <WhatIsTrollModelCompo
                    trollModel={this.state.showTrollModel}
                    hideTrollModel={() => this.setState({ showTrollModel: false })} />

                <FastPassModelCompo
                    showFastPassModel={this.state.showFastPassModel}
                    hideFastPassModel={(value) => this.onHideFastPassModel(value)}
                />

                <QuestionMarkModel
                    heading={'Powerups'}
                    content={'Indicates whether the subscription renews automatically. If true, the subscription is active, and will automatically renew on the next billing date. Otherwise, indicates that the user has canceled the subscription. '}
                    showQuestionModel={this.state.showQuestionModel}
                    closeQuestionModel={() => this.setState({ showQuestionModel: false })}
                />


                <Spinner visible={this.state.showSpinner} />
            </SafeAreaView>
        );
    }
    methodSelectPackage(item, index) {
        if (Helper.userSettings && Helper.userSettings.user_coins && Number(Helper.userSettings.user_coins) >= Number(item.coins)) {
            this.setState({ selected_pack_index: index, selected_pack: item.id })
        }
        else {
            Toast.show("Wallet coins is not enough to buy a package.", { position: 0 })
        }
    }
    render_Activity_footer = () => {
        var footer_View = (
            <View style={{ flex: 1, backgroundColor: '#ffffff', alignItems: 'center', justifyContent: 'center', padding: 15 }}>
                <ActivityIndicator size="large" color="#000000" />
            </View>
        );
        return footer_View;
    };

    componentWillUnmount() {
        if (this.purchaseUpdateSubscription) {
            this.purchaseUpdateSubscription.remove();
            this.purchaseUpdateSubscription = null;
        }
        if (this.purchaseErrorSubscription) {
            this.purchaseErrorSubscription.remove();
            this.purchaseErrorSubscription = null;
        }
    }

    requestPurchase = async (sku): void => {
        try {
            console.log('requestPurchaserequestPurchase', sku)
            RNIap.requestPurchase(sku);
        } catch (err) {
            this.setState({ showIapIndicator: false })
            console.log('requestPurchase error => ', err);
        }
    };

    getItems = async (): void => {
        try {
            console.log('itemSkus[0]', itemSkus[0]);
            const products: Product[] = await RNIap.getProducts(itemSkus);
            this.requestPurchase(itemSkus[0]);
        } catch (err) {
            this.setState({ showIapIndicator: false })
            console.log('getItems || purchase error => ', err);
        }
    };

    requestSubscription = async (sku) => {

        console.log("sku-------", sku)
        // InappPurchase.methodInit()

        try {
            if (Platform.OS == 'ios') {
                await this.getSubscriptions();
                await RNIap.requestSubscription(sku);
            } else {
                this.getItems();
            }

        } catch (err) {
            alert(err.toLocaleString());
        }
    };

    async getSubscriptions() {
        try {
            const products = await RNIap.getSubscriptions(itemSubs);

            console.log('Products => ', products);
            // this.setState({ productList: products });
        } catch (err) {
            alert('alert2', JSON.stringify(err))
            console.log('getSubscriptions error => ', err);
        }
    }
}


const styles = StyleSheet.create({

    subContainer: {
        flexDirection: "row",
        alignItems: "center"
    },
    coin: {
        height: 25,
        width: 25,
        resizeMode: "contain",
        marginRight: 10
    },
    coinText: {
        color: "white",
        fontFamily: fonts.SFProRegular,
        fontSize: 15
    },
    cashIn: {
        color: "#f2f2f8",
        fontSize: 30,
        fontFamily: fonts.Montserrat_ExtraBold
    },
    userImg: {
        height: 60,
        width: 60,
    },

    kIcon: { height: '100%', width: '100%', justifyContent: 'center', alignItems: 'center', borderRadius: 8, backgroundColor: 'wheat' },
    topContainer: { flexDirection: "row", alignItems: "center", paddingHorizontal: 10, marginTop: 30, marginBottom: 15 },
    whiteContainer: { backgroundColor: "white", borderRadius: 25, width: "100%", },
    card: { flexDirection: "row", alignItems: "center", justifyContent: "space-between", padding: 15 },
    rowCenter: { flexDirection: "row", alignItems: "center" },
    dotView: { backgroundColor: "#e7e7ef", height: 28, width: 28, borderRadius: 14, justifyContent: "center", alignItems: "center", marginRight: 10 },
    textOne: { color: "#606076", fontSize: 16, fontFamily: fonts.Montserrat_ExtraBold },



});