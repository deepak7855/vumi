import React, { Component } from 'react';
import { View, Text, FlatList, Image, TouchableOpacity, StyleSheet, StatusBar } from 'react-native';
import { GHeader, Images, IconInput } from '../common';
import fonts from '../common/fonts';

export default class BonusBox extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    render() {
        return (
            <View style={{ flex: 1, backgroundColor: 'white', }}>
                <StatusBar backgroundColor="white" />
                <GHeader
                    bArrow
                    greyBackButton
                    color="#606076"
                    backgroundColor="white"
                    title1="Bonus Box"
                />
                <View style={{ flex: 0.4, backgroundColor: '#efeff5', alignItems: "center", justifyContent: "center" }}>
                    <Text style={{ color: "#c3c3d6", fontSize: 16, fontFamily: fonts.Poppins_Bold }}> IMAGE / GIF </Text>
                </View>

                <View style={{ alignItems: "center", marginTop: 50 }}>
                    <IconInput
                        limagePath={Images.profile.info}
                        style={styles.coinInput}
                        placeholder={"Coins"}
                        placeholderTextColor="#dcdce2"
                        leftImagetyle={{ height: 30, width: 30, borderRadius: 15 }}
                    />
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    coinInput: {
        height: 60,
        fontSize: 20,
        textAlign:"center",
        fontFamily: fonts.Montserrat_Bold,
        color: "#6d6d76",
        paddingRight:30
    },

});