import React, { Component } from 'react'

import {
    Text, View, StyleSheet, Keyboard,
    TextInput,
    Image,
    TouchableOpacity,
    Dimensions,
    ScrollView
} from 'react-native'



import { Images, string, styles, Colors, GHeaderTitle, } from '../common'
import { Actions } from 'react-native-router-flux'
import pushToScreen from '../navigation/NavigationActions';


export class Account extends Component {
    render() {
        return (
            <View style={{ flex: 1, }}>

                <GHeaderTitle leftText='Account' />
                <View style={{}}>
                    <View style={styles.homeProfileview} >
                        <View style={styles.homeContaninarTabvive}>
                            <Image
                                resizeMode='contain'
                                source={Images.Home.profile_img}
                                style={classStryle.userImageStyle}
                            />
                            <Text style={[styles.homeCon_Text_Log, { textDecorationLine: 'underline', color: Colors.blackgrey, marginTop: 5, }]}>{string.Home.Change}</Text>
                        </View>
                    </View>
                    <View style={[styles.homeprofilesetting, { bottom: 20 }]}>
                        {/* <TouchableOpacity onPress={()=>Actions.Setting()} */}
                        <TouchableOpacity onPress={() => pushToScreen('Setting')}

                            style={[styles.homeaddtickets,]}>
                            <Image
                                resizeMode='contain'
                                source={Images.Home.Settings}
                                style={classStryle.settingimageStyle}
                            />
                            <Text style={[styles.homeAddHeadertest, { color: Colors.blackgrey }]}>
                                {string.Home.Settings}
                            </Text>
                        </TouchableOpacity>
                    </View>
                </View>

                <View style={{ marginTop: 20, }}>

                    <TouchableOpacity style={styles.homeprofilefilde}>
                        <Text style={[styles.homeAddHeadertest, { color: Colors.blackgrey }]}>{string.Home.Name}</Text>
                    </TouchableOpacity>

                    <TouchableOpacity style={styles.homeprofilefilde}>
                        <Text style={[styles.homeAddHeadertest, { color: Colors.blackgrey }]}>{string.Home.state}</Text>
                    </TouchableOpacity>

                    <TouchableOpacity style={styles.homeprofilefilde}>
                        <Text style={[styles.homeAddHeadertest, { color: Colors.blackgrey }]}>{string.Home.Phone}</Text>
                    </TouchableOpacity>

                    <TouchableOpacity style={styles.homeprofilefilde}>
                        <Text style={[styles.homeAddHeadertest, { color: Colors.blackgrey }]}>{string.Home.email}</Text>
                    </TouchableOpacity>


                </View>


            </View>
        )
    }
}

const classStryle = StyleSheet.create({
    userImageStyle: {
        height: 80,
        width: 80
    },
    settingimageStyle: {
        height: 30,
        width: 30
    },


})


