import React, { Component } from 'react';
import { View, Text, FlatList, Image, TouchableOpacity, StyleSheet, Dimensions, StatusBar, ScrollView, TouchableWithoutFeedback } from 'react-native';
import { GHeader, Images } from '../common';
import fonts from '../common/fonts';
import { SavedAndPostRowCompo } from '../components/common/SavedAndPostRowCompo';
import FastImage from 'react-native-fast-image';
import Helper from '../lib/Helper';
import Config from '../lib/Config';
import Video from 'react-native-video';
import ProgressBar from 'react-native-progress/Bar';
import pushToScreen from '../navigation/NavigationActions';
import ImageLoadView from '../components/ImageLoadView';


const screenWidth = Dimensions.get('window').width;


export default class Saved extends Component {
  constructor(props) {
    super(props);
    this.state = {
      postDetails: this.props.navigation.getParam('postDetails', {}),
      videoPaused: false,
      progress: 0,
      duration: 0,
      currentTime: 0,
    };
    this.didBlurListener = this.props.navigation.addListener('didBlur', obj => {
      this.changeState();
    });
  }

  changeState = () => {
    this.setState({ videoPaused: true });
  };

  handleProgress = progress => {
    if (this.state.progress < progress.currentTime / progress.playableDuration)
      this.setState({
        progress: progress.currentTime / progress.playableDuration,
      });
  };

  handleLoad = meta => {
    this.setState({
      progress: 0,
      duration: meta.duration,
      currentTime: meta.currentTime,
    });
  };

  render() {
    console.log(JSON.stringify(this.state.postDetails) + "save data")

    var socillinktget = this.state.postDetails.social_link
    let socillinkt = socillinktget.toLowerCase(); //To convert Lower Case

    let sociltext = null
    let socilcolor = null

    if (socillinkt.includes("soundcloud")) {
      sociltext = "SoundCloud"
      socilcolor = "#ED7217"
    } else if (socillinkt.includes("facebook")) {
      sociltext = 'Facebook'
      socilcolor = "#3c5a9a"
    } else if (socillinkt.includes("instagram")) {
      sociltext = 'Instagram'
      socilcolor = "#dc4e5f"
    } else if (socillinkt.includes("twitter")) {
      sociltext = 'Twitter'
      socilcolor = "#59a8dc"
    } else if (socillinkt.includes("youtub")) {
      sociltext = "YouTube"
      socilcolor = "#d63b32"
    } else if (socillinkt.includes("youtu")) {
      sociltext = "YouTube"
      socilcolor = "#d63b32"
    } else if (socillinkt.includes("snapchat")) {
      sociltext = 'Snapchat'
      socilcolor = "#fffb53"
    } else if (socillinkt.includes("tiktok")) {
      sociltext = 'TikTok'
      socilcolor = "#07080a"
    } else if (socillinkt.includes("behance")) {
      sociltext = 'Behance'
      socilcolor = "#346BF6"
    } else if (socillinkt.includes("dribbble")) {
      sociltext = 'Dribbble'
      socilcolor = "#D1447A"
    } else if (socillinkt.includes("twitch")) {
      sociltext = 'Twitch'
      socilcolor = "#7E4BE7"
    } else if (socillinkt.includes("tumblr")) {
      sociltext = 'Tumblr'
      socilcolor = "#ED7217"
    } else if (socillinkt.includes("kickstarter")) {
      sociltext = 'Kickstarter'
      socilcolor = "#3BB573"
    } else if (socillinkt.includes("bandcamp")) {
      sociltext = 'Bandcamp'
      socilcolor = "#43A1B4"
    } else if (socillinkt.includes("reddit")) {
      sociltext = 'Reddit'
      socilcolor = "#E95627"
    } else if (socillinkt.includes("venmo")) {
      sociltext = 'Venmo'
      socilcolor = "#6FA2CD"
    } else if (socillinkt.includes("cash.app")) {
      sociltext = 'Cash App'
      socilcolor = "#60B636"
    } else if (socillinkt.includes("gofundme")) {
      sociltext = 'GoFundMe'
      socilcolor = "#59B773"
    } else if (socillinkt.includes("indiegogo")) {
      sociltext = 'Indiegogo'
      socilcolor = "#CD3473"
    } else if (socillinkt.includes("deviantart")) {
      sociltext = 'DeviantArt'
      socilcolor = "#62C85A"
    } else if (socillinkt.includes("caseyatkins")) {
      sociltext = 'Caseyatkins'
      socilcolor = "#6260FF"
    } else {
      sociltext = ''
      socilcolor = ""
    }
    return (
      <View style={{ flex: 1, backgroundColor: '#f5f6fa', }}>
        <StatusBar backgroundColor="white" />
        <GHeader
          bArrow
          greyBackButton
          color="#606076"
          backgroundColor="white"
          title1="Saved"
        />

        <ScrollView contentContainerStyle={{ flexGrow: 1 }}>

          {
            this.state.postDetails?.post_detail.length > 0 &&
            <View style={{}}>
              {
                this.state.postDetails?.post_detail[0].type == 'IMAGE' ?
                  // <FastImage style={{ width: screenWidth, height: screenWidth }}
                  //   source={this.state.postDetails.post_detail && this.state.postDetails.post_detail[0].thumbnail_media ? {
                  //     uri: Config.imageUrl + this.state.postDetails.post_detail[0].thumbnail_media,
                  //     priority: FastImage.priority.normal,
                  //   } : Images.User_profile_icon}
                  //   resizeMode={FastImage.resizeMode.contain} />
                  <ImageLoadView
                    resizeMode={"cover"}
                    style={{ width: screenWidth, height: screenWidth }}
                    source={this.state.postDetails.post_detail && this.state.postDetails.post_detail[0].thumbnail_media ? {
                      uri: Config.imageUrl + this.state.postDetails.post_detail[0].thumbnail_media
                    } : Images.User_profile_icon}
                  />
                  :
                  <View style={styles.squareView}>
                    <TouchableWithoutFeedback
                      onPress={() => this.setState({ videoPaused: !this.state.videoPaused })}>
                      <Video
                        source={{
                          uri:
                            Config.imageUrl + this.state.postDetails?.post_detail[0].media
                        }}
                        ref={ref => {
                          this.player = ref;
                        }}
                        onProgress={this.handleProgress}
                        onLoad={this.handleLoad}
                        repeat
                        resizeMode="cover"
                        style={styles.video}
                        paused={this.state.videoPaused}
                      />
                    </TouchableWithoutFeedback>
                    <ProgressBar
                      progress={this.state.progress}
                      color="#5856d6"
                      unfilledColor="#282828"
                      borderColor="transparent"
                      width={screenWidth}
                      borderRadius={0}
                      height={7}
                    />
                  </View>
              }
            </View>
          }




          {this.renderSaveListView(this.state.postDetails)}

          {/* <SavedAndPostRowCompo
                        roundUserImage={true}
                        fromSavedList={true}
                        hideRightButton={true} /> */}

          {/* <View style={{ paddingVertical: 30, alignItems: "center" }}>

                        <Text style={{ color: "#4c4c55", fontSize: 14, fontFamily: fonts.SFProRegular, textAlign: "center", width: "85%" }}>I’m a bedroom DJ from Philly. Check me out on the cloud. I recently made a mixtape called Jumbo Wax.</Text>

                        <TouchableOpacity style={{ backgroundColor: "#ff9c39", paddingVertical: 15, justifyContent: "center", alignItems: "center", borderRadius: 30, marginTop: 30, width: '65%' }}>
                            <Text style={{ color: "white", fontFamily: fonts.SFProDisplay_Bold }}>Soundcloud</Text>
                        </TouchableOpacity>

                    </View> */}

          <View style={{ flex: 1, backgroundColor: '#FFF', marginTop: 12 }}>
            <View style={{ alignItems: 'center', }}>
              {socilcolor != '' ?
                <TouchableOpacity
                  onPress={() => Helper.openLinkingUrl(this.state.postDetails.social_link)}

                  style={{
                    marginTop: 30,
                    borderRadius: 30,
                    backgroundColor: socilcolor, alignItems: 'center'
                  }}>
                  <Text
                    style={{
                      paddingHorizontal: 30, paddingVertical: 10,
                      fontFamily: fonts.Montserrat_Bold,
                      fontSize: 20, marginBottom: 2,
                      color: 'white',
                      textAlign: 'center'
                    }}>
                    {sociltext}
                  </Text>
                </TouchableOpacity>
                :
                null
              }
              <Text
                style={{
                  marginTop: 20,
                  fontFamily: fonts.SFProRegular,
                  fontSize: 14,
                  color: '#000',
                }}>
                {this.state.postDetails.description}
              </Text>


            </View>

          </View>

        </ScrollView>
      </View>
    );
  }

  componentWillUnmount() {
    this.setState({ videoPaused: false })
    if (this.didBlurListener) {
      this.didBlurListener.remove();
    }
  }

  onLike = (params) => {
    this.setState({ videoPaused: true }, () => {
      pushToScreen('LikesList', { post_id: this.state.postDetails.id })
    })
  }

  onViews = (params) => {
    this.setState({ videoPaused: true }, () => {
      pushToScreen('ViewsList', { post_id: this.state.postDetails.id })
    })
  }



  renderSaveListView(postDetails) {
    if (postDetails == "") {
      return (<View />)
    }
    return (
      <View style={styles.rowContainer}>
        <TouchableOpacity style={styles.userImg}>


          {/* <ImageLoadView
            resizeMode={"cover"}
            style={styles.userImg}
            source={postDetails.post_detail[0].thumbnail_media ? {
              uri: Config.imageUrl + postDetails.post_detail[0].thumbnail_media,
              priority: FastImage.priority.normal,
            } : Images.User_profile_icon}
            resizeMode={FastImage.resizeMode.stretch}
          /> */}

          <ImageLoadView
            resizeMode={"cover"}
            style={styles.userImg}
            source={postDetails.user.profile_image ? { uri: Config.imageUrl + postDetails.user.profile_image, } : Images.User_profile_icon}
          />
        </TouchableOpacity>

        <View style={{ width: "70%", marginLeft: 5, padding: 3 }}>
          {postDetails.user && <Text numberOfLines={1} style={styles.userName}>{postDetails.user.username}</Text>}
          <View style={styles.userContainer}>
            <Text style={styles.grp}>{postDetails.title}</Text>

            <View style={styles.statusView}>
              <TouchableOpacity onPress={() => this.onViews()} style={styles.statusRow}>
                <Text style={styles.views}>{Helper.convertValueBillions(postDetails.total_views)}</Text>
                <Image
                  style={styles.ViewIng}
                  source={Images.eye_grey}
                />
              </TouchableOpacity>

              <TouchableOpacity onPress={() => this.onLike()} style={styles.statusRow}>
                <Text style={styles.views}>{Helper.convertValueBillions(postDetails.total_likes)}</Text>
                <Image
                  style={styles.ViewIng}
                  source={Images.heart_grey1}
                />
              </TouchableOpacity>
            </View>
          </View>
        </View>


      </View>
    )
  }
}
const styles = StyleSheet.create({
  flatlist: {
    backgroundColor: '#f5f6fa',
    flex: 1,
    marginTop: 25
  },

  listemptyContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },

  emptyText: {
    color: "#a2a2b2",
    fontSize: 17,
    fontFamily: fonts.SFProMedium
  },

  emptyImage: {
    height: 120,
    width: 120,
    resizeMode: "contain",
    marginTop: 55,
    marginBottom: 30,
  },
  rowContainer: {
    backgroundColor: "white",
    paddingHorizontal: 10,
    paddingVertical: 15,
    flexDirection: "row",
    alignItems: 'center'
  },

  userImg: {
    height: 50,
    width: 50,
    borderRadius: 50 / 2

  },

  roundUserImg: {
    height: 40,
    width: 40,
    borderRadius: 20,
    resizeMode: "cover"
  },

  userName: {
    color: "#4c4c55",
    fontSize: 17,
    fontFamily: fonts.SFProDisplay_Semibold
  },

  grp: {
    color: "#919191",
    fontSize: 12,
    fontFamily: fonts.SFProRegular,
    width: "45%"
  },

  statusView: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    width: "50%",
    paddingRight: 10
  },

  statusRow: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center"
  },

  views: {
    color: "#919191",
    fontSize: 12,
    fontFamily: fonts.SFProRegular
  },

  ViewIng: {
    height: 10,
    width: 15,
    marginLeft: 5,
    resizeMode: "contain"
  },

  arrowContainer: {
    alignItems: "flex-end",
    justifyContent: "center",
    flex: 1,
    paddingRight: 5
  },

  arrowImg: {
    height: 15,
    width: 8,
    resizeMode: "contain"
  },

  userContainer: {
    flexDirection: "row",
    justifyContent: "space-between"
  },
  index: { marginRight: 10, textAlignVertical: "center", color: "#9d9da5", fontSize: 16, fontFamily: fonts.SFProDisplay_Semibold },
  hiddenContainer: { flexDirection: "row", alignItems: 'center', justifyContent: "space-between", flex: 1, },
  closeView: { width: 45, height: "100%", justifyContent: "center", alignItems: "center", backgroundColor: '#ff5555', },

  video: {
    width: screenWidth,
    aspectRatio: 1,

  },
  squareView: {
    height: screenWidth,
    width: screenWidth,
    justifyContent: 'center',
    alignSelf: 'center',
  },


});