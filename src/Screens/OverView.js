import React, { Component } from 'react'
import { Text, View, TouchableOpacity, ScrollView, Animated, Image, FlatList, StatusBar, SectionList, ImageBackground, RefreshControl, DeviceEventEmitter } from 'react-native'
import { Actions } from 'react-native-router-flux'
import { Images, GHeader, Loader, Colors } from '../common';
import fonts from '../common/fonts';
import { OverviewRowCompo } from '../components/overview/row/OverviewRowCompo';
import Helper from '../lib/Helper';
import Toast from 'react-native-root-toast';
import Spinner from 'react-native-loading-spinner-overlay';
import OverviewPackageInfo from '../components/overview/row/OverviewPackageInfo';
import * as Animatable from 'react-native-animatable';
import pushToScreen from '../navigation/NavigationActions';
import images from '../assets/Themes/Images';

export class OverView extends Component {

    constructor(props) {
        super(props);
        this.state = {
            overViewList: [],
            selecetdIndex: -1,
            showSpinner: true,
            fadeAnimation: new Animated.Value(0),
            showLoaderRefresh: false,


        }
    }
    componentDidMount() {
        this.functionevevnt = this.props.navigation.addListener('didFocus', () => {
            StatusBar.setBackgroundColor('white');
            StatusBar.setBarStyle('dark-content')
            this.getOverViewData(true)
            this.getSettingApi(true)
        });
        this.getOverViewData(true)
        this.getSettingApi(true)
        this.fadeIn()


    }
    fadeIn = () => {
        Animated.timing(this.state.fadeAnimation, {
            toValue: 1,
            duration: 1000
        }).start(() => {
            setTimeout(() => {
                this.fadeOut()
            }, 1000)
        })
    };

    fadeOut = () => {
        Animated.timing(this.state.fadeAnimation, {
            toValue: 0,
            duration: 1000
        }).start(() => {
            setTimeout(() => {
                this.fadeIn()
            }, 1000)
        });
    };
    componentWillUnmount() {
        this.functionevevnt.remove();
    }

    getOverViewData = (loader) => {
        console.log(loader, "loaderloaderloaderloader");
        this.setState({ showSpinner: loader })
        Helper.makeRequest({ url: "get-post", method: "POST", data: '' }).then((resp) => {
            if (resp.status == 'true') {
                var sectionsArray = [];
                resp.data.map((item, index) => {
                    sectionsArray.push({
                        title: index,
                        data: [item],
                    });
                })
                this.setState({ overViewList: sectionsArray, showSpinner: false, showLoaderRefresh: false })

            } else {
                Toast.show(resp.message)
                this.setState({ showSpinner: false, showLoaderRefresh: false })

            }
        })
    }

    onShowPackageInfo = (index) => {
        if (index == this.state.selecetdIndex) {
            this.setState({ selecetdIndex: -1 })
        } else {
            this.setState({ selecetdIndex: index })
        }
    }

    getSettingApi = (loader) => {
        console.log(loader, "loaderloaderloaderloader");
        this.setState({ showSpinner: loader });
        Helper.makeRequest({ url: 'settings', method: 'POST', data: '' }).then(
            resp => {
                if (resp.status == 'true') {
                    console.log('settings------------------------------', resp);
                    Helper.userSettings = resp.data;
                    this.setState({ showSpinner: false, showLoaderRefresh: false });
                } else {
                    Helper.userSettings = {};
                    this.setState({ showSpinner: false, showLoaderRefresh: false });
                }
            },
        );
    };
    _pullToRefresh = () => {
        this.setState({ showLoaderRefresh: true })
        this.getOverViewData(false)
        this.getSettingApi(false)
        this.fadeIn()
    }
    gotoUplodePost = () => {
        pushToScreen('UploadPost', { isComeFrom: 'overView' })
    }
    gotoRefunt = (post_id) => {
        // alert(post_id)
        this.setState({ showSpinner: true })
        Helper.makeRequest({ url: "refund-post-coin", method: "POST", data: { post_id: post_id } }).then((resp) => {
            this.setState({ showSpinner: false })
            if (resp.status == 'true') {
                this.getOverViewData(true)
                this.getSettingApi(false)
                DeviceEventEmitter.emit('refundpostcoin', status = true)
            } else {
                Toast.show(resp.message)
            }
        })
    }
    render() {
        return (
            <View style={{ flex: 1, backgroundColor: "#edeff5" }}>
                <GHeader
                    // showAddPostBtn
                    color="#606076"
                    backgroundColor="white"
                    title1="Overview"
                // onPostHeaderPress={() => pushToScreen('UploadPost', { isComeFrom: 'overView' })}
                />
                <View style={{ flexDirection: "row", paddingHorizontal: 15, paddingVertical: 12, alignItems: 'center', backgroundColor: '#fff', shadowOpacity: 1, shadowRadius: 1, shadowColor: "#edeff5", }}>
                    <View style={{ width: "50%" }}>
                        <Text style={{ color: "#7987a2", fontSize: 15, fontFamily: fonts.Montserrat_Bold }}>Score</Text>
                    </View>
                    <View style={{ width: "50%", flexDirection: "row", justifyContent: "flex-end", alignItems: "center" }}>
                        <View style={{ flexDirection: "row", alignItems: "center", marginRight: 15 }}>
                            <Image style={{ height: 17, width: 17, resizeMode: "contain", marginRight: 5 }} source={Images.eye} />
                            <Text style={{ color: "#7987a2", fontSize: 15, fontFamily: fonts.Montserrat_Bold }}>{Helper.userSettings.total_views_count}</Text>
                        </View>
                        <View style={{ flexDirection: "row", alignItems: "center" }}>
                            <Image style={{ height: 14, width: 14, resizeMode: "contain", marginRight: 5 }} source={Images.heart_grey1} />
                            <Text style={{ color: "#7987a2", fontSize: 15, fontFamily: fonts.Montserrat_Bold }}>{Helper.userSettings.total_likes_count}</Text>
                        </View>
                    </View>
                </View>

                {/* <View style={{ height: 20, backgroundColor: "#edeff5" }} /> */}
                <ScrollView>
                <TouchableOpacity onPress={() => this.gotoUplodePost()}
                    style={{ backgroundColor: Colors.bluelight, flexDirection: 'row', alignItems: 'center', justifyContent: 'center', height: 55, marginHorizontal: 25, borderRadius: 8, marginTop: 20, marginBottom: 15, }}>
                    <View style={{ flex: 1, backgroundColor: Colors.bluedark, flexDirection: 'row', alignItems: 'center', justifyContent: 'center', height: 54, borderRadius: 8, marginBottom: 10 }}>
                        <Image style={{ width: 20, height: 20, margin: 6 }} resizeMode={'contain'} source={Images.addpost}></Image>
                        <Text style={{ color: Colors.white, fontSize: 13, fontFamily: fonts.Montserrat_SemiBold }}>Upload a Post</Text>
                    </View>
                </TouchableOpacity>


                <FlatList
                    contentContainerStyle={{ flexGrow: 1, backgroundColor: "#edeff5" }}
                    showsVerticalScrollIndicator={false}
                    data={this.state.overViewList}
                    keyExtractor={(item, index) => item + index}
                    refreshControl={
                        <RefreshControl
                            refreshing={this.state.showLoaderRefresh}
                            onRefresh={this._pullToRefresh}
                        />
                    }
                    renderItem={({ item, index }) => {
                        return (
                            <>

                                <OverviewRowCompo
                                    index={item.title}
                                    // goToDetail={(post_id, total_likes, total_views) => Actions.OverviewUserInfo({ post_id, total_likes, total_views })}
                                    goToDetail={(post_id, total_likes, total_views, display_name) => pushToScreen('OverviewUserInfo', { post_id, total_likes, total_views, display_name })}
                                    gotoRefunt={(post_id) => { this.gotoRefunt(post_id) }}
                                    selecetdIndex={this.state.selecetdIndex}
                                    onShowPackageInfo={(index) => this.onShowPackageInfo(index)}
                                    data={item} />
                                <Animatable.View
                                    duration={500}
                                    easing="ease-out"
                                    animation={'zoomIn'}>
                                    <OverviewPackageInfo animationview={this.state.fadeAnimation} item={item.data[0]} index={item.title} />
                                </Animatable.View>
                            </>
                        )
                    }}
                    // renderItem={({ item, index, section }) => (
                    //     // this.state.selecetdIndex == section.title ?
                    //     <Animatable.View
                    //         duration={500}
                    //         easing="ease-out"
                    //         animation={'zoomIn'}>
                    //         <OverviewPackageInfo animationview={this.state.fadeAnimation} item={item} index={section.title} />
                    //     </Animatable.View>
                    //     // : <View />
                    // )}
                    // renderSectionHeader={({ section, index }) => {
                    //     console.log(section, 'section >>>>>>>>>>>>>>>>>>');
                    //     return (
                    //         <OverviewRowCompo
                    //             index={section.title}
                    //             // goToDetail={(post_id, total_likes, total_views) => Actions.OverviewUserInfo({ post_id, total_likes, total_views })}
                    //             goToDetail={(post_id, total_likes, total_views, display_name) => pushToScreen('OverviewUserInfo', { post_id, total_likes, total_views, display_name })}
                    //             gotoRefunt={(post_id) => { this.gotoRefunt(post_id) }}
                    //             selecetdIndex={this.state.selecetdIndex}
                    //             onShowPackageInfo={(index) => this.onShowPackageInfo(index)}
                    //             data={section} />
                    //     )
                    // }}
                    ListEmptyComponent={() => (
                        <View style={{ backgroundColor: "#edeff5", justifyContent: "center", alignItems: "center", flex: 1 }}>
                            <Text style={{ color: "#a5adc7", fontSize: 20, fontFamily: fonts.Montserrat_SemiBold }}>Cash-in coins for clout.</Text>
                            <Image
                                style={{ height: 108, width: 108, resizeMode: "contain", marginTop: 62, marginBottom: 30, }}
                                source={Images.globs_bg}
                            />
                            <Text style={{ color: "#a5adc7", fontSize: 16, fontFamily: fonts.SFProMedium }}>See your views, likes, and fans here.</Text>

                        </View>
                    )}

                />
                </ScrollView>
                <Spinner visible={this.state.showSpinner} />
            </View >
        )
    }

}


