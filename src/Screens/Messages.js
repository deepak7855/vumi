import React, { Component } from 'react';
import { View, Text, FlatList, Image, TouchableOpacity, DeviceEventEmitter, StyleSheet, StatusBar, TextInput } from 'react-native';
import { GHeader, Images, IconInput, Input, colors } from '../common';
import fonts from '../common/fonts';
import { Actions } from 'react-native-router-flux';
import pushToScreen from '../navigation/NavigationActions';
import SocketConnect from '../common/SocketConnect';
import Helper from '../lib/Helper';
import Spinner from 'react-native-loading-spinner-overlay';
import Toast from 'react-native-root-toast';
import moment from 'moment';
import Config from '../lib/Config';
import ImageLoadView from '../components/ImageLoadView';




export default class Messages extends Component {
    constructor(props) {
        super(props);
        this.state = {
            keyword: '', showSpinner: false, MessagesList: Helper.recent_chat_history,

        };

    }
    gotoChatHistory(item) {
        pushToScreen('Chat', { OtherUserId: item.other_user_id == Helper.userInfo.id ? item.user_id : item.other_user_id, OtherUserName: item.name })
    }
    MessageListUser = ({ item, index }) => (
        <TouchableOpacity onPress={() => this.gotoChatHistory(item)}
            style={styles.rowContainer}>

            {item.is_online == 1 ?
                <View style={{ height: 10, width: 10, backgroundColor: '#42f49b', borderRadius: 5, marginRight: -4, zIndex: 1, borderColor: 'white', borderWidth: 1.5 }} />
                :
                <View style={{ height: 10, width: 10, backgroundColor: '#ff2c76', borderRadius: 5, marginRight: -4, zIndex: 1, borderColor: 'white', borderWidth: 1.5 }} />

            }

            <ImageLoadView
                source={item.user_thumb_image ? { uri: Config.imageUrl + item.user_thumb_image } : Images.user_dummy}
                resizeMode={"cover"}
                style={styles.userImg}
            />
            <View style={styles.detail}>
                <Text numberOfLines={1} style={styles.userName}>{item.name}</Text>
                <Text numberOfLines={1} style={styles.time}>{item.msg}</Text>
            </View>
            <View style={{
                flex: 0.6,
                alignItems: 'flex-end', justifyContent: 'flex-end',
            }}>
                {Number(item.unread_message) > 0 ?
                    <View style={{
                        marginBottom: 2,
                        justifyContent: 'center',
                        alignItems: 'center', backgroundColor: 'rgb(255,51,137)',
                        height: 20, width: 20, borderRadius: 20 / 2
                    }}>

                        < Text numberOfLines={1} style={{ color: '#fff', fontSize: 12 }}>{Number(item.unread_message) <= 9 ? item.unread_message : "9+"}</Text>


                    </View>
                    : null
                }
                <View style={styles.btn}>
                    <Text numberOfLines={1} style={styles.unblock}>{moment.utc(item.updated_at).local().fromNow()}</Text>
                    {/* <Text style={styles.unblock}>7 hrs ago</Text> */}
                    <Image
                        source={Images.right_arrow}
                        style={{ height: 16, width: 8, resizeMode: "contain", marginLeft: 5 }}
                    />
                </View>
            </View>

        </TouchableOpacity>


    )
    componentDidMount() {
        this.focusListener = this.props.navigation.addListener("didFocus", () => {
            this.GetUserMessageList(this.state.keyword, true);
            if (SocketConnect.checkSocketConnected() == false) {
                SocketConnect.checkConnection('connect', cb => {
                    console.log('SocketConnect------------', cb);
                });
            }


        });



        this.GetMessageHistory = DeviceEventEmitter.addListener('get_message_history_response', response => this.GetMessageHistoryList(response));

    }
    GetUserMessageList(keyword, loader) {
        let formdata = {
            limit: "3",
            user_id: Helper.userInfo.id,
            start: "-1",
            keyword: keyword

        }
        // alert(JSON.stringify(formdata))

        if (SocketConnect.checkSocketConnected()) {
            //  console.log('timeout------------', page);

            this.setState({ showSpinner: false })
            SocketConnect.socketDataEvent('get_message_history', formdata);
        } else {
            SocketConnect.checkConnection('connect', cb => {
                console.log('SocketConnect------------', cb);
                this.setState({ showSpinner: false })

            });
            this.setState({ showSpinner: false })

        }
    }

    gotoSearchUser(keyword, loader) {
        this.state.keyword = keyword
        this.GetUserMessageList(this.state.keyword, loader);

    }
    GetMessageHistoryList(response) {
        if (response) {
            // console.log(JSON.stringify(response.responseData, "get_message_history_response1"));
            if (response.responseData.length > 0) {
                this.setState({ MessagesList: response.responseData })
                this.setState({ showSpinner: false })

            } else {
                // Toast.show(response.msg)
                this.setState({ showSpinner: false })
                this.setState({ MessagesList: [] })

            }
            this.setState({ showSpinner: false })

        }


    }
    componentWillUnmount() {
        if (this.focusListener) {
            this.focusListener.remove();

        }
        if (this.GetMessageHistory) {
            this.GetMessageHistory.remove();

        }

        // DeviceEventEmitter.removeAllListeners('get_message_history_response');


    }
    GotoSearchList() {
        pushToScreen('MessageSearchList')
        // alert("yes")
    }

    gotoBackMessage = () => {
        Actions.pop();
        DeviceEventEmitter.emit("updatePost", "resp")
    }
    render() {
        return (
            <View style={{ flex: 1, backgroundColor: 'white', }}>
                <Spinner visible={this.state.showSpinner} />

                <StatusBar backgroundColor="white" />
                <GHeader
                    ProfileBack
                    greyBackButton
                    color="#606076"
                    backgroundColor="white"
                    title1="Messages"
                    gotoBackMessage={() => this.gotoBackMessage()}

                />

                <View style={{ backgroundColor: '#efeff5', padding: 10, paddingHorizontal: 15, flexDirection: "row", alignItems: "center", borderBottomColor: colors.blackgrey, borderBottomWidth: 0.5, borderTopWidth: 0.5 }}>
                    <Image
                        style={{ height: 20, width: 20, borderRadius: 10, resizeMode: "contain" }}
                        source={Images.coins1.search}
                    />
                    <TextInput
                        placeholder="To who?"
                        placeholderTextColor="#aeb5d3"
                        value={this.state.keyword}
                        onChangeText={(keyword) => this.gotoSearchUser(keyword, false)}
                        style={{ paddingVertical: 0, paddingHorizontal: 10, width: "85%", color: "#39393b", fontFamily: fonts.Poppins_Medium, fontSize: 15 }}
                    />
                    {
                        this.state.keyword.length > 0 &&
                        <TouchableOpacity onPress={() => this.gotoSearchUser('', false)}>
                            <Image
                                style={{ height: 32, width: 32, borderRadius: 16, resizeMode: "contain" }}
                                source={Images.grey_close}
                            />
                        </TouchableOpacity>
                    }

                </View>

                <View style={{ flex: 1, backgroundColor: '#efeff5' }}>
                    <FlatList
                        contentContainerStyle={{ flexGrow: 1 }}
                        data={this.state.MessagesList}
                        ItemSeparatorComponent={() => <View style={{ height: 10 }} />}
                        renderItem={this.MessageListUser}
                        keyExtractor={(item, index) => index.toString()}
                        extraData={this.state}
                        ListEmptyComponent={() => (
                            <View style={styles.listemptyContainer}>
                                <Text style={styles.emptyText}>{!this.state.showSpinner ? "You have no messages" : null}.</Text>
                                {!this.state.showSpinner ?
                                    <Image
                                        style={styles.emptyImage}
                                        source={Images.emails}
                                    />
                                    : null
                                }
                                {!this.state.showSpinner ?
                                    <Text style={[styles.emptyText, { textAlign: "center" }]}>Stay in touch with your fans {'\n'} and other creators!</Text>
                                    : null
                                }
                            </View>

                        )}
                    />
                </View>
                <TouchableOpacity
                    onPress={() => this.GotoSearchList()}
                    style={{ position: "absolute", bottom: 0, right: 0 }}>
                    <Image
                        style={{ height: 100, width: 100, resizeMode: "contain" }}
                        source={Images.green_plus}
                    />
                </TouchableOpacity>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    rowContainer: {
        flex: 1,
        flexDirection: "row",
        alignItems: "center",
        paddingVertical: 15, paddingHorizontal: 11,
        backgroundColor: "white",
        width: '100%'
    },

    userImg: {
        height: 48,
        width: 48,
        borderRadius: 48/2,
        resizeMode: "cover",
    },

    detail: {
        flex: 1,
        marginLeft: 15
    },

    time: {
        color: "#9297a7",
        fontSize: 13,
        fontFamily: fonts.SFProRegular,
    },

    userName: {
        color: "#3c3c40",
        fontSize: 16,
        fontFamily: fonts.SFProDisplay_Heavy,
    },

    btn: {
        flexDirection: "row",
        alignItems: "center",
    },

    unblock: {
        color: "#8d93ac",
        fontSize: 13,
        fontFamily: fonts.SFProDisplay_Semibold,
    },
    listemptyContainer: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center"
    },

    emptyText: {
        color: "#a2a2b2",
        fontSize: 17,
        fontFamily: fonts.SFProMedium
    },

    emptyImage: {
        height: 120,
        width: 120,
        resizeMode: "contain",
        marginTop: 55,
        marginBottom: 30,
    },

});