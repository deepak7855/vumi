export * from './Login';
export * from './Profile';
export * from './SignUp';
export * from './Password';
export * from './SearchScreen';
export * from './SearchResults';
export * from './Account';
export * from './Setting';
export * from './ChangePassword';
export * from './SignUpStep3';
export * from './Notification';
export * from './Referral';
export * from './Coins1';
export * from './OverView';
export * from './NotificationList';
export * from './ForgotPassword';
export * from './Verification';
export * from './GetStarted';
export * from './Watch';
export * from './Game';
export * from './AddCards';
export * from './UploadPost';
























