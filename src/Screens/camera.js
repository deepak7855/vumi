import React, { Component } from 'react';
import { View, Text, Dimensions, Image, TouchableOpacity, StatusBar, Platform, AppState } from 'react-native';
import OpenCamra from '../common/OpenCamra';
import { Actions } from 'react-native-router-flux';
import { GHeader, Images } from '../common';
import pushToScreen from '../navigation/NavigationActions';
import { check, PERMISSIONS, RESULTS, request, openSettings } from 'react-native-permissions';
import fonts from '../common/fonts';
import Helper from '../lib/Helper';
import Toast from 'react-native-root-toast';

const screen_height = Dimensions.get('window').height;

export default class camera extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selectedCubes: 0,
            cameraImageArray: [],
            selectedImage: '',
            cameraVideoArray: [],
            hasCamraPermission: null,
            showSettingButton: false,
            flag: false
        };
        this.child = React.createRef();
    }

    capturePicture = (uri) => {
        if (this.state.selectedCubes < 4) {
            this.setState({ cameraImageArray: [...this.state.cameraImageArray, uri], selectedCubes: this.state.selectedCubes + 1 }, () => {
                console.log('arrayLendth---- ', this.state.cameraImageArray.length)
            });
        }
    }


    captureVideo = (uri) => {
        // alert(uri)
        console.log(uri);
        this.setState({ cameraVideoArray: [...this.state.cameraVideoArray, uri] }, () => {
            console.log('arrayLendth---- ', this.state.cameraVideoArray.length)
            pushToScreen('CreatePost', { media: this.props.cameraType == 'Picture' ? this.state.cameraImageArray : this.state.cameraVideoArray, mediaType: this.props.cameraType, isComeFrom: this.props.isComeFrom });

        });

    }

    onImagePreviewRetakePress = (index) => {
        let array = this.state.cameraImageArray;
        array.splice(index, 1);
        this.setState({ cameraImageArray: array, selectedCubes: array.length, cameraVideoArray: [] })
    }
    onVideosPreviewRetakePress = () => {
        this.setState({ cameraVideoArray: [] })
    }

    onImagePreviewNextPress = (index, uri) => {
        let array = this.state.cameraImageArray;
        array[index] = uri;
        this.setState({ cameraImageArray: array, selectedCubes: array.length })
        this.child.current.onCubePress(array.length);
    }

    onDonePress = () => {
        // Actions.CashIn({ media: this.props.cameraType == 'Picture' ? this.state.cameraImageArray : this.state.cameraVideoArray, mediaType: this.props.cameraType })
        // pushToScreen('CashIn', { media: this.props.cameraType == 'Picture' ? this.state.cameraImageArray : this.state.cameraVideoArray, mediaType: this.props.cameraType })
        console.log('cameraVideoArray', this.state.cameraVideoArray)
        pushToScreen('CreatePost', { media: this.props.cameraType == 'Picture' ? this.state.cameraImageArray : this.state.cameraVideoArray, mediaType: this.props.cameraType, isComeFrom: this.props.isComeFrom });

        return

        let mediaType = this.props.cameraType
        let media = this.props.cameraType == 'Picture' ? this.state.cameraImageArray : this.state.cameraVideoArray
        if (media.length > 0) {
            const data = new FormData()
            // if (this.state.selected_pack) {
            //     data.append('points', String(this.state.selected_pack))
            // } else {
            //     Toast.show('Please select View Pack')
            //     return false
            // }
            if (mediaType == 'Picture') {
                media.forEach((element, i) => {
                    data.append(`media${i + 1}`, {
                        uri: element,
                        name: (new Date().getMilliseconds() + 1) + '.jpg',
                        type: 'image/jpeg'
                    });
                });
                data.append('image_count', media.length)
                data.append('video_count', 0)
            } else {
                media.forEach((element, i) => {
                    data.append(`video${i + 1}`, {
                        uri: element,
                        name: (new Date().getMilliseconds() + 1) + '.mp4',
                        type: 'video/mp4'
                    });
                });
                data.append('image_count', 0)
                data.append('video_count', media.length)
            }

            this.setState({ showSpinner: true })
            Helper.makeRequest({ url: "add-post", method: "POST", data: data, isImage: true }).then((resp) => {
                if (resp.status == 'true') {
                    this.setState({ showSpinner: false })
                    // Actions.OverView();
                    pushToScreen('OverView');
                    Toast.show(resp.message);
                } else {
                    Toast.show(resp.message)
                }
                this.setState({ showSpinner: false })
            })
        }


    }



    render() {

        return (
            <View style={{ flex: 1, height: screen_height, backgroundColor: "#232323" }}>
                <GHeader
                    bArrow
                    greyBackButton
                    color="white"
                    backgroundColor="#232323"
                    title1={this.props.cameraType == 'Picture' ? 'Upload up to 4 Photos' : 'Upload Video'}
                    showPhotoBtn={this.props.cameraType == 'Picture' ? true : false}
                    title1FontSize={14}
                    photoBtnText={this.props.cameraType == 'Picture' ? "Done" : ''}
                    photoBtnDisabled={this.state.selectedCubes > 0 || this.state.cameraVideoArray.length > 0 ? false : true}
                    onPhotoButtonPress={() => this.props.cameraType == 'Picture' ? this.onDonePress() : {}}
                />

                {
                    this.state.hasCamraPermission !== null &&

                    <View style={{ flex: 1 }}>
                        {
                            this.state.hasCamraPermission ?

                                <OpenCamra
                                    capturePicture={(imageData) => this.capturePicture(imageData)}
                                    captureVideo={(videoData) => this.captureVideo(videoData)}
                                    cameraType={this.props.cameraType}
                                    selectedCubes={this.state.selectedCubes}
                                    cameraImageArray={this.state.cameraImageArray}
                                    onImagePreviewRetakePress={this.onImagePreviewRetakePress}
                                    onImagePreviewNextPress={this.onImagePreviewNextPress}
                                    ref={this.child}
                                    onVideoPreviewRetakePress={this.onVideosPreviewRetakePress}
                                />
                                :
                                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', backgroundColor: '#232323' }}>

                                    <Image style={{ height: 70, width: 70, resizeMode: 'contain', marginBottom: 50 }} source={Images.warning} />

                                    <Text style={{ color: 'white' }}>You need permissions to</Text>
                                    <Text style={{ color: 'white' }}>access Camera.</Text>

                                    {
                                        this.state.showSettingButton ?
                                            <TouchableOpacity onPress={() => this.openSetting()} style={{ paddingHorizontal: 15, paddingVertical: 10, borderRadius: 5, backgroundColor: 'white', marginTop: 50 }}>
                                                <Text style={{ fontSize: 18, fontFamily: fonts.SFProDisplay_Bold }}>Open Settings</Text>
                                            </TouchableOpacity>
                                            :
                                            <TouchableOpacity onPress={() => this.requestCameraPermission()} style={{ paddingHorizontal: 15, paddingVertical: 10, borderRadius: 5, backgroundColor: 'white', marginTop: 50 }}>
                                                <Text style={{ fontSize: 18, fontFamily: fonts.SFProDisplay_Bold }}>Request</Text>
                                            </TouchableOpacity>
                                    }

                                </View>
                        }
                    </View>
                }

            </View>
        );
    }

    openSetting = () => {
        this.setState({ flag: true }, () => {
            openSettings().catch(() => console.warn('cannot open settings'));
        })
    }

    requestCameraPermission = () => {
        request(Platform.OS == 'android' ? PERMISSIONS.ANDROID.CAMERA : PERMISSIONS.IOS.CAMERA).then((result) => {
            if (result == 'granted') {
                this.requestAudioPermission()
            } else if (result == 'blocked') {
                this.setState({ showSettingButton: true, hasCamraPermission: false })
            } else {
                this.setState({ hasCamraPermission: false })
            }
        });
    }

    requestAudioPermission = () => {
        request(Platform.OS == 'android' ? PERMISSIONS.ANDROID.RECORD_AUDIO : PERMISSIONS.IOS.MICROPHONE).then((result) => {
            if (result == 'granted') {
                this.setState({ hasCamraPermission: true })
            } else if (result == 'blocked') {
                this.setState({ showSettingButton: true, hasCamraPermission: false })
            } else {
                this.setState({ hasCamraPermission: false })
            }
        });
    }


    componentDidMount() {
        this.functionevevnt = this.props.navigation.addListener('didFocus', () => {
            // StatusBar.setBackgroundColor('#232323');
            // StatusBar.setBarStyle('light-content')
            StatusBar.setHidden(true)
        });

        setTimeout(() => {
            this.checkCameraPermission();
        }, 1000)

        AppState.addEventListener('change', this._handleAppStateChange);

    }

    _handleAppStateChange = (nextAppState) => {
        if (this.state.flag && nextAppState === 'active') {
            // Do something here on app active foreground mode.
            this.checkCameraPermission();
            console.log("App is in Active Foreground Mode.")
        }
    };


    checkCameraPermission = () => {
        this.setState({ flag: false })
        check(Platform.OS == 'android' ? PERMISSIONS.ANDROID.CAMERA : PERMISSIONS.IOS.CAMERA)
            .then((result) => {
                switch (result) {
                    case RESULTS.UNAVAILABLE:
                        console.log('This feature is not available (on this device / in this context)');
                        break;
                    case RESULTS.DENIED:
                        this.requestCameraPermission()
                        break;
                    case RESULTS.GRANTED:
                        this.checkAudioPermission()
                        console.log('The permission is granted');
                        break;
                    case RESULTS.BLOCKED:
                        this.setState({ showSettingButton: true, hasCamraPermission: false })
                        console.log('video -> The permission is denied and not requestable anymore');
                        break;
                }
            })


    }


    checkAudioPermission = () => {
        check(Platform.OS == 'android' ? PERMISSIONS.ANDROID.RECORD_AUDIO : PERMISSIONS.IOS.MICROPHONE)
            .then((result) => {
                switch (result) {
                    case RESULTS.UNAVAILABLE:
                        console.log('This feature is not available (on this device / in this context)');
                        break;
                    case RESULTS.DENIED:
                        this.requestAudioPermission();
                        break;
                    case RESULTS.GRANTED:
                        this.setState({ hasCamraPermission: true })
                        console.log('The permission is granted');
                        break;
                    case RESULTS.BLOCKED:
                        this.setState({ showSettingButton: true, hasCamraPermission: false })
                        console.log('Audio -> The permission is denied and not requestable anymore');
                        break;
                }
            })
    }

    componentWillUnmount() {
        this.functionevevnt.remove();
        AppState.removeEventListener('change', this._handleAppStateChange);
    }
}
