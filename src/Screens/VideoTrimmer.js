import React, { Component } from 'react';
import { View, Text, Dimensions, TouchableOpacity } from 'react-native';
import { ProcessingManager, Trimmer } from 'react-native-video-processing';
import Video from 'react-native-video';

const screenWidth = Dimensions.get('window').width;

export default class VideoTrimmer extends Component {
    constructor(props) {
        super(props);
        this.state = {
            videoUri: ''
        };
    }

    getTrimedVideo = (e) => {
        var options = {
            startTime: e.startTime,
            endTime: e.endTime
        }
        console.log(options)
        const timer = setTimeout(() => {
            ProcessingManager.trim(this.props.uri[0].uri, options).then((data) => {

                this.setState({ videoUri: data }, () => {
                    console.log("new video url--- ", data)
                })
            }, 5000);
            return () => clearTimeout(timer);

        });
    }

    render() {
        console.log("render url-----   ", this.state.videoUri)
        return (
            <View style={{ flex: 1 }}>
                <Trimmer
                    source={this.props.uri[0].uri}
                    height={100}
                    width={300}
                    themeColor={'white'} // iOS only
                    thumbWidth={30} // iOS only
                    trackerColor={'green'} // iOS only
                    onChange={(e) => this.getTrimedVideo(e)}
                />

                <Video
                    source={{ uri: this.state.videoUri }}   // Can be a URL or a localfile.
                    ref={(ref) => {
                        this.player = ref
                    }}
                    muted={true}
                    paused={false}
                    style={{ height: screenWidth, width: screenWidth, marginTop: 10, }} />


            </View>
        );
    }
    componentDidMount = () => {
        
        this.setState({ videoUri: this.props.uri[0].uri })
    };

}
