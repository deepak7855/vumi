import React, { Component } from 'react'
import { Text, View, Image, FlatList, DeviceEventEmitter, ActivityIndicator, RefreshControl, TouchableOpacity } from 'react-native'
import {
    Images, Fontfamily, GHeader,
} from '../common';

import { Actions } from 'react-native-router-flux';

import Helper from '../lib/Helper';
import Toast from 'react-native-root-toast';
import Spinner from 'react-native-loading-spinner-overlay';
import Config from '../lib/Config';

import moment from 'moment';
import { BlockModelCompo } from '../components/Watch/BlockModelCompo';
import SocketConnect from '../common/SocketConnect';
import { InfoModelCompo } from '../components/Watch/InfoModelCompo';
import pushToScreen from '../navigation/NavigationActions';
import ImageLoadView from '../components/ImageLoadView';




// const Item = ({ title, title2, time, img, color, index }) => {
//     return (

//         <View style={{
//             flex: 1,
//             marginVertical: 5,
//             paddingVertical: 15,
//             marginHorizontal: 10
//         }}>

//             <View style={{ flex: 1, flexDirection: 'row', }}>

//                 <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center' }}>
//                     <View style={{ height: 8, width: 8, backgroundColor: 'red', borderRadius: 4, marginRight: 3, zIndex: 1 }} />
//                     <View style={{ flexDirection: "row", alignItems: "flex-end" }}>
//                         <Image
//                             resizeMode='contain'
//                             source={Images.user_dummy}
//                             style={{ height: 50, width: 50, marginRight: 10 }}
//                         />

//                         <Image
//                             resizeMode='contain'
//                             source={index % 2 == 0 ? Images.yellow_star : Images.heart}
//                             style={{ height: 10, width: 10, marginLeft: -25, zIndex: 1 }}
//                         />
//                     </View>
//                 </View>

//                 <View style={{ flex: 6, marginHorizontal: 20 }}>



//                     <View style={{
//                         justifyContent: 'space-between',
//                         flex: 1,
//                         flexDirection: 'row',
//                         alignItems: 'center',

//                     }}>

//                         <View style={{ flex: 1, flexDirection: 'row' }}>
//                             <View style={{ flex: 5, justifyContent: 'center', marginLeft: 4 }}>

//                                 <View style={{ flexDirection: 'row' }}>
//                                     <Text style={{ fontFamily: Fontfamily.MSB, fontSize: 14 }}>{title}</Text>
//                                     <Text style={{ fontSize: 14, marginHorizontal: 3 }}>{title2}</Text>
//                                 </View>
//                                 <Text style={{ fontSize: 14, color: '#ddd' }}>{time}</Text>
//                             </View>

//                             <View style={{ flex: 1, }}>
//                                 <Image
//                                     resizeMode='contain'
//                                     source={Images.profile.tree}
//                                     style={{ height: 50, width: 50, }}
//                                 />
//                             </View>
//                         </View>
//                     </View>
//                 </View>
//             </View>
//         </View>
//     );
// }

export class NotificationList extends Component {

    constructor(props) {
        super(props);
        this.state = {
            showSpinner: false,
            arrNotificationData: [],
            first_post_id: '',
            showLoaderPageing: false,
            showLoaderRefresh: false,
            selectedRowDetail: {},
            showBlockModel: false,
            showInfoModel: false,

        };
    }

    componentDidMount() {
        this.getNotificationList(true, "")

    }

    onUserImagePress = (item) => {

        let profile_image = item.type == "POST_LIKE" ? item.data.sender_info.profile_image : item.type == "COIN_TRANSFER" ? item.data.profile_image : item.type == "POST_COMMENT" ? item.data.sender_info.profile_image : item.type == "TEAM_JOIN" ? item.data.profile_image : '';
        let username = item.type == "POST_LIKE" ? item.data.sender_info.username : item.type == "COIN_TRANSFER" ? item.data.username : item.type == "POST_COMMENT" ? item.data.sender_info.username : item.type == "TEAM_JOIN" ? item.data.username : '';
        let id = item.type == "POST_LIKE" ? item.data.sender_info.id : item.type == "COIN_TRANSFER" ? item.data.id : item.type == "POST_COMMENT" ? item.data.sender_info.id : item.type == "TEAM_JOIN" ? item.data.id : '';
        let is_blocked = item.type == "POST_LIKE" ? item.data.post_info.user.is_blocked : item.type == "COIN_TRANSFER" ? item.is_blocked : item.type == "POST_COMMENT" ? item.data.post_info.user.is_blocked : item.type == "TEAM_JOIN" ? item.is_blocked : '';
        let location = item.type == "POST_LIKE" ? item.data.sender_info.country_name : item.type == "COIN_TRANSFER" ? item.data.country_name : item.type == "POST_COMMENT" ? item.data.sender_info.country_name : item.type == "TEAM_JOIN" ? item.data.country_name : ''

        let data = {
            user: {
                id,
                username,
                is_blocked,
                profile_image,
                location
            }
        }



        this.setState({ showInfoModel: true, selectedRowDetail: data })
    }

    onBlock = (item) => {
        this.setState({ showBlockModel: false })
        let formdata = {
            user_id: Helper.userInfo.id,
            other_user_id: item.user.id,
        }
        if (SocketConnect.checkSocketConnected()) {
            SocketConnect.socketDataEvent('user_block_unblock', formdata);



        } else {
            SocketConnect.checkConnection('connect', cb => {
                console.log('SocketConnect------------', cb);
            });
        }
    }

    goToChat = (item) => {
        this.setState({ showInfoModel: false }, () => {
            pushToScreen('Chat', { OtherUserId: item.user.id, OtherUserName: item.user.username });
        })
    }

    getNotificationList = (spinner, last_id) => {
        this.setState({ showSpinner: spinner })
        Helper.makeRequest({ url: "notifications", method: "POST", data: { last_id: last_id } }).then((resp) => {
            if (last_id == "") {
                this.state.arrNotificationData = []
            }
            if (resp.status == 'true') {
                if (resp.data && resp.data.data && resp.data.data.length > 0) {
                    this.state.arrNotificationData = [...this.state.arrNotificationData, ...resp.data.data]
                }
                this.state.first_post_id = resp.data.first_id ? resp.data.first_id : ""

            } else {
                Toast.show(resp.message)
            }

            this.state.showLoaderPageing = false
            this.state.showLoaderRefresh = false
            this.state.showSpinner = false

            this.setState({})
        })
    }

    onScroll = () => {
        console.log("onScroll-------")
        if (!this.state.showLoaderPageing && this.state.arrNotificationData.length > 0 && this.state.arrNotificationData[this.state.arrNotificationData.length - 1].id != this.state.first_post_id && this.state.first_post_id != '') {
            this.setState({ showLoaderPageing: true })
            this.getNotificationList(false, this.state.arrNotificationData[this.state.arrNotificationData.length - 1].id);
        }
    }
    _pullToRefresh = () => {
        this.setState({ showLoaderRefresh: true })
        this.getNotificationList(false, "");
    }
    render_Activity_footer = () => {
        var footer_View = (
            <View style={{ flex: 1, backgroundColor: '#ffffff', alignItems: 'center', justifyContent: 'center', padding: 15 }}>
                <ActivityIndicator size="large" color="#000000" />
            </View>
        );
        return footer_View;
    };

    gotoBackMessage = () => {
        Actions.pop();
        DeviceEventEmitter.emit("updatePost", "resp")
    }


    NotificationItem = ({ item, index }) => {
        let likeonline = item.type == "POST_LIKE" ? item.data.sender_info.is_online : item.type == "COIN_TRANSFER" ? item.data.is_online : item.type == "POST_COMMENT" ? item.data.sender_info.is_online : item.type == "TEAM_JOIN" ? item.data.is_online : item.type == "POST_SAVE" ? item.data.is_online : item.type == "DAILY_BONUS" ? item.data.is_online : 0
        let userimage = item.type == "POST_LIKE" ? item.data.sender_info.profile_image : item.type == "COIN_TRANSFER" ? item.data.profile_image : item.type == "POST_COMMENT" ? item.data.sender_info.profile_image : item.type == "TEAM_JOIN" ? item.data.profile_image : item.type == "POST_SAVE" ? item.data.profile_image : item.type == "DAILY_BONUS" ? item.data.profile_image : null
        let favicons = item.type == "POST_LIKE" ? Images.heart : item.type == "COIN_TRANSFER" ? Images.coin : item.type == "POST_COMMENT" ? Images.chats : item.type == "POST_SAVE" ? Images.y_star : null
        let username = item.type == "POST_LIKE" ? item.data.sender_info.username : item.type == "COIN_TRANSFER" ? item.data.username : item.type == "POST_COMMENT" ? item.data.sender_info.username : item.type == "TEAM_JOIN" ? item.data.username : item.type == "POST_SAVE" ? item.data.username : item.type == "DAILY_BONUS" ? item.data.username : item.data.username
        let messagetxt = item.type == "POST_LIKE" ? item.message : item.type == "COIN_TRANSFER" ? item.message : item.type == "POST_COMMENT" ? item.message : item.type == "TEAM_JOIN" ? item.message : item.type == "POST_SAVE" ? item.message : item.type == "DAILY_BONUS" ? item.message : item.message
        let messagedone = messagetxt.replace(username, "");
        let viewlikeimg = item.type == "POST_LIKE" ? item.data.post_info.post_detail[0].thumbnail_media : item.type == "POST_COMMENT" ? item.data.post_info.post_detail[0].thumbnail_media : ''

        return (
            <View style={{
                flex: 1,
                marginVertical: 5,
                paddingVertical: 15,
                marginHorizontal: 10
            }}>

                <View style={{ flex: 1, flexDirection: 'row', }}>

                    <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center' }}>
                        {likeonline == 1 ?
                            < View style={{ height: 8, width: 8, backgroundColor: '#42f49b', borderRadius: 5, marginRight: 3, zIndex: 1 }} />
                            :
                            <View style={{ height: 8, width: 8, backgroundColor: '#ff2c76', borderRadius: 5, marginRight: 3, zIndex: 1, }} />

                        }
                        <TouchableOpacity onPress={() => this.onUserImagePress(item)} style={{ flexDirection: "row", alignItems: "flex-end" }}>

                            {/* <Image
                                resizeMode='cover'
                                source={userimage ? { uri: Config.imageUrl + userimage } : Images.user_dummy}
                                style={{
                                    height: 50, width: 50, marginRight: 10, borderRadius: 50 / 2,
                                }}
                            /> */}
                            <ImageLoadView
                                resizeMode='cover'
                                source={userimage ? { uri: Config.imageUrl + userimage } : Images.user_dummy}
                                style={{
                                    height: 50, width: 50, marginRight: 10, borderRadius: 50 / 2,
                                }}
                            />
                            <Image
                                resizeMode='contain'
                                source={favicons}
                                style={{ height: 10, width: 10, marginLeft: -25, zIndex: 1 }}
                            />
                        </TouchableOpacity>
                    </View>

                    <View style={{ flex: 6, marginHorizontal: 20 }}>
                        <View style={{
                            justifyContent: 'space-between',
                            flex: 1,
                            flexDirection: 'row',
                            alignItems: 'center',

                        }}>

                            <View style={{ flex: 1, flexDirection: 'row' }}>
                                <View style={{ flex: 5, justifyContent: 'center', marginRight: 50 }}>

                                    <View style={{ flexDirection: 'row' }}>
                                        <Text style={{ fontFamily: Fontfamily.MSB, fontSize: 14, color: '#606076' }}>{username} </Text>
                                        <Text numberOfLines={1} style={{ fontSize: 14, marginRight: 3, color: '#606076' }}>{messagedone}</Text>
                                    </View>
                                    <Text numberOfLines={1} style={{ fontSize: 14, color: '#ddd' }}>{moment.utc(item.created_at).local().fromNow()}</Text>

                                </View>

                                <View style={{ flex: 1, }}>
                                    <Image
                                        resizeMode='contain'
                                        source={viewlikeimg ? { uri: Config.imageUrl + viewlikeimg } : ''}
                                        style={{ height: 50, width: 50, }}
                                    />
                                </View>
                            </View>
                        </View>
                    </View>
                </View>
            </View>
        )


    }

    render() {
        return (
            <View style={{ flex: 1, backgroundColor: '#f5f6fa', }}>

                <GHeader
                    ProfileBack
                    greyBackButton
                    color="#606076"
                    backgroundColor="white"
                    title1="Notifications"
                    gotoBackMessage={() => this.gotoBackMessage()}

                />
                <Spinner visible={this.state.showSpinner} />


                <View style={{ backgroundColor: 'white', flex: 1 }}>
                    <FlatList
                        contentContainerStyle={{ flexGrow: 1 }}
                        data={this.state.arrNotificationData}
                        ItemSeparatorComponent={() => <View style={{ height: 3, backgroundColor: '#f5f6fa' }} />}
                        // renderItem={({ item, index }) => <Item
                        //     title={item.title1} img={item.img}
                        //     time={item.time}
                        //     title2={item.title2}
                        //     color={item.color}
                        //     index={index}
                        // />}
                        renderItem={this.NotificationItem}
                        keyExtractor={(item, index) => index.toString()}
                        onEndReached={this.onScroll}
                        onEndReachedThreshold={0.5}
                        ListFooterComponent={this.state.showLoaderPageing && this.render_Activity_footer}
                        ListEmptyComponent={() => (
                            <View style={{
                                flex: 1,
                                backgroundColor: 'white',
                                justifyContent: "center",
                                alignItems: "center"
                            }}>
                                {!this.state.showSpinner &&
                                    <Text style={{
                                        color: "#a5adc7",
                                        fontSize: 16,
                                        fontFamily: fonts.SFProMedium
                                    }}>Notifications Not Found!</Text>
                                }
                            </View>
                        )}
                        refreshControl={
                            <RefreshControl
                                refreshing={this.state.showLoaderRefresh}
                                onRefresh={this._pullToRefresh}
                            />
                        }
                    />
                </View>

                {
                    this.state.showBlockModel &&
                    <BlockModelCompo
                        showBlockModel={this.state.showBlockModel}
                        onBlock={(item) => this.onBlock(item)}
                        hideBlockModel={() => this.setState({ showBlockModel: false })}
                        selectedRowDetail={this.state.selectedRowDetail}
                    />
                }
                {
                    this.state.showInfoModel &&
                    <InfoModelCompo
                        showInfoModel={this.state.showInfoModel}
                        hideInfoModel={() => this.setState({ showInfoModel: false })}
                        onShowBlockModel={() => this.onShowBlockModel()}
                        goToChat={(item) => this.goToChat(item)}
                        selectedRowDetail={this.state.selectedRowDetail}
                    />
                }

            </View>
        )
    }
}

