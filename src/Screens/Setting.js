import React, { useState, useEffect } from 'react';
import {
    Text, View, StyleSheet, TouchableOpacity,
    Switch,
    ScrollView,
    Image
} from 'react-native'

import { Colors, Images, GHeader } from '../common'
import fonts from '../common/fonts';
import { Actions } from 'react-native-router-flux';
import { LogoutModelCompo } from '../components/common/LogoutModelCompo';
import { RateModelCompo } from '../components/common/RateModelCompo';
import Toast from 'react-native-root-toast';
import Spinner from 'react-native-loading-spinner-overlay';
import Helper from '../lib/Helper';
import pushToScreen from '../navigation/NavigationActions';

export const Setting = ({ children, style }) => {

    const [switchValue, setSwitchValue] = useState(Helper.userInfo.is_vip != 1 ? false : true);
    const [showLogoutModel, setShowLogoutModel] = useState(false);
    const [showRateModel, setShowRateModel] = useState(false);
    const [showSpinner, setSpinner] = useState(false);
    const [userInfo, setUserInfo] = useState(null);
    const [Loader, setLoader] = useState(false);


    const onLogout = (type) => {
        if (type == false) {
            setShowLogoutModel(false)
        } else if (type == true) {
            setLoader(true)
            var data = {
                device_type: Helper.device_type,
                device_id: Helper.device_token
            }
            Helper.makeRequest({ url: "logout", method: "POST", data: data }).then((resp) => {
                setLoader(false)
                setShowLogoutModel(false)
                if (resp.status == 'true') {
                    Helper.userInfo = {};
                    Helper.saveDataInAsync("userInfo", "");
                    Helper.saveDataInAsync("token", "")
                    Helper.saveDataInAsync("watchListContant", "");

                    setTimeout(() => {
                        Actions.reset('Auth')
                        pushToScreen('GetStarted')
                    }, 100);
                    // pushToScreen('GetStarted')

                } else {
                    Toast.show(resp.message)
                }
            })
        }

    }

    const onRate = (value) => {
        console.log(value)
        setShowRateModel(false)

    }

    const user = async () => {
        const data = await Helper.getDataFromAsync("userInfo");
        setUserInfo(data);
    }


    useEffect(() => {
        user()
    }, [userInfo]);



    return (
        <View style={{ flex: 1, backgroundColor: "#efeff5" }}>
            <GHeader
                bArrow
                greyBackButton
                color="#606076"
                backgroundColor="white"
                title1="Settings"
            />
            <Spinner visible={showSpinner} />

            <ScrollView contentContainerStyle={{ flexGrow: 1 }}>
                <View style={{ padding: 20 }}>
                    <Text style={settingStyle.heading}>MY CHANNEL</Text>
                </View>

                <TouchableOpacity onPress={() => { pushToScreen('ChannelOptions') }} style={settingStyle.row}>
                    <Text style={settingStyle.rowTxt}>Channel Options</Text>
                    <Image
                        source={Images.right_arrow}
                        style={{ height: 16, width: 8, resizeMode: "contain", marginHorizontal: 5 }}
                    />
                </TouchableOpacity>

                <View style={settingStyle.row}>
                    <View>
                        <Text style={settingStyle.rowTxt}>Vumi VIP</Text>
                    </View>
                    <Switch
                        // onValueChange={() => { setSwitchValue(!switchValue) }}
                        value={switchValue}
                        ios_backgroundColor={Colors.red}
                    />
                </View>

                <TouchableOpacity onPress={() => { pushToScreen('WhatIsVumiVip') }} style={settingStyle.row}>
                    <Text style={[settingStyle.rowTxt, { color: "#6cbbff" }]}>Learn more about VIP</Text>
                    <Image
                        source={Images.right_arrow}
                        style={{ height: 16, width: 8, resizeMode: "contain", marginHorizontal: 5 }}
                    />
                </TouchableOpacity>

                <View style={{ padding: 20 }}>
                    <Text style={settingStyle.heading}>COMMUNITY</Text>
                </View>


                <TouchableOpacity onPress={() => { pushToScreen('HowToGetViews') }} style={settingStyle.row}>
                    <Text style={settingStyle.rowTxt}>How to Get Views</Text>
                    <Image
                        source={Images.right_arrow}
                        style={{ height: 16, width: 8, resizeMode: "contain", marginHorizontal: 5 }}
                    />
                </TouchableOpacity>

                <TouchableOpacity onPress={() => { pushToScreen('Kickstarter') }} style={settingStyle.row}>
                    <Text style={settingStyle.rowTxt}>Kickstarter</Text>
                    <Image
                        source={Images.right_arrow}
                        style={{ height: 16, width: 8, resizeMode: "contain", marginHorizontal: 5 }}
                    />
                </TouchableOpacity>

                <TouchableOpacity onPress={() => { pushToScreen('Faq') }} style={settingStyle.row}>
                    <Text style={settingStyle.rowTxt}>FAQs</Text>
                    <Image
                        source={Images.right_arrow}
                        style={{ height: 16, width: 8, resizeMode: "contain", marginHorizontal: 5 }}
                    />
                </TouchableOpacity>

                <TouchableOpacity onPress={() => { pushToScreen('BonusBox') }} style={settingStyle.row}>
                    <Text style={settingStyle.rowTxt}>Bonus Box</Text>
                    <Image
                        source={Images.right_arrow}
                        style={{ height: 16, width: 8, resizeMode: "contain", marginHorizontal: 5 }}
                    />
                </TouchableOpacity>

                <TouchableOpacity onPress={() => { pushToScreen('BlockedUsers') }} style={settingStyle.row}>
                    <Text style={settingStyle.rowTxt}>Blocked Users</Text>
                    <Image
                        source={Images.right_arrow}
                        style={{ height: 16, width: 8, resizeMode: "contain", marginHorizontal: 5 }}
                    />
                </TouchableOpacity>


                <TouchableOpacity onPress={() => pushToScreen('ChangePassword')} style={settingStyle.row}>
                    <Text style={settingStyle.rowTxt}>Change Password</Text>
                    <Image
                        source={Images.right_arrow}
                        style={{ height: 16, width: 8, resizeMode: "contain", marginHorizontal: 5 }}
                    />
                </TouchableOpacity>

                <TouchableOpacity onPress={() => setShowLogoutModel(true)} style={settingStyle.row}>
                    <Text style={settingStyle.rowTxt}>Logout</Text>
                    <Image
                        source={Images.right_arrow}
                        style={{ height: 16, width: 8, resizeMode: "contain", marginHorizontal: 5 }}
                    />
                </TouchableOpacity>

            </ScrollView>
            <TouchableOpacity onPress={() => setShowRateModel(true)} style={{ padding: 20, backgroundColor: 'white', justifyContent: "center", alignItems: "center", marginTop: 5 }}>
                <Text style={{ color: "#666674", fontSize: 14, fontFamily: fonts.Poppins_Bold }}>Rate Vumi</Text>
            </TouchableOpacity>

            <LogoutModelCompo
                showLogoutModel={showLogoutModel}
                Loader={Loader}
                userData={userInfo ? { username: userInfo.username, user_image: userInfo.profile_image } : null}
                setShowLogoutModel={(type) => onLogout(type)}
            />

            <RateModelCompo
                setShowLRateModel={(value) => onRate(value)}
                showRateModel={showRateModel}
            />


        </View>
    )

}


const settingStyle = StyleSheet.create({
    row: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        backgroundColor: 'white',
        height: 55,
        paddingHorizontal: 20,
        marginVertical: 5,
    },
    roeTxt: {
        color: "#666674",
        fontSize: 14,
        fontFamily: fonts.Poppins_Bold
    },
    heading: { color: "#b9b9ce", fontSize: 12, fontFamily: fonts.Montserrat_Bold }
});