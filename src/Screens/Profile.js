import React, { Component } from 'react'
import { Text, View, TouchableOpacity, StyleSheet, Alert, DeviceEventEmitter, ActivityIndicator, Image, FlatList, ScrollView, StatusBar, SafeAreaView, ImageBackground, RefreshControl } from 'react-native'
import { Actions } from 'react-native-router-flux'
import {
    Images, Colors, Fontfamily, height,
} from '../common';
import fonts from '../common/fonts';
import { ProgressBarThird } from '../components/ProgressBarThird';
import { WhatAreTeamsModelCompo } from '../components/common/WhatAreTeamsModelCompo';
import Helper from '../lib/Helper';
import Config from '../lib/Config';
import pushToScreen from '../navigation/NavigationActions';
import Spinner from 'react-native-loading-spinner-overlay';
import Toast from 'react-native-root-toast';

import { BlockModelCompo } from '../components/Watch/BlockModelCompo';
import SocketConnect from '../common/SocketConnect';
import { InfoModelCompo } from '../components/Watch/InfoModelCompo';
import ImageLoadView from '../components/ImageLoadView';

export class Profile extends Component {



    constructor(props) {
        super(props);
        this.state = {
            showTeamsModel: false,
            profile_image: Images.User_profile_icon,
            username: '',
            coins: null,
            views: null,
            likes: null,
            location: '',
            arrayJoinTeams: [],
            showSpinner: false,
            bonus_points: '',
            LatestPostImage: '',
            LatestPostTitle: '',
            showJoinModel: false, first_post_id: '', showLoaderPageing: false, refreshing: false,
            MessageCounts: 0,
            NotificationCounts: 0,
            is_vip: 0,
            selectedRowDetail: {},
            showBlockModel: false,
            showInfoModel: false,
            profile_imageview: '',
            tooltipViewCoins: false, tooltipLikes: false, tooltipView: false,
            ViewSignupPoints: 0,
            CoinForVip: 0,
            showLoaderRefresh: false
        };
    }



    componentDidMount = async () => {
        this.functionevevnt = this.props.navigation.addListener('didFocus', async () => {
            const data = await Helper.getDataFromAsync("userInfo");
            // console.log(JSON.stringify(data), "data>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
            this.setState({
                profile_image: { uri: Config.imageUrl + data.profile_image },
                username: data.username,
                coins: data.coins,
                views: data.views,
                likes: data.likes,
                location: data.location,
                bonus_points: data.bonus_points,
                LatestPostImage: { uri: data.latest_post && data.latest_post.post_detail && data.latest_post.post_detail[0].thumbnail_media ? Config.imageUrl + data.latest_post.post_detail[0].thumbnail_media : Images.user_dummy },
                LatestPostTitle: data.latest_post && data.latest_post.title && data.latest_post.title,
                MessageCounts: data.unread_msg_count,
                NotificationCounts: data.unread_notification_count,
                profile_imageview: { uri: data.latest_viewer && data.latest_viewer.profile_image ? Config.imageUrl + data.latest_viewer.profile_image : '' },
                ViewSignupPoints: data.view_signup_points,
                CoinForVip: data.coin_for_vip
            })
            StatusBar.setBackgroundColor('white');
            StatusBar.setBarStyle('dark-content')
        });
        this.UpdateJoinTeam = DeviceEventEmitter.addListener('UpdateJoinTeamData', event => this.add(event));
        this.UpdatePost = DeviceEventEmitter.addListener('updatePost', updatePost => this.updatePost(updatePost));
        this.getProfileApi(true);
        this.getJoinTeamsbymeApi(false, '');




    };
    add(event) {
        if (event) {
            this.getJoinTeamsbymeApi(false, '');
        }

    }
    updatePost(updatePost) {
        if (updatePost) {
            this.getProfileApi(false);
        }
    }


    getJoinTeamsbymeApi = (spinner, last_id) => {
        this.setState({ showSpinner: spinner })
        Helper.makeRequest({ url: "team-joined-me", method: "POST", data: { last_id: last_id } }).then((responsedata) => {
            if (last_id == "") {
                this.state.arrayJoinTeams = []
            }
            this.state.first_post_id = ""
            if (responsedata.status == 'true') {
                if (responsedata.data && responsedata.data.data && responsedata.data.data.length > 0) {
                    this.state.arrayJoinTeams = [...this.state.arrayJoinTeams, ...responsedata.data.data]
                }
                this.state.first_post_id = responsedata.data.first_id ? responsedata.data.first_id : ""
            } else {
                // Toast.show(responsedata.message)
            }

            this.state.showLoaderPageing = false
            this.state.showSpinner = false
            this.setState({})
        })
    }
    // getJoinTeamsbymeApi = () => {
    //     this.setState({ showSpinner: false })
    //     Helper.makeRequest({ url: 'team-joined-by-me', method: "POST", data: '' }).then((responsedata) => {
    //         console.log("team-joined-by-me------------------------------", responsedata)
    //         if (responsedata.status == 'true') {
    //             this.setState({ showSpinner: false })
    //             this.setState({ arrayJoinTeams: responsedata.data.data })
    //             // Toast.show(responsedata.message)

    //         } else {
    //             this.setState({ showSpinner: false })
    //             Toast.show(responsedata.message)

    //         }
    //     })
    // }

    getProfileApi = (spinner) => {
        console.log(spinner, "spinner");
        this.setState({ showSpinner: spinner })
        Helper.makeRequest({ url: 'get-profile', method: "POST", data: '' }).then((responsedata) => {
            console.log("get-profile------------------------------", responsedata)
            if (responsedata.status == 'true') {
                let ProfileData = responsedata.data
                this.setState({
                    showSpinner: false,
                    showLoaderRefresh: false,
                    profile_image: { uri: Config.imageUrl + ProfileData.profile_image },
                    username: ProfileData.username,
                    coins: ProfileData.coins,
                    views: ProfileData.views,
                    likes: ProfileData.likes,
                    location: ProfileData.location,
                    bonus_points: ProfileData.bonus_points,
                    LatestPostImage: { uri: ProfileData.latest_post && ProfileData.latest_post.post_detail && ProfileData.latest_post.post_detail[0].thumbnail_media ? Config.imageUrl + ProfileData.latest_post.post_detail[0].thumbnail_media : Images.user_dummy },
                    LatestPostTitle: ProfileData.latest_post && ProfileData.latest_post.title && ProfileData.latest_post.title,
                    MessageCounts: ProfileData.unread_msg_count,
                    NotificationCounts: ProfileData.unread_notification_count,
                    is_vip: ProfileData.is_vip,
                    profile_imageview: { uri: ProfileData.latest_viewer && ProfileData.latest_viewer.profile_image ? Config.imageUrl + ProfileData.latest_viewer.profile_image : '' },
                    ViewSignupPoints: ProfileData.view_signup_points,
                    CoinForVip: ProfileData.coin_for_vip,
                })
                Helper.saveDataInAsync("userInfo", ProfileData);

                // Toast.show(responsedata.message)

            } else {
                this.setState({ showSpinner: false, showLoaderRefresh: false, })
                // Toast.show(responsedata.message)

            }
        })
    }
    componentWillUnmount = () => {
        if (this.functionevevnt) {
            this.functionevevnt.remove();
        }
        if (this.UpdateJoinTeam) {
            this.UpdateJoinTeam.remove();
        }
        if (this.UpdatePost) {
            this.UpdatePost.remove();
        }


    };

    onUserImagePress = (item) => {
        console.log(item, 'item>>>>>>>>>>>>>>>');
        item['is_blocked'] = 0;
        let data = {
            user: {
                id: item.id,
                username: item.username,
                profile_image: item.profile_image,
                location: item.location
            }
        }
        this.setState({ showInfoModel: true, selectedRowDetail: data })
    }

    onBlock = (item) => {
        this.setState({ showBlockModel: false })
        let formdata = {
            user_id: Helper.userInfo.id,
            other_user_id: item.user.id,
        }
        console.log('formdata-----------  ', formdata)
        if (SocketConnect.checkSocketConnected()) {
            SocketConnect.socketDataEvent('user_block_unblock', formdata);

            this.getJoinTeamsbymeApi(false, '');

        } else {
            SocketConnect.checkConnection('connect', cb => {
                console.log('SocketConnect------------', cb);
            });
        }
    }

    goToChat = (item) => {
        this.setState({ showInfoModel: false }, () => {
            pushToScreen('Chat', { OtherUserId: item.user.id, OtherUserName: item.user.username });
        })
    }

    onShowBlockModel = () => {
        this.setState({ showInfoModel: false }, () => {
            this.setState({ showBlockModel: true })
        })
    }
    gotoLeftTeam(username) {
        Alert.alert(
            'Vumi',
            'Are you sure you want to left ' + '"' + username + '"' + ' team.',

            [
                { text: "No", onPress: () => { console.log("Logout no") } },
                { text: "Yes", onPress: () => { this.gotoLeft(username) } },
            ],
            { cancelable: false }
        )

    }
    gotoLeft(username) {
        this.setState({ showSpinner: true })
        Helper.makeRequest({ url: "left-team", method: "POST", data: { username: username } }).then((responsedata) => {
            if (responsedata.status == 'true') {
                this.setState({ showSpinner: false })
                this.getJoinTeamsbymeApi(true, '');


            } else {
                this.setState({ showSpinner: false })
                Toast.show(responsedata.message)
            }

        })
    }

    TeamsJoinList = ({ item, index }) => (
        <TouchableOpacity onPress={() => this.onUserImagePress(item)} style={{ marginHorizontal: 5 }}>
            <ImageLoadView
                resizeMode='cover'
                source={item.profile_image ? { uri: Config.imageUrl + item.profile_image } : Images.user_dummy}
                style={{ height: 48, width: 48, resizeMode: "cover", borderRadius: 48 / 2 }}

            />
            <Text style={{ marginVertical: 3, color: "#acacac", fontFamily: fonts.SFProDisplay_Semibold, fontSize: 13, textAlign: 'center' }}>{item.bonus_points > 0 ? item.bonus_points : "0"}</Text>
        </TouchableOpacity>

        // <View style={{ flex: 1, marginHorizontal: 20 }}>
        //     <Text style={{ fontFamily: Fontfamily.PB, color: 'rgb(96,96,111)', marginVertical: 10 }}>Join Team</Text>
        //     <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between' }}>
        //         <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center' }}>
        //             <ImageLoadView
        //                 resizeMode='cover'
        //                 source={item.profile_image ? { uri: Config.imageUrl + item.profile_image } : Images.user_dummy}
        //                 style={{ height: 40, width: 40, }}
        //             />
        //             <Text style={{ marginLeft: 10, fontFamily: Fontfamily.MSB }}>{item.username ? item.username : ""}</Text>
        //         </View>
        //         <TouchableOpacity onPress={() => this.gotoLeftTeam(item.username)} style={{
        //             justifyContent: 'center',
        //             alignItems: 'center', backgroundColor: 'red',
        //             height: 25, width: 80, borderRadius: 13
        //         }}>
        //             <Text style={{ color: '#fff', fontSize: 12, fontFamily: Fontfamily.PB }}>Leave Team</Text>
        //         </TouchableOpacity>
        //     </View>

        // </View>
    );
    onScroll = () => {
        if (!this.state.showLoaderPageing && this.state.arrayJoinTeams.length > 0 && this.state.arrayJoinTeams[this.state.arrayJoinTeams.length - 1].id != this.state.first_post_id && this.state.first_post_id != '') {
            this.setState({ showLoaderPageing: true })
            this.getJoinTeamsbymeApi(false, this.state.arrayJoinTeams[this.state.arrayJoinTeams.length - 1].id);
        }
    }
    render_Activity_footer = () => {
        var footer_View = (
            <View style={{ flex: 1, backgroundColor: '#ffffff', alignItems: 'center', justifyContent: 'center', padding: 15 }}>
                <ActivityIndicator size="large" color="#000000" />
            </View>
        );
        return footer_View;
    };

    onBackAfterUnlock = () => {
        this.getSettingApi(false);
    }

    getSettingApi = (spinner) => {
        console.log(spinner, "spinner");
        this.setState({ showSpinner: spinner });
        Helper.makeRequest({ url: 'settings', method: 'POST', data: '' }).then(
            resp => {
                if (resp.status == 'true') {
                    Helper.userSettings = resp.data;
                    this.getProfileApi(true);
                    this.setState({ showSpinner: false, showLoaderRefresh: false });
                } else {
                    Helper.userSettings = {};
                    this.setState({ showSpinner: false, showLoaderRefresh: false });
                }
            },
        );
    };

    gotoShowViewCoinsTooltip = () => {
        this.setState({ tooltipViewCoins: true })
    }
    gotoShowLikesTooltip = () => {
        this.setState({ tooltipLikes: true })
    }
    gotoShowViewTooltip = () => {
        this.setState({ tooltipView: true })
    }
    gotoHideTooltip = () => {
        this.setState({ tooltipViewCoins: false, tooltipLikes: false, tooltipView: false })

    }
    _pullToRefresh = () => {
        this.setState({ showLoaderRefresh: true, })
        this.getProfileApi(false);
        this.getSettingApi(false);
        this.getJoinTeamsbymeApi(false, '');


    }

    render() {
        // console.log(this.state.profile_imageview, ">>>>>>>>>>>>>>>>>>>>>>>>>>this.state.profile_image");
        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: '#fff' }}>
                <Spinner visible={this.state.showSpinner} />

                <TouchableOpacity disabled={this.state.tooltipViewCoins || this.state.tooltipLikes || this.state.tooltipView == true ? false : true} onPressIn={() => { this.gotoHideTooltip() }} style={{ flex: 1, backgroundColor: '#eff1f7' }}>
                    {/* <StatusBar backgroundColor="white" barStyle="dark-content" /> */}
                    <View style={{ height: 50, flexDirection: "row", alignItems: "center", justifyContent: "space-between", paddingHorizontal: 15, backgroundColor: "#fff", marginBottom: 1.5 }}>
                        <TouchableOpacity onPress={() => { pushToScreen('Setting') }}>
                            <Image
                                source={Images.setting}
                                style={{ height: 22, width: 22, resizeMode: "contain" }}
                            />
                        </TouchableOpacity>
                        <Text style={{ fontSize: 18, fontFamily: Fontfamily.PoPB, color: '#606076' }}>Me</Text>
                        <View style={{ height: 22, width: 22, }} />
                    </View>
                    {/* <View style={{ flex: 1, backgroundColor: '#eff1f7',shadowOpacity:2, }}></View> */}
                    <ScrollView style={{}} contentContainerStyle={{ flexGrow: 1, }} showsVerticalScrollIndicator={false}
                        refreshControl={
                            <RefreshControl
                                refreshing={this.state.showLoaderRefresh}
                                onRefresh={this._pullToRefresh}
                            />
                        }
                    >

                        <View style={{ height: 300, backgroundColor: 'white', justifyContent: 'center', alignItems: 'center', }}>
                            <View style={{ marginBottom: 20, borderWidth: 0, alignItems: 'center' }}>
                                <View style={{ marginBottom: 10, }}>
                                    <View style={{
                                        height: 110, width: 110, borderRadius: 60, alignItems: 'center', justifyContent: 'center'
                                    }}>
                                        {/* <Image
                                            source={this.state.profile_image}
                                            style={Style.bigO}
                                        /> */}
                                        <ImageLoadView
                                            style={Style.bigO}
                                            resizeMode={'cover'}
                                            source={this.state.profile_image}
                                        />
                                    </View>


                                    <TouchableOpacity hitSlop={{ top: 20, bottom: 20, left: 20, right: 20 }} onPress={() => pushToScreen('EditProfile')} style={Style.smallO}>
                                        <Image
                                            style={{ height: 25, width: 25, resizeMode: "contain" }}
                                            source={Images.edit}
                                        />
                                    </TouchableOpacity>

                                </View>
                                <Text style={{ fontSize: 21, fontFamily: fonts.Montserrat_Bold }}>{this.state.username}</Text>
                                <Text style={{ fontSize: 14, color: 'rgb(193,189,214)', fontFamily: fonts.SFProDisplay_Semibold }}>{this.state.location == "null" || this.state.location == '' ? '' : this.state.location}</Text>
                                {/* rgb 193 189 214 */}
                            </View>




                            <View style={{ flexDirection: 'row', width: '100%', alignItems: 'center', }}>
                                <TouchableOpacity
                                    onLongPress={() => this.gotoShowViewCoinsTooltip()}
                                    onPressIn={() => this.gotoHideTooltip()} style={{ alignItems: 'center', width: "33.33%" }}>
                                    <Image
                                        resizeMode='contain'
                                        source={Images.profile.coin}
                                        style={{ height: 30, width: 30, }}
                                    />
                                    <Text style={{ fontSize: 17, fontFamily: Fontfamily.MB, marginTop: 5 }}>{Helper.userSettings.user_coins ? Helper.userSettings.user_coins : "0"}</Text>
                                    <Text style={{ color: '#000', fontSize: 10 }}>{Helper.userSettings.user_coins > 1 ? "VIEW COINS" : "VIEW COIN"}</Text>
                                </TouchableOpacity>

                                <TouchableOpacity
                                    onLongPress={() => this.gotoShowLikesTooltip()}
                                    onPressIn={() => this.gotoHideTooltip()}
                                    style={{ alignItems: 'center', width: "33.33%" }}>
                                    <Image
                                        resizeMode='contain'
                                        source={Images.profile.heart}
                                        style={{ height: 30, width: 30, }}
                                    />
                                    <Text style={{ fontSize: 17, fontFamily: Fontfamily.MB, marginTop: 5 }}>{Helper.userSettings.total_likes_count ? Helper.userSettings.total_likes_count : '0'}</Text>
                                    <Text style={{ color: '#000', fontSize: 10 }}>{Helper.userSettings.total_likes_count > 1 ? 'LIKES' : 'LIKE'}</Text>
                                </TouchableOpacity>

                                <TouchableOpacity
                                    onLongPress={() => this.gotoShowViewTooltip()}
                                    onPressIn={() => this.gotoHideTooltip()}

                                    style={{ alignItems: 'center', width: "33.33%", }}>
                                    <ImageLoadView
                                        style={{ height: 30, width: 30, borderRadius: 30 / 2, }}
                                        resizeMode={'cover'}
                                        source={this.state.profile_imageview ? this.state.profile_imageview : Images.User_profile_icon}
                                    />
                                    <Text style={{ fontSize: 17, fontFamily: Fontfamily.MB, marginTop: 5 }}>{Helper.userSettings.total_views_count ? Helper.userSettings.total_views_count : "0"}</Text>

                                    <Text style={{ color: '#000', fontSize: 10 }}>{Helper.userSettings.total_views_count > 1 ? 'VIEWS' : 'VIEW'}</Text>
                                </TouchableOpacity>
                            </View>


                        </View>
                        {this.state.tooltipViewCoins == true && Helper.userSettings.user_coins > 0 ?
                            <ImageBackground source={Images.profile.borderview}
                                style={{ width: 125, height: 50, position: 'absolute', zIndex: 1, left: 1, marginTop: 150 }}>
                                <Text numberOfLines={1} style={{ fontSize: 10, fontFamily: Fontfamily.MB, textAlign: 'center', marginTop: 15, marginHorizontal: 13 }}>{Helper.userSettings.user_coins ? Helper.userSettings.user_coins + (Helper.userSettings.user_coins > 1 ? " VIEW COINS" : " VIEW COIN") : "0"}</Text>
                            </ImageBackground>
                            : null
                        }
                        {this.state.tooltipLikes == true && Helper.userSettings.total_likes_count > 0 ?
                            <ImageBackground source={Images.profile.borderview}
                                style={{ width: 125, height: 50, position: 'absolute', zIndex: 1, alignSelf: 'center', alignItems: 'center', marginTop: 150 }}>
                                <Text numberOfLines={1} style={{ fontSize: 10, fontFamily: Fontfamily.MB, textAlign: 'center', marginTop: 15, marginHorizontal: 13 }}>{Helper.userSettings.total_likes_count ? Helper.userSettings.total_likes_count + (Helper.userSettings.total_likes_count > 1 ? " LIKES" : " LIKE") : "0"}</Text>
                            </ImageBackground>
                            : null
                        }
                        {this.state.tooltipView == true && Helper.userSettings.total_views_count > 0 ?
                            <ImageBackground source={Images.profile.borderview}
                                style={{ width: 125, height: 50, position: 'absolute', zIndex: 1, right: 1, marginTop: 150 }}>
                                <Text numberOfLines={1} style={{ fontSize: 10, fontFamily: Fontfamily.MB, textAlign: 'center', marginTop: 15, marginHorizontal: 13 }}>{Helper.userSettings.total_views_count ? Helper.userSettings.total_views_count + (Helper.userSettings.total_views_count > 1 ? " Views" : " View") : "0"}</Text>
                            </ImageBackground>
                            : null
                        }
                        < View style={{ backgroundColor: 'white', marginBottom: 10 }}>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginHorizontal: 20, marginTop: 10 }}>
                                <Text style={{ fontFamily: Fontfamily.PB, color: 'rgb(96,96,111)' }}>Team</Text>
                                <TouchableOpacity onPress={() => this.setState({ showTeamsModel: true })}>
                                    <Image
                                        resizeMode='contain'
                                        source={Images.profile.info}
                                        style={{ height: 30, width: 30, }}
                                    />
                                </TouchableOpacity>
                            </View>


                            <View style={{ marginVertical: 13 }}>

                                <TouchableOpacity onPress={() => pushToScreen('Teams')}>
                                    <ImageBackground style={{ height: 100, width: "100%", resizeMode: "contain", justifyContent: "center", alignItems: "center" }} source={Images.blue_box}>
                                        <Text style={{ fontFamily: Fontfamily.PB, color: 'white', fontSize: 16 }}>Create a Team</Text>
                                        <Text style={{ fontFamily: fonts.SFProDisplay_Bold, color: 'white', fontSize: 15 }}>Get 1000’s of bonus coins!</Text>
                                        {/* <Text style={{ fontFamily: fonts.SFProDisplay_Bold, color: 'white', fontSize: 15 }}>Get {this.state.bonus_points ? this.state.bonus_points : "0"}’s of bonus coins!</Text> */}

                                    </ImageBackground>
                                </TouchableOpacity>
                                {this.state.arrayJoinTeams.length > 0 ?
                                    <FlatList
                                        horizontal
                                        style={{ marginHorizontal: 15 }}
                                        showsHorizontalScrollIndicator={false}
                                        data={this.state.arrayJoinTeams}
                                        renderItem={this.TeamsJoinList}
                                        // renderItem={() => <Item title={43} img={Images.dummy_user} />}
                                        keyExtractor={(item, index) => index.toString()}
                                        extraData={this.state}
                                        onEndReached={() => this.onScroll()}
                                        onEndReachedThreshold={0.5}
                                        ListFooterComponent={this.state.showLoaderPageing && this.render_Activity_footer}
                                        ListEmptyComponent={() => (
                                            <View style={{ flex: 1, justifyContent: "center", alignItems: "center", alignSelf: 'center', marginHorizontal: 10 }}>

                                            </View>
                                        )}
                                    />
                                    : null
                                }
                            </View>

                        </View>



                        <TouchableOpacity onPress={() => pushToScreen('Messages')}
                            style={{ flexDirection: 'row', height: 60, backgroundColor: 'white', alignItems: 'center', justifyContent: 'space-between' }}>
                            <View style={{ justifyContent: 'space-between', marginHorizontal: 20, }}>
                                <Text style={{ fontFamily: Fontfamily.PB, color: 'rgb(96,96,111)' }}>Messages</Text>
                            </View>
                            <View style={{ flexDirection: 'row', alignItems: 'center', marginHorizontal: 20 }}>
                                {Number(this.state.MessageCounts) > 0 ?
                                    <View style={{
                                        justifyContent: 'center', marginHorizontal: 10,
                                        alignItems: 'center', backgroundColor: 'rgb(255,51,137)',
                                        height: 25, width: 25, borderRadius: 13
                                    }}>
                                        < Text style={{ color: '#fff' }}>{Number(this.state.MessageCounts) <= 9 ? this.state.MessageCounts : "9+"}</Text>


                                    </View>
                                    :
                                    null
                                }
                                <Image
                                    source={Images.right_arrow}
                                    style={{ height: 20, width: 10, resizeMode: "contain", marginHorizontal: 5 }}
                                />
                            </View>
                        </TouchableOpacity>



                        <View style={{ backgroundColor: 'white', justifyContent: 'center', marginVertical: 10, paddingVertical: 10 }}>
                            <View style={{ flexDirection: 'row', justifyContent: 'space-between', marginHorizontal: 20, }}>
                                <View style={{ flexDirection: 'row', alignItems: 'center', justifyContent: 'space-around' }}>
                                    <Text style={{ fontFamily: Fontfamily.PB, color: '#6260ff' }}>Vumi VIP</Text>
                                    {this.state.is_vip == 1 && <Image
                                        source={Images.ticks}
                                        style={{ height: 20, width: 20, resizeMode: "contain", marginHorizontal: 10 }}
                                    />}
                                </View>
                                <TouchableOpacity disabled={this.state.is_vip == 1} onPress={() => this.state.is_vip != 1 && pushToScreen('VumiVip', { onBackAfterUnlock: this.onBackAfterUnlock.bind(this) })} style={{
                                    justifyContent: 'center', marginHorizontal: 10,
                                    alignItems: 'center', backgroundColor: Colors.abuttonColor,
                                    height: 25, width: 80, borderRadius: 13
                                }}>
                                    <Text style={{ color: '#fff', fontSize: 12, fontFamily: Fontfamily.PB }}>{this.state.is_vip == 1 ? "ACTIVATED" : "ACTIVE"}
                                    </Text>
                                </TouchableOpacity>

                            </View>


                            {this.state.is_vip != 1 && <View style={{ flexDirection: "row", alignItems: "flex-end", justifyContent: "space-between", paddingHorizontal: 20, paddingTop: 10 }}>
                                <View style={{ flexDirection: "row", alignItems: "flex-end" }}>
                                    <Text style={{ fontFamily: fonts.SFProDisplay_Heavy, color: '#7d8181', fontSize: 12 }}>Collect </Text>

                                    <Text style={{ fontFamily: Fontfamily.PB, color: '#7d8181', fontSize: 12 }}>{Number(this.state.CoinForVip)}</Text>
                                    <Text style={{ fontFamily: Fontfamily.PB, color: '#7d8181', fontSize: 8, }}> VC </Text>
                                    <Text style={{ fontFamily: fonts.SFProDisplay_Heavy, color: '#7d8181', fontSize: 12 }}>to unlock the Free Trial </Text>
                                </View>
                                <View style={{ flexDirection: "row", alignItems: "flex-end" }}>
                                    <Image
                                        source={Images.coin}
                                        style={{ height: 15, width: 15, resizeMode: "contain", }}
                                    />
                                    {/* <Text style={{ fontFamily: Fontfamily.PB, color: '#04e6a0', fontSize: 14, }}> {Helper.userSettings.user_coins} </Text> */}
                                    <Text style={{ fontFamily: Fontfamily.PB, color: '#04e6a0', fontSize: 14, }}> {Number(this.state.ViewSignupPoints)}</Text>
                                    <Text style={{ color: '#7d8181', fontSize: 12, fontFamily: fonts.SFProDisplay_Heavy, opacity: 0.3 }}> / {Number(this.state.CoinForVip)}</Text>
                                </View>

                            </View>}
                            {this.state.is_vip != 1 && <View style={{ width: "100%", paddingVertical: 10, paddingHorizontal: 20 }}>
                                <ProgressBarThird
                                    total={Number(this.state.CoinForVip)}
                                    completed={Number(this.state.ViewSignupPoints) > Number(this.state.CoinForVip) ? Number(this.state.CoinForVip) : Number(this.state.ViewSignupPoints)}
                                    colorCompleted={'#3fefc6'}
                                    colorUncompleted={'#e5fdf9'}
                                />
                            </View>}
                        </View>
                        {this.state.LatestPostTitle ?
                            <View style={{ height: 100, backgroundColor: 'white', }}>
                                <Text style={{ fontFamily: Fontfamily.PB, color: 'rgb(96,96,111)', marginHorizontal: 20, marginVertical: 10 }}>Posts</Text>
                                <TouchableOpacity onPress={() => pushToScreen('PostsList')} style={{ flexDirection: 'row', marginHorizontal: 20, alignItems: 'center' }}>
                                    <ImageLoadView
                                        resizeMode='cover'
                                        source={this.state.LatestPostImage}
                                        style={{ height: 40, width: 40, }}
                                    />
                                    <Text style={{ marginHorizontal: 10, fontFamily: Fontfamily.MSB }}>{this.state.LatestPostTitle}</Text>
                                </TouchableOpacity>
                            </View>
                            : null
                        }

                        <TouchableOpacity onPress={() => pushToScreen('SavedList')} style={{ flexDirection: 'row', height: 60, marginVertical: 10, backgroundColor: 'white', alignItems: 'center', justifyContent: 'space-between' }}>
                            <View style={{ justifyContent: 'space-between', marginHorizontal: 20, }}>
                                <Text style={{ fontFamily: Fontfamily.PB, color: 'rgb(96,96,111)' }}>Saved</Text>
                            </View>
                            <View style={{ flexDirection: 'row', alignItems: 'center', marginHorizontal: 20 }}>
                                <Image
                                    source={Images.right_arrow}
                                    style={{ height: 20, width: 10, resizeMode: "contain", marginHorizontal: 5 }}
                                />
                            </View>
                        </TouchableOpacity>




                        <TouchableOpacity
                            onPress={() => pushToScreen('NotificationList')}
                            style={{ flexDirection: 'row', height: 60, backgroundColor: 'white', alignItems: 'center', justifyContent: 'space-between' }}>
                            <View style={{ justifyContent: 'space-between', marginHorizontal: 20, }}>
                                <Text style={{ fontFamily: Fontfamily.PB, color: 'rgb(96,96,111)' }}>Notifications</Text>
                            </View>
                            <View style={{ flexDirection: 'row', alignItems: 'center', marginHorizontal: 20 }}>
                                {Number(this.state.ProfileData) > 0 ?
                                    <View style={{
                                        justifyContent: 'center', marginHorizontal: 10,
                                        alignItems: 'center', backgroundColor: 'rgb(255,51,137)',
                                        height: 25, width: 25, borderRadius: 13
                                    }}>
                                        < Text style={{ color: '#fff' }}>{Number(this.state.ProfileData) <= 9 ? this.state.ProfileData : "9+"}</Text>
                                    </View>
                                    : null
                                }
                                <Image
                                    source={Images.right_arrow}
                                    style={{ height: 20, width: 10, resizeMode: "contain", marginHorizontal: 5 }}
                                />
                            </View>
                        </TouchableOpacity>




                    </ScrollView>
                </TouchableOpacity >

                {
                    this.state.showBlockModel &&
                    <BlockModelCompo
                        showBlockModel={this.state.showBlockModel}
                        onBlock={(item) => this.onBlock(item)}
                        hideBlockModel={() => this.setState({ showBlockModel: false })}
                        selectedRowDetail={this.state.selectedRowDetail}
                    />
                }
                {
                    this.state.showInfoModel &&
                    <InfoModelCompo
                        showInfoModel={this.state.showInfoModel}
                        hideInfoModel={() => this.setState({ showInfoModel: false })}
                        onShowBlockModel={() => this.onShowBlockModel()}
                        goToChat={(item) => this.goToChat(item)}
                        selectedRowDetail={this.state.selectedRowDetail}
                    />
                }

                <WhatAreTeamsModelCompo
                    showTeamsModel={this.state.showTeamsModel}
                    hideTeamsModel={() => this.setState({ showTeamsModel: false })}
                />

            </SafeAreaView >
        )
    }




}


const Style = StyleSheet.create({
    bigO: {
        height: 110, width: 110,
        borderRadius: 60,
    },
    smallO: {
        position: 'absolute', bottom: 5, right: 5,
        height: 25, width: 25,
    },
    safeArea: {
        paddingHorizontal: 10,
        flexDirection: 'row',
        height: 60,
    },




})