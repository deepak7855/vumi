import React, { Component } from 'react'
import { View, ImageBackground, StatusBar, Dimensions, TouchableOpacity, Text, BackHandler } from 'react-native'
import {
    GButton, IconInput, TxtW, Images, GAuthHeader,
    Colors, ScrollContainer, ScrollButtonTop,
} from '../common';
import { Actions } from 'react-native-router-flux';
import Helper from '../lib/Helper';
import Toast from 'react-native-root-toast';
import Spinner from 'react-native-loading-spinner-overlay';
import pushToScreen from '../navigation/NavigationActions';


export class CreatePassword extends Component {

    constructor(props) {
        super(props);
        this.state = {
            login: false,
            btnColor: true,
            reEnerPassword: '',
            password: '',
            enableBtn: false,
            securePasswordEnter: true,
            secureRePasswordEnter: true,
            showSpinner: false
        };
    }



    onReEnterPassword = (text) => {
        text.length >= 8 && this.state.password == text ?
            this.setState({ enableBtn: true, reEnerPassword: text }) :
            this.setState({ enableBtn: false, reEnerPassword: text })
    }

    onPasswordInput = (text) => {
        text.length >= 8 && this.state.reEnerPassword == text ?
            this.setState({ enableBtn: true, password: text }) :
            this.setState({ enableBtn: false, password: text })
    }

    onNext = () => {

        if (this.props.whereFrom == 'ForgotPassword') {
            var data = {
                country_code: this.props.country_code,
                phone: this.props.phone,
                password: this.state.password
            }
            this.setState({ showSpinner: true })
            Helper.makeRequest({ url: "reset-password", method: "POST", data: data }).then((resp) => {
                if (resp.status == 'true') {
                    Toast.show(resp.message)
                    Actions.reset('Auth');
                } else {
                    Toast.show(resp.message)
                }
                this.setState({ showSpinner: false })
            })


        } else {

            var tempData = this.props.data;
            let test = new FormData();
            test.append('profile_image', {
                uri: tempData.profile_image,
                name: 'test.jpg',
                type: 'image/jpeg'
            });

            test.append('password', this.state.password);
            test.append('country_code', tempData.country_code);
            test.append('location', tempData.location);
            test.append('phone', tempData.phone);
            test.append('username', tempData.username);
            if (tempData.referral_code) {
                test.append('referral_code', tempData.referral_code);
            }

            this.setState({ showSpinner: true })
            Helper.makeRequest({ url: "signup", method: "POST", data: test, isImage: true }).then((resp) => {
                if (resp.status == 'true') {
                    Helper.userInfo = resp.data;
                    Helper.saveDataInAsync("userInfo", resp.data);
                    Helper.saveDataInAsync("token", resp.token)
                    // Actions.Referral();
                    pushToScreen('Referral');
                } else {
                    Toast.show(resp.message)
                }
                this.setState({ showSpinner: false })
            })

        }
    }

    render() {
        return (
            <ImageBackground resizeMode='cover' source={Images.Loginbg} style={{ flex: 1, alignItems: 'center' }}>

                <GAuthHeader
                    bArrow={this.props.whereFrom == 'ForgotPassword' ? false : true}
                    title1="Create Password" />

                <ScrollContainer containerStyle={{ alignItems: 'center', justifyContent: 'center', flex: 1 }}>
                    <View style={{}}>

                        <IconInput
                            value={this.state.password}
                            placeholder='New Password'
                            onChangeText={(text) => this.onPasswordInput(text)}
                            secureTextEntry={this.state.securePasswordEnter}
                            rimagePath={this.state.securePasswordEnter ? Images.eye_hidden : Images.eye_grey}
                            onClick={() => this.setState({ securePasswordEnter: !this.state.securePasswordEnter })}
                        />
                        <Text style={{ color: 'white', fontSize: 11, marginLeft: 15, marginTop: -10 }}>*Minimum 8 characters required</Text>
                        <IconInput
                            value={this.state.reEnerPassword}
                            onChangeText={(text) => this.onReEnterPassword(text)}
                            placeholder='Re-enter Password'
                            secureTextEntry={this.state.secureRePasswordEnter}
                            rimagePath={this.state.secureRePasswordEnter ? Images.eye_hidden : Images.eye_grey}
                            onClick={() => this.setState({ secureRePasswordEnter: !this.state.secureRePasswordEnter })}
                        />
                        <Text style={{ color: 'white', fontSize: 11, marginLeft: 15, marginTop: -10 }}>*Minimum 8 characters required</Text>

                    </View>


                </ScrollContainer>

                <ScrollButtonTop style={{ alignItems: 'center', }}>
                    <GButton
                        height={65}
                        radius={32.5}
                        disabled={!this.state.enableBtn}
                        bText='Next'
                        txtcolor='#fff'
                        bgColor={this.state.enableBtn ? Colors.abuttonColor : Colors.btxtColor}
                        onPress={() => this.onNext()}
                    />
                </ScrollButtonTop>

                <Spinner visible={this.state.showSpinner} />
            </ImageBackground>
        )
    }
    backAction = () => {
        if (Actions.currentScene == "CreatePassword" && this.props.whereFrom == 'ForgotPassword') {
            return true
        }

    };
    async componentDidMount() {

        this.backHandler = BackHandler.addEventListener(
            "hardwareBackPress",
            this.backAction
        );


    }

    componentWillUnmount() {
        this.backHandler.remove();
    }
}
