import React, { useState, useEffect } from 'react'
import {
    Text, View, ImageBackground, Keyboard, TouchableOpacity
} from 'react-native';
import { BulletComponent } from '../components'
import { IconInput, Images, ScrollEnable, GAuthHeader, Fontfamily, Colors } from '../common';
import { Actions } from 'react-native-router-flux';
import { ResendOtpModelCompo } from '../components/common/ResendOtpModelCompo';
import Toast from 'react-native-root-toast';
import Helper from '../lib/Helper';
import Spinner from 'react-native-loading-spinner-overlay';
import CountDown from 'react-native-countdown-component';
import pushToScreen from '../navigation/NavigationActions';


export const Verification = (props) => {
    const [otp, setotp] = useState('')
    const [showSpinner, setSpinner] = useState(false)
    const [resendOtpModel, setResendOtpModel] = useState(false);
    const [new_otp, set_new_otp] = useState('');
    const [resendOption, showResendOption] = useState(false);

    const onOTPEnter = (text) => {
        setotp(text)
        if (text.length == 4) {
            Keyboard.dismiss();
            if (text == (new_otp || props.otp)) {
                if (props.whereFrom == 'SignUp') {
                    // Actions.SignUpStep3({ phone: props.phone, country_code: props.country_code })
                    pushToScreen('SignUpStep3', { phone: props.phone, country_code: props.country_code,location:props.location })
                } else {
                    // Actions.CreatePassword({ whereFrom: props.whereFrom, phone: props.phone, country_code: props.country_code })
                    pushToScreen('CreatePassword', { whereFrom: props.whereFrom, phone: props.phone, country_code: props.country_code })
                }
            } else {
                Toast.show("OTP Didn't Match")
            }

        }
    }

    const onResend = () => {
        setResendOtpModel(false);
        var data = {
            country_code: props.country_code,
            phone: props.phone,
            type: props.types
        }
        setSpinner(true);
        Helper.makeRequest({ url: "resend-otp", method: "POST", data: data }).then((resp) => {
            if (resp.status == 'true') {
                setotp('');
                set_new_otp(resp.data.otp);
                showResendOption(false);
                Toast.show(resp.message);
            } else {
                Toast.show(resp.message);
            }
            setSpinner(false);
        })
    }




    return (
        <ImageBackground resizeMode='cover'
            source={Images.Loginbg}
            style={{ flex: 1, alignItems: 'center' }}>

            <GAuthHeader bArrow title1="Verify Code" rightItem>
                <BulletComponent numberof={3} activeIndex={1} />
            </GAuthHeader>

            <ScrollEnable>
                <View style={{ alignItems: 'center' }}>
                    <View style={{ width: 260 }}>
                        <Text style={{
                            textAlign: 'center', color: '#fff', lineHeight: 20,
                            marginVertical: 60, fontFamily: Fontfamily.MB, fontSize: 15
                        }}>
                            Please enter the verification code sent to +{props.country_code} {props.phone}
                        </Text>
                    </View>

                    <IconInput
                        placeholder="Enter Code"
                        keyboardType='numeric'
                        style={{ textAlign: 'center', }}
                        value={otp}
                        maxLength={4}
                        onChangeText={(text) => onOTPEnter(text)}
                    />

                    {
                        resendOption ?
                            <TouchableOpacity onPress={() => { setResendOtpModel(true) }}>
                                <Text style={{
                                    textAlign: 'center', color: Colors.txtColor,
                                    fontFamily: Fontfamily.MB, fontSize: 15,
                                    marginVertical: 10
                                }}>Resend Verification Code</Text>

                            </TouchableOpacity>
                            :
                            <View style={{ flexDirection: "row", alignItems: "center" }} >
                                <Text style={{ textAlign: 'center', color: Colors.txtColor, fontFamily: Fontfamily.MB, fontSize: 15, marginVertical: 10 }}>
                                    Try again in
                               </Text>
                                <CountDown
                                    until={30}
                                    size={10}
                                    onFinish={() => showResendOption(true)}
                                    digitStyle={{ backgroundColor: 'transparent', }}
                                    digitTxtStyle={{ color: Colors.txtColor, fontFamily: Fontfamily.MB, fontSize: 15, }}
                                    timeToShow={['S']}
                                    timeLabels={{ s: null }}
                                />
                            </View>
                    }

                </View>
            </ScrollEnable>

            <ResendOtpModelCompo
                resendOtpModel={resendOtpModel}
                phone={props.phone}
                country_code={props.country_code}
                onResend={() => onResend()}
                onCloseModel={() => setResendOtpModel(false)}
            />
            <Spinner visible={showSpinner} />
        </ImageBackground>



    );
}

