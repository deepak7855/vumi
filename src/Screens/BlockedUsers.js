import React, { Component } from 'react';
import { View, Text, FlatList, Image, TouchableOpacity, DeviceEventEmitter, StyleSheet, StatusBar, RefreshControl, ActivityIndicator } from 'react-native';
import { GHeader, Images } from '../common';
import fonts from '../common/fonts';
import Helper from '../lib/Helper';
import Toast from 'react-native-root-toast';
import Spinner from 'react-native-loading-spinner-overlay';
import Config from '../lib/Config';
import moment from 'moment';
import SocketConnect from '../common/SocketConnect';
import ImageLoadView from '../common/ImageLoadView';

const ListRow = () => {
    return (
        <View style={styles.rowContainer}>
            <Image
                source={Images.dummy_user}
                style={styles.userImg}
            />
            <View style={styles.detail}>
                <Text numberOfLines={1} style={styles.userName}>Nicholasso</Text>
                <Text style={styles.time}>4h ago</Text>
            </View>

            <TouchableOpacity style={styles.btn}>
                <Text style={styles.unblock}>Unblock</Text>
            </TouchableOpacity>
        </View>
    )

}

export default class BlockedUsers extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showSpinner: false,
            arrSavePostList: [],
            first_post_id: '',
            showLoaderPageing: false,
            showLoaderRefresh: false

        };
    }
    componentDidMount() {
        this.getBlockListData(true, "")
        this.UserBlockUnBlock = DeviceEventEmitter.addListener('user_block_unblock_response', response => this.UserBlockUnBlockResponse(response));

    }
    componentWillUnmount() {
        this.UserBlockUnBlock.remove();


    }
    UserBlockUnBlockResponse(response) {
        if (response) {
            this.getBlockListData(true, "")
        }
    }
    getBlockListData = (spinner, last_id) => {
        this.setState({ showSpinner: spinner })
        Helper.makeRequest({ url: "blocked-users", method: "POST", data: { last_id: last_id } }).then((resp) => {
            if (last_id == "") {
                this.state.arrSavePostList = []
            }
            if (resp.status == 'true') {
                if (resp.data && resp.data.data && resp.data.data.length > 0) {
                    this.state.arrSavePostList = [...this.state.arrSavePostList, ...resp.data.data]
                }
                this.state.first_post_id = resp.data.first_id ? resp.data.first_id : ""

            } else {
                Toast.show(resp.message)
            }

            this.state.showLoaderPageing = false
            this.state.showLoaderRefresh = false
            this.state.showSpinner = false

            this.setState({})
        })
    }

    onScroll = () => {
        console.log("onScroll-------")
        if (!this.state.showLoaderPageing && this.state.arrSavePostList.length > 0 && this.state.arrSavePostList[this.state.arrSavePostList.length - 1].ubid != this.state.first_post_id && this.state.first_post_id != '') {
            this.setState({ showLoaderPageing: true })
            this.getBlockListData(false, this.state.arrSavePostList[this.state.arrSavePostList.length - 1].ubid);
        }
    }
    _pullToRefresh = () => {
        this.setState({ showLoaderRefresh: true })
        this.getBlockListData(false, "");
    }
    render_Activity_footer = () => {
        var footer_View = (
            <View style={{ flex: 1, backgroundColor: '#ffffff', alignItems: 'center', justifyContent: 'center', padding: 15 }}>
                <ActivityIndicator size="large" color="#000000" />
            </View>
        );
        return footer_View;
    };
    BlockUserList = ({ item, index }) => (

        <View style={[styles.rowContainer]}>
            {/* <Image
                resizeMode={'cover'}
                source={item.profile_image ? { uri: Config.imageUrl + item.profile_image } : Images.user_dummy}
                style={styles.userImg}
            /> */}
            <ImageLoadView
                resizeMode={'cover'}
                source={item.profile_image ? { uri: Config.imageUrl + item.profile_image } : Images.user_dummy}
                style={styles.userImg}
            />
            <View style={styles.detail}>
                <Text numberOfLines={1} style={styles.userName}>{item.username}</Text>
                <Text style={styles.time}>{moment.utc(item.created_at).local().fromNow()}</Text>

            </View>

            <TouchableOpacity onPress={() => this.gotoUnblock(item, index)}
                style={styles.btn}>
                <Text style={styles.unblock}>Unblock</Text>
            </TouchableOpacity>
        </View>
    )


    gotoUnblock(item, index) {
        let formdata = {
            user_id: item.user_id,
            other_user_id: item.other_id,
        }
        // alert(JSON.stringify(formdata))
        if (SocketConnect.checkSocketConnected()) {
            SocketConnect.socketDataEvent('user_block_unblock', formdata);
        } else {
            SocketConnect.checkConnection('connect', cb => {
                console.log('SocketConnect------------', cb);
            });

        }


    }
    render() {
        return (
            <View style={{ flex: 1, backgroundColor: 'white', }}>
                <StatusBar backgroundColor="white" />
                <Spinner visible={this.state.showSpinner} />

                <GHeader
                    bArrow
                    greyBackButton
                    color="#606076"
                    backgroundColor="white"
                    title1="Blocked Users"
                />
                <View style={{ flex: 1, backgroundColor: '#efeff5', paddingVertical: 10 }}>

                    <FlatList
                        contentContainerStyle={{ flexGrow: 1 }}
                        data={this.state.arrSavePostList}
                        ItemSeparatorComponent={() => <View style={{ height: 10 }} />}
                        refreshControl={
                            <RefreshControl
                                refreshing={this.state.showLoaderRefresh}
                                onRefresh={this._pullToRefresh}
                            />
                        }
                        renderItem={this.BlockUserList}
                        keyExtractor={(item, index) => index.toString()}
                        onEndReached={this.onScroll}
                        onEndReachedThreshold={0.5}
                        ListFooterComponent={this.state.showLoaderPageing && this.render_Activity_footer}
                        ListEmptyComponent={() => (
                            <View style={styles.emptyContainer}>
                                {!this.state.showSpinner &&
                                    <Text style={styles.emptyText}>No Record Found!</Text>
                                }
                            </View>
                        )}
                    />
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    rowContainer: {
        flexDirection: "row",
        alignItems: "center",
        padding: 15,
        backgroundColor: "white",
        width: '100%'
    },

    userImg: {
        height: 48,
        width: 48,
        borderRadius: 24,
        resizeMode: "cover"
    },

    detail: {
        width: "50%",
        justifyContent: "center",
        marginHorizontal: 15
    },

    time: {
        color: "#4c4c55",
        fontSize: 13,
        fontFamily: fonts.Poppins_Medium,
        opacity: 0.5
    },

    userName: {
        color: "#4c4c55",
        fontSize: 17,
        fontFamily: fonts.SFProDisplay_Bold,
    },

    btn: {
        paddingVertical: 8,
        paddingHorizontal: 18,
        borderRadius: 25,
        borderColor: "#f4f4f5",
        borderWidth: 2
    },

    unblock: {
        color: "#b0b0c3",
        fontSize: 14,
        fontFamily: fonts.SFProDisplay_Bold,
    },
    emptyContainer: {
        flex: 1,
        backgroundColor: "#edeff5",
        justifyContent: "center",
        alignItems: "center"
    },

    emptyText: {
        color: "#a5adc7",
        fontSize: 16,
        fontFamily: fonts.SFProMedium
    }


});