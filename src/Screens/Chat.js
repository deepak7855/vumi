import React, { Component } from 'react'
import { Text, View, FlatList, Alert, Image, TouchableOpacity, TextInput, SafeAreaView, LayoutAnimation, Keyboard, Platform, Dimensions, DeviceEventEmitter, ActivityIndicator, StyleSheet, StatusBar } from 'react-native'
import DeviceInfo from 'react-native-device-info';
import { GHeader, Images, colors } from '../common';
import fonts from '../common/fonts';
import { hasNotch } from 'react-native-device-info';

import SocketConnect from '../common/SocketConnect';
import Helper from '../lib/Helper';
import Spinner from 'react-native-loading-spinner-overlay';
import Toast from 'react-native-root-toast';
import Config from '../lib/Config';
import ImageLoadView from '../components/ImageLoadView';
const ScreenWidth = Dimensions.get('window').width;

export default class Chat extends Component {

    constructor(props) {
        super(props);
        this.state = {
            message: "",
            showSpinner: false, allChatList: [],
            first_message_id: 0,
            isLoader: false,
            isFirstLoader: true,
            last_Id: '',
            showMenu: false,
            isBlocked: false, isBlockedByMe: false,
            KeyboardHeight: 0

        };
        this.getChatHistory("after", "");
        this.getCheckUserStatus();

    }

    _keyboardDidShow(e) {
        const visibleHeight = e.endCoordinates.height + 10;
        this.setState({ KeyboardHeight: visibleHeight });
    }
    _keyboardDidHide() {
        LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
        this.setState({ KeyboardHeight: 0 })
    }

    componentDidMount() {
        // this.focusListener = this.props.navigation.addListener("didFocus", () => {
        // });
        this.functionevevnt = this.props.navigation.addListener('didFocus', () => {
            this.updateStatusBar();
        });
        if (Platform.OS === 'ios') {
            this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this._keyboardDidShow.bind(this));
            this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this._keyboardDidHide.bind(this));
        }

        this.GetThreadHistory = DeviceEventEmitter.addListener('get_user_thread_response', response => this.GetThreadHistoryList(response));
        this.MultiUserList = DeviceEventEmitter.addListener('multi_user', response => this.GetMultiList(response));
        this.ReceiveMessage = DeviceEventEmitter.addListener('receive_message', response => this.GetReceiveMessageList(response));
        this.UserCheckStatus = DeviceEventEmitter.addListener('check_user_status_response', response => this.UserCheckStatusResponse(response));
        this.UserBlockUnBlock = DeviceEventEmitter.addListener('user_block_unblock_response', response => this.UserBlockUnBlockResponse(response));

        if (SocketConnect.checkSocketConnected() == false) {
            SocketConnect.checkConnection('connect', cb => {
                console.log('SocketConnect------------', cb);
            });
        }


    }


    getChatHistory(chattype, last_Id) {
        let formdata = {
            limit: "20",
            user_id: Helper.userInfo.id,
            start: "-1",
            other_user_id: this.props.OtherUserId,
            last_id: last_Id,
            type: chattype,
        }
        // alert(JSON.stringify(formdata))

        if (SocketConnect.checkSocketConnected()) {
            //  console.log('timeout------------', page);
            // this.setState({ showSpinner: true })
            SocketConnect.socketDataEvent('get_user_thread', formdata);
        } else {

            SocketConnect.checkConnection('connect', cb => {
                console.log('SocketConnect------------', cb);
            });
            this.setState({ isFirstLoader: false });

            this.setState({ showSpinner: false })

        }
    }
    getCheckUserStatus() {
        let formdata = {
            user_id: Helper.userInfo.id,
            other_user_id: this.props.OtherUserId,
        }
        // alert(JSON.stringify(formdata))
        if (SocketConnect.checkSocketConnected()) {
            SocketConnect.socketDataEvent('check_user_status', formdata);
        } else {
            SocketConnect.checkConnection('connect', cb => {
                console.log('SocketConnect------------', cb);
            });
        }
    }

    GetThreadHistoryList(response) {

        // console.log('thread_response----', response);

        if (response.responseData.length > 0) {
            // response.responseData[response.responseData.length - 1].id;
            this.state.first_message_id = response.first_message_id;
            this.state.isLoader = false;
            this.state.isFirstLoader = false;
            let newmessage = [];
            let allChatList = this.state.allChatList;
            for (let msg of response.responseData) {
                if (!allChatList.some(allChatList => allChatList.id == msg.id)) {
                    newmessage.push(msg);
                }
            }
            this.setState({
                allChatList: [...this.state.allChatList, ...newmessage]
            });
        } else {
            this.state.isLoader = false;
            this.setState({ isFirstLoader: false });
        }



    }

    sendMessage = () => {
        // if (this.state.message.trim() != '') {
        //     this.setState({ message: '' })
        // }
        if (this.state.message.trim() != '') {
            let formdata = {
                device_type: Helper.device_type,
                device_token: Helper.device_token,
                message_type: "TEXT",
                msg: this.state.message,
                other_data: '',
                user_id: Helper.userInfo.id,
                thumb_image: "",
                other_user_id: this.props.OtherUserId,
            }
            SocketConnect.checkConnection('connect', cb => {
                console.log('SocketConnect------------', cb);
                if (SocketConnect.checkSocketConnected()) {
                    SocketConnect.socketDataEvent('send_message', formdata);
                    this.setState({ message: '' });
                } else {
                    Helper.showToast("You are offline")
                    SocketConnect.checkConnection('connect', cb => {
                        console.log('SocketConnect------------', cb);
                    });

                }
            });


            // SocketConnect.socketDataEvent('send_message', formdata);
            // this.setState({ message: '' });
        }
    }

    GetMultiList(response) {
        // console.log('multi_user-----', response);
        if (this.state.allChatList.length == 0) {
            this.state.first_message_id = response.responseData.id;
        }

        var sameId = false;
        this.state.allChatList.map((item) => {
            if (item.id == response.responseData.id) {
                sameId = true
            }
        });

        if (!sameId) {
            this.state.allChatList.unshift(response.responseData);
            this.setState({ change: true });
        }
    }

    GetReceiveMessageList(response) {
        // console.log('receive_message-----', response);
        if (this.props.OtherUserId != response.responseData.user_id) {
            return;
        }
        var sameId = false;
        this.state.allChatList.map((item) => {
            if (item.id == response.responseData.id) {
                sameId = true
            }
        });

        if (!sameId) {
            this.state.allChatList.unshift(response.responseData);
            this.setState({ change: true });
        }
        let formdata = {
            msg_id: response.responseData.id,
            is_read: 1,
            user_id: Helper.userInfo.id,
            other_user_id: this.props.OtherUserId,


        }
        SocketConnect.socketDataEvent('read_message_update', formdata);



    }
    UserCheckStatusResponse(response) {
        this.state.isBlocked = response.responseData.is_blocked === true ? true : false;
        this.state.isBlockedByMe = response.responseData.is_blocked_by_me === true ? true : false;
        this.HideMenu()

    }
    UserBlockUnBlockResponse(response) {
        if (response) {
            this.getCheckUserStatus()
            this.HideMenu()
            Toast.show(response.msg)

        }
    }




    gotoClickDot = () => {
        this.setState({ showMenu: !this.state.showMenu });
    }
    HideMenu = () => {
        this.setState({ showMenu: false });
    }
    _BlockUnBlock = () => {
        let BlockUnBlock = this.state.isBlockedByMe ? "Unblock" : "Block"
        this.state.isBlockedByMe ?
            this.gotoBlockUser()
            :
            Alert.alert(
                "Vumi",
                "Are you sure you want to " + BlockUnBlock + " " + this.props.OtherUserName + "?",
                [
                    {
                        text: "CANCEL",
                        onPress: () => this.HideMenu()
                    },
                    {
                        text: this.state.isBlockedByMe ? "Unblock" : "Block",
                        onPress: () => this.gotoBlockUser()
                    },

                ],
                { cancelable: false },
            )




    }
    gotoBlockUser() {
        let formdata = {
            user_id: Helper.userInfo.id,
            other_user_id: this.props.OtherUserId,
        }
        // alert(JSON.stringify(formdata))
        if (SocketConnect.checkSocketConnected()) {
            SocketConnect.socketDataEvent('user_block_unblock', formdata);
        } else {
            SocketConnect.checkConnection('connect', cb => {
                console.log('SocketConnect------------', cb);
            });

        }
    }
    componentWillUnmount() {
        if (this.functionevevnt) {
            this.functionevevnt.remove();
        }
        if (this.GetThreadHistory) {
            this.GetThreadHistory.remove();
        }
        if (this.MultiUserList) {
            this.MultiUserList.remove();
        }
        if (this.ReceiveMessage) {
            this.ReceiveMessage.remove();
        }
        if (this.UserCheckStatus) {
            this.UserCheckStatus.remove();
        }
        if (this.UserBlockUnBlock) {
            this.UserBlockUnBlock.remove();
        }

        if (Platform.OS === 'ios') {
            this.keyboardDidShowListener.remove();
            this.keyboardDidHideListener.remove();
        }
    }

    _renderItem = ({ item, index }) => {
        return (
            <View style={{ paddingHorizontal: 10 }}>

                {item.user_id == Helper.userInfo.id ?
                    <View style={{ width: "100%", }}>
                        {/* {
                            item.time && <Text style={styles.time}>{item.time}</Text>
                        } */}
                        <View style={styles.rightView}>
                            <View style={styles.rightTxtView}>
                                <Text style={styles.rightTxt}>{item.msg}</Text>
                            </View>
                            <ImageLoadView
                                resizeMode="cover"
                                style={styles.userImg}
                                source={item.user_image ? { uri: Config.imageUrl + item.user_image } : Images.user_dummy}

                            />
                        </View>
                    </View>
                    :
                    < View style={{ width: "100%", }}>
                        {/* {
                            item.time && <Text style={styles.time}>{item.time}</Text>
                        } */}

                        <View style={{ marginTop: 12, flexDirection: "row", }}>
                            {/* <Image
                                resizeMode="cover"
                                style={styles.userImg}
                                source={item.user_image ? { uri: Config.imageUrl + item.user_image } : Images.user_dummy}
                            /> */}

                            <ImageLoadView
                                resizeMode="cover"
                                style={styles.userImg}
                                source={item.user_image ? { uri: Config.imageUrl + item.user_image } : Images.user_dummy}

                            />
                            <View style={styles.leftMsgView}>
                                <Text style={styles.leftText}>{item.msg}</Text>
                            </View>

                        </View>
                    </View>


                }
            </View>
        )
    }


    onScrollpage = () => {
        if (!this.state.isLoader && this.state.first_message_id != this.state.allChatList[this.state.allChatList.length - 1].id) {

            this.state.isLoader = true
            this.setState({ isLoader: true });
            this.getChatHistory("before", this.state.allChatList[this.state.allChatList.length - 1].id);

        }
    }

    render() {
        return (
            <SafeAreaView style={{ backgroundColor: "white", flex: 1, paddingBottom: 10, }}>
                <StatusBar backgroundColor="white" />

                <GHeader
                    bArrow
                    greyBackButton
                    color="#606076"
                    backgroundColor="white"
                    title1={this.props.OtherUserName ? this.props.OtherUserName : null}
                    showDotButton
                    gotoClickDot={this.gotoClickDot}
                    rightIcon={this.state.showMenu ? Images.three_dot : Images.three_dot}

                // rightIcon={this.state.showMenu ? Images.grey_close : Images.three_dot}

                />
                {/* {this.state.isFirstLoader ? this._renderFooter : null} */}
                <View style={{ backgroundColor: "#f5f6fa", flex: 1, paddingBottom: Platform.OS === 'ios' ? this.state.KeyboardHeight : 0 }}>

                    <FlatList
                        onTouchEnd={() => { this.HideMenu(); }}
                        contentContainerStyle={{ flexGrow: 1 }}
                        data={this.state.allChatList}
                        ref={ref => this.flatList = ref}
                        keyboardShouldPersistTaps="handled"
                        showsVerticalScrollIndicator={false}
                        extraData={this.state}
                        inverted
                        renderItem={this._renderItem}
                        ListFooterComponent={this.state.isLoader ? this._renderFooter : <View />}
                        onEndReached={this.onScrollpage}
                        onEndReachedThreshold={0.5}
                        keyExtractor={(index) => { index.toString(); }}


                    />

                    {this.state.isBlocked || this.state.isBlockedByMe ?
                        <View style={{ backgroundColor: "#f5f6fa", width: "100%", paddingTop: 15 }}>
                            <View style={styles.messagebottomContainer}>
                                <Text style={styles.messagetext}>You can't message</Text>

                            </View>

                        </View>
                        :
                        <View style={{ backgroundColor: "#f5f6fa", width: "100%", paddingTop: 15 }}>
                            <View style={styles.bottomContainer}>
                                <TextInput
                                    placeholder="Write a message..."
                                    placeholderTextColor={'#989ca8'}
                                    style={styles.input}
                                    value={this.state.message}
                                    multiline
                                    onChangeText={(value) => { this.setState({ message: value }); }}
                                // onChangeText={(text) => this.setState({ message: text })}
                                />
                                <TouchableOpacity
                                    style={{ marginRight: 10 }}
                                    onPress={() => this.state.message.length > 0 && this.sendMessage()}>
                                    <Image resizeMode="cover"
                                        style={styles.sendBtn}
                                        source={Images.send}
                                    />
                                </TouchableOpacity>
                            </View>
                        </View>
                    }
                </View>
                {this.state.showMenu &&


                    <View style={[styles.menuCard, { marginTop: 10 }]}>
                        <TouchableOpacity
                            onPress={this._BlockUnBlock}
                            style={[styles.opationView, {}]}>

                            <View style={styles.viewOfMenuText}>
                                <Text style={styles.menuText}>{this.state.isBlockedByMe ? "Unblock" : "Block"}</Text>
                            </View>
                        </TouchableOpacity>

                    </View>

                }
            </SafeAreaView >
        );
    }

    _renderFooter = () => {
        var footer_View = (
            <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', padding: 10 }}>
                <ActivityIndicator size='small' color="#000000" />
            </View>
        );
        return footer_View;
    };
    updateStatusBar = () => {
        if (Platform.OS == 'android') {
            StatusBar.setBackgroundColor('white');

        }
        StatusBar.setBarStyle('dark-content');
    }

}

const styles = StyleSheet.create({
    userImg: { height: 32, width: 32, borderRadius: 32 / 2, marginBottom: 15, },
    leftMsgView: { backgroundColor: '#e1e3eb', paddingHorizontal: 10, paddingVertical: 5, borderRadius: 13, marginLeft: 10, alignSelf: "flex-start", maxWidth: "65%" },
    leftText: { color: '#6d7183', fontSize: 18, fontFamily: fonts.SFProMedium },
    time: { alignSelf: "center", marginBottom: 10, color: "#6f6f6f", fontFamily: fonts.SFProDisplay_Semibold, marginTop: 15 },
    rightView: { marginTop: 12, flexDirection: "row", justifyContent: "flex-end" },
    rightTxtView: { backgroundColor: '#628cff', paddingHorizontal: 10, paddingVertical: 5, borderRadius: 13, alignSelf: "flex-start", maxWidth: '70%', marginRight: 10 },
    rightTxt: { color: 'white', fontSize: 18, fontFamily: fonts.SFProMedium },
    bottomContainer: { backgroundColor: "white", alignItems: "center", flexDirection: "row", marginBottom: 5, width: '90%', alignSelf: "center", justifyContent: "space-between", borderRadius: 25, paddingVertical: 10, maxHeight: 100, },
    input: { paddingHorizontal: 15, paddingVertical: 0, color: '#989ca8', fontFamily: fonts.SFProRegular, borderRadius: 25, width: '85%', },
    sendBtn: { height: 24, width: 24, resizeMode: "contain" },
    menuCard: {
        position: "absolute", marginHorizontal: 15, padding: 15, borderColor: colors.lightgrey,
        borderWidth: 1, borderRadius: 10, backgroundColor: "#ffffff", width: "50%", right: 1,
        top: Platform.OS === "ios" ? hasNotch() ? 48 * 2 : 38 * 2 : StatusBar.currentHeight + 25,
    },
    opationView: {
        flexDirection: "row", alignItems: "center"
    },
    viewOfMenuText: {
        marginLeft: 10
    },
    menuText: {
        fontSize: 16,
        color: "#4A4A4A"
    },
    messagebottomContainer: {
        height: 45,
        backgroundColor: "white", alignItems: 'center', justifyContent: 'center', flexDirection: "row", marginBottom: 5, marginHorizontal: 15, borderRadius: 25,
    },

    messagetext: {
        fontSize: 16,
        textAlign: 'center',
        color: "#4A4A4A"
    },
});