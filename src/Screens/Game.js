import React, { Component } from 'react'
import { Text, View, StyleSheet, StatusBar, SafeAreaView, Image, Animated } from 'react-native'
import { CoinAndSwitchComponent } from '../components/Watch/CoinAndSwitchComponent';
import { Images } from '../common/Images';
import { Colors } from '../common/Colors';
import Helper from '../lib/Helper';



export class Game extends Component {
    constructor(props) {
        super(props);
        this.state = {
            fadeAnimation: new Animated.Value(0),
            showSpinner:false
        };
    }
    render() {
        return (
            <SafeAreaView style={styles.container}>
                <View style={styles.container1}>
                    <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center', }}>
                        <Image
                            source={Images.coin}
                            style={styles.coin}
                        />
                        <Text style={styles.coinText}>{Helper.userSettings.user_coins ? Helper.userSettings.user_coins : "0"}</Text>
                    </View>
                    <View style={{ flex: 2 }}>
                        <Text style={styles.comingsoon}>COMING SOON</Text>

                    </View>
                    <View style={{ flex: 1 }}></View>



                </View>

                <Animated.View
                    style={[
                        styles.fadingContainer,
                        {
                            opacity: this.state.fadeAnimation
                        }
                    ]}
                >
                    <Text style={styles.fadingText}>INSERT (1) VIEW TO PLAY</Text>
                </Animated.View>
            </SafeAreaView>
        )
    }



    componentDidMount() {
        this.functionevevnt = this.props.navigation.addListener('didFocus', () => {
            StatusBar.setBackgroundColor('black');
            StatusBar.setBarStyle('light-content')
            this.getSettingApi()
        });
        // Animated.timing(this.state.fadeAnimation, {
        //     toValue: 1,
        //     duration: 4000
        // }).start();


        // Animated.timing(this.state.fadeAnimation, {
        //     toValue: 1,
        //     duration: 4000
        //   }).start();

        this.fadeIn()
    }
   
    componentWillUnmount() {
        this.functionevevnt.remove();
    }
    getSettingApi = () => {
        this.setState({ showSpinner: true });
        Helper.makeRequest({ url: 'settings', method: 'POST', data: '' }).then(
            resp => {
                if (resp.status == 'true') {
                    Helper.userSettings = resp.data;
                    this.setState({});

                } else {
                    Helper.userSettings = {};
                    this.setState({});
                }
                this.setState({ showSpinner: false });
            },
        );
    };
    fadeIn = () => {
        Animated.timing(this.state.fadeAnimation, {
            toValue: 1,
            duration: 1000
        }).start(() => {
            setTimeout(() => {
                this.fadeOut()
            }, 1000)
        })
    };

    fadeOut = () => {
        Animated.timing(this.state.fadeAnimation, {
            toValue: 0,
            duration: 1000
        }).start(() => {
            setTimeout(() => {
                this.fadeIn()
            }, 1000)
        });
    };
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: "black"
    },
    container1: {
        flexDirection: "row",
        alignItems: "center",
        paddingHorizontal: 10,
        paddingVertical: 8,
        height: 60,
        backgroundColor: "black"
    },
    coin: {
        height: 22,
        width: 22,
        resizeMode: "contain",
        marginRight: 10
    },
    coinText: {
        fontSize: 13,
        color: "white",
        textAlign: 'center'
    },
    comingsoon: {
        fontSize: 12,
        color: "blue",
        textAlign: 'center'
    },
    fadingContainer: {
        flex: 1,
        alignItems: 'center', justifyContent: 'center'

    },
    fadingText: {
        fontSize: 12,
        textAlign: "center",
        margin: 10,
        color: "#fff"
    },
});