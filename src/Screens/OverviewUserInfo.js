import React, { Component } from 'react';
import { View, Text, StyleSheet, TouchableOpacity, FlatList, Image, StatusBar } from 'react-native';
import { Actions } from 'react-native-router-flux'
import {
    Images, Colors, GHeader,
} from '../common';
import fonts from '../common/fonts';
import { ViewsTabRow } from '../components/overview/overViewUserInfo/ViewsTabRow';
import { CommentTabRow } from '../components/overview/overViewUserInfo/CommentTabRow';
import ViewPager from '@react-native-community/viewpager';
import Helper from '../lib/Helper';
import Toast from 'react-native-root-toast';
import Spinner from 'react-native-loading-spinner-overlay';
import pushToScreen from '../navigation/NavigationActions';
import { BlockModelCompo } from '../components/Watch/BlockModelCompo';
import SocketConnect from '../common/SocketConnect';
import { InfoModelCompo } from '../components/Watch/InfoModelCompo';



export default class OverviewUserInfo extends Component {

    constructor(props) {
        super(props);
        this.state = {
            selectedTab: 0,
            showSpinner: true,
            likersList: [],
            viewersList: [],
            commentList: [],
            current_page: 1,
            selectedRowDetail: {},
            showBlockModel: false,
            showInfoModel: false,
        };

    }

    onTabPress = (value) => {
        this.viewpager.setPage(value);
    }

    getLikes = () => {
        Helper.makeRequest({ url: "liker-list", method: "POST", data: { post_id: this.props.post_id } }).then((resp) => {
            if (resp.status == 'true') {
                this.setState({ likersList: resp.data })
            } else {
                Toast.show(resp.message)
            }
            this.setState({ showSpinner: false })
        })
    }

    getViews = () => {
        Helper.makeRequest({ url: "viewer-list", method: "POST", data: { post_id: this.props.post_id } }).then((resp) => {
            if (resp.status == 'true') {
                this.setState({ viewersList: resp.data })
            } else {
                Toast.show(resp.message)
            }
            this.setState({ showSpinner: false })
        })
    }

    getComments = () => {
        Helper.makeRequest({ url: `comment-list?page=${this.state.current_page}`, method: "POST", data: { post_id: this.props.post_id, is_unique: 1 } }).then((resp) => {
            if (resp.status == 'true') {
                if (this.state.current_page == 1) {
                    this.state.commentList = []
                }
                let commentList = this.state.commentList.concat(resp.data.data);
                this.setState({ commentList, last_page: resp.data.last_page, })
            } else {
                Toast.show(resp.message)
            }
            this.setState({ showSpinner: false })
        })
    }

    onEndReached = () => {
        if (this.state.current_page < this.state.last_page) {
            this.setState(
                {
                    current_page: this.state.current_page + 1,
                },
                () => {
                    this.getComments();
                },
            );
        }
    };

    onCommentIconPress = (post_id) => {
        pushToScreen('Comments', { post_id, fromOverView: true })
    };

    onUserImagePress = (item) => {
        this.setState({ showInfoModel: true, selectedRowDetail: item })
    }

    onBlock = (item) => {
        this.setState({ showBlockModel: false })
        let formdata = {
            user_id: Helper.userInfo.id,
            other_user_id: item.user.id,
        }
        if (SocketConnect.checkSocketConnected()) {
            SocketConnect.socketDataEvent('user_block_unblock', formdata);

            this.setState({
                likersList: [],
                viewersList: [],
                commentList: [],
            }, () => {
                this.getViews();
                this.getLikes();
                this.getComments();
            })

        } else {
            SocketConnect.checkConnection('connect', cb => {
                console.log('SocketConnect------------', cb);
            });
        }
    }

    onShowBlockModel = () => {
        this.setState({ showInfoModel: false }, () => {
            this.setState({ showBlockModel: true })
        })
    }


    goToChat = (item) => {
        this.setState({ showInfoModel: false }, () => {
            pushToScreen('Chat', { OtherUserId: item.user.id, OtherUserName: item.user.username });
        })
    }



    render() {
        return (
            <View style={{ flex: 1, backgroundColor: "white" }}>
                <StatusBar barStyle="dark-content" backgroundColor="white" />

                <GHeader
                    bArrow
                    greyBackButton
                    color="#606076"
                    backgroundColor="white"
                    title1={this.props.display_name}
                />
                <Text style={styles.like}>{this.props.total_likes} Likes / {this.props.total_views} Views</Text>

                <View style={styles.tabView}>
                    <TouchableOpacity onPress={() => this.onTabPress(0)} style={[styles.tabBtn, { backgroundColor: this.state.selectedTab == 0 ? "#716fff" : "white", }]}>
                        <Text style={[styles.tabText, { color: this.state.selectedTab == 0 ? "white" : '#c2c2d8', }]}>Views</Text>
                    </TouchableOpacity>

                    <TouchableOpacity onPress={() => this.onTabPress(1)} style={[styles.tabBtn, { backgroundColor: this.state.selectedTab == 1 ? "#ff66c4" : "white", }]}>
                        <Text style={[styles.tabText, { color: this.state.selectedTab == 1 ? "white" : '#c2c2d8', }]}>Likes</Text>
                    </TouchableOpacity>

                    <TouchableOpacity onPress={() => this.onTabPress(2)} style={[styles.tabBtn, { backgroundColor: this.state.selectedTab == 2 ? "#a983ff" : "white", }]}>
                        <Text style={[styles.tabText, { color: this.state.selectedTab == 2 ? "white" : '#c2c2d8', }]}>Comments</Text>
                    </TouchableOpacity>
                </View>

                <View style={{ height: 1, backgroundColor: "#F5F5F5", marginTop: 15, width: "100%" }} />

                <ViewPager
                    ref={(viewpager) => { this.viewpager = viewpager }}
                    onPageScroll={(e) => this.setState({ selectedTab: e.nativeEvent.position })}
                    style={styles.viewPager} initialPage={0}

                >
                    {/* viewer List */}
                    <View key="1">
                        <FlatList
                            contentContainerStyle={{ flexGrow: 1 }}
                            showsHorizontalScrollIndicator={false}
                            data={this.state.viewersList}
                            ItemSeparatorComponent={() => <View style={{ height: 10, backgroundColor: '#f5f6fa' }} />}
                            renderItem={({ item }) => <ViewsTabRow
                                fromViews={true}
                                item={item}
                                onCommentIconPress={(id) => this.onCommentIconPress(id)}
                                onUserImagePress={(item) => this.onUserImagePress(item)}
                            />}
                            keyExtractor={(item, index) => index}
                            ListEmptyComponent={() => (
                                <View style={{ flex: 1, backgroundColor: "#edeff5", justifyContent: "center", alignItems: "center" }}>
                                    <Text style={{ color: "#a5adc7", fontSize: 16, fontFamily: fonts.SFProMedium }}>You have no views yet!</Text>
                                </View>
                            )}
                        />
                    </View>
                    {/* Liker List */}
                    <View key="2">
                        <FlatList
                            contentContainerStyle={{ flexGrow: 1 }}
                            showsHorizontalScrollIndicator={false}
                            data={this.state.likersList}
                            ItemSeparatorComponent={() => <View style={{ height: 10, backgroundColor: '#f5f6fa' }} />}
                            renderItem={({ item }) => <ViewsTabRow
                                fromLikes={true}
                                item={item}
                                onCommentIconPress={(id) => this.onCommentIconPress(id)}
                                onUserImagePress={(item) => this.onUserImagePress(item)}
                            />}
                            keyExtractor={(item, index) => index}
                            ListEmptyComponent={() => (
                                <View style={{ flex: 1, backgroundColor: "#edeff5", justifyContent: "center", alignItems: "center" }}>
                                    <Text style={{ color: "#a5adc7", fontSize: 16, fontFamily: fonts.SFProMedium }}>You have no likes yet!</Text>
                                </View>
                            )}
                        />
                    </View>

                    {/* Comments List */}
                    <View key="3">
                        <FlatList
                            contentContainerStyle={{ flexGrow: 1 }}
                            showsHorizontalScrollIndicator={false}
                            data={this.state.commentList}
                            ItemSeparatorComponent={() => <View style={{ height: 10, backgroundColor: '#f5f6fa' }} />}
                            renderItem={({ item }) => <CommentTabRow
                                item={item}
                                onCommentIconPress={(id) => this.onCommentIconPress(id)}
                                onUserImagePress={(item) => this.onUserImagePress(item)}
                            />}
                            keyExtractor={(item, index) => index}
                            onEndReached={this.onEndReached}
                            onEndReachedThreshold={0.5}
                            ListEmptyComponent={() => (
                                <View style={{ flex: 1, backgroundColor: "#edeff5", justifyContent: "center", alignItems: "center" }}>
                                    <Text style={{ color: "#a5adc7", fontSize: 16, fontFamily: fonts.SFProMedium }}>You have no comments yet!</Text>
                                </View>
                            )}
                        />
                    </View>
                </ViewPager>
                <Spinner visible={this.state.showSpinner} />
                {
                    this.state.showBlockModel &&
                    <BlockModelCompo
                        showBlockModel={this.state.showBlockModel}
                        onBlock={(item) => this.onBlock(item)}
                        hideBlockModel={() => this.setState({ showBlockModel: false })}
                        selectedRowDetail={this.state.selectedRowDetail}
                    />
                }
                {
                    this.state.showInfoModel &&
                    <InfoModelCompo
                        showInfoModel={this.state.showInfoModel}
                        hideInfoModel={() => this.setState({ showInfoModel: false })}
                        onShowBlockModel={() => this.onShowBlockModel()}
                        goToChat={(item) => this.goToChat(item)}
                        selectedRowDetail={this.state.selectedRowDetail}
                    />
                }
            </View>
        );
    }
    componentDidMount = () => {
        this.getLikes();
        this.getViews();
        this.functionEvent = this.props.navigation.addListener('didFocus', () => {
            this.setState({ showSpinner: true }, () => {
                this.getComments()
            })
        });
        this.getComments()
    };
    compnentWillUnMount = () => {
        if (this.functionEvent) {
            this.functionEvent.remove();
        }
    }
}

const styles = StyleSheet.create({
    tabBtn: { paddingVertical: 10, paddingHorizontal: 10, borderRadius: 30, width: '32%', alignItems: "center" },
    tabText: { fontFamily: fonts.Poppins_Bold, fontSize: 15 },
    like: { color: "#aaaab7", fontFamily: fonts.Poppins_Medium, fontSize: 12, alignSelf: "center" },
    tabView: { flexDirection: "row", alignItems: "center", justifyContent: "space-around", marginTop: 15, marginHorizontal: 10 },

    viewPager: {
        flex: 1,
    },

});