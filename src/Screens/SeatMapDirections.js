import React, { Component } from 'react'
import { Text, View, ImageBackground, StatusBar, Image, StyleSheet, Dimensions } from 'react-native'
// import {Header} from '../common/GHeader'
// const { itemWidth, itemheight } = Dimensions.get('window');
import { Colors, string, Images, GHeader } from '../common'

export class SeatMapDirections extends Component {
    constructor(props) {
        super(props);
        this.state = {

        }
    }
    componentDidMount() {


    }
    componentWillUnmount() {

    }




    render() {
        return (
            <View>
                <StatusBar 
                // barStyle='default'
                     hidden={false}
                    backgroundColor="#fff"
                    translucent={true}
                />

                <ImageBackground
                    imageStyle={{ borderBottomLeftRadius: 15, borderBottomRightRadius: 15 }}
                    source={Images.Home.Group_Seat}
                    style={styles.SeatMapcontainer}>
                    <GHeader
                        Title={'Ant Man and The Wasp'}
                        colortext={'#fff'}
                        whiteTint={'#fff'}
                        Backgroun
                        bArrow

                    />
                    <View style={styles.mapContant}>
                        <View style={styles.mapContanttextviwe}>
                            <Image
                                resizeMode='contain'
                                source={Images.Home.clock}
                                style={styles.imag}
                            />
                            <Text style={[styles.AddTicket, { color: Colors.white, fontSize: 15, paddingHorizontal: 10, }]}>{string.mapdirecations.time}</Text>

                        </View>

                        <View style={styles.mapContant1}>
                            <Image
                                resizeMode='contain'
                                source={Images.Home.film}
                                style={styles.imag}
                            />
                            <Text style={[styles.AddTicket, { color: Colors.white, fontSize: 15, paddingHorizontal: 10, }]}>{string.mapdirecations.film}</Text>

                        </View>


                    </View>
                    <View style={styles.mapContant2} >
                        <Text style={[styles.AddTicket, { color: Colors.white, fontSize: 15, paddingHorizontal: 10, }]}>{string.mapdirecations.Screen}</Text>
                    </View>

                </ImageBackground>

                <View style={{ marginHorizontal: 15, marginVertical: 15, paddingVertical: 5, flexDirection: 'row', borderWidth: 1, borderColor: 'grey', borderRadius: 15, }}>

                    <View style={{ flex: 1, alignItems: 'center', marginVertical: 20, borderRightWidth: 1, borderRightColor: 'grey' }}>
                        <Text style={[styles.AddTicketsaddress, { color: Colors.blackgrey, fontSize: 15, }]}>{string.mapdirecations.Seat}</Text>
                        <Text style={[styles.AddTicketsaddress, { color: Colors.blackgrey, fontSize: 15, }]}>{string.mapdirecations.SeatD}</Text>

                    </View>

                    <View style={{ flex: 1, alignItems: 'center', marginVertical: 20, borderRightWidth: 1, borderRightColor: 'grey' }}>
                        <Text style={[styles.AddTicketsaddress, { color: Colors.blackgrey, fontSize: 15, }]}>{string.mapdirecations.StartIn}</Text>
                        <Text style={[styles.AddTicketsaddress, { color: Colors.blackgrey, fontSize: 15, }]}>{string.mapdirecations.mints}</Text>

                    </View>

                    <View style={{ flex: 1, alignItems: 'center', marginVertical: 20, }}>
                        <Text style={[styles.AddTicketsaddress, { color: Colors.blackgrey, fontSize: 15, }]}>{string.mapdirecations.Remaining}</Text>
                        <Text style={[styles.AddTicketsaddress, { color: Colors.blackgrey, fontSize: 15, }]}>{string.mapdirecations.mints1}</Text>

                    </View>
                </View>
            </View>
        )
    }
}
const styles = StyleSheet.create({
    SeatMapcontainer: {
        width: "100%",
        height: Dimensions.get('window').height / 1.4,
        borderRadius: 30,
        borderTopEndRadius: 20,
        resizeMode: 'contain',
    },
    imag: {
        width: 20,
        height: 20,
    },
    AddTicketsaddress: {
      //  fontFamily: "Metropolis-Medium"
    },
    AddTicket: {
       // fontFamily: "Metropolis-ExtraLight"
    },
    mapContant: {
        top: -5,
        flexDirection: 'row', alignItems: 'center', justifyContent: 'center',
    },
    mapContanttextviwe: {
        flexDirection: 'row', paddingLeft: 10,
    },
    mapContant1: {
        flexDirection: 'row', paddingLeft: 10,
    },
    mapContant2: {
        marginTop: 40, alignItems: 'center', justifyContent: 'center'
    }
})
