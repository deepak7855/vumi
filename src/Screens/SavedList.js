import React, { Component } from 'react';
import { View, Text, FlatList, Image, RefreshControl, DeviceEventEmitter, TouchableOpacity, StyleSheet, StatusBar, TouchableHighlight, ActivityIndicator } from 'react-native';
import { GHeader, Images } from '../common';
import fonts from '../common/fonts';
import { SavedAndPostRowCompo } from '../components/common/SavedAndPostRowCompo';
import { SwipeListView } from 'react-native-swipe-list-view';
import { Actions } from 'react-native-router-flux';
import pushToScreen from '../navigation/NavigationActions';
import Helper from '../lib/Helper';
import Toast from 'react-native-root-toast';
import Spinner from 'react-native-loading-spinner-overlay';
import FastImage from 'react-native-fast-image';
import Config from '../lib/Config';
import { BlockModelCompo } from '../components/Watch/BlockModelCompo';
import { InfoModelCompo } from '../components/Watch/InfoModelCompo';
import SocketConnect from '../common/SocketConnect';
import ImageLoadView from '../components/ImageLoadView';




export default class SavedList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showSpinner: false,
      arrSavePostList: [],
      first_post_id: '',
      showLoaderPageing: false,
      showLoaderRefresh: false,
      selectedRowDetail: {},
      showBlockModel: false,
      showInfoModel: false,
    };
  }
  componentDidMount() {
    this.getSavePostListData(true, "")
  }
  getSavePostListData = (spinner, last_id) => {
    this.setState({ showSpinner: spinner })
    Helper.makeRequest({ url: "saved-post-list", method: "POST", data: { last_id: last_id } }).then((resp) => {
      if (last_id == "") {
        this.state.arrSavePostList = []
      }
      if (resp.status == 'true') {
        if (resp.data && resp.data.data && resp.data.data.length > 0) {
          this.state.arrSavePostList = [...this.state.arrSavePostList, ...resp.data.data]
        }
        this.state.first_post_id = resp.data.first_id ? resp.data.first_id : ""
      } else {
        Toast.show(resp.message)
      }

      this.state.showLoaderPageing = false
      this.state.showLoaderRefresh = false
      this.state.showSpinner = false

      this.setState({})
    })
  }
  unlikeSavePost(id, index) {
    this.setState({ showSpinner: true })
    let data = {
      post_id: id,
    }
    Helper.makeRequest({ url: "post-save", method: "POST", data: data }).then((resp) => {
      if (resp.status == 'true') {
        this.state.arrSavePostList.splice(index, 1)
      } else {
      }
      Toast.show(resp.message)

      this.setState({ showSpinner: false })

    })

  }
  onScroll = () => {
    if (!this.state.showLoaderPageing && this.state.arrSavePostList.length > 0 && this.state.arrSavePostList[this.state.arrSavePostList.length - 1].id != this.state.first_post_id && this.state.first_post_id != '') {
      this.setState({ showLoaderPageing: true })
      this.getSavePostListData(false, this.state.arrSavePostList[this.state.arrSavePostList.length - 1].id);
    }
  }
  _pullToRefresh = () => {
    this.setState({ showLoaderRefresh: true })
    this.getSavePostListData(false, "");
  }
  renderSaveListView = (({ item, index }) => {
    let postDetails = (item.post && item.post.post_detail && item.post.post_detail.length > 0) ? item.post : ""
    if (postDetails == "") {
      return (<View />)
    }
    return (
      <View style={styles.rowContainer}>
        <TouchableOpacity onPress={() => this.setState({ showInfoModel: true, selectedRowDetail: item.post })}>
          <ImageLoadView
            resizeMode={"cover"}
            style={styles.userImg}
            source={postDetails.post_detail[0].thumbnail_media ? {
              uri: Config.imageUrl + postDetails.post_detail[0].thumbnail_media
            } : Images.User_profile_icon}
          />
        </TouchableOpacity>

        <TouchableOpacity onPress={() => pushToScreen('Saved', { postDetails: postDetails })} style={{ width: "70%", marginLeft: 5, padding: 3 }}>
          {postDetails.user && <Text numberOfLines={1} style={styles.userName}>{postDetails.display_name}</Text>}
          <View style={styles.userContainer}>
            <Text style={styles.grp}>{postDetails.title}</Text>

            <View style={styles.statusView}>
              <View style={styles.statusRow}>
                <Text style={styles.views}>{Helper.convertValueBillions(postDetails.total_views)}</Text>
                <Image
                  style={styles.ViewIng}
                  source={Images.eye_grey}
                />
              </View>

              <View style={styles.statusRow}>
                <Text style={styles.views}>{Helper.convertValueBillions(postDetails.total_likes)}</Text>
                <Image
                  style={styles.ViewIng}
                  source={Images.heart_grey1}
                />
              </View>
            </View>
          </View>
        </TouchableOpacity>
        <TouchableOpacity onPress={() => pushToScreen('Saved', { postDetails: postDetails })} style={styles.arrowContainer}>
          <Image
            source={Images.right_arrow}
            style={styles.arrowImg}
          />
        </TouchableOpacity>
      </View>
    )
  })

  render_Activity_footer = () => {
    var footer_View = (
      <View style={{ flex: 1, backgroundColor: '#ffffff', alignItems: 'center', justifyContent: 'center', padding: 15 }}>
        <ActivityIndicator size="large" color="#000000" />
      </View>
    );
    return footer_View;
  };
  gotoBackMessage = () => {
    Actions.pop();
    DeviceEventEmitter.emit("updatePost", "resp")
  }

  onBlock = (item) => {
    this.setState({ showBlockModel: false })
    let formdata = {
      user_id: Helper.userInfo.id,
      other_user_id: item.user.id,
    }
    if (SocketConnect.checkSocketConnected()) {
      SocketConnect.socketDataEvent('user_block_unblock', formdata);

      this.setState({
        arrSavePostList: []
      }, () => {
        this.getSavePostListData(true, "");
      })

    } else {
      SocketConnect.checkConnection('connect', cb => {
        console.log('SocketConnect------------', cb);
      });
    }
  }

  onShowBlockModel = () => {
    this.setState({ showInfoModel: false }, () => {
      this.setState({ showBlockModel: true })
    })
  }


  goToChat = (item) => {
    this.setState({ showInfoModel: false }, () => {
      pushToScreen('Chat', { OtherUserId: item.user.id, OtherUserName: item.user.username });
    })
  }



  render() {
    return (
      <View style={{ flex: 1, backgroundColor: '#f5f6fa', }}>
        <StatusBar backgroundColor="white" />
        <GHeader
          ProfileBack
          greyBackButton
          color="#606076"
          backgroundColor="white"
          title1="Saved"
          gotoBackMessage={() => this.gotoBackMessage()}

        />

        <View style={styles.flatlist}>
          <SwipeListView
            rightOpenValue={-45}
            disableRightSwipe
            contentContainerStyle={{ flexGrow: 1 }}
            refreshControl={
              <RefreshControl
                refreshing={this.state.showLoaderRefresh}
                onRefresh={this._pullToRefresh}
              />
            }
            renderHiddenItem={(rowData, rowMap) => {
              return (
                <View style={styles.hiddenContainer}>
                  <View style={{ width: "50%", backgroundColor: 'transparent' }} />
                  <TouchableOpacity onPress={() => this.unlikeSavePost(rowData.item.post_id, rowData.index)} style={styles.closeView}>
                    <Image
                      source={Images.close}
                      style={{ height: 55, width: 55, resizeMode: "contain", }}
                    />
                  </TouchableOpacity>
                </View>
              )
            }}
            data={this.state.arrSavePostList}
            ItemSeparatorComponent={() => <View style={{ height: 5, backgroundColor: '#f5f6fa' }} />}
            // renderItem={({ item, index }) => (
            //   <TouchableHighlight onPress={() => pushToScreen('Saved')}>
            //     <SavedAndPostRowCompo fromSavedList />
            //   </TouchableHighlight>
            // )}
            renderItem={this.renderSaveListView}
            keyExtractor={item => item.id}
            onEndReached={this.onScroll}
            onEndReachedThreshold={0.5}
            ListFooterComponent={this.state.showLoaderPageing && this.render_Activity_footer}
            ListEmptyComponent={() => (
              <View style={styles.listemptyContainer}>
                {!this.state.showSpinner &&
                  <View style={styles.listemptyContainer}>
                    <Text style={styles.emptyText}>You haven’t saved any posts yet…</Text>
                    <Image

                      style={styles.emptyImage}
                      source={Images.star}
                    />
                    <Text style={[styles.emptyText, { textAlign: "center" }]}>Tap the star button on posts{'\n'} to save them.</Text>

                  </View>
                }
              </View>

            )}
          />
        </View>

        {
          this.state.showBlockModel &&
          <BlockModelCompo
            showBlockModel={this.state.showBlockModel}
            onBlock={(item) => this.onBlock(item)}
            hideBlockModel={() => this.setState({ showBlockModel: false })}
            selectedRowDetail={this.state.selectedRowDetail}
          />
        }
        {
          this.state.showInfoModel &&
          <InfoModelCompo
            showInfoModel={this.state.showInfoModel}
            hideInfoModel={() => this.setState({ showInfoModel: false })}
            onShowBlockModel={() => this.onShowBlockModel()}
            goToChat={(item) => this.goToChat(item)}
            selectedRowDetail={this.state.selectedRowDetail}
          />
        }

        <Spinner visible={this.state.showSpinner} />

      </View>
    );
  }
}


const styles = StyleSheet.create({
  flatlist: {
    backgroundColor: '#f5f6fa',
    flex: 1,
    marginTop: 25
  },

  listemptyContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },

  emptyText: {
    color: "#a2a2b2",
    fontSize: 17,
    fontFamily: fonts.SFProMedium
  },

  emptyImage: {
    height: 120,
    width: 120,
    resizeMode: "contain",
    marginTop: 55,
    marginBottom: 30,
  },
  rowContainer: {
    backgroundColor: "white",
    paddingHorizontal: 10,
    paddingVertical: 15,
    flexDirection: "row"
  },

  userImg: {
    height: 60,
    width: 60,
    // borderWidth:0.5
  },

  roundUserImg: {
    height: 40,
    width: 40,
    borderRadius: 20,
    resizeMode: "cover"
  },

  userName: {
    color: "#4c4c55",
    fontSize: 17,
    fontFamily: fonts.SFProDisplay_Semibold
  },

  grp: {
    color: "#919191",
    fontSize: 12,
    fontFamily: fonts.SFProRegular,
    width: "45%"
  },

  statusView: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    width: "50%",
    paddingRight: 10
  },

  statusRow: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center"
  },

  views: {
    color: "#919191",
    fontSize: 12,
    fontFamily: fonts.SFProRegular
  },

  ViewIng: {
    height: 10,
    width: 15,
    marginLeft: 5,
    resizeMode: "contain"
  },

  arrowContainer: {
    alignItems: "flex-end",
    justifyContent: "center",
    flex: 1,
    paddingRight: 5
  },

  arrowImg: {
    height: 15,
    width: 8,
    resizeMode: "contain"
  },

  userContainer: {
    flexDirection: "row",
    justifyContent: "space-between", marginTop: 5
  },
  index: { marginRight: 10, textAlignVertical: "center", color: "#9d9da5", fontSize: 16, fontFamily: fonts.SFProDisplay_Semibold },
  hiddenContainer: { flexDirection: "row", alignItems: 'center', justifyContent: "space-between", flex: 1, },
  closeView: { width: 45, height: "100%", justifyContent: "center", alignItems: "center", backgroundColor: '#ff5555', }



});