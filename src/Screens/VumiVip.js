import React, { Component } from 'react';
import { View, Text, FlatList, Image, TouchableOpacity, DeviceEventEmitter, StyleSheet, StatusBar, ImageBackground, Dimensions, ScrollView, Platform, ActivityIndicator } from 'react-native';
import { colors, GHeader, Images } from '../common';
import fonts from '../common/fonts';
import { ProgressBarSecond } from '../components/ProgressBarSecond';
import { PlanButtonCompo } from '../components/vumiVip/PlanButtonCompo';
import { Actions } from 'react-native-router-flux';
import pushToScreen from '../navigation/NavigationActions';
import Toast from 'react-native-root-toast';
import Spinner from 'react-native-loading-spinner-overlay';
import Config from '../lib/Config';
import Helper from '../lib/Helper';
// import InappPurchase from '../lib/InappPurchase'

const screenWidth = Dimensions.get('window').width;
const color = ['#6260ff', '#e555ff', '#04e6a0', 'rgb(29,31,52)'];
const planColor = ['#4c4ad6', '#c437dd', '#15ce95', 'black'];
const planTypes = ["blue", 'purple', 'green', 'black'];
const itemSubs = Platform.select({
    ios: ['com.vumi.fastpass', 'com.vumi.onemonth.nonnewing', 'com.vumi.sixmonth.nonnewing', 'com.vumi.twelvemonth.nonnewing'],
    android: ['com.vumi.fastpass.android', 'com.vumi.onemonth.nonnewing.android', 'com.vumi.sixmonth.nonnewing.android', 'com.vumi.twelvemonth.nonnewing.android'],
});

import RNIap, {
    Product,
    ProductPurchase,
    PurchaseError,
    acknowledgePurchaseAndroid,
    purchaseErrorListener,
    purchaseUpdatedListener,
} from 'react-native-iap';
export default class VumiVip extends Component {
    constructor(props) {
        super(props);
        this.state = {
            activepage: 0,
            selectedPlan: -1,
            activeBgColor: color[0],
            activePlanColor: planColor[0],
            activePlanType: planTypes[0],
            arrVipPackages: [],
            showIapIndicator: false
        };
    }
    async componentDidMount() {
        console.log('Helper.userSettings', Helper.userSettings)
        try {
            const result = await RNIap.initConnection();
            await RNIap.consumeAllItemsAndroid();
        } catch (err) {
            console.log('error in cdm => ', err);
        }
        this.getVipPackagesApi()


        this.purchaseUpdateSubscription = purchaseUpdatedListener(async (purchase: SubscriptionPurchase) => {
            console.log('purchaseUpdatedListener', purchase);
            this.setState({ showIapIndicator: false })
            const receipt = purchase.transactionReceipt;
            // console.log('receipt', receipt);
            if (receipt) {
                if (Platform.OS === 'ios') {
                    await RNIap.finishTransactionIOS(purchase.transactionId);
                } else if (Platform.OS === 'android') {
                    // If consumable (can be purchased again)
                    await RNIap.consumePurchaseAndroid(purchase.purchaseToken);
                    // If not consumable
                    await RNIap.acknowledgePurchaseAndroid(purchase.purchaseToken);
                }

                this.methodPurchaseVip(purchase.transactionId)

            }
        });

        this.purchaseErrorSubscription = purchaseErrorListener((error: PurchaseError) => {
            this.setState({ showIapIndicator: false })
            console.warn('purchaseErrorListener', error);
        });


    }
    componentWillUnmount() {
        if (this.purchaseUpdateSubscription) {
            this.purchaseUpdateSubscription.remove();
            this.purchaseUpdateSubscription = null;
        }
        if (this.purchaseErrorSubscription) {
            this.purchaseErrorSubscription.remove();
            this.purchaseErrorSubscription = null;
        }
    }

    DotView = () => {
        return (
            <View style={{ flexDirection: "row", justifyContent: "center", paddingVertical: 10, }}>
                {
                    [1, 2, 3, 4].map((item, index) => (
                        <View style={{ height: 10, width: 10, borderRadius: 5, marginLeft: 5, backgroundColor: this.state.activepage == index ? '#b9b7d9' : "#03022b" }} />
                    ))
                }
            </View>
        )
    }

    handlePageChange = (e) => {
        var offset = e.nativeEvent.contentOffset;
        if (offset) {
            var page = Math.ceil(offset.x / Dimensions.get('window').width);
            this.setState({ activepage: page, activeBgColor: color[page], activePlanColor: planColor[page], activePlanType: planTypes[page] }, () => {
                console.log(this.state.activePlanType)
            })
        }
    }

    onPlanSelect = (planId) => {
        this.setState({ selectedPlan: planId })
        // alert('yes')
    }

    getVipPackagesApi = () => {
        this.setState({ showSpinner: true });
        Helper.makeRequest({ url: 'get-vip-package', method: 'POST', data: '' }).then(
            resp => {
                if (resp.status == 'true') {
                    if (resp.data && resp.data.length > 0) {
                        this.setState({ arrVipPackages: resp.data, selectedPlan: resp.data[0].id });
                    }
                    else {
                        this.setState({});
                    }
                } else {
                    this.setState({});
                }
                this.setState({ showSpinner: false });
            },
        );
    };

    requestPurchase = async (sku): void => {
        try {
            console.log('requestPurchaserequestPurchase', sku)
            RNIap.requestPurchase(sku);
        } catch (err) {
            this.setState({ showIapIndicator: false })
            console.log('requestPurchase error => ', err);
        }
    };

    getItems = async (): void => {
        try {
            console.log('itemSkus[0]', itemSkus[0]);
            const products: Product[] = await RNIap.getProducts(itemSkus);
            this.requestPurchase(itemSkus[0]);
        } catch (err) {
            this.setState({ showIapIndicator: false })
            console.log('getItems || purchase error => ', err);
        }
    };

    requestSubscription = async (sku) => {

        console.log("sku-------", sku)
        // InappPurchase.methodInit()

        try {
            if (Platform.OS == 'ios') {
            await this.getSubscriptions();
            await RNIap.requestSubscription(sku);
            } else {
                this.getItems();
            }
        } catch (err) {
            alert(err.toLocaleString());
        }
    };

    async getSubscriptions() {
        try {
            const products = await RNIap.getSubscriptions(itemSubs);

            console.log('Products => ', products);
            // this.setState({ productList: products });
        } catch (err) {
            alert('alert2', JSON.stringify(err))
            console.log('getSubscriptions error => ', err);
        }
    }
    methodPurchaseVipPackagesApi = () => {
        // InappPurchase.requestSubscription("com.vumi.onemonth.nonnewing")

        this.setState({ showIapIndicator: true }, () => {
            if (this.state.selectedPlan == -1) {
                return
            } else if (this.state.selectedPlan == 1) {
                this.requestSubscription(itemSubs[1])
            } else if (this.state.selectedPlan == 2) {
                this.requestSubscription(itemSubs[2])
            } else if (this.state.selectedPlan == 3) {
                this.requestSubscription(itemSubs[3])
            }
        })




        return


        // this.setState({ showSpinner: true });
        // Helper.makeRequest({ url: 'purchase-vip-package', method: 'POST', data: { "vip_package_id": this.state.selectedPlan, "transaction_id": (new Date().getMilliseconds() + 1) } }).then(
        //     resp => {
        //         console.log('purchase-vip-package------------------------------', resp);

        //         if (resp.status == 'true') {
        //             Helper.userInfo.is_vip = 1
        //             Helper.saveDataInAsync("userInfo", Helper.userInfo);


        //         } else {
        //         }
        //         Helper.showToast(resp.message);

        //         this.setState({ showSpinner: false });
        //     },
        // );
    };

    methodPurchaseVip(transactionId) {
        this.setState({ showSpinner: true });
        Helper.makeRequest({ url: 'purchase-vip-package', method: 'POST', data: { "vip_package_id": this.state.selectedPlan, "transaction_id": transactionId } }).then(
            resp => {
                console.log('purchase-vip-package------------------------------', resp);
                this.setState({ showSpinner: false });

                Helper.showToast(resp.message);

                if (resp.status == 'true') {
                    Helper.userInfo.is_vip = 1
                    Helper.saveDataInAsync("userInfo", Helper.userInfo);
                    DeviceEventEmitter.emit("updatePost", "resp")

                    Actions.pop()

                } else {
                }

            },
        );
    }


    onUnlock = () => {
        Helper.makeRequest({ url: "purchase-temp-vip-package", method: "POST", data: null }).then((resp) => {
            if (resp.status == 'true') {
                Toast.show('Eligible for VIP Sucessfully');
                this.props.navigation.state.params.onBackAfterUnlock()
                Actions.pop();
            } else {
                Toast.show(resp.message)
            }
            this.setState({ showSpinner: false })
        })
    }


    render() {
        return (
            <View style={{ flex: 1, backgroundColor: 'white', }}>
                <StatusBar backgroundColor="white" />
                <GHeader
                    bArrow
                    greyBackButton
                    color="#606076"
                    backgroundColor="white"
                    title1="Vumi VIP"
                />
                <ScrollView contentContainerStyle={{ flexGrow: 1 }}>
                    <FlatList
                        horizontal
                        data={[0, 1, 2, 3]}
                        pagingEnabled
                        showsHorizontalScrollIndicator={false}
                        scrollEventThrottle={16}
                        onMomentumScrollEnd={this.handlePageChange}
                        renderItem={({ item, index }) => (<View style={{ flex: 1 }}>
                            {
                                index == 0 && <>
                                    <ImageBackground
                                        source={Images.slide1}
                                        style={{ height: screenWidth + 80, width: screenWidth }}>

                                        {this.DotView()}

                                        <Text style={{ color: "white", fontSize: 28, fontFamily: fonts.Poppins_Bold, marginHorizontal: 10 }}>Get more views, faster</Text>
                                        <Text style={{ color: "white", fontSize: 17, fontFamily: fonts.Poppins_Medium, marginHorizontal: 10 }}>With Vumi VIP, get up to 9 coins per view.</Text>
                                        <View style={{ alignItems: "flex-end", marginRight: 20, }}>
                                            <View style={{ marginTop: screenWidth / 5 }}>

                                                {/* <View style={{ flexDirection: "row" }}>
                                                <Text style={{ color: "white", fontSize: 45, fontFamily: fonts.Poppins_Bold, marginTop: 36 }}>X</Text>
                                                <Text style={{ color: "white", fontSize: 80, fontFamily: fonts.Poppins_Bold, }}>3</Text>
                                            </View> */}
                                                <Image style={{ height: 90, width: 90 }} resizeMode={'contain'} source={Images.X3}></Image>
                                                <Text style={{ color: "white", fontSize: 12, fontFamily: fonts.Poppins_Bold, }}>COIN BOOSTER!</Text>
                                            </View>
                                        </View>

                                    </ImageBackground>
                                </>
                            }

                            {
                                index == 1 &&
                                <ImageBackground
                                    source={Images.slide2}
                                    style={{ height: screenWidth + 80, width: screenWidth }}>

                                    {this.DotView()}

                                    <Text style={{ color: "white", fontSize: 28, fontFamily: fonts.Poppins_Bold, marginHorizontal: 10, width: '65%' }}>2 Fast Passes per month</Text>
                                    <Text style={{ color: "white", fontSize: 17, fontFamily: fonts.Poppins_Medium, marginHorizontal: 10, width: '75%' }}>Skip to the front of the queue, and get your views faster!</Text>
                                    <View style={{ alignItems: "flex-end", marginRight: 20 }}>
                                        {/* <View style={{ flexDirection: "row", marginTop: screenWidth / 8 }}>
                                            <Text style={{ color: "white", fontSize: 45, fontFamily: fonts.Poppins_Bold, marginTop: 36 }}>X</Text>
                                            <Text style={{ color: "white", fontSize: 80, fontFamily: fonts.Poppins_Bold, }}>2</Text>
                                        </View> */}
                                        <Image style={{ height: 90, width: 90 }} resizeMode={'contain'} source={Images.X2}></Image>

                                        <Text style={{ color: "white", fontSize: 12, fontFamily: fonts.Poppins_Bold, marginRight: 10 }}>FAST PASS</Text>
                                        <Text style={{ color: "white", fontSize: 10, fontFamily: fonts.Poppins_Bold, marginRight: 20 }}>PER MO.</Text>

                                    </View>

                                </ImageBackground>
                            }
                            {
                                index == 2 &&
                                <ImageBackground
                                    source={Images.slide3}
                                    style={{ height: screenWidth + 80, width: screenWidth }}>

                                    {this.DotView()}

                                    <Text style={{ color: "white", fontSize: 28, fontFamily: fonts.Poppins_Bold, marginHorizontal: 10 }}>Extended Team Bonus</Text>
                                    <Text style={{ color: "white", fontSize: 17, fontFamily: fonts.Poppins_Medium, marginHorizontal: 10 }}>Keep your teammates twice as long  and get bigger bonuses.</Text>
                                    <View style={{ alignItems: "flex-start", marginLeft: 20, }}>
                                        {/* <View style={{ flexDirection: "row", marginTop: screenWidth / 6 }}>
                                            <Text style={{ color: "white", fontSize: 45, fontFamily: fonts.Poppins_Bold, marginTop: 36 }}>X</Text>
                                            <Text style={{ color: "white", fontSize: 80, fontFamily: fonts.Poppins_Bold, }}>2</Text>
                                        </View> */}
                                        <Image style={{ height: 90, width: 90 }} resizeMode={'contain'} source={Images.X2}></Image>

                                        <Text style={{ color: "white", fontSize: 12, fontFamily: fonts.Poppins_Bold, marginLeft: 10 }}>LONGER</Text>
                                        <Text style={{ color: "white", fontSize: 10, fontFamily: fonts.Poppins_Bold, marginLeft: 3 }}>TEAM BONUS!</Text>
                                    </View>

                                </ImageBackground>
                            }
                            {
                                index == 3 &&
                                <ImageBackground
                                    source={Images.slide4}
                                    style={{ height: screenWidth + 80, width: screenWidth }}>

                                    {this.DotView()}

                                    <Text style={{ color: "white", fontSize: 28, fontFamily: fonts.Poppins_Bold, marginHorizontal: 10 }}>Cash in bigger packs</Text>
                                    <Text style={{ color: "white", fontSize: 17, fontFamily: fonts.Poppins_Medium, marginHorizontal: 10 }}>Get packs that range from 100K to 1 MILLION!</Text>
                                    <View style={{ alignItems: "center", marginTop: screenWidth / 3, justifyContent: 'center' }}>
                                        {/* <Text style={{ color: "white", fontSize: 40, fontFamily: fonts.Poppins_Bold, }}>100K</Text> */}
                                        <Image style={{ height: 90, width: 90 }} resizeMode={'contain'} source={Images.K100}></Image>

                                        <Text style={{ color: "white", fontSize: 18, fontFamily: fonts.Poppins_Bold, marginTop: -25 }}>Views</Text>
                                    </View>

                                </ImageBackground>
                            }


                        </View>)}
                    />

                    <View style={{ flexDirection: "row", width: "33.33%", justifyContent: "space-between", marginRight: 1 }}>

                        {
                            this.state.arrVipPackages.map((item, index) => (
                                <PlanButtonCompo
                                    onPlanSelect={this.onPlanSelect}
                                    activeBgColor={this.state.activeBgColor}
                                    planName={item.title}
                                    planColor={this.state.activePlanColor}
                                    planType={this.state.activePlanType}
                                    month={item.months}
                                    price={item.price}
                                    packageId={item.id}
                                    selectedPlan={this.state.selectedPlan}
                                />
                            ))
                        }
                        {/* <PlanButtonCompo
                            onPlanSelect={this.onPlanSelect}
                            activeBgColor={this.state.activeBgColor}
                            planName={'Best Deal'}
                            planColor={this.state.activePlanColor}
                            planType={this.state.activePlanType}
                            month={12}
                            selectedPlan={this.state.selectedPlan}
                        />

                        <PlanButtonCompo
                            onPlanSelect={this.onPlanSelect}
                            planName={'Most Popular'}
                            month={6}
                            planColor={this.state.activePlanColor}
                            activeBgColor={this.state.activeBgColor}
                            selectedPlan={this.state.selectedPlan}
                            planType={this.state.activePlanType}
                        />

                        <PlanButtonCompo
                            onPlanSelect={this.onPlanSelect}
                            planName={'Try-Out'}
                            planColor={this.state.activePlanColor}
                            activeBgColor={this.state.activeBgColor}
                            month={1}
                            selectedPlan={this.state.selectedPlan}
                            planType={this.state.activePlanType}
                        /> */}
                    </View>


                    <TouchableOpacity disabled={this.state.showIapIndicator} onPress={() => { this.methodPurchaseVipPackagesApi() }} style={[styles.btn, { backgroundColor: "#753ef0", }]}>
                        {
                            this.state.showIapIndicator ?
                                <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', }}>
                                    <Text style={{ fontFamily: fonts.SFProRegular, color: colors.white }}> Processing </Text>
                                    <ActivityIndicator color={colors.white} />
                                </View>
                                :
                                <Text style={styles.btnTxt}>Purchase</Text>
                        }
                    </TouchableOpacity>

                    {/* <TouchableOpacity onPress={() => { pushToScreen('AddCards') }} style={[styles.btn, { backgroundColor: "#15ce95", }]} >
                        <Text style={styles.btnTxt}>Renew VIP</Text>
                    </TouchableOpacity>

                    <TouchableOpacity onPress={() => { }} style={[styles.btn, { backgroundColor: "#f03e67" }]} >
                        <Text style={styles.btnTxt}>Cancel VIP</Text>
                    </TouchableOpacity> */}


                    <View style={{ backgroundColor: "#f5f6fa", paddingVertical: 15 }}>
                        <View style={styles.whiteCard}>
                            <View style={{ flexDirection: "row", justifyContent: "center" }}>
                                <Text style={styles.boldTxt}>Collect </Text>
                                <Text style={styles.lightTxt}>10,000 </Text>
                                <Text style={styles.vc}>vc </Text>
                                <Text style={styles.boldTxt}>to Unlock 3-day </Text>
                                <Text style={styles.lightTxt}>Free Trial </Text>
                            </View>

                            <View style={{ paddingVertical: 10, flexDirection: "row" }}>
                                <Image
                                    source={Images.coin}
                                    style={styles.progressCoin}
                                />
                                <View style={{ width: "90%" }}>
                                    <ProgressBarSecond
                                        total={10000}
                                        completed={Number(Helper.userSettings.user_coins) > 10000 ? 10000 : Number(Helper.userSettings.user_coins)}
                                        actualValue={Number(Helper.userSettings.user_coins)}
                                        colorCompleted={'#3fefc6'}
                                        colorUncompleted={'#eaedf4'} />
                                </View>
                            </View>
                        </View>

                        <View style={styles.whiteCard}>
                            <View style={{ flexDirection: "row", justifyContent: "center" }}>
                                <Text style={styles.boldTxt}>Unlock 3-day </Text>
                                <Text style={styles.lightTxt}>Free Trial </Text>
                            </View>

                            <TouchableOpacity disabled={Number(Helper.userSettings.user_coins) < 10000} onPress={() => { this.onUnlock() }} style={[styles.unlockBtn, { opacity: Number(Helper.userSettings.user_coins) < 10000 ? .6 : 1, }]}>
                                <Text style={styles.unlockTxt}>Unlock</Text>
                            </TouchableOpacity>
                        </View>

                    </View>

                </ScrollView>
                <Spinner visible={this.state.showSpinner} />

            </View>


        );
    }
}


const styles = StyleSheet.create({
    btn: { width: "90%", justifyContent: "center", alignItems: "center", height: 80, marginVertical: 20, alignSelf: "center", borderRadius: 20 },
    btnTxt: { color: "white", fontSize: 18, fontFamily: fonts.Poppins_Bold },
    whiteCard: { width: "90%", backgroundColor: "white", justifyContent: "center", alignItems: "center", height: 80, marginVertical: 10, alignSelf: "center", borderRadius: 20 },
    vc: { color: "#7c7c7c", fontSize: 9, fontFamily: fonts.Poppins_Bold },
    boldTxt: { color: "#b2b2b2", fontSize: 13, fontFamily: fonts.Poppins_Bold },
    lightTxt: { color: "#7c7c7c", fontSize: 13, fontFamily: fonts.Poppins_Bold },
    progressCoin: { height: 25, width: 25, resizeMode: "contain", marginRight: -15, zIndex: 1, },
    unlockBtn: { paddingVertical: 5, paddingHorizontal: 18, backgroundColor: "#2cdda6", borderRadius: 25 },
    unlockTxt: { color: "white", fontSize: 18, fontFamily: fonts.Poppins_Bold },

});