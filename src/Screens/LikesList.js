import React, { Component } from 'react';
import { View, Text, FlatList } from 'react-native';
import Spinner from 'react-native-loading-spinner-overlay';
import Toast from 'react-native-root-toast';
import { GHeader } from '../common';
import fonts from '../common/fonts';
import { ViewsTabRow } from '../components/overview/overViewUserInfo/ViewsTabRow';
import Helper from '../lib/Helper';

class LikesList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            likersList: [],
            showSpinner:true
        };
    }

    getLikes = () => {
        this.setState({ showSpinner: true })
        Helper.makeRequest({ url: "liker-list", method: "POST", data: { post_id: this.props.post_id } }).then((resp) => {
            if (resp.status == 'true') {
                if (resp.data.length > 0) {
                    this.setState({ likersList: resp.data, showMessage: '' })
                } else {
                    this.setState({ showMessage: 'You have no likes yet!' })
                }
            } else {
                Toast.show(resp.message)
            }
            this.setState({ showSpinner: false })
        })
    }

    render() {
        return (
            <View style={{ flex: 1, }}>
                <GHeader
                    bArrow
                    greyBackButton
                    color="#606076"
                    backgroundColor="white"
                    title1="Likes"
                />
                <FlatList
                    contentContainerStyle={{ flexGrow: 1 }}
                    data={this.state.likersList}
                    ItemSeparatorComponent={() => <View style={{ height: 10, backgroundColor: '#f5f6fa' }} />}
                    renderItem={({ item }) => <ViewsTabRow fromLikes={true} item={item} />}
                    keyExtractor={(item, index) => index}
                    ListEmptyComponent={() => (
                        <View style={{ flex: 1, backgroundColor: "#edeff5", justifyContent: "center", alignItems: "center" }}>
                            <Text style={{ color: "#a5adc7", fontSize: 16, fontFamily: fonts.SFProMedium }}>{this.state.showMessage}</Text>
                        </View>
                    )}
                />
                <Spinner visible={this.state.showSpinner} />
            </View>
        );
    }
    componentDidMount = () => {
        this.getLikes();
    };

}

export default LikesList;
