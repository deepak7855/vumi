import React, { Component } from 'react';
import { View, Text, FlatList } from 'react-native';
import Spinner from 'react-native-loading-spinner-overlay';
import Toast from 'react-native-root-toast';
import { GHeader } from '../common';
import fonts from '../common/fonts';
import { ViewsTabRow } from '../components/overview/overViewUserInfo/ViewsTabRow';
import Helper from '../lib/Helper';
import pushToScreen from '../navigation/NavigationActions';

class ViewsList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            viewersList: []
        };
    }

    getViews = () => {
        this.setState({showSpinner:true})
        Helper.makeRequest({ url: "viewer-list", method: "POST", data: { post_id: this.props.post_id } }).then((resp) => {
            if (resp.status == 'true') {

                if (resp.data.length > 0) {
                    this.setState({ viewersList: resp.data, showMessage: '' })
                } else {
                    this.setState({ showMessage: 'You have no views yet!' })
                }
            } else {
                Toast.show(resp.message)
            }
            this.setState({ showSpinner: false })
        })
    }
    onCommentIconPress = (post_id) => {
        pushToScreen('Comments', { post_id, fromOverView: true })
    };
    render() {
        return (
            <View style={{ flex: 1, }}>
                <GHeader
                    bArrow
                    greyBackButton
                    color="#606076"
                    backgroundColor="white"
                    title1="Views"
                />
                <FlatList
                    contentContainerStyle={{ flexGrow: 1 }}
                    data={this.state.viewersList}
                    ItemSeparatorComponent={() => <View style={{ height: 10, backgroundColor: '#f5f6fa' }} />}
                    renderItem={({ item }) => <ViewsTabRow
                    onCommentIconPress={(id) => this.onCommentIconPress(id)}

                     fromViews={true} item={item} />}
                    keyExtractor={(item, index) => index}
                    ListEmptyComponent={() => (
                        <View style={{ flex: 1, backgroundColor: "#edeff5", justifyContent: "center", alignItems: "center" }}>
                            <Text style={{ color: "#a5adc7", fontSize: 16, fontFamily: fonts.SFProMedium }}>{this.state.showMessage}</Text>
                        </View>
                    )}
                />
                <Spinner visible={this.state.showSpinner} />
            </View>
        );
    }
    componentDidMount = () => {
        this.getViews();
    };

}

export default ViewsList;
