import React, { useEffect, useState } from 'react'
import {
    Text, View, ImageBackground,
    TextInput, StyleSheet, Keyboard,
    TouchableOpacity
} from 'react-native'


import {
    GButton, Images, GAuthHeader, Fontfamily,
    ScrollContainer,
    Colors, ScrollButtonTop,
} from '../common';

import { Actions } from 'react-native-router-flux';
import { validationFunction } from '../common/validationFunctions';
import Spinner from 'react-native-loading-spinner-overlay';
import ModalFilterPicker from '../components/Filterpicker/src/index';
import Toast from 'react-native-root-toast';
import Helper from '../lib/Helper';
import pushToScreen from '../navigation/NavigationActions';

export const ForgotPassword = ({ children, style }) => {


    const [showCountryModel, setShowCountryModel] = useState(false);
    const [countryCode, setCountryCode] = useState('1')
    const [countryName, setCountryName] = useState('US')
    const [mobile_number, set_mobile_number] = useState('');
    const [showSpinner, setSpinner] = useState(false)
    const [, setOtp] = useState(null);
    const [countryList, setCountryList] = useState([]);

    const onSelect = (country) => {
        setCountryCode(country.phonecode);
        setCountryName(country.sortname);
        setShowCountryModel(false)
    }

    const onPhoneInput = (text) => {
        set_mobile_number(text)
    }

    useEffect(() => {
        setSpinner(true)
        Helper.makeRequest({ url: "country", method: "POST", data: '' }).then((resp) => {
            if (resp.status == 'true') {
                setCountryList(resp.data)
            } else {
                Toast.show(resp.message)
            }
            setSpinner(false)
        })
    }, []);


    const onNext = () => {

        if (validationFunction.checkNumber("Mobile Number", 10, 15, mobile_number)) {
            Keyboard.dismiss();
            var data = {
                country_code: countryCode,
                phone: mobile_number,
                is_send_otp: 0
            }
            setSpinner(true)
            Helper.makeRequest({ url: "forgot-password", method: "POST", data: data }).then((resp) => {
                if (resp.status == 'true') {
                    setOtp(resp.data.otp)
                    // Actions.Verification({ whereFrom: "ForgotPassword", phone: mobile_number, otp: resp.data.otp, country_code: countryCode , types:'FORGOT'})
                    pushToScreen('Verification', { whereFrom: "ForgotPassword", phone: mobile_number, otp: resp.data.otp, country_code: countryCode, types: 'FORGOT' })
                } else {
                    setOtp(null)
                    Toast.show(resp.message);
                }
                setSpinner(false)
            })
        }


    }

    return (
        <ImageBackground resizeMode='cover'
            source={Images.Loginbg}
            style={{ flex: 1, alignItems: 'center' }}>

            <GAuthHeader bArrow title1="Forgot Password" />

            <ScrollContainer containerStyle={{ alignItems: 'center', justifyContent: 'center', flex: 1 }}>
                <View style={{ alignSelf: 'center', marginBottom: 30 }}>
                    <Text style={Style.StringStyle}>What's your phone number?</Text>
                </View>
                <View style={Style.inputBox}>
                    <TouchableOpacity style={{ flexDirection: "row" }} onPress={() => setShowCountryModel(true)}>
                        <Text style={Style.CnameStyle}>{countryName}</Text>
                        <Text style={Style.CcStyle}>+{countryCode}</Text>
                    </TouchableOpacity>
                    <TextInput
                        placeholder="Your Phone"
                        autoCorrect={false}
                        style={Style.UerinputBox}
                        value={mobile_number}
                        keyboardType='number-pad'
                        onChangeText={(text) => onPhoneInput(text)}
                    />
                </View>
            </ScrollContainer>

            <ScrollButtonTop>
                <GButton
                    height={65}
                    radius={32.5}
                    disabled={mobile_number.length >= 10 ? false : true}
                    bText='Next'
                    txtcolor='white'
                    bgColor={mobile_number.length >= 10 ? Colors.abuttonColor : Colors.btxtColor}
                    onPress={() => onNext()}
                />
            </ScrollButtonTop>

            <ModalFilterPicker
                placeholderText="Search Country..."
                visible={showCountryModel}
                onSelect={onSelect}
                onCancel={() => setShowCountryModel(false)}
                options={countryList}
            />

            <Spinner visible={showSpinner} />

        </ImageBackground>
    );
}

const Style = StyleSheet.create({
    inputBox: {
        marginTop: 15, height: 70, borderRadius: 40,
        alignItems: 'center', backgroundColor: '#fff',
        flexDirection: 'row', width: 300
    },
    CcStyle: {
        fontSize: 22, marginHorizontal: 18, marginLeft: 5,
        fontFamily: Fontfamily.MEB,
    },
    UerinputBox: {
        height: 50, width: 200,
        fontSize: 20,
        fontFamily: Fontfamily.MEB,
    },
    CnameStyle: {
        paddingLeft: 25, marginRight: 5, marginTop: 5,
        fontFamily: 'HelveticaNeueThin', fontSize: 18, fontWeight: '400'
    },
    StringStyle: { fontSize: 16, color: 'white', fontFamily: Fontfamily.MSB },

})

