import React, { useState, useEffect } from 'react'
import { View, StyleSheet, TouchableOpacity, Text, BackHandler } from 'react-native'
import {
    GAuthHeader, IconInput,
    Colors, Fontfamily, Images,
} from '../common';

import { Actions } from 'react-native-router-flux';
import { ScrollView, } from 'react-native-gesture-handler';
import Spinner from 'react-native-loading-spinner-overlay';
import Helper from '../lib/Helper';
import Toast from 'react-native-root-toast';
import pushToScreen from '../navigation/NavigationActions';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view';


export const Referral = (props) => {
    const [userName, setUserName] = useState('');
    const [isReferralCodeValid, setReferralCodeValid] = useState(false);
    const [showSpinner, setSpinner] = useState(false);
    const [showTick, setTick] = useState(null);


    const onUserNameEnter = (text) => {
        setUserName(text)
    }

    const backAction = () => {
        if (Actions.currentScene == "Referral") {
            BackHandler.exitApp()
        }

    };



    useEffect(() => {
        BackHandler.addEventListener("hardwareBackPress", backAction);

        return () => {
            BackHandler.removeEventListener("hardwareBackPress", backAction);
        };
    }, [backAction]);

    useEffect(() => {
        const timer = setTimeout(() => {
            if (userName.length > 4) {
                check_referral_code(userName)
            } else {
                setTick(null)
            }
        }, 1000);
        return () => clearTimeout(timer);
    }, [userName]);

    const check_referral_code = (text) => {
        var data = {
            referral_code: text
        }
        setSpinner(true)
        Helper.makeRequest({ url: "verify-referral", method: "POST", data: data }).then((resp) => {

            if (resp.status == 'true') {
                setReferralCodeValid(true)
                setTick(true)
            } else {
                setReferralCodeValid(false)
                setTick(true)
            }
            setSpinner(false)
        })
    }

    const onSkip = () => {

        if (isReferralCodeValid) {

            var data = {
                referral_code: userName
            }
            setSpinner(true)
            Helper.makeRequest({ url: "apply-referral", method: "POST", data: data }).then((resp) => {

                if (resp.status == 'true') {
                    Toast.show(resp.message);
                    pushToScreen('YourBonus');
                } else {
                    Toast.show(resp.message);
                }
                setSpinner(false)
            })

        } else {
            pushToScreen('YourBonus');
        }

    }

    {
        return (
            <View style={{ flex: 1, backgroundColor: Colors.btxtColor }}>


                <GAuthHeader title1="Referral code" rightItem>
                    <TouchableOpacity onPress={() => { onSkip() }}>
                        <Text style={{ fontSize: 14, color: '#fff', fontFamily: Fontfamily.MEB, textAlign: 'right', marginRight: 8 }}>{isReferralCodeValid ? 'Next' : 'Skip'} </Text>
                    </TouchableOpacity>
                </GAuthHeader>

                <KeyboardAwareScrollView contentContainerStyle={{ flexGrow: 1, justifyContent: "space-between" }}>

                    <View />
                    <View style={{ marginLeft: 30 }}>
                        <Text style={[Style.whiteTxt, { color: 'rgb(137,121,255)', }]}>Get a</Text>
                        <Text style={Style.whiteTxt}>+{Helper.userInfo.referral_coins ? Helper.userInfo.referral_coins : '0'} view</Text>
                        <Text style={[Style.whiteTxt, { color: 'rgb(137,121,255)', }]}>bonus!</Text>
                    </View>

                    <View style={{ backgroundColor: 'rgb (111,87,255)', paddingBottom: 15, alignItems: 'center' }}>
                        <IconInput
                            placeholder="Enter friend's username"
                            value={userName}
                            onChangeText={(text) => { onUserNameEnter(text) }}
                            style={{ textAlign: 'center', }}
                            rightIconDisable
                            rimagePath={showTick !== null ? isReferralCodeValid ? Images.ticks : Images.cross_pink : null}
                        />

                    </View>
                </KeyboardAwareScrollView>
                <Spinner visible={showSpinner} />

            </View>
        )
    }
}




const Style = StyleSheet.create({



    bigO: {
        height: 200, width: 200, borderWidth: 0,
        borderRadius: 100, borderColor: 'rgb(112,105,255)',
        justifyContent: 'center',
        alignItems: 'center',


    },
    smallO: {
        position: 'absolute', bottom: 5, right: 5,
        borderWidth: 5, borderColor: 'rgb(112,105,255)',
        backgroundColor: Colors.abuttonColor,
        height: 45, width: 45, borderRadius: 23.5,
        justifyContent: 'center', alignItems: 'center'

    },
    whiteTxt: {
        color: '#fff',
        fontFamily: Fontfamily.MEB,
        fontSize: 40,

    },
    notiInfo: {
        marginTop: 40, fontSize: 20,
        fontFamily: Fontfamily.MSB,
        color: 'rgb(137,121,255)',
    }



})
