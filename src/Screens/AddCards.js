import React, { Component } from 'react';
import { StyleSheet, View, Text, TextInput, ScrollView } from 'react-native';
import {
    GButton, GHeader,
    Colors,
} from '../common';
import fonts from '../common/fonts';


export class AddCards extends Component {
    render() {
        return (

            <ScrollView contentContainerStyle={{ flexGrow: 1, backgroundColor: '#f5f6fa', }}>

                <View style={{ flex: 3, }}>
                    <GHeader
                        bArrow
                        greyBackButton
                        color="#606076"
                        title1FontSize={14}
                        backgroundColor="white"
                        title1="Add Debit/Credit Card"
                    />
                    <View style={{ flex: 1, }}>

                        <View style={{ height: 60, flexDirection: 'row', paddingHorizontal: 15, marginVertical: 3, backgroundColor: 'white', alignItems: 'center', marginTop: 0 }}>

                            <Text style={{ color: 'black', fontSize: 14, fontFamily: fonts.SFProRegular }}>CARD NUMBER</Text>

                            <View style={{ marginLeft: 10 }}>
                                <TextInput
                                    placeholder="Card number"
                                    style={Styles.textInput_Style} />
                            </View>
                        </View>
                        <View style={{ height: 60, flexDirection: 'row', paddingHorizontal: 15, backgroundColor: 'white', alignItems: 'center' }}>
                            <View>
                                <Text style={{ color: 'black', fontSize: 14, fontFamily: fonts.SFProRegular }}>Expiration Date</Text>
                            </View>
                            <View style={{ marginLeft: 10 }}>
                                <TextInput
                                    placeholder="MM/YY"
                                    style={Styles.textInput_Style} />
                            </View>
                        </View>

                        <View style={{ height: 60, flexDirection: 'row', paddingHorizontal: 15, marginVertical: 3, backgroundColor: 'white', alignItems: 'center' }}>
                            <View>
                                <Text style={{ color: 'black', fontSize: 14, fontFamily: fonts.SFProRegular }}>Security Code</Text>
                            </View>
                            <View style={{ marginLeft: 10 }}>
                                <TextInput
                                    placeholder="CVV"
                                    style={Styles.textInput_Style} />
                            </View>
                        </View>
                        <View style={{ height: 60, flexDirection: 'row', paddingHorizontal: 15, backgroundColor: 'white', alignItems: 'center' }}>
                            <View>
                                <Text style={{ color: 'black', fontSize: 14, fontFamily: fonts.SFProRegular }}>Zip Code</Text>
                            </View>
                            <View style={{ marginLeft: 10 }}><TextInput
                                placeholder="Zip Code"
                                style={Styles.textInput_Style} />

                            </View>

                        </View>


                        <View style={{
                            flex: 1,
                            justifyContent: 'flex-end',
                            alignItems: 'center',
                            paddingBottom: 20
                        }}>
                            <GButton
                                height={60}
                                radius={30}
                                bText='CONTINUE'
                                txtcolor='rgb(251,252,254)'
                                bgColor={Colors.dblue}
                                fontSize={16}
                                onPress={() => { }}
                            />

                        </View>
                    </View>

                </View>
            </ScrollView>
        )
    }
}


const Styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#f5f6fa',


    },
    textInput_Style: {
        fontFamily: fonts.SFProMedium,
        color: "black"
    }

});