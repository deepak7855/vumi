import React, { } from 'react'
import { View, StyleSheet, Text } from 'react-native'
import {
    GButton, ScrollContainer, Colors, ScrollButtonTop, Fontfamily,
} from '../common';

import { Actions } from 'react-native-router-flux';
import pushToScreen from '../navigation/NavigationActions';

export const YourBonus = (props) => {

    {
        return (
            <View style={{ flex: 1, backgroundColor: Colors.btxtColor }}>

                <ScrollContainer containerStyle={{ flex: 1, justifyContent: 'center' }}>

                    <View style={{ marginLeft: 30 }}>
                        <Text style={[Style.whiteTxt, { color: 'rgb(137,121,255)', }]}>You get a</Text>
                        <Text style={Style.whiteTxt}>+200 view</Text>
                        <Text style={[Style.whiteTxt, { color: 'rgb(137,121,255)', }]}>Sign-Up bonus!</Text>


                        <View style={{ width: 280, }}>
                            <Text style={Style.notiInfo}>We’ll send push notifications
                            to let you know when you get more engagement.</Text>
                        </View>

                    </View>



                </ScrollContainer>

                <ScrollButtonTop style={{ marginBottom: 0, alignItems: 'center' }}>
                    <GButton
                        height={65}
                        radius={32.5}
                        bText='Next'
                        txtcolor='#fff'
                        bgColor={Colors.abuttonColor}
                        onPress={() => { pushToScreen('Notification') }}
                    />

                </ScrollButtonTop>


            </View>
        )
    }
}








const Style = StyleSheet.create({



    bigO: {
        height: 200, width: 200, borderWidth: 0,
        borderRadius: 100, borderColor: 'rgb(112,105,255)',
        justifyContent: 'center',
        alignItems: 'center',


    },
    smallO: {
        position: 'absolute', bottom: 5, right: 5,
        borderWidth: 5, borderColor: 'rgb(112,105,255)',
        backgroundColor: Colors.abuttonColor,
        height: 45, width: 45, borderRadius: 23.5,
        justifyContent: 'center', alignItems: 'center'

    },
    whiteTxt: {
        color: '#fff',
        fontFamily: Fontfamily.MEB,
        fontSize: 40,

    },
    notiInfo: {
        marginTop: 40, fontSize: 20,
        fontFamily: Fontfamily.MSB,
        color: 'rgb(137,121,255)',
    }



})

