import React, { Component } from 'react';
import {
  Text,
  View,
  StyleSheet,
  Image,
  ScrollView,
  Dimensions,
  FlatList,
  TouchableOpacity,
  Modal,
  StatusBar,
  BackHandler,
  Platform,
  Alert,
  SafeAreaView,
  TouchableWithoutFeedback,
  RefreshControl,
  Linking,
  Animated,
  NativeModules,
  DeviceEventEmitter,

} from 'react-native';
import { CoinAndSwitchComponent } from '../components/Watch/CoinAndSwitchComponent';
import { Colors } from '../common/Colors';
import { UserDetailComponent } from '../components/Watch/UserDetailComponent';
import { UserDetailShowAds } from '../components/Watch/UserDetailShowAds';


import { Images } from '../common/Images';
import { VideoDetailCompo } from '../components/Watch/VideoDetailCompo';
import Video from 'react-native-video';
import { GetMoreCoinsModelCompo } from '../components/Watch/GetMoreCoinsModelCompo';
import { Actions } from 'react-native-router-flux';
import Helper from '../lib/Helper';
import ProgressBar from 'react-native-progress/Bar';
import Toast from 'react-native-root-toast';
import Spinner from 'react-native-loading-spinner-overlay';
import Config from '../lib/Config';
import ImageViewSwiper from '../components/Watch/ImageViewSwiper';
import pushToScreen from '../navigation/NavigationActions';
import { hasNotch } from 'react-native-device-info';

// import PostLikeView from '../components/Watch/PostLikeView'
import FastImage from 'react-native-fast-image';
import Emojiimages from '../assets/Themes/Images';
import { BlurView } from '@react-native-community/blur';
import fonts from '../common/fonts';
import SocketConnect from '../common/SocketConnect';
import { InfoModelCompo } from '../components/Watch/InfoModelCompo';
import { BlockModelCompo } from '../components/Watch/BlockModelCompo';
import VideoWithProgress from '../components/Watch/VideoWithProgress';
import images from '../assets/Themes/Images';

const { StatusBarManager } = NativeModules;
const screenHeight2 = Dimensions.get('screen').height;
const windowHeight = Dimensions.get('window').height;
const navbarHeight = screenHeight2 - windowHeight - StatusBarManager.HEIGHT;


const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;
const screenCellHeight = hasNotch() ? Dimensions.get('window').height - (55 + 60 + 20 + 58) : Dimensions.get('window').height - (55 + 60 + 20);



export class Watch extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modalVisible: false,
      videoPaused: false,
      progress: 0,
      coins: 0,
      showSpinner: false,
      overlayShow: -1,
      arrReportList: [],
      reportViewOverleyShowIndex: -1,
      emojiViewShowIndex: -1,
      duration: 0,
      currentTime: 0,
      doubleTapIndex: -1,
      showInfoModel: false,
      showBlockModel: false,
      selectedRowDetail: {},
      screen_Height: 0,
      selectedIndex: 0,
      ShowModalVisible: false,
      currentCoins: 0,
      adminshowindex: Helper.userSettings.admin_post_count,
      watchList: [],
      currentindex: 0,
      arrAdminPost: [],
      current_page: 1,
      currentAddindex: 0,
      currentindexitem: 0,
      arrServerAdminPost: [],




    };
    this.updateStatusBar();
    this.timeout = null;
    this.interval = null;
    this.didBlurListener = this.props.navigation.addListener('didBlur', obj => {
      this.changeState();
    });
  }

  changeState = () => {
    this.setState({ videoPaused: true });
    if (this.timeout) {
      clearTimeout(this.timeout)
    }
    // if (this.interval) {
    //   clearInterval(this.interval)
    // }
  };
  getSettingApi = () => {
    this.setState({ showSpinner: true });
    Helper.makeRequest({ url: 'settings', method: 'POST', data: '' }).then(
      resp => {
        if (resp.status == 'true') {
          console.log('settings coins------------------------------', resp.data.user_coins);
          Helper.userSettings = resp.data;
          this.setState({ coins: resp.data.user_coins });
        } else {
          Helper.userSettings = {};
          this.setState({});
        }
        this.setState({ showSpinner: false, isRefreshing: false });
      },
    );
  };

  getWatchList = () => {
    this.setState({ showSpinner: true, });
    setTimeout(() => {
      this.setState({ showSpinner: false, });
    }, 7000);
    Helper.makeRequest({
      url: `get-home-feed?page=${this.state.current_page}`,
      method: 'POST',
      data: '',
    }).then(resp => {
      if (resp.status == 'true') {
        if (this.state.current_page == 1) {
          this.state.watchList = []
          this.setState({ screen_Height: 0 })
        }
        let watchList = this.state.watchList.concat(this.mergeArray(resp.data.data));
        this.setState({ watchList, last_page: resp.data.last_page, showSpinner: false, isRefreshing: false });
        setTimeout(() => {
          this.methodPostView(0)
          this.handelProgressWorking();
        }, 500);

      } else {
        this.setState({ showSpinner: false, isRefreshing: false });
        Toast.show(resp.message);
      }
    });
  };


  mergeArray = (arr) => {
    let dummyArray = [];
    let currentAddindex = this.state.currentAddindex;
    arr.map((e, index) => {
      let add = false;
      if (index != 0) {
        add = index % this.state.adminshowindex == 0
        console.log(add)
      }
      if (add) {
        this.state.arrAdminPost[currentAddindex] ? dummyArray.push({ ...this.state.arrAdminPost[currentAddindex], admin: true }) : null;
        currentAddindex = this.state.arrAdminPost.length - 1 == currentAddindex ? 0 : currentAddindex + 1;
      }
      dummyArray.push(e);

    })
    this.setState({ currentAddindex })
    return dummyArray
  }



  getShowAdsAdminApi = () => {
    this.setState({ showSpinner: true, });

    Helper.makeRequest({
      url: `get-admin-post`,
      method: 'POST',
      data: '',
    }).then(resp => {
      // console.log(JSON.stringify(resp) + "resprespresprespresprespresp>>");
      if (resp.status == 'true') {
        if(resp.data){
          this.setState({ currentindex: 0, arrAdminPost: resp.data, arrServerAdminPost: JSON.stringify(resp.data) })
        }
        else{
          this.setState({ currentindex: 0, arrAdminPost: [], arrServerAdminPost: '[]' })
        }
      } else {
        Toast.show(resp.message);
      }

      this.getWatchList();

    });
  };
  onEndReached = () => {
    if (this.state.current_page < this.state.last_page) {
      this.setState(
        {
          current_page: this.state.current_page + 1,
        },
        () => {
          this.getWatchList();
        },
      );
    }
  };

  getModalContent = () => {
    Helper.makeRequest({ url: 'app-settings', method: 'POST', data: '' }).then(
      resp => {
        if (resp.status == 'true') {
          Helper.modalData = resp.data;
          this.setState({});
        } else {
          Helper.modalData = {};
          this.setState({});
        }
        this.setState({ showSpinner: false, isRefreshing: false });
      },
    );
  };


  onRefresh = () => {
    this.setState({ selectedIndex: -1, watchList: [] }, () => {
      this.state.current_page = 1
      this.getWatchList();
      this.getSettingApi();
      this.state.currentindex = 0

    });
  };

  onPress = val => {
    this.setState({ modalVisible: val });
  };

  handleProgressPress = e => {
    const position = e.nativeEvent.locationX;
    const progress = (position / (screenWidth - 8)) * this.state.duration;
    this.player.seek(progress);
  };





  _onViewableItemsChanged = props => {
    const changed = props.changed;
    //console.log('changed', changed);

    changed.forEach(item => {
      // console.log('_onViewableItemsChanged', item);
      // if (item.isViewable) {
      //   this.setState({selectedIndex: item.index, progress: 0}, () => {
      //     clearTimeout(this.timeout);
      //     this.timeout = setTimeout(() => {
      //       this.updatePostViewStatus();
      //     }, 3000);
      //   });
      // }
    });
  };

  viewabilityConfig = {
    viewAreaCoveragePercentThreshold: 80,
  };
  handleAdminPage = e => {
    let offset = e.nativeEvent.contentOffset;
    if (offset) {
      let height = Platform.OS == 'ios' ? screenCellHeight : this.state.screen_Height
      let page = Math.ceil(offset.y / height);
      this.state.arrAdminPost.splice(0, 1)
      if (this.state.arrAdminPost.length == 0) {
        this.state.arrAdminPost = this.state.arrServerAdminPost ? JSON.parse(this.state.arrServerAdminPost) : []
      }
      this.setState({ currentindex: 0 })
    }
  }
  handlePageChange = e => {
    var offset = e.nativeEvent.contentOffset;

    if (offset) {
      var height = Platform.OS == 'ios' ? screenCellHeight : this.state.screen_Height
      var page = Math.ceil(offset.y / height);


      if (page != this.state.selectedIndex) {

        // this.state.currentindexitem = this.state.currentindex + 1

        this.handelProgressWorking();

        if (this.state.overlayShow != -1) {
          this.setState({ overlayShow: -1, doubleTapIndex: -1 })
        }
        this.methodPostView(page)

      }

    }
  };

  handelProgressWorking = () => {
    clearInterval(this.interval);
    this.setState({
      progress: 0,
      duration: 0,
    }, () => {

      this.interval = setInterval(() => {
        if (this.state.duration <= 6) {
          let duration = this.state.duration + .2
          this.setState({
            duration,
            progress: duration / 6,
          }, () => {
            // console.log(this.state.progress, '   ', duration)
          });
        } else if (this.state.progress > 6) {
          clearInterval(this.interval);
        }
      }, 200)
    });
  }



  methodPostView = async page => {

    if (this.state.watchList && this.state.watchList.length > page) {

      let token = await Helper.getDataFromAsync("token")
      let dic = this.state.watchList[page];


      if (page > 0) {

        let currentindex = this.state.currentindex == this.state.adminshowindex ? 0 : this.state.currentindex + 1;
        this.setState({ currentindex })
        this.state.watchList.splice(0, 1);
        this.flatListRef.scrollToIndex({ animated: false, index: 0 });
      }

      if (dic.post_detail[0].type == 'VIDEO') {
        this.setState({ videoPaused: false, selectedIndex: 0, progress: 0 })
      } else {
        this.setState({ videoPaused: true, selectedIndex: 0, progress: 0 })
      }


      clearTimeout(this.timeout);
      this.timeout = setTimeout(() => {
        if (SocketConnect.checkSocketConnected()) {

          if (dic.user.id != Helper.userInfo.id) {
            console.log('timeout------------', dic.user.id);
            // console.log(dic.id+ "<<<<<<<<<<<<<<<<<<<<< CURRENT INDEX ITEM");
            // console.log(token + "<<<<<<<<<<<<<<<<<<<<< LLLLLLLL ITEM");

            let currentitem = this.state.watchList?.[0];
            // console.log(JSON.stringify(currentitem) + "<<<<<<<<<<<<<<<<<<<<<CURRENT ITEM");

            !currentitem.admin ? SocketConnect.socketDataEvent('view_post', { post_id: dic.id, token: token }) : null

            // setTimeout(() => {
            //   this.getSettingApi();
            // }, 1000)

          }
        } else {
          SocketConnect.checkConnection('connect', cb => {
            console.log('SocketConnect------------', cb);
          });
        }
      }, 6000);
    }
  }
  updatePostViewStatus = () => {
    // if (this.state.watchList.length > 0) {
    //     if (this.state.watchList[this.state.selectedIndex].is_view == 0) {
    //         let data = {
    //             post_id: this.state.watchList[this.state.selectedIndex].id
    //         }
    //         Helper.makeRequest({ url: "post-view", method: "POST", data: data }).then((resp) => {
    //             if (resp.status == 'true') {
    //                 let list = this.state.watchList
    //                 let listRow = resp.data
    //                 // listRow.is_view = 1;
    //                 // listRow.total_views = listRow.total_views + 1
    //                 list[this.state.selectedIndex] = listRow;
    //                 this.setState({ watchList: list })
    //             } else {
    //                 Toast.show(resp.message)
    //             }
    //         })
    //     }
    // }
  };

  onLikePress = (id, is_like, index) => {
    let data = {
      post_id: id,
      is_like: is_like,
    };
    Helper.makeRequest({ url: 'post-like', method: 'POST', data: data }).then(
      resp => {
        if (resp.status == 'true') {
          let list = this.state.watchList;
          let listRow = resp.data;
          // listRow.is_like = is_like;
          // listRow.total_likes = is_like == 1 ? (listRow.total_likes + 1) : (listRow.total_likes - 1);
          list[index] = listRow;
          this.setState({ watchList: list });
        } else {
          Toast.show(resp.message);
        }
      },
    );
  };

  openMenu = () => {
    this.menu.open();
  };

  methodReportSubmit(item, index) {
    let dicPost = this.state.watchList[this.state.reportViewOverleyShowIndex];
    this.setState({ showSpinner: true });
    Helper.makeRequest({
      url: 'flag-post',
      method: 'POST',
      data: { post_id: dicPost.id, flag_reason_id: item.id },
    }).then(resp => {
      if (resp.status == 'true') {
        let dic = dicPost;
        dic.is_flag_by_me = 1;
        this.state.watchList.splice(this.state.reportViewOverleyShowIndex, 1, dic);
        Helper.userSettings.today_flag =
          Number(Helper.userSettings.today_flag) + 1;
      }
      Helper.showToast(resp.message);
      this.setState({ reportViewOverleyShowIndex: -1, showSpinner: false });
    });
  }
  renderReportView = ({ item, index }) => {
    return (
      <TouchableOpacity
        onPress={() => this.methodReportSubmit(item, index)}
        style={{
          padding: 15,
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'space-between',
          backgroundColor: 'white',
          borderBottomColor: 'black',
          borderBottomWidth: 1.0,
        }}>
        <Text
          style={{
            fontSize: 15,
            fontFamily: fonts.SFProDisplay_Semibold,
            width: '90%',
          }}>
          {item.title}
        </Text>
        <Image
          resizeMode={'contain'}
          style={{ width: 15, height: 15, tintColor: Colors.black }}
          source={Images.right_arrow}
        />
      </TouchableOpacity>
    );
  };

  renderReportModal() {
    return (
      <Modal
        animationType={'fade'}
        transparent={true}
        visible={true}
        onRequestClose={() => { }}>
        <View style={{ flex: 1, justifyContent: 'flex-end' }}>
          <View
            style={{
              width: '100%',
              alignItems: 'center',
              backgroundColor: Colors.white,
              paddingVertical: 15,
              borderBottomColor: Colors.coolGrey,
              borderBottomWidth: 1.0,

            }}>
            <Text
              style={{
                fontFamily: fonts.SFProDisplay_Semibold,
                fontSize: 20,
                color: Colors.black,
                textAlign: 'center',
              }}>
              Report
            </Text>
            <TouchableOpacity style={{ position: 'absolute', zIndex: 1, right: 15, top: 10 }} onPress={() => this.setState({ reportViewOverleyShowIndex: -1 })}>
              <Image style={{ height: 30, width: 30, tintColor: 'grey' }} resizeMode={'contain'} source={Images.cross_pink} ></Image>
            </TouchableOpacity>
          </View>
          <View
            style={{ width: '100%', height: '50%', justifyContent: 'flex-end' }}>
            <FlatList
              style={{ flex: 1, backgroundColor: Colors.white }}
              data={this.state.arrReportList}
              renderItem={this.renderReportView}
              keyExtractor={(item, index) => index}
            />
          </View>
        </View>
      </Modal>
    );
  }
  methodVideoPause(index) {
    if (this.state.doubleTapIndex == index) {
      this.setState({ overlayShow: index, videoPaused: true, doubleTapIndex: -1 })
      return
    }
    this.state.doubleTapIndex = index
    this.setState({
      videoPaused: !this.state.videoPaused,
    })
    setTimeout(() => {
      this.state.doubleTapIndex = -1
    }, 400);
  }

  onShowBlockModel = () => {
    this.setState({ showInfoModel: false }, () => {
      this.setState({ showBlockModel: true })
    })
  }


  goToChat = (item) => {
    this.setState({ showInfoModel: false }, () => {
      pushToScreen('Chat', { OtherUserId: item.user.id, OtherUserName: item.user.username });
    })
  }

  onBlock = (item) => {
    this.setState({ showBlockModel: false })
    let formdata = {
      user_id: Helper.userInfo.id,
      other_user_id: item.user.id,
    }
    if (SocketConnect.checkSocketConnected()) {
      SocketConnect.socketDataEvent('user_block_unblock', formdata);
      this.setState({ watchList: [] }, () => {
        this.getWatchList();
      })

    } else {
      SocketConnect.checkConnection('connect', cb => {
        console.log('SocketConnect------------', cb);
      });
    }
  }

  onImagePress = (item, index) => {

    if (this.state.doubleTapIndex == index) {
      this.setState({ overlayShow: index, videoPaused: true, doubleTapIndex: -1 })
      return
    }
    this.state.doubleTapIndex = index
    setTimeout(() => {
      this.state.doubleTapIndex = -1
    }, 400);

  }

  openUrl = (url) => {
    Linking.canOpenURL(url).then(supported => {
      if (supported) {
        Linking.openURL(url);
      } else {
        Toast.show('Fail to open Url.');
        console.log("Don't know how to open URI: " + url);
      }
    });
  }


  // render() {
  //   return (
  //     <SafeAreaView style={styles.container}>
  //       <CoinAndSwitchComponent coin={this.state.coins} toggleValue={true} />
  //       <FlatList
  //         data={this.state.watchList}
  //         extraData={this.state.watchList}
  //         keyExtractor={(item, index) => index}
  //         pagingEnabled
  //         onEndReached={this.onEndReached}
  //         onEndReachedThreshold={0.1}
  //         // viewabilityConfig={this.viewabilityConfig}
  //         // onViewableItemsChanged={this._onViewableItemsChanged}
  //         onMomentumScrollEnd={this.handlePageChange}
  //         scrollEventThrottle={16}
  //         windowSize={5}
  //         // getItemLayout={(_data, index) => ({
  //         //   length: screenHeight - screenHeight / 4.6,
  //         //   offset: (screenHeight - screenHeight / 4.6) * index,
  //         //   index,
  //         // })}
  //         refreshControl={
  //           <RefreshControl
  //             refreshing={this.state.isRefreshing}
  //             onRefresh={this.onRefresh.bind(this)}
  //           />
  //         }
  //         removeClippedSubviews={true}
  //         renderItem={({ item, index }) => (
  //           // <View style={{height: screenHeight - screenHeight / 4.6}}>
  //           <View style={{ height: Platform.OS == 'ios' ? screenCellHeight : screenCellHeightAndroid }}>
  //             <View style={[styles.subContainer]}>
  //               <UserDetailComponent
  //                 onUserPress={(item) => this.setState({ showInfoModel: true, selectedRowDetail: item })}
  //                 item={item} />
  //               {item.post_detail && item.post_detail.length > 0 ? (
  //                 <View

  //                   // onPress={() => {
  //                   //   if (item.post_detail[0].type == 'IMAGE') {
  //                   //     if (this.state.doubleTapIndex == index) {
  //                   //       this.setState({ overlayShow: index, videoPaused: true, doubleTapIndex: -1 })
  //                   //       return
  //                   //     }
  //                   //     this.state.doubleTapIndex = index
  //                   //     setTimeout(() => {
  //                   //       this.state.doubleTapIndex = -1

  //                   //     }, 400);
  //                   //   }
  //                   // }}
  //                   style={styles.squareView}>
  //                   {item.post_detail[0].type == 'VIDEO' ? (
  //                     <VideoWithProgress
  //                       methodVideoPause={(index) => this.methodVideoPause(index)}
  //                       uri={Config.imageUrl + item.post_detail[0].media}
  //                       paused={
  //                         this.state.selectedIndex == index &&
  //                           !this.state.videoPaused
  //                           ? false
  //                           : true
  //                       }
  //                       index={index}

  //                     />
  //                   ) : (
  //                       <View style={styles.squareView}>
  //                         <ImageViewSwiper
  //                           onImagePress={(item, index) => this.onImagePress(item, index)}
  //                           index={index}
  //                           data={item.post_detail} />
  //                       </View>
  //                     )}

  //                   {this.state.overlayShow == index &&
  //                     this.methodRenderOverlayView(item, index)}
  //                 </View>
  //               ) : (
  //                   <View style={styles.squareView}>
  //                     <Image
  //                       style={styles.squareView}
  //                       source={Images.watch_video}
  //                     />
  //                   </View>
  //                 )}
  //               {this.state.emojiViewShowIndex == index &&
  //                 this.rendeEmojiView()}
  //               {/* <PostLikeView /> */}
  //               <VideoDetailCompo
  //                 isEmojiShow={
  //                   this.state.emojiViewShowIndex == index ? false : true
  //                 }
  //                 //  onLikePress={(post_id, is_like, index) => this.openMenu(post_id, is_like, index)}
  //                 likeImage={
  //                   item.is_like && item.is_like < 7
  //                     ? arrEmaoji[parseInt(item.is_like) - 1].image
  //                     : Images.heart_grey
  //                 }
  //                 EmojiViewShow={index => this.methodEmojiViewShow(index)}
  //                 onLikePress={(post_id, is_like, index) =>
  //                   this.onLikePress(post_id, is_like, index)
  //                 }
  //                 item={item}
  //                 index={index}
  //                 onChatPress={post_id => pushToScreen('Comments', { post_id })}
  //               />
  //             </View>

  //           </View>
  //         )}
  //       />

  //       {this.state.reportViewOverleyShowIndex != -1 && this.state.arrReportList.length > 0 &&
  //         this.renderReportModal()}

  //       {
  //         this.state.showInfoModel &&
  //         <InfoModelCompo
  //           showInfoModel={this.state.showInfoModel}
  //           hideInfoModel={() => this.setState({ showInfoModel: false })}
  //           onShowBlockModel={() => this.onShowBlockModel()}
  //           goToChat={(item) => this.goToChat(item)}
  //           selectedRowDetail={this.state.selectedRowDetail}
  //         />
  //       }

  //       {
  //         this.state.showBlockModel &&
  //         <BlockModelCompo
  //           showBlockModel={this.state.showBlockModel}
  //           onBlock={(item) => this.onBlock(item)}
  //           hideBlockModel={() => this.setState({ showBlockModel: false })}
  //           selectedRowDetail={this.state.selectedRowDetail}
  //         />
  //       }

  //       <Spinner visible={this.state.showSpinner} />

  //     </SafeAreaView>
  //   );
  // }

  getWindowDimension(event) {
    this.setState({ screen_Height: event.nativeEvent.layout.height - 60 })
  }

  onBackFromComments = (params) => {
    if (params) {
      let list = this.state.watchList;
      let listRow = this.state.watchList[this.state.selectedIndex];
      listRow['comment_count'] = params;
      listRow['is_commented'] = 1;
      list[this.state.selectedIndex] = listRow
      this.setState({ watchList: list })
    }
  }





  ModelShow = () => (
    <Modal
      backdropOpacity={0.9}
      transparent={true}
      isVisible={this.state.ShowModalVisible}>
      <View style={{
        flex: 1, backgroundColor: 'rgba(52, 52, 52, 0.9)',
      }}>
        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', }}>
          <Text style={{ color: "#ffffff", fontFamily: fonts.SFProDisplay_Heavy, fontSize: 22, marginTop: 20 }}>+{this.state.currentCoins}</Text>

        </View>


        {/* <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', }}>
          <View style={{ width: '90%', backgroundColor: 'white', borderRadius: 25, justifyContent: "space-between", alignItems: "center", paddingVertical: 25 }}>
            <Text style={{ color: "#383841", fontFamily: fonts.Poppins_Medium, fontSize: 16, }}>Coins</Text>

            <View style={{ marginVertical: 20, alignItems: "center" }}>
              <Image
                source={Images.coin}
                style={{ height: 70, width: 70, borderRadius: 35, resizeMode: "cover" }}
              />
              <Text style={{ color: "#383841", fontFamily: fonts.SFProDisplay_Heavy, fontSize: 22, marginTop: 20 }}>+{this.state.currentCoins}</Text>

            </View>

          </View>
          <TouchableOpacity onPress={() => this.setState({ ShowModalVisible: false, currentCoins: 0 })}>
            <Image
              source={Images.grey_close}
              style={{ height: 50, width: 50, borderRadius: 25, resizeMode: "contain", marginTop: 20 }}
            />
          </TouchableOpacity>
        </View> */}
      </View>
    </Modal >
    // <Modal
    //   animationType="fade"
    //   transparent={true}
    //   visible={this.state.ShowModalVisible}
    //   onRequestClose={() => {
    //     this.setState({ ShowModalVisible: false })
    //   }}>
    //   <View style={{
    //     flex: 1,
    //     backgroundColor: 'rgba(52, 52, 52, 0.8)',
    //     justifyContent: 'center',
    //   }}>
    //     <View style={{
    //       borderWidth: 1,
    //       borderRadius: (25, 25, 5, 5),
    //       backgroundColor:'#ffffff',
    //       elevation: 5,
    //     }}>

    //       <Text style={{ fontSize: 30 }}>ggggghghjgshagdhggfghsfghd</Text>
    //     </View>
    //   </View>
    // </Modal>

  );

  render() {
    // console.log(JSON.stringify(this.state.watchList) + ">>>>>>>>>>>>>>>>>>>>>>>>>watchList");
    // console.log(JSON.stringify(this.state.adminshowindex) + ">>>>>>>>>>>>>>>>>>>>>>>>>adminshowindex");



    return (<SafeAreaView style={styles.container}>
      {this.state.ShowModalVisible == true ? this.ModelShow() : null}
      <View style={{ flex: 1 }}>
        <CoinAndSwitchComponent coin={this.state.coins} admintotalView={this.state.adminshowindex} currentshowView={this.state.currentindex} toggleValue={true} />
        {this.state.screen_Height == 0 && Platform.OS == 'android' && <View
          style={{
            position: 'absolute',
            left: 0,
            right: 0,
            top: 0,
            bottom: 0,
          }}
          onLayout={(event) => this.getWindowDimension(event)}
        />}


        {/* <View style={{ width: '100%', height: Platform.OS == 'android' ? this.state.screen_Height : screenCellHeight }}>
          <View style={[styles.subContainer]}>
            <UserDetailShowAds
            />

            <View style={styles.squareView}>
              <Image
                style={styles.squareView}
                source={Images.watch_video}
              />
            </View>
          </View>
        </View> */}

        {this.state.currentindex != 0 && this.state.adminshowindex == this.state.currentindex ?
          <FlatList
            data={this.state.arrAdminPost}
            extraData={this.state}
            keyExtractor={(item, index) => index}
            pagingEnabled
            onMomentumScrollEnd={this.handleAdminPage}
            scrollEventThrottle={16}
            windowSize={5}
            removeClippedSubviews={true}
            renderItem={({ item, index }) => (
              <View style={{ width: '100%', height: Platform.OS == 'android' ? this.state.screen_Height : screenCellHeight }}>
                {/* {console.log("<<<<<<<<<<<<<?????????????????????????????",item)} */}
                <View style={[styles.subContainer]}>
                  <UserDetailComponent
                    onUserPress={(item) => this.setState({ showInfoModel: true, selectedRowDetail: item })}
                    item={item} />
                  {item.post_detail && item.post_detail.length > 0 ? (
                    <View style={styles.squareView}>
                      {item.post_detail[0].type == 'VIDEO' ? (
                        <VideoWithProgress
                          methodVideoPause={(index) => this.methodVideoPause(index)}
                          uri={Config.imageUrl + item.post_detail[0].media}
                          paused={
                            this.state.selectedIndex == index &&
                              !this.state.videoPaused
                              ? false
                              : true
                          }
                          index={index}
                        />
                      ) : (
                          <View style={styles.squareView}>
                            <ImageViewSwiper
                              onImagePress={(item, index) => this.onImagePress(item, index)}
                              index={index}
                              data={item.post_detail} />
                          </View>
                        )}

                      {this.state.overlayShow == index && !item.admin &&
                        this.methodRenderOverlayView(item, index)}
                    </View>
                  ) : (
                      <View style={styles.squareView}>
                        <Image
                          style={styles.squareView}
                          source={Images.watch_video}
                        />
                      </View>
                    )}
                  {
                    !item.admin && this.state.selectedIndex == index && item.user.id != Helper.userInfo.id ?
                      <View style={{ flexDirection: 'row' }}>
                        <ProgressBar
                          progress={this.state.progress}
                          color="#5856d6"
                          unfilledColor="#282828"
                          borderColor="transparent"
                          width={screenWidth - 15}
                          borderRadius={0}
                          height={7}
                        />
                        <View style={{ height: 8, width: 15, backgroundColor: '#e1d500' }} />
                      </View>
                      : null
                  }

                  {this.state.emojiViewShowIndex == index &&
                    this.rendeEmojiView()}
                  {!item.admin &&
                    <VideoDetailCompo
                      isEmojiShow={
                        this.state.emojiViewShowIndex == index ? false : true
                      }
                      likeImage={
                        item.is_like && item.is_like < 7
                          ? arrEmaoji[parseInt(item.is_like) - 1].image
                          : Images.heart_grey
                      }
                      EmojiViewShow={index => this.methodEmojiViewShow(index)}
                      onLikePress={(post_id, is_like, index) =>
                        this.onLikePress(post_id, is_like, index)
                      }
                      item={item}
                      index={index}
                      onChatPress={post_id => pushToScreen('Comments', { post_id, onBackFromComments: this.onBackFromComments.bind(this) })}
                    />
                  }
                </View>


              </View>
            )
            }
          />
          :
          <FlatList
            ref={(ref) => { this.flatListRef = ref; }}
            data={this.state.watchList}
            extraData={this.state}
            keyExtractor={(item, index) => index}
            pagingEnabled
            onEndReached={this.onEndReached}
            onEndReachedThreshold={0.1}
            // viewabilityConfig={this.viewabilityConfig}
            // onViewableItemsChanged={this._onViewableItemsChanged}
            onMomentumScrollEnd={this.handlePageChange}
            scrollEventThrottle={16}
            windowSize={5}
            // getItemLayout={(_data, index) => ({
            //   length: screenHeight - screenHeight / 4.6,
            //   offset: (screenHeight - screenHeight / 4.6) * index,
            //   index,
            // })}
            refreshControl={
              <RefreshControl
                refreshing={this.state.isRefreshing}
                onRefresh={this.onRefresh.bind(this)}
              />
            }
            removeClippedSubviews={true}
            renderItem={({ item, index }) => (
              <View style={{ width: '100%', height: Platform.OS == 'android' ? this.state.screen_Height : screenCellHeight }}>
                {/* {console.log("<<<<<<<<<<<<<?????????????????????????????",item)} */}
                <View style={[styles.subContainer]}>
                  <UserDetailComponent
                    onUserPress={(item) => this.setState({ showInfoModel: true, selectedRowDetail: item })}
                    item={item} />
                  {item.post_detail && item.post_detail.length > 0 ? (
                    <View style={styles.squareView}>
                      {item.post_detail[0].type == 'VIDEO' ? (
                        <VideoWithProgress
                          methodVideoPause={(index) => this.methodVideoPause(index)}
                          uri={Config.imageUrl + item.post_detail[0].media}
                          paused={
                            this.state.selectedIndex == index &&
                              !this.state.videoPaused
                              ? false
                              : true
                          }
                          index={index}
                        />
                      ) : (
                          <View style={styles.squareView}>
                            <ImageViewSwiper
                              onImagePress={(item, index) => this.onImagePress(item, index)}
                              index={index}
                              data={item.post_detail} />
                          </View>
                        )}

                      {this.state.overlayShow == index && !item.admin &&
                        this.methodRenderOverlayView(item, index)}
                    </View>
                  ) : (
                      <View style={styles.squareView}>
                        <Image
                          style={styles.squareView}
                          source={Images.watch_video}
                        />
                      </View>
                    )}
                  {
                    !item.admin && this.state.selectedIndex == index && item.user.id != Helper.userInfo.id ?
                      <View style={{ flexDirection: 'row' }}>
                        <ProgressBar
                          progress={this.state.progress}
                          color="#5856d6"
                          unfilledColor="#282828"
                          borderColor="transparent"
                          width={screenWidth - 15}
                          borderRadius={0}
                          height={7}
                        />
                        <View style={{ height: 8, width: 15, backgroundColor: '#e1d500' }} />
                      </View>
                      : null
                  }

                  {this.state.emojiViewShowIndex == index &&
                    this.rendeEmojiView()}
                  {!item.admin &&
                    <VideoDetailCompo
                      isEmojiShow={
                        this.state.emojiViewShowIndex == index ? false : true
                      }
                      likeImage={
                        item.is_like && item.is_like < 7
                          ? arrEmaoji[parseInt(item.is_like) - 1].image
                          : Images.heart_grey
                      }
                      EmojiViewShow={index => this.methodEmojiViewShow(index)}
                      onLikePress={(post_id, is_like, index) =>
                        this.onLikePress(post_id, is_like, index)
                      }
                      item={item}
                      index={index}
                      onChatPress={post_id => pushToScreen('Comments', { post_id, onBackFromComments: this.onBackFromComments.bind(this) })}
                    />
                  }
                </View>


              </View>
            )
            }
          />}

        {this.state.reportViewOverleyShowIndex != -1 && this.state.arrReportList.length > 0 &&
          this.renderReportModal()}

        {
          this.state.showInfoModel &&
          <InfoModelCompo
            showInfoModel={this.state.showInfoModel}
            hideInfoModel={() => this.setState({ showInfoModel: false })}
            onShowBlockModel={() => this.onShowBlockModel()}
            goToChat={(item) => this.goToChat(item)}
            selectedRowDetail={this.state.selectedRowDetail}
          />
        }

        {
          this.state.showBlockModel &&
          <BlockModelCompo
            showBlockModel={this.state.showBlockModel}
            onBlock={(item) => this.onBlock(item)}
            hideBlockModel={() => this.setState({ showBlockModel: false })}
            selectedRowDetail={this.state.selectedRowDetail}
          />
        }

        <Spinner visible={this.state.showSpinner} />
      </View>
    </SafeAreaView>
    )
  }


  methodRenderOverlayView(item, index) {
    // console.log(item.social_link, ">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>so");

    // var url = "http://example.com?myVar=test&otherVariable=someData&number=123"
    var socillinkt = item.social_link
    let sociltext = null
    let socilcolor = null

    if (socillinkt.includes("soundcloud")) {
      sociltext = "SoundCloud"
      socilcolor = "#ED7217"
    } else if (socillinkt.includes("facebook")) {
      sociltext = 'Facebook'
      socilcolor = "#3c5a9a"
    } else if (socillinkt.includes("instagram")) {
      sociltext = 'Instagram'
      socilcolor = "#dc4e5f"
    } else if (socillinkt.includes("twitter")) {
      sociltext = 'Twitter'
      socilcolor = "#59a8dc"
    } else if (socillinkt.includes("youtub")) {
      sociltext = "YouTube"
      socilcolor = "#d63b32"
    } else if (socillinkt.includes("youtu")) {
      sociltext = "YouTube"
      socilcolor = "#d63b32"
    } else if (socillinkt.includes("snapchat")) {
      sociltext = 'Snapchat'
      socilcolor = "#fffb53"
    } else if (socillinkt.includes("tiktok")) {
      sociltext = 'TikTok'
      socilcolor = "#07080a"
    } else if (socillinkt.includes("behance")) {
      sociltext = 'Behance'
      socilcolor = "#346BF6"
    } else if (socillinkt.includes("dribbble")) {
      sociltext = 'Dribbble'
      socilcolor = "#D1447A"
    } else if (socillinkt.includes("twitch")) {
      sociltext = 'Twitch'
      socilcolor = "#7E4BE7"
    } else if (socillinkt.includes("tumblr")) {
      sociltext = 'Tumblr'
      socilcolor = "#ED7217"
    } else if (socillinkt.includes("kickstarter")) {
      sociltext = 'Kickstarter'
      socilcolor = "#3BB573"
    } else if (socillinkt.includes("bandcamp")) {
      sociltext = 'Bandcamp'
      socilcolor = "#43A1B4"
    } else if (socillinkt.includes("reddit")) {
      sociltext = 'Reddit'
      socilcolor = "#E95627"
    } else if (socillinkt.includes("venmo")) {
      sociltext = 'Venmo'
      socilcolor = "#6FA2CD"
    } else if (socillinkt.includes("cash.app")) {
      sociltext = 'Cash App'
      socilcolor = "#60B636"
    } else if (socillinkt.includes("gofundme")) {
      sociltext = 'GoFundMe'
      socilcolor = "#59B773"
    } else if (socillinkt.includes("indiegogo")) {
      sociltext = 'Indiegogo'
      socilcolor = "#CD3473"
    } else if (socillinkt.includes("deviantart")) {
      sociltext = 'DeviantArt'
      socilcolor = "#62C85A"
    } else if (socillinkt.includes("caseyatkins")) {
      sociltext = 'Caseyatkins'
      socilcolor = "#6260FF"
    }

    return (
      <TouchableOpacity
        onPress={() => this.setState({ overlayShow: -1 })}
        style={{
          flex: 1,
          position: 'absolute',
          zIndex: 1,
          height: '100%',
          width: '100%',
        }}>
        <BlurView
          style={styles.absolute}
          blurType="dark"
          blurAmount={10}
          reducedTransparencyFallbackColor="black"
        />
        <View
          style={{
            padding: 10,
            alignSelf: 'center',
            alignItems: 'center',
            justifyContent: 'center',
            flex: 1,
            position: 'absolute',
            zIndex: 1,
            height: '100%',
            width: '100%',
          }}>
          <Text
            style={{
              fontFamily: fonts.SFProMedium,
              fontSize: 17,
              color: 'white',
            }}>
            {item.description}
          </Text>
          {item.social_link != null && item.social_link != '' &&
            <View style={{
              alignItems: 'center', marginTop: 10
            }}>
              {/* <Image style={{ width: 30, height: 30, resizeMode: 'contain' }}
                source={sociltext} /> */}
              <TouchableOpacity onPress={() => this.openUrl(item.social_link)}>
                <Text
                  style={{
                    marginLeft: 10,
                    fontFamily: fonts.SFProMedium,
                    fontSize: 17,
                    color: 'white',
                  }}>
                  {item.social_link}
                </Text>
              </TouchableOpacity>

              <TouchableOpacity onPress={() => this.openUrl(item.social_link)} style={{
                marginTop: 15,
                borderRadius: 30,
                backgroundColor: socilcolor, alignItems: 'center'

              }}>
                <Text
                  style={{
                    paddingHorizontal: 30, paddingVertical: 12,
                    fontFamily: fonts.Montserrat_Bold,
                    fontSize: 20, marginBottom: 2,
                    color: 'white',
                    textAlign: 'center'
                  }}>
                  {sociltext}
                </Text>
              </TouchableOpacity>

            </View>
          }
          {/* <View
            style={{
              width: 200,
              height: 40,
              borderRadius: 20,
              marginTop: 10,
              backgroundColor: Colors.orange,
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <Text
              style={{
                fontFamily: fonts.SFProDisplay_Bold,
                fontSize: 17,
                color: 'white',
              }}>
              {'Soundcloud'}
            </Text>
          </View> */}
        </View>
        <View
          style={{
            flexDirection: 'row',
            zIndex: 1,
            position: 'absolute',
            bottom: 15,
            left: 15,
          }}>
          {item.is_flag_by_me == 0 &&
            item.is_flag_clear == 0 &&
            item.user &&
            item.user.flag_ban == 0 &&
            Helper.userSettings.today_flag < 5 ? (
              <TouchableOpacity
                style={{ marginRight: 15 }}
                onPress={() =>
                  this.setState({ reportViewOverleyShowIndex: index })
                }>
                <Image style={{ width: 40, height: 40 }} source={Images.flag} />
              </TouchableOpacity>
            ) : (
              item.is_flag_by_me == 1 &&
              item.is_flag_clear == 0 &&
              item.user &&
              item.user.flag_ban == 0 && (
                <TouchableOpacity style={{ marginRight: 15 }}>
                  <Image
                    style={{ width: 40, height: 40 }}
                    source={Images.flag_colors}
                  />
                </TouchableOpacity>
              )
            )}
          <TouchableOpacity onPress={() => this.methodFavourite(item, index)}>
            <Image
              style={{ width: 40, height: 40 }}
              source={item.is_save == 1 ? Images.starColor : Images.starGrey}
            />
          </TouchableOpacity>
        </View>
      </TouchableOpacity>
    );
  }
  startSeekVideo = () => { };
  methodFavourite(item, index) {
    this.setState({ showSpinner: true });
    Helper.makeRequest({
      url: 'post-save',
      method: 'POST',
      data: { post_id: item.id },
    }).then(resp => {
      if (resp.status == 'true') {
        let dic = item;
        dic.is_save = item.is_save == 0 ? 1 : 0;
        this.state.watchList.splice(index, 1, dic);
      }
      Helper.showToast(resp.message);
      this.setState({ showSpinner: false });
    });
  }
  getReportList() {
    this.setState({ showSpinner: true });
    Helper.makeRequest({ url: 'get-flag-reason', method: 'POST', data: '' }).then(
      resp => {
        if (resp.status == 'true') {
          this.state.arrReportList = resp.data;
        } else {
          this.state.arrReportList = [];
        }
        this.setState({ showSpinner: false });
      },
    );
  }
  endSeekVideo = time => { };
  rendeEmojiView() {
    return (
      <Animated.View
        style={{
          flex: 1,
          position: 'absolute',
          flexDirection: 'row',
          zIndex: 1,
          backgroundColor: '#fff',
          borderRadius: 23,
          height: 46,
          justifyContent: 'center',
          alignItems: 'center',
          elevation: 2,
          shadowColor: 'rgba(0,0,0,0.5)',
          shadowOpacity: 1.0,
          paddingHorizontal: 5,
          bottom: 80,
          right: 5,
        }}>
        {arrEmaoji.map((item, index) => {
          return this.renderEmoji(item, index);
        })}
      </Animated.View>
    );
  }
  renderEmoji(item, index) {
    return (
      <TouchableOpacity onPress={() => this.methodEmojiClick(item.id)}>
        <FastImage
          style={{ width: 36, height: 36, marginLeft: index == 0 ? 0 : 5 }}
          source={item.gif}
        // source={{
        //     uri: index == 0 ? item.url : item.url,
        // }}
        />
      </TouchableOpacity>
    );
  }
  methodEmojiViewShow(index) {
    this.setState({ emojiViewShowIndex: index });
  }
  methodEmojiClick(id) {
    let dic = this.state.watchList[this.state.emojiViewShowIndex];
    dic.is_like = id;
    this.state.watchList.splice(this.state.emojiViewShowIndex, 1, dic);
    let post_id = dic.id;
    let is_like = id;
    let index = this.state.emojiViewShowIndex;
    this.setState({ emojiViewShowIndex: -1 });

    this.onLikePress(post_id, is_like, index);
  }

  backAction = () => {
    if (Actions.currentScene == 'Watch') {
      Alert.alert('Hold on!', 'Are you sure you want to Exit App', [
        {
          text: 'Cancel',
          onPress: () => null,
          style: 'cancel',
        },
        { text: 'YES', onPress: () => BackHandler.exitApp() },
      ]);
      return true;
    }
  };

  updateStatusBar = () => {
    StatusBar.setBackgroundColor('black');
    StatusBar.setHidden(false);
    StatusBar.setBarStyle('light-content');
  };

  async componentDidMount() {
    this.backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      this.backAction,
    );

    let userInfo = await Helper.getDataFromAsync('userInfo');
    if (userInfo) {
      Helper.userInfo = userInfo;
    }


    this.functionEvent = this.props.navigation.addListener('didFocus', () => {
      this.methodDidMountCall();
      if (this.state.watchList.length > 0) {
        this.handelProgressWorking();
      }
    });

    this.getShowAdsAdminApi();
    this.methodDidMountCall();
    this.getViewCoinsListner = DeviceEventEmitter.addListener('view_post_response', response => this.setViewCoins(response));
  }

  setViewCoins = (params) => {
    this.setState({
      coins: this.state.coins + params,
      currentCoins: params,
      ShowModalVisible: true,
      videoPaused: true
    })
    setTimeout(() => {
      this.setState({
        ShowModalVisible: false,
        currentCoins: 0,
        videoPaused: false

      })
    }, 3000);
  }


  methodDidMountCall() {
    this.state.current_page = 1

    this.getSettingApi();
    this.getReportList();
    this.getModalContent();
    SocketConnect.checkConnection('connect', cb => {
      console.log('SocketConnect------------', cb);
    });

    this.setState({ videoPaused: false });
  }

  componentWillUnmount() {
    if (this.didBlurListener) {
      this.didBlurListener.remove();
    }
    if (this.backHandler) {
      this.backHandler.remove();
    }
    if (this.functionEvent) {
      this.functionEvent.remove();
    }
    if (this.getViewCoinsListner) {
      this.getViewCoinsListner.remove();
    }
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.lightBlack,
  },
  absolute: {
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
  },
  subContainer: {
    paddingVertical: 20, flex: 1,
  },

  video: {
    width: screenWidth,
    aspectRatio: 1,
    flex: 1,
  },
  squareView: {
    height: screenWidth,
    width: screenWidth,
    justifyContent: 'center',
  },
});

const arrEmaoji = [
  {
    id: 1,
    url:
      'https://raw.githubusercontent.com/duytq94/facebook-reaction-animation2/master/Images/like.gif',
    image: Emojiimages.like_static_fill,
    gif: Emojiimages.like_gif,
  },
  {
    id: 2,
    url:
      'https://raw.githubusercontent.com/duytq94/facebook-reaction-animation2/master/Images/love.gif',
    image: Emojiimages.love_gif,
    gif: Emojiimages.love_gif,
  },
  {
    id: 3,
    url:
      'https://raw.githubusercontent.com/duytq94/facebook-reaction-animation2/master/Images/haha.gif',
    image: Emojiimages.haha_static,
    gif: Emojiimages.haha_static,
  },
  {
    id: 4,
    url:
      'https://raw.githubusercontent.com/duytq94/facebook-reaction-animation2/master/Images/wow.gif',
    image: Emojiimages.wow_static,
    gif: Emojiimages.wow_static,
  },
  {
    id: 5,
    url:
      'https://raw.githubusercontent.com/duytq94/facebook-reaction-animation2/master/Images/sad.gif',
    image: Emojiimages.sad_static,
    gif: Emojiimages.sad_gif,
  },
  {
    id: 6,
    url:
      'https://raw.githubusercontent.com/duytq94/facebook-reaction-animation2/master/Images/angry.gif',
    image: Emojiimages.angry_static,
    gif: Emojiimages.angry_gif,
  },
];