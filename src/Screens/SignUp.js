import React, { useState, useEffect } from 'react'
import {
    Text, View, ImageBackground, LayoutAnimation,
    TextInput, Image,
    StyleSheet, StatusBar,
    Keyboard,
    TouchableOpacity
} from 'react-native'
import Spinner from 'react-native-loading-spinner-overlay';

import {
    GButton, Images, GAuthHeader, Fontfamily,
    ScrollContainer,
    Colors, ScrollButtonTop,
} from '../common';

import { BulletComponent } from '../components'
import { Actions } from 'react-native-router-flux';
import Helper from '../lib/Helper';
import { validationFunction } from '../common/validationFunctions';
import ModalFilterPicker from '../components/Filterpicker/src/index';
import Toast from 'react-native-root-toast';
import pushToScreen from '../navigation/NavigationActions';


export const SignUp = ({ children, style }) => {
    const [state, setstate] = useState(false)
    const [showCountryModel, setShowCountryModel] = useState(false);
    const [countryCode, setCountryCode] = useState('1')
    const [countryName, setCountryName] = useState('US')
    const [countryNameFull, setCountryNameFull] = useState('United States')
    const [mobile_number, set_mobile_number] = useState('');
    const [showRightTick, setRightTick] = useState(null);
    const [btnDisable, setBtnDisable] = useState(true)
    const [showSpinner, setSpinner] = useState(false)
    const [otp, setOtp] = useState(null);
    const [countryList, setCountryList] = useState([]);


    const onSelect = (country) => {
        setCountryCode(country.phonecode);
        setCountryName(country.sortname);
        setCountryNameFull(country.name);

        setShowCountryModel(false);
        console.log(JSON.stringify(country) + "country");
    }

    useEffect(() => {
        if (mobile_number.length >= 10) {
            check_user_PhoneNumber(mobile_number)
        }
    }, [countryCode]);

    const onPhoneInput = (text) => {
        setstate(true)
        setBtnDisable(true)
        set_mobile_number(text)
        if (text.length < 10) {
            setRightTick(null)
        }
    }

    useEffect(() => {
        setSpinner(true)
        Helper.makeRequest({ url: "country", method: "POST", data: '' }).then((resp) => {
            if (resp.status == 'true') {
                setCountryList(resp.data)
            } else {
                Toast.show(resp.message)
            }
            setSpinner(false)
        })
    }, []);

    useEffect(() => {
        const timer = setTimeout(() => {
            if (mobile_number.length >= 10) {
                if (validationFunction.checkNumber("Mobile Number", 10, 15, mobile_number)) {
                    check_user_PhoneNumber(mobile_number)
                } else {
                    setRightTick(null)
                }
            }
        }, 1000);
        return () => clearTimeout(timer);
    }, [mobile_number]);



    const check_user_PhoneNumber = (text) => {
        Keyboard.dismiss();
        var data = {
            country_code: countryCode,
            phone: text,
            is_send_otp: 0
        }
        setSpinner(true)
        Helper.makeRequest({ url: "check-user-phone", method: "POST", data: data }).then((resp) => {
            if (resp.status == 'true') {
                setRightTick(true)
                setBtnDisable(false)
                setSpinner(false)
                setOtp(resp.data.otp)
            } else {
                setRightTick(false)
                setBtnDisable(true)
                setSpinner(false)
                setOtp(null)
            }
        })
    }

    const onSignUp = () => {
        // Actions.Verification({ whereFrom: "SignUp", phone: mobile_number, otp, country_code: countryCode, types: 'SIGNUP' })
        pushToScreen('Verification', { whereFrom: "SignUp", phone: mobile_number, otp, country_code: countryCode, location: countryNameFull, types: 'SIGNUP' })
        console.log(JSON.stringify(countryNameFull) + "location: countryNameFull");
    }

    return (
        <ImageBackground resizeMode='cover'
            source={Images.Loginbg}
            style={{ flex: 1, alignItems: 'center' }}>
            {/* <StatusBar hidden /> */}
            <GAuthHeader bArrow title1="Sign Up" rightItem>
                <BulletComponent numberof={3} activeIndex={1} />
            </GAuthHeader>

            <View style={{ height: 100, width: 100, marginTop: -15 }}>
                <Image
                    source={Images.cat}
                    style={{ height: 100, width: 100, display: state ? "flex" : "none", resizeMode: "contain" }}
                />
            </View>


            <ScrollContainer style={{ borderWidth: 0, width: '90%' }} containerStyle={{ alignItems: 'center', justifyContent: 'center', flex: 1, width: "100%", marginTop: -100 }}>
                <View style={Style.inputBox}>
                    <TouchableOpacity style={{ flexDirection: "row" }} onPress={() => setShowCountryModel(true)}>
                        <Text style={Style.CnameStyle}>{countryName}</Text>
                        <Text style={Style.CcStyle}>+{countryCode}</Text>
                    </TouchableOpacity>
                    <View style={{ width: '60%' }}>
                        <TextInput
                            placeholder="Your Phone"
                            autoCorrect={false}
                            style={Style.UerinputBox}
                            value={mobile_number}
                            keyboardType='number-pad'
                            onChangeText={(text) => onPhoneInput(text)}
                        />
                    </View>
                    <View style={{ flexDirection: "row", justifyContent: "flex-end", flex: 1 }}>
                        <Image
                            source={showRightTick !== null ? showRightTick ? Images.ticks : Images.cross_pink : null}
                            style={{ height: 25, width: 25, resizeMode: "contain", alignSelf: "flex-end" }}
                        />
                    </View>
                </View>
            </ScrollContainer>

            <ScrollButtonTop>
                <GButton
                    height={65}
                    disabled={btnDisable || mobile_number.length < 10 ? true : false}
                    radius={32.5}
                    onPress={() => { onSignUp() }}
                    bText='Next'
                    txtcolor="white"
                    bgColor={btnDisable || mobile_number.length < 10 ? Colors.btxtColor : Colors.abuttonColor}
                />
            </ScrollButtonTop>

            <ModalFilterPicker
                placeholderText="Search Country..."
                visible={showCountryModel}
                onSelect={onSelect}
                onCancel={() => setShowCountryModel(false)}
                options={countryList}
            />

            <Spinner visible={showSpinner} />
        </ImageBackground>
    );
}

const Style = StyleSheet.create({
    inputBox: {
        marginTop: 15, height: 70, borderRadius: 40,
        alignItems: 'center', backgroundColor: 'white',
        flexDirection: 'row', width: '100%',
        paddingHorizontal: 15,
    },
    CcStyle: {
        fontSize: 22, marginRight: 5,
        fontFamily: Fontfamily.MEB,
    },
    UerinputBox: {
        height: 50, width: '100%',
        backgroundColor: 'white',
        fontSize: 20,
        fontFamily: Fontfamily.MEB,
    },
    CnameStyle: {
        marginRight: 5, marginTop: 5,
        fontFamily: 'HelveticaNeueThin', fontSize: 18, fontWeight: '400'
    },

})








