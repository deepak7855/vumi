import React, { Component } from 'react';
import { View, Text, StyleSheet, Image, StatusBar, Keyboard, LayoutAnimation, Platform } from 'react-native';
import { GHeader, Images, IconInput, GButton } from '../common';
import fonts from '../common/fonts';
import { ViewSendCoinModel } from '../components/Coins/ViewSendCoinModel';
import Helper from '../lib/Helper';
import Spinner from 'react-native-loading-spinner-overlay';
import pushToScreen from '../navigation/NavigationActions';
import LottieView from 'lottie-react-native';
import ImageLoadView from '../common/ImageLoadView';
// import JsonFiles from '../assets/JsonFiles/';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'

export default class SendCoin extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showViewSendModel: false,
            showSpinner: false,
            other_user_id: this.props.preData ? this.props.preData : Helper.userInfo.id == this.props.OtherUserData.otheruser.id ? this.props.OtherUserData.user : this.props.OtherUserData.otheruser,
            user: Helper.userInfo,
            coins: '',
            note: '',
            KeyboardHeight: 0

        };
    }
    componentDidMount() {
        if (Platform.OS === 'ios') {
            this.keyboardDidShowListener = Keyboard.addListener('keyboardDidShow', this._keyboardDidShow.bind(this));
            this.keyboardDidHideListener = Keyboard.addListener('keyboardDidHide', this._keyboardDidHide.bind(this));
        }
        this._onIsreadOn()
    }

    _keyboardDidShow(e) {
        const visibleHeight = e.endCoordinates.height + 10;
        this.setState({ KeyboardHeight: visibleHeight });
    }
    _keyboardDidHide() {
        LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
        this.setState({ KeyboardHeight: 0 })
    }
    componentWillUnmount() {
        if (Platform.OS === 'ios') {
            this.keyboardDidShowListener.remove();
            this.keyboardDidHideListener.remove();
        }
    }
    _onIsreadOn() {
        if(this.props.OtherUserData){
         let data = {
            coin_id: this.props.OtherUserData.id,
            
        }
        

        Helper.makeRequest({ url: "read-coin-history", method: "POST", data: data }).then((resp) => {
            
            console.log(resp.message);
        })

    }
    }
    _onsendPress() {
        if (this.state.coins == '') {
            return Helper.showToast('Please enter coins')
        }
        let data = {
            other_user_id: this.state.other_user_id.id,
            coins: this.state.coins,
            note: this.state.note
        }
        this.setState({ showSpinner: true })

        Helper.makeRequest({ url: "send-coin", method: "POST", data: data }).then((resp) => {
            this.setState({ showSpinner: false })
            if (resp.status == 'true') {
                pushToScreen('CoinOld', {})
            } else {
                Helper.showToast(resp.message)
            }
        })
    }

    onChangeText(txt) {
        if (txt == "" || (txt > 0 && Number(Helper.userSettings.user_coins) > Number(txt))) {
            this.setState({ coins: txt })
        }
    }

    render() {

        // console.log(this.props.preData, "this.props.preData");
        // console.log(this.props.OtherUserData.otheruser, "this.props.OtherUserData.otheruser");


        console.log(this.state.other_user_id, "other_user_id");
        console.log(this.state.user, "user");

        return (
            <View style={{ flex: 1, flexDirection: "column" }}>
                <Spinner visible={this.state.showSpinner} />
                <StatusBar barStyle="dark-content" backgroundColor="white" />
                <View style={{ flex: 1, flexDirection: "column" }}>
                    <GHeader
                        bArrow
                        greyBackButton
                        color={'#606076'}
                        title1="Send Coins"
                        backgroundColor="white"
                        showCoinsAtRight
                    />
                    <KeyboardAwareScrollView contentContainerStyle={{ flexGrow: 1 }} showsVerticalScrollIndicator={false}
                        style={{ paddingBottom: Platform.OS === 'ios' ? this.state.KeyboardHeight : 0 }} >
                        <View style={styles.topContainer} >
                            <View style={styles.center}>
                                {/* <Image
                                    style={styles.userImage}
                                    source={{ uri: Helper.getImageUrl(this.state.user.profile_image) }}
                                /> */}
                                <ImageLoadView
                                    resizeMode="cover"
                                    style={styles.userImage}
                                    source={{ uri: Helper.getImageUrl(this.state.user.profile_image) }}
                                />
                                <Text style={styles.user}>{this.state.user && this.state.user.username}</Text>
                            </View>

                            <View style={[styles.dotView, { height: 180, width: 180, zIndex: -1, position: 'relative' }]}>
                                <LottieView source={Images.SendingCoins} autoPlay loop />
                                {/* 
                                {
                                    [0, 1, 2, 3].map((index) => (
                                        <View style={{ height: 6, width: 6, borderRadius: 3, backgroundColor: index == 3 ? '#ffe837' : '#ebebeb', marginHorizontal: 2 }} />
                                    ))
                                }

                                <View style={{ height: 9, width: 9, borderRadius: 4.5, backgroundColor: '#ffe837', marginHorizontal: 2 }} /> */}

                            </View>
                            <View style={styles.center}>
                                {/* <Image
                                    style={styles.userImage}
                                    source={{ uri: Helper.getImageUrl(this.state.other_user_id.profile_image) }}
                                /> */}
                                <ImageLoadView
                                    resizeMode="cover"
                                    style={styles.userImage}
                                    source={{ uri: Helper.getImageUrl(this.state.other_user_id.profile_image) }}
                                />
                                <Text style={styles.user}>{this.state.other_user_id.username}</Text>
                            </View>
                        </View>

                        <View style={[styles.midContainer]}>
                            <View>
                                <IconInput
                                    limagePath={Images.profile.coin}
                                    style={styles.coinInput}
                                    placeholder={"Coins"}
                                    keyboardType="numeric"
                                    placeholderTextColor="#dcdce2"
                                    onChangeText={(txt) => this.onChangeText(txt)}
                                    value={this.state.coins}
                                    leftImagetyle={{ height: 35, width: 35, borderRadius: 17.5 }}
                                />

                                <IconInput
                                    style={styles.noteInput}
                                    placeholder={"Add a note…"}
                                    placeholderTextColor="#dcdce2"
                                    multiline
                                    onChangeText={(txt) => this.setState({ note: txt })}
                                    leftImagetyle={{ height: 35, width: 35, borderRadius: 17.5 }}
                                />

                            </View>

                            <View >
                                <View style={styles.name}>
                                    <Text style={styles.textOne}>You’re sending </Text>
                                    <Text style={styles.textTwo}>{this.state.other_user_id.username} {this.state.coins}</Text>
                                    <Text style={{ fontSize: 10 }}>vc</Text>
                                </View>

                                <GButton
                                    bgColor="#04e6a0"
                                    radius={30}
                                    height={60}
                                    txtcolor="white"
                                    style={{ marginVertical: 15, }}
                                    bText="VIEWS SEND"
                                    //  onPress={() => this.setState({ showViewSendModel: true })}
                                    onPress={() => this._onsendPress()}
                                />
                            </View>
                        </View>
                    </KeyboardAwareScrollView>
                </View>

                <ViewSendCoinModel
                    showViewSendModel={this.state.showViewSendModel}
                    hideViewModel={() => this.setState({ showViewSendModel: false })}
                />

            </View >
        );
    }
}


const styles = StyleSheet.create({
    topContainer: {
        flexDirection: "row",
        justifyContent: "space-around",
        alignItems: "center",
        paddingHorizontal: 35,
        marginTop: 15
    },

    center: {
        justifyContent: "center",
        alignItems: "center"
    },

    userImage: {
        height: 120,
        width: 120,
        borderRadius: 60,
        resizeMode: "cover"
    },

    user: {
        color: "#4c4c55",
        fontSize: 14,
        fontFamily: fonts.SFProDisplay_Light
    },

    dotView: {
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center"
    },

    midContainer: {
        flexDirection: "column",
        alignItems: "center",
        marginTop: 15,
        justifyContent: "space-between",
        flex: 1,
    },

    coinInput: {
        height: 60,
        fontSize: 23,
        fontFamily: fonts.SFProDisplay_Bold,
        color: "#626262"
    },

    noteInput: {
        height: 60,
        fontSize: 16,
        fontFamily: fonts.Poppins_Medium,
        color: "#626262",
        paddingTop: 18
    },

    name: {
        flexDirection: "row",
        justifyContent: "center"
    },

    textOne: {
        fontFamily: fonts.SFProDisplay_Light,
        fontSize: 14,
        color: "#4c4c55"
    },

    textTwo: {
        fontFamily: fonts.Montserrat_ExtraBold,
        fontSize: 14,
        color: "#4c4c55"
    },



});



