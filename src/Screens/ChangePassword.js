import React, { Component } from 'react'
import { Text, View, Keyboard, KeyboardAvoidingView, StyleSheet, Dimensions, TouchableOpacity, TextInput } from 'react-native'
import {
    GHeader, Colors
} from '../common';

import { Actions } from 'react-native-router-flux';
import fonts from '../common/fonts';
import { validationFunction } from '../common/validationFunctions';
import Toast from 'react-native-root-toast';
import Helper from '../lib/Helper';
import Spinner from 'react-native-loading-spinner-overlay';

export class ChangePassword extends Component {

    constructor(props) {
        super(props);
        this.state = {
            re_password: "",
            old_password: "",
            new_password: ""
        };
    }

    onSave = () => {
        Keyboard.dismiss();
        if (
            validationFunction.checkNotNull("Old Password", 8, 40, this.state.old_password) &&
            validationFunction.checkNotNull("New Password", 8, 40, this.state.new_password) &&
            validationFunction.checkNotNull("Confirm Password", 8, 40, this.state.re_password)
        ) {
            if (this.state.new_password != this.state.re_password) { Toast.show('Password Does Not Match!'); return false }

            var data = {
                old_password: this.state.old_password,
                new_password: this.state.new_password,
            }
            this.setState({ showSpinner: true })
            Helper.makeRequest({ url: "change-password", method: "POST", data: data }).then((data) => {
                if (data.status == 'true') {
                    Toast.show(data.message);
                    Actions.pop();
                } else {
                    Toast.show(data.message);
                }
                this.setState({ showSpinner: false })
            })
        }
    }

    render() {
        return (
            <KeyboardAvoidingView style={{ flexGrow: 1 }}>
                <GHeader
                    bArrow
                    greyBackButton
                    color="#606076"
                    backgroundColor="white"
                    title1="Change Password"
                />
                <View style={{ paddingHorizontal: '10%', justifyContent: "space-between", flex: 1, backgroundColor: "#f5f6fa" }}>

                    <View>
                        <TextInput
                            style={styles.textinput}
                            placeholder='Old Password'
                            secureTextEntry
                            value={this.state.old_password}
                            onChangeText={(text) => { this.setState({ old_password: text }) }}
                        />

                        <TextInput
                            style={styles.textinput}
                            placeholder='New Password'
                            secureTextEntry
                            value={this.state.new_password}
                            onChangeText={(text) => { this.setState({ new_password: text }) }}
                        />
                        <TextInput
                            style={styles.textinput}
                            placeholder='Confirm Password'
                            secureTextEntry
                            value={this.state.re_password}
                            onChangeText={(text) => { this.setState({ re_password: text }) }}
                        />

                    </View>

                    <TouchableOpacity
                        disabled={this.state.new_password == "" || this.state.old_password == "" || this.state.re_password == "" ? true : false}
                        onPress={() => this.onSave()} style={[styles.btn, { opacity: this.state.new_password == "" || this.state.old_password == "" || this.state.re_password == "" ? 0.7 : 1 }]}>
                        <Text style={{ fontSize: 22, fontFamily: fonts.Montserrat_ExtraBold, color: 'white' }}>Save</Text>
                    </TouchableOpacity>
                </View>
                <Spinner visible={this.state.showSpinner} />
            </KeyboardAvoidingView>



        )
    }
}

const styles = StyleSheet.create({
    textinput: { borderBottomColor: Colors.blackgrey, borderBottomWidth: 1, color: Colors.blackgrey, fontSize: 18, fontFamily: fonts.Montserrat_Bold, paddingVertical: 0, marginTop: 50 },
    btn: { marginBottom: 10, backgroundColor: '#5e5ce6', paddingVertical: 10, width: "100%", justifyContent: "center", alignItems: "center", borderRadius: 30 }
});