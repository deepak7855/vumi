import React, { Component } from 'react';
import { View, Text, StyleSheet, FlatList, ActivityIndicator, Image, TouchableOpacity } from 'react-native';
import { SearchInputBarCompo } from '../components/Coins/SearchInputBarCompo';
import { GHeader, colors } from '../common';
import pushToScreen from '../navigation/NavigationActions';
import Helper from '../lib/Helper';
import Spinner from 'react-native-loading-spinner-overlay';
import Config from '../lib/Config';
import ImageLoadView from '../components/ImageLoadView';

const DATA = [0, 1, 2, 3, 4, 5, 6];


export default class MessageSearchList extends Component {
    constructor(props) {
        super(props);
        this.state = {
            SendCoinsData: [],
            searchtype: "",
            keyword: '',
            showSpinner: false,
            first_id_OutCoins: '',
            first_id_InCoins: '',
            currentPage: 1,
            totalPage: 1,
            totalRecord: 0,
            showLoaderPage: false,
            isRefresh: false


        };
    }

    onSearchTypeChange = (type, loader) => {
        this.state.searchtype = type

        if (this.state.searchtype == "") {
            if (this.state.SendCoinsData.length == 0) {
                this.state.currentPage = 1
                this._onSerach(this.state.keyword, true)
            }
        }

        this.setState({ showLoaderPage: false })

    }
    componentDidMount = () => {
        this.state.currentPage = 1
        this._onSerach(this.state.keyword, true)
    };
    _onSerach = (keyword, loader) => {
        this.setState({ keyword: keyword })
        let data = {
            keyword: keyword,
            page: this.state.currentPage
        }
        this.setState({ showSpinner: loader })
        Helper.makeRequest({ url: 'search', method: "POST", data: data }).then((resp) => {
            this.state.showLoaderPage = false

            if (this.state.currentPage == 1) {
                this.state.SendCoinsData = []
            }
            if (resp.status == 'true') {
                // console.log(resp, '<<<<<<')
                if (resp.data.data && resp.data.data.length > 0) {
                    this.state.SendCoinsData = [...this.state.SendCoinsData, ...resp.data.data]
                }
                this.state.totalPage = resp.data.last_page
                this.state.totalRecord = resp.data.total
                console.log("SendCoinsData------", this.state.SendCoinsData.length)

                console.log("totalPage------", this.state.totalPage)
                this.setState({ isRefresh: !this.state.isRefresh });

            } else {
                // Helper.showToast(resp.message)
            }

            this.state.showSpinner = false
            this.setState({ isRefresh: !this.state.isRefresh });

        })
    }

    methodSearch(txt) {
        this.state.keyword = txt
        if (this.state.searchtype == "") {

            this.state.first_id_InCoins = ""
            this.state.first_id_OutCoins = ""
            this.setState({})

            this.state.currentPage = 1
            this._onSerach(this.state.keyword, false)


        }
        else {
            this.state.currentPage = 1
            this.state.SendCoinsData = []
            this.setState({})
        }
    }
    render_Activity_footer = () => {
        var footer_View = (
            <View style={{ flex: 1, backgroundColor: '#ffffff', alignItems: 'center', justifyContent: 'center', padding: 15 }}>
                <ActivityIndicator size="large" color="#000000" />
            </View>
        );
        return footer_View;
    };
    onScroll = () => {
        console.log("onScroll-------")

        if (this.state.searchtype == "") {
            if (!this.state.showLoaderPage && this.state.totalRecord > this.state.SendCoinsData.length && this.state.totalPage > this.state.currentPage) {
                this.setState({ showLoaderPage: true })
                this.state.currentPage = this.state.currentPage + 1
                this._onSerach(this.state.keyword, false)
            }
        }


    }
    gotoChatHistory(item) {
        // var ChatData = {
        //     other_user_id: item.id,
        //     // user_id: Helper.userInfo.id,
        //     // last_id: "0",
        //     // limit = "20",
        //     // type = "",

        // }
        pushToScreen('Chat', { OtherUserId: item.id, OtherUserName: item.username })
    }
    SearchMessageItem = ({ item, index }) => (
        <TouchableOpacity onPress={() => this.gotoChatHistory(item)} style={{ flexDirection: "row", alignItems: 'center', backgroundColor: "white", padding: 10, borderBottomWidth: 1, borderBottomColor: colors.blackgrey }}>
            <View style={{ flexDirection: "row", alignItems: 'flex-end', }}>
                <View style={{ borderRadius: 48 / 2 }}>
                    <ImageLoadView
                        resizeMode={"cover"}
                        source={item.profile_image ? { uri: Config.imageUrl + item.profile_image } : Images.user_dummy}
                        style={{ height: 48, width: 48, resizeMode: 'cover', borderRadius: 48 / 2 ,}}

                    />
                </View>
                {item.is_online == 1 ?
                    <View
                        style={{ height: 10, width: 10, borderRadius: 5, backgroundColor: "#42f49b", borderColor: 'white', borderWidth: 1, marginLeft: -15, }}
                    />
                    :
                    <View
                        style={{ height: 10, width: 10, borderRadius: 5, backgroundColor: "#ff2c76", borderColor: 'white', borderWidth: 1, marginLeft: -15, }}
                    />
                }
            </View>
            <View style={{ width: "60%", marginLeft: 10, }}>
                <Text style={{ color: "#4c4c55", fontFamily: fonts.SFProDisplay_Bold, fontSize: 18 }}>{item.username}</Text>

            </View>

        </TouchableOpacity>
    );
    render() {


        return (
            <View style={{ flex: 1 }}>
                <GHeader
                    backgroundColor="white"
                    title1="Message List"
                    bArrow
                    greyBackButton
                    color="#606076"
                />


                <View style={{ paddingHorizontal: 15, paddingBottom: 10 }}>
                    <SearchInputBarCompo
                        placeholder="Search users"
                        width={'100%'}
                        onChangeText={(keyword) => this.methodSearch(keyword)}
                        value={this.state.keyword}
                    />
                </View>

                <FlatList
                    showsHorizontalScrollIndicator={false}
                    ref={(ref) => this.flatListRef = ref}
                    data={this.state.SendCoinsData}
                    renderItem={this.SearchMessageItem}
                    // ItemSeparatorComponent={() => (<View style={styles.separater} />)}
                    keyExtractor={(item, index) => index.toString()}
                    onEndReached={this.onScroll}
                    onEndReachedThreshold={0.5}
                    ListFooterComponent={this.state.showLoaderPage && this.render_Activity_footer}
                    extraData={this.state}
                />
                <Spinner visible={this.state.showSpinner} />

            </View>
        );
    }
}


const styles = StyleSheet.create({

    separater: {
        height: 2,
        backgroundColor: "#F0F0F0",
        width: "100%"
    }
});