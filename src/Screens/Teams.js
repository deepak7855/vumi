import React, { Component } from 'react'
import { Text, View, ActivityIndicator, RefreshControl, DeviceEventEmitter, StyleSheet, Image, FlatList, TouchableOpacity, Alert } from 'react-native'
import { Actions } from 'react-native-router-flux'
import {
    Images, Colors, GHeader,
} from '../common';
import fonts from '../common/fonts';
import { JoinModelCompo } from '../components/common/JoinModelCompo.js';
import Spinner from 'react-native-loading-spinner-overlay';
import Helper from '../lib/Helper';
import Toast from 'react-native-root-toast';
import Config from '../lib/Config';
import pushToScreen from '../navigation/NavigationActions';
import { BlockModelCompo } from '../components/Watch/BlockModelCompo';
import SocketConnect from '../common/SocketConnect';
import { InfoModelCompo } from '../components/Watch/InfoModelCompo';
import ImageLoadView from '../components/ImageLoadView';
import moment from 'moment';



export default class Teams extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showJoinModel: false, first_post_id: '', showLoaderPageing: false, refreshing: false,
            arrayJoinTeams: [], showSpinner: false, username: "", TotalBonus: 0,
            selectedRowDetail: {},
            showBlockModel: false,
            showInfoModel: false,
        };
    }
    componentDidMount = () => {
        this.getJoinTeamsApi(true, '');
    };




    _pullToRefresh = () => {
        this.setState({ refreshing: true });
        this.getJoinTeamsApi(false, "");
    }
    // getJoinTeamsApi = (loader) => {
    //     loader == true ?
    //         this.setState({ showSpinner: true })
    //         :
    //         this.setState({ showSpinner: false })

    //     Helper.makeRequest({ url: 'team-joined-me', method: "POST", data: '' }).then((responsedata) => {
    //         console.log("settings------------------------------", responsedata)
    //         if (responsedata.status == 'true') {
    //             this.setState({
    //                 showSpinner: false, refreshing: false,
    //             })
    //             this.setState({
    //                 arrayJoinTeams: responsedata.data.data,
    //                 TotalBonus: responsedata.data.total_bonus_points
    //             })


    //         } else {
    //             this.setState({
    //                 showSpinner: false, refreshing: false,
    //             })
    //             Toast.show(responsedata.message)


    //         }
    //     })
    // }



    getJoinTeamsApi = (spinner, last_id) => {
        this.setState({ showSpinner: spinner })
        Helper.makeRequest({ url: "team-joined-by-me", method: "POST", data: { last_id: last_id } }).then((responsedata) => {
            if (last_id == "") {
                this.state.arrayJoinTeams = []
            }
            if (responsedata.status == 'true') {
                if (responsedata.data && responsedata.data.data && responsedata.data.data.length > 0) {
                    this.state.arrayJoinTeams = [...this.state.arrayJoinTeams, ...responsedata.data.data]
                }
                this.state.first_post_id = responsedata.data.first_id ? responsedata.data.first_id : ""
            } else {
                Toast.show(responsedata.message)
            }

            this.state.showLoaderPageing = false
            this.state.refreshing = false
            this.state.showSpinner = false
            this.state.TotalBonus = responsedata.data.total_bonus_points ? responsedata.data.total_bonus_points : '0'
            this.setState({})
        })
    }
    onJoinHeaderPress = () => {
        this.setState({ showJoinModel: true })
    }

    onJoin = (username) => {
        this.JoinTeamList(username);
    }


    JoinTeamList = (username) => {
        if (username.length == 0) {
            alert("Please enter username.")

        } else {
            this.setState({ showSpinner: true })
            let data = {
                username: username,

            }
            Helper.makeRequest({ url: "create-team", method: "POST", data: data }).then((responsedata) => {
                if (responsedata.status == 'true') {
                    alert(responsedata.message)
                    this.setState({ showSpinner: false, showJoinModel: false })
                    // this.getJoinTeamsApi(false);
                    DeviceEventEmitter.emit("UpdateJoinTeamData", responsedata)

                } else {
                    alert(responsedata.message)
                    this.setState({ showSpinner: false })

                }

            })
        }


    }
    NotJoinTeam = () => {
        this.setState({ showJoinModel: false })
    }

    onUserImagePress = (item) => {
        this.setState({ showInfoModel: true, selectedRowDetail: { user: item } })
    }

    onBlock = (item) => {
        this.setState({ showBlockModel: false })
        let formdata = {
            user_id: Helper.userInfo.id,
            other_user_id: item.user.id,
        }
        if (SocketConnect.checkSocketConnected()) {
            SocketConnect.socketDataEvent('user_block_unblock', formdata);
            this.getJoinTeamsApi(true, '');

        } else {
            SocketConnect.checkConnection('connect', cb => {
                console.log('SocketConnect------------', cb);
            });
        }
    }

    onShowBlockModel = () => {
        this.setState({ showInfoModel: false }, () => {
            this.setState({ showBlockModel: true })
        })
    }


    goToChat = (item) => {
        this.setState({ showInfoModel: false }, () => {
            pushToScreen('Chat', { OtherUserId: item.user.id, OtherUserName: item.user.username });
        })
    }

    TeamsJoinList = ({ item, index }) => (
        <View style={{ flexDirection: "row", alignItems: 'center', backgroundColor: "white", padding: 10, }}>
            <View style={{ flexDirection: "row", alignItems: 'flex-end', }}>
                <TouchableOpacity onPress={() => this.onUserImagePress(item)} style={{ borderRadius: 48 / 2 }}>

                    <ImageLoadView
                        resizeMode={"cover"}
                        source={item.profile_image ? { uri: Config.imageUrl + item.profile_image } : Images.user_dummy}
                        style={{ height: 48, width: 48, resizeMode: 'cover', borderRadius: 48 / 2, borderWidth: 0.5 }}

                    />
                </TouchableOpacity>
                {item.is_online == 1 ?
                    <View
                        style={{ height: 10, width: 10, borderRadius: 5, backgroundColor: "#42f49b", borderColor: 'white', borderWidth: 1, marginLeft: -15, }}
                    />
                    :
                    <View
                        style={{ height: 10, width: 10, borderRadius: 5, backgroundColor: "#ff2c76", borderColor: 'white', borderWidth: 1, marginLeft: -15, }}
                    />
                }
            </View>
            <View style={{ width: "60%", marginLeft: 10, }}>
                <Text style={{ color: "#4c4c55", fontFamily: fonts.SFProDisplay_Bold, fontSize: 18 }}>{item.username ? item.username : ""}</Text>
                {item.is_left == 1 ?
                    <Text style={{ color: "#4c4c55", fontFamily: fonts.SFProMedium, opacity: 0.5 }}>{moment.utc(item.left_time).local().fromNow()} left</Text>
                    :
                    null
                }
            </View>
            <View style={{ width: "25%", alignItems: 'flex-end', justifyContent: "center" }}>
                <Text style={{ color: "#4c4c55", fontFamily: fonts.Montserrat_SemiBold, fontSize: 18 }}>{item.bonus_points == 0 ? '0' : "+" + item.bonus_points}</Text>

            </View>
        </View>
    );

    onScroll = () => {

        if (!this.state.showLoaderPageing && this.state.arrayJoinTeams.length > 0 && this.state.arrayJoinTeams[this.state.arrayJoinTeams.length - 1].id != this.state.first_post_id && this.state.first_post_id != '') {
            this.setState({ showLoaderPageing: true })
            this.getJoinTeamsApi(false, this.state.arrayJoinTeams[this.state.arrayJoinTeams.length - 1].id);
        }
    }
    render_Activity_footer = () => {
        var footer_View = (
            <View style={{ flex: 1, backgroundColor: '#ffffff', alignItems: 'center', justifyContent: 'center', padding: 15 }}>
                <ActivityIndicator size="large" color="#000000" />
            </View>
        );
        return footer_View;
    };
    gotoResetAllTeam() {
        Alert.alert(
            'Vumi',
            'Are you sure you want to reset your team.',

            [
                { text: "No", onPress: () => { console.log("Logout no") } },
                { text: "Yes", onPress: () => { this.gotoReset() } },
            ],
            { cancelable: false }
        )

    }
    gotoReset() {
        this.setState({ showSpinner: true })
        Helper.makeRequest({ url: "reset-team", method: "POST" }).then((responsedata) => {
            if (responsedata.status == 'true') {
                Toast.show(responsedata.message)
                this.setState({ showSpinner: false })
                this.getJoinTeamsApi(true, '');

            } else {
                Toast.show(responsedata.message)
                this.setState({ showSpinner: false })

            }

        })
    }
    render() {
        return (
            <View style={{ flex: 1, }}>
                <GHeader
                    bArrow
                    greyBackButton
                    color="#606076"
                    backgroundColor="white"
                    title1="Teams"
                    showJoinBtn
                    onJoinHeaderPress={this.onJoinHeaderPress}
                />
                <Spinner visible={this.state.showSpinner} />

                <View style={{ flexDirection: "row", padding: 10, justifyContent: "space-between" }}>
                    <Text style={{ color: "#aaadb7", fontSize: 14, fontFamily: fonts.Poppins_Bold }}> Teammates </Text>
                    <Text style={{ color: "#aaadb7", fontSize: 14, fontFamily: fonts.SFProMedium }}> Bonus </Text>
                </View>


                <View style={{ flex: 1, backgroundColor: "#f5f6fa", paddingVertical: 10 }}>
                    <FlatList
                        contentContainerStyle={{ flexGrow: 1 }}
                        data={this.state.arrayJoinTeams}
                        ItemSeparatorComponent={() => <View style={{ height: 10, backgroundColor: '#f5f6fa' }} />}
                        // renderItem={({ item }) => <ListRow />}
                        renderItem={this.TeamsJoinList}
                        extraData={this.state}
                        onEndReached={this.onScroll}
                        onEndReachedThreshold={0.5}
                        ListFooterComponent={this.state.showLoaderPageing && this.render_Activity_footer}
                        refreshControl={
                            <RefreshControl
                                refreshing={this.state.refreshing}
                                onRefresh={this._pullToRefresh}
                            />
                        }
                        ListEmptyComponent={() => (

                            <View style={{ flex: 1, backgroundColor: "#f5f6fa", justifyContent: "center", alignItems: "center" }}>
                                {!this.state.showSpinner &&
                                    <Text style={{ color: "#a5adc7", fontSize: 16, fontFamily: fonts.SFProMedium }}>No Record Found!</Text>
                                }
                            </View>
                        )}
                    />
                </View>

                <View style={{ flexDirection: "row", paddingHorizontal: 10, paddingVertical: 17, alignItems: 'center' }}>
                    <View style={{ flex: 0.6 }}>
                        <Text style={{ color: "#aaadb7", fontSize: 14, fontFamily: fonts.SFProRegular }}> Total Bonus </Text>

                    </View>
                    <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                        <Text numberOfLines={1} style={{ color: "#15151c", fontSize: 28, fontFamily: fonts.SFProDisplay_Heavy }}>{this.state.TotalBonus}</Text>
                    </View>
                    {this.state.arrayJoinTeams.length > 0 ?
                        <TouchableOpacity onPress={() => this.gotoResetAllTeam()} style={{ flex: 0.5, backgroundColor: 'red', height: 35, width: 90, borderRadius: 30, alignItems: 'center', justifyContent: 'center' }}>
                            <Text style={{ color: '#fff', fontSize: 15, fontFamily: fonts.Montserrat_Bold, textAlign: 'center' }}>Reset</Text>
                        </TouchableOpacity>
                        :
                        <View style={{ flex: 0.5, height: 35, borderRadius: 30, alignItems: 'center', justifyContent: 'center' }}></View>}
                </View>

                <JoinModelCompo
                    showJoinModel={this.state.showJoinModel}
                    onJoin={(username) => this.onJoin(username)}
                    NotJoinTeam={() => this.NotJoinTeam()}
                // username={() => alert(this.props.username)}
                />
                {
                    this.state.showBlockModel &&
                    <BlockModelCompo
                        showBlockModel={this.state.showBlockModel}
                        onBlock={(item) => this.onBlock(item)}
                        hideBlockModel={() => this.setState({ showBlockModel: false })}
                        selectedRowDetail={this.state.selectedRowDetail}
                    />
                }
                {
                    this.state.showInfoModel &&
                    <InfoModelCompo
                        showInfoModel={this.state.showInfoModel}
                        hideInfoModel={() => this.setState({ showInfoModel: false })}
                        onShowBlockModel={() => this.onShowBlockModel()}
                        goToChat={(item) => this.goToChat(item)}
                        selectedRowDetail={this.state.selectedRowDetail}
                    />
                }
            </View>
        );
    }
}


const styles = StyleSheet.create({

});