import React, { Component, useState } from 'react'
import {
    Text, View, ImageBackground, Image,
    StatusBar, SafeAreaView, Dimensions
} from 'react-native'
import {
    GButton, Input, IconInput, Colors,
    Images, GAuthHeader,
    ScrollContainer, ScrollButtonTop
} from '../common';

import { BulletComponent } from '../components'
import { TouchableOpacity } from 'react-native-gesture-handler';


const { height, width } = Dimensions.get('window')


export const Password = () => {
    const [state, setstate] = useState(true)
    const [btnstate, btnsetstate] = useState(true)

    _changeIcon = (value) => {
        setstate(value)
    }

    return (
        <ImageBackground resizeMode='cover' source={Images.Loginbg} style={{ flex: 1, alignItems: 'center' }}>

            <GAuthHeader bArrow title1="Create Password" rightItem>
                <BulletComponent numberof={3} activeIndex={2} />
            </GAuthHeader>

            <ScrollContainer containerStyle={{ alignItems: 'center', justifyContent: 'center', flex: 1 }}>
                <View style={{}}>
                    <IconInput
                        active={!state}
                        rimagePath={Images.changePasword.eye}
                        placeholder='New Password'
                        onChangeText={()=>btnsetstate(false)}
                        secureTextEntry={state}
                        onClick={() => _changeIcon(false)}
                    />
                    <IconInput
                        rimagePath={Images.changePasword.smile}
                        placeholder='Re-enter Password'
                        onChangeText={()=>btnsetstate(false)}
                        secureTextEntry={state}
                        onClick={() => _changeIcon(false)}
                    />


                </View>


            </ScrollContainer>
            <ScrollButtonTop style={{ alignItems: 'center', }}>
                <GButton
                    height={65}
                    radius={32.5}
                    onPress={() => { }}
                    bText='Next'
                    txtcolor='#9180f4'
                    bgColor={!btnstate ? Colors.abuttonColor : Colors.btxtColor}
                    onPress={() => { }}
                />
            </ScrollButtonTop>


        </ImageBackground>
    )

}
