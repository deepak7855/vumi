import React, {Component} from 'react';
import {
  Text,
  View,
  StyleSheet,
  Image,
  ScrollView,
  Dimensions,
  FlatList,
  TouchableOpacity,
  Modal,
  StatusBar,
  BackHandler,
  Alert,
  SafeAreaView,
  TouchableWithoutFeedback,
  RefreshControl,
  Animated,
  StatusBarIOS
} from 'react-native';
import {CoinAndSwitchComponent} from '../components/Watch/CoinAndSwitchComponent';
import {Colors} from '../common/Colors';
import {UserDetailComponent} from '../components/Watch/UserDetailComponent';
import {Images} from '../common/Images';
import {VideoDetailCompo} from '../components/Watch/VideoDetailCompo';
import Video from 'react-native-video';
import {GetMoreCoinsModelCompo} from '../components/Watch/GetMoreCoinsModelCompo';
import {Actions} from 'react-native-router-flux';
import Helper from '../lib/Helper';
import ProgressBar from 'react-native-progress/Bar';
import Toast from 'react-native-root-toast';
import Spinner from 'react-native-loading-spinner-overlay';
import Config from '../lib/Config';
import ViewPager from '@react-native-community/viewpager';
import ImageViewSwiper from '../components/Watch/ImageViewSwiper';
import pushToScreen from '../navigation/NavigationActions';
import {
  MenuContext,
  Menu,
  MenuOptions,
  MenuOption,
  MenuTrigger,
} from 'react-native-popup-menu';
// import PostLikeView from '../components/Watch/PostLikeView'
import FastImage from 'react-native-fast-image';
import Emojiimages from '../assets/Themes/Images';
import {BlurView} from '@react-native-community/blur';
import fonts from '../common/fonts';
import SocketConnect from '../common/SocketConnect';

const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;
const screenCellHeight = Dimensions.get('window').height - (20+109)

// var ProgressBar = require('../components/Watch/VideoProgressBar');

export class WatchOld extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modalVisible: false,
      videoPaused: false,
      progress: 0,
      coins: '',
      watchList: [],
      showSpinner: false,
      overlayShow: -1,
      arrReportList: [],
      reportViewOverleyShowIndex: -1,
      current_page: 1,
      emojiViewShowIndex: -1,
      duration: 0,
      currentTime: 0,
    };
    this.timeout = null;
    this.didBlurListener = this.props.navigation.addListener('didBlur', obj => {
      this.changeState();
    });
  }

  changeState = () => {
    this.setState({videoPaused: true, watchList: []});
    if(this.timeout){
      clearTimeout(this.timeout)
    }
  };
  getSettingApi = () => {
    this.setState({showSpinner: true});
    Helper.makeRequest({url: 'settings', method: 'POST', data: ''}).then(
      resp => {
        if (resp.status == 'true') {
          console.log('settings------------------------------', resp);
          Helper.userSettings = resp.data;
          this.setState({});
        } else {
          Helper.userSettings = {};
          this.setState({});
        }
        this.setState({showSpinner: false});
      },
    );
  };

  getWatchList = () => {
    this.setState({showSpinner: true});
    Helper.makeRequest({
      url: `get-home-feed?page=${this.state.current_page}`,
      method: 'POST',
      data: '',
    }).then(resp => {
      if (resp.status == 'true') {

        let watchList = this.state.watchList.concat(resp.data.data);
        this.setState({watchList, last_page: resp.data.last_page});
        setTimeout(() => {
            this.methodPostView(0)
        }, 500);

      } else {
        Toast.show(resp.message);
      }
      this.setState({showSpinner: false});
    });
  };

  onEndReached = () => {
    if (this.state.current_page < this.state.last_page) {
      this.setState(
        {
          current_page: this.state.current_page + 1,
        },
        () => {
          this.getWatchList();
        },
      );
    }
  };

  onRefresh = () => {
    this.setState({selectedIndex: -1, watchList: []}, () => {
      this.getSettingApi();
      this.state.current_page = 1
      this.getWatchList();
    });
  };

  onPress = val => {
    this.setState({modalVisible: val});
  };

  handleProgressPress = e => {
    const position = e.nativeEvent.locationX;
    const progress = (position / (screenWidth - 8)) * this.state.duration;
    this.player.seek(progress);
  };

  handleProgress = progress => {
    // console.log("handleProgress-------------",progress)
    this.setState({
      progress: parseFloat(progress.currentTime / progress.playableDuration),
    });
  };

  handleLoad = meta => {
    this.setState({
      progress: 0,
      duration: meta.duration,
      currentTime: meta.currentTime,
    });
  };

  _onViewableItemsChanged = props => {
    const changed = props.changed;
    console.log('changed', changed);

    changed.forEach(item => {
      console.log('_onViewableItemsChanged', item);
      // if (item.isViewable) {
      //   this.setState({selectedIndex: item.index, progress: 0}, () => {
      //     clearTimeout(this.timeout);
      //     this.timeout = setTimeout(() => {
      //       this.updatePostViewStatus();
      //     }, 3000);
      //   });
      // }
    });
  };

  viewabilityConfig = {
    viewAreaCoveragePercentThreshold: 80,
  };

   handlePageChange  =  e => {
    var offset = e.nativeEvent.contentOffset;
    if (offset) {
      var page = Math.ceil(offset.y / Dimensions.get('window').height);
      console.log('handlePageChange-----------', page);

      this.methodPostView(page)

    }
  };
   methodPostView = async page => {
    if (this.state.watchList && this.state.watchList.length > page) {
        let token = await Helper.getDataFromAsync("token")
        let dic = this.state.watchList[page];
        this.state.selectedIndex = page;
        // let count = 0
        // setInterval(() => {
        //     console.log(count+1)
        //     count = count + 1

        // }, 1000);
        clearTimeout(this.timeout);
        this.timeout = setTimeout(() => {
          if (SocketConnect.isSocketConnected) {
            SocketConnect.socketDataEvent('view_post', {post_id: dic.id,token:token});
          } else {
            SocketConnect.checkConnection('connect', cb => {
              console.log('SocketConnect------------', cb);
            });
          }
        }, 6000);
      }
  }
  updatePostViewStatus = () => {
    // if (this.state.watchList.length > 0) {
    //     if (this.state.watchList[this.state.selectedIndex].is_view == 0) {
    //         let data = {
    //             post_id: this.state.watchList[this.state.selectedIndex].id
    //         }
    //         Helper.makeRequest({ url: "post-view", method: "POST", data: data }).then((resp) => {
    //             if (resp.status == 'true') {
    //                 let list = this.state.watchList
    //                 let listRow = resp.data
    //                 // listRow.is_view = 1;
    //                 // listRow.total_views = listRow.total_views + 1
    //                 list[this.state.selectedIndex] = listRow;
    //                 this.setState({ watchList: list })
    //             } else {
    //                 Toast.show(resp.message)
    //             }
    //         })
    //     }
    // }
  };

  onLikePress = (id, is_like, index) => {
    let data = {
      post_id: id,
      is_like: is_like,
    };
    Helper.makeRequest({url: 'post-like', method: 'POST', data: data}).then(
      resp => {
        if (resp.status == 'true') {
          let list = this.state.watchList;
          let listRow = resp.data;
          // listRow.is_like = is_like;
          // listRow.total_likes = is_like == 1 ? (listRow.total_likes + 1) : (listRow.total_likes - 1);
          list[index] = listRow;
          this.setState({watchList: list});
        } else {
          Toast.show(resp.message);
        }
      },
    );
  };

  openMenu = () => {
    this.menu.open();
  };

  methodReportSubmit(item, index) {
    let dicPost = this.state.watchList[this.state.reportViewOverleyShowIndex];
    this.setState({showSpinner: true});
    Helper.makeRequest({
      url: 'flag-post',
      method: 'POST',
      data: {post_id: dicPost.id, flag_reason_id: item.id},
    }).then(resp => {
      if (resp.status == 'true') {
        let dic = dicPost;
        dic.is_flag_by_me = 1;
        this.state.watchList.splice(index, 1, dic);
        Helper.userSettings.today_flag =
          Number(Helper.userSettings.today_flag) + 1;
      }
      Helper.showToast(resp.message);
      this.setState({reportViewOverleyShowIndex: -1, showSpinner: false});
    });
  }
  renderReportView = ({item, index}) => {
    return (
      <TouchableOpacity
        onPress={() => this.methodReportSubmit(item, index)}
        style={{
          padding: 15,
          flexDirection: 'row',
          alignItems: 'center',
          justifyContent: 'space-between',
          backgroundColor: 'white',
          borderBottomColor: 'black',
          borderBottomWidth: 1.0,
        }}>
        <Text
          style={{
            fontSize: 15,
            fontFamily: fonts.SFProDisplay_Semibold,
            width: '90%',
          }}>
          {item.title}
        </Text>
        <Image
          resizeMode={'contain'}
          style={{width: 15, height: 15, tintColor: Colors.black}}
          source={Images.right_arrow}
        />
      </TouchableOpacity>
    );
  };

  renderReportModal() {
    return (
      <Modal
        animationType={'fade'}
        transparent={true}
        visible={true}
        onRequestClose={() => {}}>
        <View style={{flex: 1, justifyContent: 'flex-end'}}>
          <View
            style={{
              width: '100%',
              alignItems: 'center',
              backgroundColor: Colors.white,
              paddingVertical: 15,
              borderBottomColor: Colors.coolGrey,
              borderBottomWidth: 1.0,
              
            }}>
            <Text
              style={{
                fontFamily: fonts.SFProDisplay_Semibold,
                fontSize: 20,
                color: Colors.black,
                textAlign: 'center',
              }}>
              Report
            </Text>
            <TouchableOpacity style={{position:'absolute',zIndex:1,right:15,top:10}} onPress={()=> this.setState({reportViewOverleyShowIndex : -1})}>
              <Image style={{height:30,width:30,tintColor:'grey' }} resizeMode={'contain'} source={Images.cross_pink} ></Image>
            </TouchableOpacity>
          </View>
          <View
            style={{width: '100%', height: '50%', justifyContent: 'flex-end'}}>
            <FlatList
              style={{flex: 1, backgroundColor: Colors.white}}
              data={this.state.arrReportList}
              renderItem={this.renderReportView}
              keyExtractor={(item, index) => index}
            />
          </View>
        </View>
        <Spinner visible={this.state.showSpinner} />
      </Modal>
    );
  }

  render() {
    return (
      <SafeAreaView style={styles.container}>
        <CoinAndSwitchComponent coin={this.state.coins} toggleValue={true} />

        {this.state.watchList.length > 0 && (
          <FlatList
            data={this.state.watchList}
            keyExtractor={(item, index) => index}
            pagingEnabled
            onEndReached={this.onEndReached}
            onEndReachedThreshold={0.1}
            // viewabilityConfig={this.viewabilityConfig}
            // onViewableItemsChanged={this._onViewableItemsChanged}
            onMomentumScrollEnd={this.handlePageChange}
            scrollEventThrottle={16}
            windowSize={5}
            // getItemLayout={(_data, index) => ({
            //   length: screenHeight - screenHeight / 4.6,
            //   offset: (screenHeight - screenHeight / 4.6) * index,
            //   index,
            // })}
            refreshControl={
              <RefreshControl
                refreshing={this.state.isRefreshing}
                onRefresh={this.onRefresh.bind(this)}
              />
            }
            removeClippedSubviews={true}
            renderItem={({item, index}) => (
              // <View style={{height: screenHeight - screenHeight / 4.6}}>
              <View style={{height: screenCellHeight,backgroundColor:'red'}}>

                <View style={styles.subContainer}>
                  <UserDetailComponent item={item} />
                  {item.post_detail && item.post_detail.length > 0 ? (
                    <TouchableOpacity
                      onPress={() => this.setState({overlayShow: index})}
                      style={styles.squareView}>
                      {item.post_detail[0].type == 'VIDEO' ? (
                        <View style={{flex: 1, alignItems: 'center'}}>
                          <View style={styles.squareView}>
                            <TouchableWithoutFeedback
                              onPress={() =>
                                this.setState({
                                  videoPaused: !this.state.videoPaused,
                                })
                              }>
                              <Video
                                source={{
                                  uri:
                                    Config.imageUrl + item.post_detail[0].media,
                                }}
                                ref={ref => {
                                  this.player = ref;
                                }}
                                onProgress={this.handleProgress}
                                onLoad={this.handleLoad}
                                repeat
                                resizeMode="cover"
                                style={styles.video}
                                paused={
                                  this.state.selectedIndex == index &&
                                  !this.state.videoPaused
                                    ? false
                                    : true
                                }
                              />
                            </TouchableWithoutFeedback>
                          </View>
                          {this.state.progress ? (
                            <ProgressBar
                              progress={this.state.progress}
                              color="#5856d6"
                              unfilledColor="#282828"
                              borderColor="transparent"
                              width={screenWidth - 8}
                              borderRadius={0}
                              height={7}
                            />
                          ) : (
                            <View
                              style={{
                                width: screenWidth - 8,
                                height: 7,
                                backgroundColor: '#282828',
                              }}
                            />
                          )}
                        </View>
                      ) : (
                        <View style={styles.squareView}>
                          <ImageViewSwiper data={item.post_detail} />
                        </View>
                      )}

                      {this.state.overlayShow == index &&
                        this.methodRenderOverlayView(item, index)}
                    </TouchableOpacity>
                  ) : (
                    <View style={styles.squareView}>
                      <Image
                        style={styles.squareView}
                        source={Images.watch_video}
                      />
                    </View>
                  )}
                  {this.state.emojiViewShowIndex == index &&
                    this.rendeEmojiView()}
                  {/* <PostLikeView /> */}
                  <VideoDetailCompo
                    isEmojiShow={
                      this.state.emojiViewShowIndex == index ? false : true
                    }
                    //  onLikePress={(post_id, is_like, index) => this.openMenu(post_id, is_like, index)}
                    likeImage={
                      item.is_like && item.is_like < 7
                        ? arrEmaoji[parseInt(item.is_like) - 1].image
                        : Images.heart_grey
                    }
                    EmojiViewShow={index => this.methodEmojiViewShow(index)}
                    onLikePress={(post_id, is_like, index) =>
                      this.onLikePress(post_id, is_like, index)
                    }
                    item={item}
                    index={index}
                    onChatPress={post_id => pushToScreen('Comments', {post_id})}
                  />
                </View>
              </View>
            
            
            )}
          />
        )}

        {/* <GetMoreCoinsModelCompo
                    modalVisible={this.state.modalVisible}
                    toggleModel={(val) => this.onPress(val)}
                /> */}

        <Spinner visible={this.state.showSpinner} />

        {this.state.reportViewOverleyShowIndex != -1 &&
          this.renderReportModal()}
      </SafeAreaView>
    );
  }
  methodRenderOverlayView(item, index) {
    console.log('methodRenderOverlayView', item);
    return (
      <TouchableOpacity
        onPress={() => this.setState({overlayShow: -1})}
        style={{
          flex: 1,
          position: 'absolute',
          zIndex: 1,
          height: '100%',
          width: '100%',
        }}>
        <BlurView
          style={styles.absolute}
          blurType="dark"
          blurAmount={10}
          reducedTransparencyFallbackColor="black"
        />
        <View
          style={{
            padding: 10,
            alignSelf: 'center',
            alignItems: 'center',
            justifyContent: 'center',
            flex: 1,
            position: 'absolute',
            zIndex: 1,
            height: '100%',
            width: '100%',
          }}>
          <Text
            style={{
              fontFamily: fonts.SFProMedium,
              fontSize: 17,
              color: 'white',
            }}>
            {item.description}
          </Text>
          <Text
            style={{
              fontFamily: fonts.SFProMedium,
              fontSize: 17,
              color: 'white',
              marginTop: 10,
            }}>
            {item.social_link}
          </Text>
          <View
            style={{
              width: 200,
              height: 40,
              borderRadius: 20,
              marginTop: 10,
              backgroundColor: Colors.orange,
              alignItems: 'center',
              justifyContent: 'center',
            }}>
            <Text
              style={{
                fontFamily: fonts.SFProDisplay_Bold,
                fontSize: 17,
                color: 'white',
              }}>
              {'Soundcloud'}
            </Text>
          </View>
        </View>
        <View
          style={{
            flexDirection: 'row',
            zIndex: 1,
            position: 'absolute',
            bottom: 15,
            left: 15,
          }}>
          {item.is_flag_by_me == 0 &&
          item.is_flag_clear == 0 &&
          item.user &&
          item.user.flag_ban == 0 &&
          Helper.userSettings.today_flag < 5 ? (
            <TouchableOpacity
              style={{marginRight: 15}}
              onPress={() =>
                this.setState({reportViewOverleyShowIndex: index})
              }>
              <Image style={{width: 40, height: 40}} source={Images.flag} />
            </TouchableOpacity>
          ) : (
            item.is_flag_by_me == 1 &&
            item.is_flag_clear == 0 &&
            item.user &&
            item.user.flag_ban == 0 && (
              <TouchableOpacity style={{marginRight: 15}}>
                <Image
                  style={{width: 40, height: 40}}
                  source={Images.flag_colors}
                />
              </TouchableOpacity>
            )
          )}
          <TouchableOpacity onPress={() => this.methodFavourite(item, index)}>
            <Image
              style={{width: 40, height: 40}}
              source={item.is_save == 1 ? Images.starColor : Images.starGrey}
            />
          </TouchableOpacity>
        </View>
      </TouchableOpacity>
    );
  }
  startSeekVideo = () => {};
  methodFavourite(item, index) {
    this.setState({showSpinner: true});
    Helper.makeRequest({
      url: 'post-save',
      method: 'POST',
      data: {post_id: item.id},
    }).then(resp => {
      if (resp.status == 'true') {
        let dic = item;
        dic.is_save = item.is_save == 0 ? 1 : 0;
        this.state.watchList.splice(index, 1, dic);
      }
      Helper.showToast(resp.message);
      this.setState({showSpinner: false});
    });
  }
  getReportList() {
    this.setState({showSpinner: true});
    Helper.makeRequest({url: 'get-flag-reason', method: 'POST', data: ''}).then(
      resp => {
        if (resp.status == 'true') {
          this.state.arrReportList = resp.data;
        } else {
          this.state.arrReportList = [];
        }
        this.setState({showSpinner: false});
      },
    );
  }
  endSeekVideo = time => {};
  rendeEmojiView() {
    return (
      <Animated.View
        style={{
          flex: 1,
          position: 'absolute',
          flexDirection: 'row',
          zIndex: 1,
          backgroundColor: '#fff',
          borderRadius: 23,
          height: 46,
          justifyContent: 'center',
          alignItems: 'center',
          elevation: 2,
          shadowColor: 'rgba(0,0,0,0.5)',
          shadowOpacity: 1.0,
          paddingHorizontal: 5,
          bottom: 80,
          right: 5,
        }}>
        {arrEmaoji.map((item, index) => {
          return this.renderEmoji(item, index);
        })}
      </Animated.View>
    );
  }
  renderEmoji(item, index) {
    return (
      <TouchableOpacity onPress={() => this.methodEmojiClick(item.id)}>
        <FastImage
          style={{width: 36, height: 36, marginLeft: index == 0 ? 0 : 5}}
          source={item.gif}
          // source={{
          //     uri: index == 0 ? item.url : item.url,
          // }}
        />
      </TouchableOpacity>
    );
  }
  methodEmojiViewShow(index) {
    this.setState({emojiViewShowIndex: index});
  }
  methodEmojiClick(id) {
    let dic = this.state.watchList[this.state.emojiViewShowIndex];
    dic.is_like = id;
    this.state.watchList.splice(this.state.emojiViewShowIndex, 1, dic);
    let post_id = dic.id;
    let is_like = id;
    let index = this.state.emojiViewShowIndex;
    this.setState({emojiViewShowIndex: -1});

    this.onLikePress(post_id, is_like, index);
  }

  backAction = () => {
    if (Actions.currentScene == 'Watch') {
      Alert.alert('Hold on!', 'Are you sure you want to Exit App', [
        {
          text: 'Cancel',
          onPress: () => null,
          style: 'cancel',
        },
        {text: 'YES', onPress: () => BackHandler.exitApp()},
      ]);
      return true;
    }
  };

  updateStatusBar = () => {
    StatusBar.setBackgroundColor('black');
    StatusBar.setHidden(false);
    StatusBar.setBarStyle('light-content');
  };

  async componentDidMount() {
    this.backHandler = BackHandler.addEventListener(
      'hardwareBackPress',
      this.backAction,
    );

    let userInfo = await Helper.getDataFromAsync('userInfo');
    if (userInfo) {
      Helper.userInfo = userInfo;
      this.setState({coins: userInfo.coins});
    }

    this.functionEvent = this.props.navigation.addListener('didFocus', () => {
      this.methodDidMountCall();
    });
    this.methodDidMountCall();
  }

  methodDidMountCall() {
    this.state.current_page = 1
    this.updateStatusBar();
    this.getWatchList();
    this.getSettingApi();
    this.getReportList();
    SocketConnect.checkConnection('connect', cb => {
      console.log('SocketConnect------------', cb);
    });
    this.setState({videoPaused: false});
  }

  componentWillUnmount() {
    this.didBlurListener.remove();
    this.backHandler.remove();
    this.functionEvent.remove();
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.lightBlack,
  },
  absolute: {
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
  },
  subContainer: {
    paddingVertical: 20,backgroundColor:'blue',
  },

  video: {
    width: screenWidth,
    aspectRatio: 1,
    flex: 1,
  },
  squareView: {
    height: screenWidth,
    width: screenWidth,
    justifyContent: 'center',
  },
});

const arrEmaoji = [
  {
    id: 1,
    url:
      'https://raw.githubusercontent.com/duytq94/facebook-reaction-animation2/master/Images/like.gif',
    image: Emojiimages.like_static_fill,
    gif: Emojiimages.like_gif,
  },
  {
    id: 2,
    url:
      'https://raw.githubusercontent.com/duytq94/facebook-reaction-animation2/master/Images/love.gif',
    image: Emojiimages.love_gif,
    gif: Emojiimages.love_gif,
  },
  {
    id: 3,
    url:
      'https://raw.githubusercontent.com/duytq94/facebook-reaction-animation2/master/Images/haha.gif',
    image: Emojiimages.haha_static,
    gif: Emojiimages.haha_static,
  },
  {
    id: 4,
    url:
      'https://raw.githubusercontent.com/duytq94/facebook-reaction-animation2/master/Images/wow.gif',
    image: Emojiimages.wow_static,
    gif: Emojiimages.wow_static,
  },
  {
    id: 5,
    url:
      'https://raw.githubusercontent.com/duytq94/facebook-reaction-animation2/master/Images/sad.gif',
    image: Emojiimages.sad_static,
    gif: Emojiimages.sad_gif,
  },
  {
    id: 6,
    url:
      'https://raw.githubusercontent.com/duytq94/facebook-reaction-animation2/master/Images/angry.gif',
    image: Emojiimages.angry_static,
    gif: Emojiimages.angry_gif,
  },
];
