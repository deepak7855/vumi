import React, { Component } from 'react';
import {
    Text, View, StyleSheet, TouchableOpacity, Image, StatusBar
} from 'react-native';
import {
    Images, GHeader,
    CustomeImage,
} from '../common';
import fonts from '../common/fonts';
import { ChooseCameraOrGalleryModel } from '../components/photoVideo/ChooseCameraOrGalleryModel';
import { Actions } from 'react-native-router-flux';
import { ChooseVideoFromGalleryOrCameraModel } from '../components/photoVideo/ChooseVideoFromGalleryOrCameraModel';
import { UploadMusicModelCompo } from '../components/photoVideo/UploadMusicModelCompo';
import pushToScreen from '../navigation/NavigationActions';
import ImagePicker from 'react-native-image-crop-picker';
// , {
//     id: 3,
//     image: Images.uploadpost1.music,
//     title: 'Music',
//     label: 'Upload online at vumi.app/upload'
// }
export class UploadPost extends Component {
    constructor(props) {
        super(props);

        this.state = {
            data: [{
                id: 1,
                image: Images.uploadpost1.videonew,
                title: 'Video',
                color: 'rgb(112, 200, 247)',
                height: 29, width: 29
            }, {
                id: 2,
                image: Images.uploadpost1.photosnew,
                title: 'Photos',
                color: 'rgb(112, 133, 247)',
                height: 25, width: 25
            }],

            showPhotoModel: false,
            showVideoModel: false,
            showMusicModel: false

        }
    }


    onRowPress = (item) => {
        if (item.id == 1) {
            this.setState({ showVideoModel: true })
        } else if (item.id == 2) {
            this.setState({ showPhotoModel: true })
        } else {
            this.setState({ showMusicModel: true })
        }
    }
    methodsGalleryClick() {
        ImagePicker.openPicker({
            multiple: true,
            mediaType: 'video',
            maxFiles: 1
        }).then(images => {
            console.log("methodsGalleryClick", images);
        });
    }

    render() {
        return (
            <View style={Styles.container1}>
                <GHeader
                    bArrow
                    greyBackButton
                    color="#606076"
                    backgroundColor="white"
                    title1="Upload Post"
                />
                <View style={{flex:1, backgroundColor:'#fff',marginTop:1,paddingTop:25}}>
                    {
                        this.state.data.map((item) => (
                            <TouchableOpacity onPress={() => { this.onRowPress(item) }} style={[Styles.View1]}>
                                <View style={{ flex: 1, flexDirection: "row" }}>
                                    <View style={{ backgroundColor: item.color, height: 75, width: 75, justifyContent: 'center', alignItems: 'center', borderRadius: 75 / 2 }}>
                                        <CustomeImage
                                            size={25}
                                            source={item.image}
                                            style={{ height: item.height, width: item.width }}
                                        />
                                    </View>
                                    <View style={{ justifyContent: 'center', marginLeft: 10 }}>
                                        <Text style={Styles.texstyle}>{item.title}</Text>
                                        {item.label && <Text style={{ color: '#ddd', fontFamily: fonts.SFProRegular }}>{item.label}</Text>}
                                    </View>
                                </View>
                                {/* <View style={{ justifyContent: 'center' }} hitSlop={{ top: 20, bottom: 20, left: 20, right: 20 }}>
                                <Image
                                    source={Images.right_arrow}
                                    style={{ height: 25, width: 25, resizeMode: "contain", alignItems: 'center', }}
                                />
                            </View> */}
                            </TouchableOpacity>
                        ))
                    }
                </View>


                <ChooseCameraOrGalleryModel
                    showPhotoModel={this.state.showPhotoModel}
                    openCamera={() => { this.setState({ showPhotoModel: false }, () => pushToScreen('camera', { cameraType: 'Picture', isComeFrom: this.props.isComeFrom })) }}
                    openCameraGallery={() => { this.setState({ showPhotoModel: false }, () => pushToScreen('CameraGallery', { galleryType: 'Photos', cameraType: 'Picture', isComeFrom: this.props.isComeFrom })) }}
                    hideModel={() => this.setState({ showPhotoModel: false })}

                />

                <ChooseVideoFromGalleryOrCameraModel
                    showVideoModel={this.state.showVideoModel}
                    openVideoCamera={() => { this.setState({ showVideoModel: false }, () => pushToScreen('camera', { cameraType: 'Video', isComeFrom: this.props.isComeFrom })) }}
                    openVideoGallery={() => { this.setState({ showVideoModel: false }, () => pushToScreen('CameraGallery', { galleryType: 'Videos', cameraType: 'Videos', isComeFrom: this.props.isComeFrom })) }}
                    hideModel={() => this.setState({ showVideoModel: false })}
                // openVideoGallery={() => this.methodsGalleryClick()}

                />

                <UploadMusicModelCompo
                    showMusicModel={this.state.showMusicModel}
                    onMusicUpload={() => { }}
                    hideModel={() => this.setState({ showMusicModel: false })}
                />

                {/* <View style={{ alignSelf: 'center', bottom: 30, position: 'absolute' }}>
                    <Text style={{ color: '#ddd' }}>New Post types comming soon!</Text>
                </View> */}



            </View>
        )
    }
    componentDidMount() {
        this.functionevevnt = this.props.navigation.addListener('didFocus', () => {
            StatusBar.setHidden(false)
            StatusBar.setBackgroundColor('white');
            StatusBar.setBarStyle('dark-content')
        });
    }

    componentWillUnmount() {
        this.functionevevnt.remove();
    }
}

const Styles = StyleSheet.create({
    container1: {
        flex: 1,
        backgroundColor: '#f4f4f4'
    },
    View1: {
        paddingHorizontal: 18,
        height: 110,
        alignItems: "center",
        flexDirection: 'row',
        backgroundColor: 'white',
        justifyContent: "space-between",
        
    },
    View2: {
        flex: 1,
        alignItems: 'center',
        flexDirection: 'row'

    },
    texstyle: {
        fontSize: 14,
        fontFamily: fonts.Montserrat_Bold
    }

})