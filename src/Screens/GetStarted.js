import React, { Component } from 'react'
import {
    Text, View, ImageBackground, Image, StatusBar, TouchableOpacity, StyleSheet, BackHandler, Linking, SafeAreaView, Platform
} from 'react-native'
import {
    GButton, Images, ScrollEnable, Colors, Fontfamily,
} from '../common';
import { Actions } from 'react-native-router-flux';
import pushToScreen from '../navigation/NavigationActions';
import Config from '../lib/Config';
import LottieView from 'lottie-react-native';
import fonts from '../common/fonts';

export class GetStarted extends Component {
    state = {
        login: false,
        btnColor: true
    }
    render() {
        return (
            <ImageBackground resizeMode={'cover'} style={{ width: '100%', height: '100%' }} source={Images.splashGif} >
                <StatusBar hidden />
                <SafeAreaView style={{ flex: 1 }}>
                    <View style={[{
                        flexDirection: "column",
                        flex: 1
                    }]}>
                        <View style={{ flex: 5 }} >
                            <View style={{ width: '100%', flex: 1, flexDirection: 'row', position: 'absolute', zIndex: 1, alignItems: 'center', justifyContent: 'space-between' }}>
                                <View style={[style.container1, { left: 1, top: 1, alignItems: 'flex-start' }]}>
                                    <TouchableOpacity
                                        style={style.containerTop}>

                                        <Text style={style.signIn}>Vumi</Text>
                                    </TouchableOpacity>

                                </View>
                                <View style={[style.container1, { right: 1, top: 1, alignItems: 'flex-end' }]}>
                                    <TouchableOpacity onPress={() => pushToScreen('Login')}
                                        style={style.containerTop}>
                                        <View style={style.containerTop2}>
                                            <Image
                                                resizeMode='contain'
                                                source={Images.userName}
                                                style={{
                                                    height: 9, width: 9,
                                                    tintColor: Colors.btxtColor,
                                                }}
                                            />
                                        </View>

                                        <Text style={style.signIn}>Sign In</Text>
                                    </TouchableOpacity>
                                </View>
                            </View>
                        </View>
                        <View style={{ flex: 5, padding: 10, justifyContent: 'flex-end' }}>
                            <View style={style.bottomView}>
                                <View style={{ alignItems: 'center' }}>
                                    <View style={{ flexDirection: 'row', alignItems: 'center' }}>
                                        <Text style={{ fontFamily: Fontfamily.PoPB, color: 'rgb(199,201,251)', fontSize: 10 }}>By tapping</Text>
                                        <Text style={{ fontFamily: Fontfamily.PoPB, color: 'rgb(229,230,253)', fontSize: 12 }}> Get Started,</Text>
                                        <Text style={{ fontFamily: Fontfamily.PoPB, color: 'rgb(185,172,255)', fontSize: 10 }}> I agree to Vumi’s</Text>
                                    </View>

                                    <View style={style.tpStyle}>
                                        <TouchableOpacity style={{ borderBottomWidth: 1, borderBottomColor: 'rgb(253,224,76)' }} onPress={() => Linking.openURL(Config.termsAndConditions)}>
                                            <Text style={[style.tpTxtStyle, { fontFamily: Fontfamily.PoPB, }]}>Terms of Service</Text>
                                        </TouchableOpacity>
                                        <Text style={{ color: 'rgb(199,201,251)', marginHorizontal: 3, fontSize: 10, fontFamily: Fontfamily.PoPB }}>and</Text>
                                        <TouchableOpacity style={{ borderBottomWidth: 1, borderBottomColor: 'rgb(253,224,76)' }} onPress={() => Linking.openURL(Config.privacyPolicy)}>
                                            <Text style={[style.tpTxtStyle, { fontFamily: Fontfamily.PoPB, }]}>Privacy Policy</Text>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            </View>

                            <TouchableOpacity onPress={() => { pushToScreen('SignUp') }}
                                style={{ backgroundColor: Colors.bluelight, alignItems: 'center', justifyContent: 'center', height: 60, borderRadius: 30, }}>
                                <View style={{ width: '100%', backgroundColor: Colors.bluedark, flexDirection: 'row', alignItems: 'center', justifyContent: 'center', height: 60, borderRadius: 30, marginBottom: 5 }}>
                                    <Text style={{
                                        color: Colors.white, fontSize: 13, fontFamily: Fontfamily.MEB
                                    }}>Get More Views!</Text>
                                </View>
                            </TouchableOpacity>
                        </View>
                    </View>
                </SafeAreaView>
            </ImageBackground>
        )

    }

    backAction = () => {
        if (Actions.currentScene == "GetStarted") {
            BackHandler.exitApp()
        }

    };

    async componentDidMount() {
        this.backHandler = BackHandler.addEventListener(
            "hardwareBackPress",
            this.backAction
        );
    }

    componentWillUnmount() {
        this.backHandler.remove();
    }
}


const style = StyleSheet.create({
    container1: {
        marginTop: 10,
        justifyContent: 'center',
        paddingHorizontal: 15
    },
    containerTop: {
        height: 38,
        borderRadius: 20,
        width: 110,
        justifyContent: 'center',
        alignItems: 'center',
        marginHorizontal: 5,
        flexDirection: 'row',
        borderWidth: 3, borderColor: '#fff'
        // backgroundColor: Colors.btxtColor
    },
    containerTop2: {
        height: 20,
        backgroundColor: 'white',
        width: 20,
        borderRadius: 20 / 2,
        justifyContent: 'center',
        alignItems: 'center'
    },
    signIn: {
        marginHorizontal: 8,
        marginRight: 5,
        color: 'white',
        fontSize: 15,
        fontFamily: Fontfamily.MEB
    },
    centerFlex: {
        flex: 3,
        justifyContent: 'center',
        alignItems: 'center',
    },
    centerFlexTop: {
        height: 90, width: 230,
        borderWidth: 10,
        borderColor: '#fff',
        backgroundColor: Colors.btxtColor,
        borderRadius: 55,
        justifyContent: 'center',
        alignItems: 'center',
    },
    vumiTxt: {
        fontSize: 50,
        fontWeight: 'bold',
        color: 'white',
        fontFamily: Fontfamily.PB

    },
    instaTxt: {
        fontSize: 20,
        color: 'white',
        fontFamily: Fontfamily.PB
    },
    bottomView: {
        padding: 10,
        marginBottom: 8
    },
    tpStyle: {
        flexDirection: 'row',
        marginTop: 10, marginTop: 0,
        alignItems: 'center',
    },
    tpTxtStyle: { color: 'rgb(253,224,76)', fontSize: 12, }
})