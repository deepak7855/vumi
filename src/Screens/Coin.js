import React, { Component } from 'react';
import {
  View,
  Text,
  StyleSheet,
  ScrollView,
  Image,
  DeviceEventEmitter,
  Animated, Platform,
  FlatList, StatusBar, SafeAreaView, RefreshControl
} from 'react-native';
import { Images, GSmallButton, Fontfamily, colors } from '../common';
import { SearchConditionsCompo } from '../components/Coins/SearchConditionsCompo';
import { Actions } from 'react-native-router-flux';
import { CoinListRow } from '../components/Coins/CoinListRow';
import { SearchInputBarCompo } from '../components/Coins/SearchInputBarCompo';
import fonts from '../common/fonts';
import { CashInNoCoinsModel } from '../components/Coins/CashInNoCoinsModel';
import { WhatIsTrollModelCompo } from '../components/common/WhatIsTrollModelCompo';
import pushToScreen from '../navigation/NavigationActions';
import Helper from '../lib/Helper';
import Toast from 'react-native-root-toast';
import Spinner from 'react-native-loading-spinner-overlay';
import moment from 'moment';
import SocketConnect from '../common/SocketConnect';
import { BlockModelCompo } from '../components/Watch/BlockModelCompo';



class Coin extends Component {
  constructor(props) {
    super(props);
    this.state = {
      arrCoinHistory: [],
      searchType: "",
      modelVisible: false,
      trollModel: false,
      notFound: false,
      first_data_id: '',
      keyword: '', showLoaderPage: false,
      temp_post_count: 0,
      selectedRowDetail: {},
      showBlockModel: false,
      fadeAnim: new Animated.Value(380),
      scrollUp: true,
      scrolldown: false,
      showLoaderRefresh: false
    };
    this.arrayholder = [];
  }

  onSearchTypeChange = (searchType) => {
    if (this.state.searchType == searchType) {
      return
    }
    this.state.searchType = searchType
    this.getSendCoinHistory(searchType, "")
    this.setState({})
  }

  showNoCoinsModel = (val) => {
    this.setState({ modelVisible: val })

  }

  showTrollModel = (val) => {
    this.setState({ trollModel: val })
  }
  // type: IN or OUT
  // keyword: test
  // last_id: 15

  getSendCoinHistory(type, last_id, loader = true) {
    if (last_id == "" && loader) {
      this.setState({ showSpinner: true })

    }
    let data = {
      type: type,
      last_id: last_id,
      keyword: this.state.keyword
    }
    Helper.makeRequest({ url: "send-coin-history", method: "POST", data: data }).then((resp) => {
      if (resp.status == 'true') {
        if (data.type == this.state.searchType && data.keyword == this.state.keyword) {
          if (last_id == "") {
            this.state.arrCoinHistory = []
          }
          if (resp.data && resp.data.data && resp.data.data.length > 0) {
            this.state.arrCoinHistory = [...this.state.arrCoinHistory, ...resp.data.data]
          }
          this.state.first_data_id = resp.data.first_id
        }
        this.setState({ showSpinner: false, showLoaderPage: false, showLoaderRefresh: false });
      } else {
        // Toast.show(resp.message)
        if (data.type == this.state.searchType && data.keyword == this.state.keyword) {

          if (last_id == "") {
            this.state.arrCoinHistory = []
          }
        }
        this.setState({ showSpinner: false, showLoaderPage: false, showLoaderRefresh: false });
      }

    })
  }
  getSettingApi = () => {
    this.setState({ showSpinner: true })
    Helper.makeRequest({ url: 'settings', method: "POST", data: '' }).then((resp) => {
      if (resp.status == 'true') {
        console.log("settings------------------------------", resp)
        Helper.userSettings = resp.data
        this.setState({ temp_post_count: resp.data.temp_post_count })

      } else {
        Helper.userSettings = {}
        this.setState({})

      }
      this.setState({ showSpinner: false })
    })
  }
  _onSerchHistory(keyword) {
    this.state.keyword = keyword
    this.getSendCoinHistory(this.state.searchType, "", false)
  }

  methodCashInClick() {
    if (this.state.temp_post_count > 0) {
      pushToScreen('CashIn', { fromCoins: true })
    } else {
      Toast.show("Please create post first.")
    }
  }


  renderHeader() {
    return (
      <Animated.View style={{ alignItems: "center", marginTop: this.state.fadeAnim == 0 ? 0 : 50, height: this.state.fadeAnim }}>
        <View style={{ flexDirection: "row", marginBottom: 30 }}>
          <Text style={{ fontSize: 50, fontFamily: Fontfamily.PB, }}>{Helper.userSettings.user_coins}</Text>
          <Text style={{ fontSize: 14, marginTop: 10, color: '#acacbc', fontFamily: Fontfamily.PB, }}> VC</Text>
        </View>
        <GSmallButton
          height={55}
          width={250}
          bgColor={'rgb(4,230,160)'}
          bText='Cash-In'
          // onPress={() => Actions.CashIn()}
          onPress={() => this.methodCashInClick()}
        />
        <View style={{ marginVertical: 5 }} />
        <GSmallButton
          height={55}
          width={250}
          bgColor={'rgb(62,221,242)'}
          bText='Send View Coins'
          // onPress={() => Actions.RecentSendCoins()}
          onPress={() => pushToScreen('RecentSendCoins')}

        />
        <View style={{ paddingVertical: 20, backgroundColor: colors.white, elevation: 4, shadowColor: 'black', shadowOpacity: 0.5, marginTop: 30, shadowOffset: { width: 1, height: 1, }, alignItems: 'flex-end' }}>
          <SearchConditionsCompo fromCoins onSearchTypeChange={(val) => this.onSearchTypeChange(val)} />
        </View>
        {this.state.searchType == 'MIND' ?
          null :
          <View style={{ paddingHorizontal: 15, width: '100%', paddingTop: 10 }}>
            <SearchInputBarCompo placeholder="Search transfers"
              width={'100%'}
              onChangeText={(txt) => this._onSerchHistory(txt)}
            />
          </View>
        }

      </Animated.View>
    )
  }

  onEndReached = () => {
    if (!this.state.showLoaderPage && this.state.arrCoinHistory.length > 0 && this.state.arrCoinHistory[this.state.arrCoinHistory.length - 1].id != this.state.first_data_id && this.state.first_data_id != '') {
      this.setState({ showLoaderPage: true })
      this.getSendCoinHistory(this.state.searchType, this.state.arrCoinHistory[this.state.arrCoinHistory.length - 1].id, false)
    }
  }

  onUserImagePress = (item) => {
    let info = {}
    info['user'] = item.otheruser;
    this.setState({ showBlockModel: true, selectedRowDetail: info })
  }

  onBlock = (item) => {
    this.setState({ showBlockModel: false, selectedRowDetail: {} })
    let formdata = {
      user_id: Helper.userInfo.id,
      other_user_id: item.user.id,
    }
    if (SocketConnect.checkSocketConnected()) {
      SocketConnect.socketDataEvent('user_block_unblock', formdata);
    } else {
      SocketConnect.checkConnection('connect', cb => {
        console.log('SocketConnect------------', cb);
      });
    }
  }

  fadeIn = () => {
    // Will change fadeAnim value to 1 in 5 seconds
    Animated.timing(this.state.fadeAnim, {
      toValue: 380,
      duration: 1000
    }).start();
  };

  fadeOut = () => {
    // Will change fadeAnim value to 0 in 5 seconds
    Animated.timing(this.state.fadeAnim, {
      toValue: 0,
      duration: 1000
    }).start();
  };

  onScroll = (e) => {
    if (e.nativeEvent.contentOffset.y > 0 && this.state.scrollUp) {
      this.setState({ scrollUp: false, scrolldown: true }, () => {
        this.fadeOut()
      })
    }
    if (e.nativeEvent.contentOffset.y < 10 && this.state.scrolldown && !this.state.scrollUp) {
      this.setState({ scrolldown: false, scrollUp: true }, () => {
        this.fadeIn()
      })
    }
  }
  _pullToRefresh = () => {
    this.setState({ showLoaderRefresh: true })
    this.getSendCoinHistory("", "")

  }
  renderListView() {
    alert('yes')

    return (
      <View style={Style.scrollViewContent}>
        {
          this.state.searchType == '' || this.state.searchType == 'IN' || this.state.searchType == 'OUT' ?
            <View style={{ flex: 1 }}>
              <FlatList
                contentContainerStyle={{ flexGrow: 1 }}
                showsHorizontalScrollIndicator={false}
                data={this.state.arrCoinHistory}
                extraData={this.state.arrCoinHistory}
                onEndReached={this.onEndReached}
                onEndReachedThreshold={0.5}
                onScroll={(e) => this.onScroll(e)}
                
                renderItem={({ item }) => <CoinListRow
                  item={item}
                  title={item.title} img={item.img}
                  onRowPress={() => pushToScreen('SendCoin', { OtherUserData: item })}
                  onUserImagePress={(item) => this.onUserImagePress(item)}
                />
                }
                ItemSeparatorComponent={() => (<View style={Style.separater} />)}
                keyExtractor={(item, index) => index.toString()}
                ListEmptyComponent={() => (
                  <View style={{ flex: 1, backgroundColor: "#f5f6fa", alignItems: "center", justifyContent: "center", marginTop: 0 }} >
                    <Text style={{ fontSize: 16, fontFamily: fonts.SFProMedium, color: "#a2a2b2" }}>You have no transfers.</Text>
                    <Text style={{ fontSize: 14, fontFamily: fonts.SFProMedium, color: "#c7cbd8", marginBottom: 15 }}>Send and receive views, for free!</Text>
                    <Image
                      style={{ height: 100, width: 100, borderRadius: 50, resizeMode: "contain" }}
                      source={Images.mind}

                    />
                  </View>
                )}
              />
            </View>
            :
            <View style={{ flex: 1, backgroundColor: '#fff' }}>
              {
                Helper.userSettings?.today_points > 0 ?
                  <View style={{ flexDirection: 'row', width: '100%', paddingHorizontal: 20, height: 55, alignItems: 'center', justifyContent: 'space-between', }}>
                    <Text style={{ fontSize: 16 }}>You mined <Text style={{ fontFamily: fonts.Montserrat_Bold, fontSize: 20 }}>{Helper.userSettings?.today_points}vc</Text></Text>
                    <Text style={{ fontSize: 16 }}>{moment(new Date()).utc().format('LL')}</Text>
                  </View>
                  :
                  <View style={{ flex: 1, backgroundColor: "#f5f6fa", alignItems: "center", justifyContent: "center", }} >
                    <Text style={{ fontSize: 16, fontFamily: fonts.SFProMedium, color: "#a2a2b2" }}>You haven’t mined any coins.</Text>
                    <Text style={{ fontSize: 14, fontFamily: fonts.SFProMedium, color: "#c7cbd8", marginBottom: 15 }}>Watch content to earn view coins!</Text>
                    <Image
                      style={{ height: 100, width: 100, borderRadius: 50, resizeMode: "contain" }}
                      source={Images.all}
                    />
                  </View>
              }

            </View>
        }
        {
          this.state.showBlockModel &&
          <BlockModelCompo
            showBlockModel={this.state.showBlockModel}
            onBlock={(item) => this.onBlock(item)}
            hideBlockModel={() => this.setState({ showBlockModel: false })}
            selectedRowDetail={this.state.selectedRowDetail}
          />
        }
      </View>
    )
  }

  UserBlockUnBlockResponse(response) {
    if (response) {
      this.setState({
        arrCoinHistory: [],
      }, () => {
        this.getSendCoinHistory('', '');
      })
    }
  }


  render() {
    return (
      <SafeAreaView style={{ flex: 1 }}>
        {this.renderHeader()}
        {this.renderListView()}
        <CashInNoCoinsModel
          modelVisible={this.state.modelVisible}
          showNoCoinsModel={(val) => this.showNoCoinsModel(val)} />

        <WhatIsTrollModelCompo
          trollModel={this.state.trollModel}
          showTrollModel={(val) => this.showTrollModel(val)} />

        <Spinner visible={this.state.showSpinner} />

      </SafeAreaView >
    );
  }

  componentDidMount = () => {
    console.log('Helper.userInfo----------------   ', Helper.userInfo)
    this.functionevevnt = this.props.navigation.addListener('didFocus', () => {
      if (Platform.OS == 'android') {

        StatusBar.setBackgroundColor('white');

      }
      StatusBar.setBarStyle('dark-content')

      this.getSettingApi()
      this.getSendCoinHistory("", "")

    });
    this.UserBlockUnBlock = DeviceEventEmitter.addListener('user_block_unblock_response', response => this.UserBlockUnBlockResponse(response));
  };
  componentWillUnmount = () => {
    this.functionevevnt.remove();
    if (this.UserBlockUnBlock) {
      this.UserBlockUnBlock.remove();
    }
  };
}

export default Coin;


const Style = StyleSheet.create({
  tabtxtstyle: {
    fontSize: 15,
    color: 'rgb(216,217,219)',
    fontFamily: Fontfamily.PB

  },
  separater: {
    height: 2,
    backgroundColor: "#F0F0F0",
    width: "100%"
  },
  scrollViewContent: {
    backgroundColor: "white",
    flex: 1
  }
})