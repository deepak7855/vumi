
import React, { useState, useEffect } from 'react'
import { View, ImageBackground, Image, StyleSheet, TouchableOpacity, BackHandler } from 'react-native'
import {
    GButton, Images, GAuthHeader, ScrollEnable,
    IconInput,
    Colors,
} from '../common';
import Spinner from 'react-native-loading-spinner-overlay';
import { BulletComponent } from '../components'
import { Actions } from 'react-native-router-flux';
import CameraController from '../common/CameraController';
import Helper from '../lib/Helper';
import pushToScreen from '../navigation/NavigationActions';

export const SignUpStep3 = (props) => {
    const [userName, setUserName] = useState('')
    const [btnColor, changeBtnColor] = useState(false);
    const [userImage, setUserImage] = useState(Images.userName);
    const [imagePicked, setImagePicked] = useState(false);
    const [showTick, setShowTick] = useState(false);
    const [isUserNameValid, setValidUserName] = useState(null);
    const [showSpinner, setSpinner] = useState(false)

    const onUserNameEnter = (text) => {
        changeBtnColor(false)
        setShowTick(false)
        setUserName(text)
    }

    useEffect(() => {
        const timer = setTimeout(() => {
            if (userName.length > 4) {
                checkUserName(userName.trim())
            } else {
                setShowTick(false)
                changeBtnColor(false)
            }
        }, 1000);
        return () => clearTimeout(timer);
    }, [userName]);


    useEffect(() => {
        if (isUserNameValid && imagePicked) {
            changeBtnColor(true)
        }
    }, [isUserNameValid, imagePicked]);

    const checkUserName = (text) => {
        var data = {
            username: text
        }
        setSpinner(true)
        Helper.makeRequest({ url: "check-username", method: "POST", data: data }).then((resp) => {
            if (resp.status == 'true') {
                setValidUserName(true)
                imagePicked ? changeBtnColor(true) : changeBtnColor(false);
            } else {
                setValidUserName(false)
            }
            setShowTick(true)
            setSpinner(false)
        })
    }

    const onChooseImage = () => {
        console.log("response");

        CameraController.selecteImage((response) => {
            console.log(response,"response");
            if (response.uri) {
                let tempdata = new FormData();
                tempdata.append('profile_image', {
                    uri: response.uri,
                    name: 'test.jpg',
                    type: 'image/jpeg'
                });
                setUserImage({ uri: response.uri })
                setImagePicked(true)
                // isUserNameValid ? changeBtnColor(true) : changeBtnColor(false)
            }
        });
    }

    const onNext = () => {
        var data = {
            country_code: props.country_code,
            location: props.location,
            phone: props.phone,
            username: userName,
            profile_image: userImage.uri
        }
        console.log(JSON.stringify(data), data);

        // Actions.CreatePassword({ data })
        pushToScreen('CreatePassword', { data })
    }

    const backAction = () => {
        if (Actions.currentScene == "SignUpStep3") {
            return true
        }
    };
    useEffect(() => {
        BackHandler.addEventListener("hardwareBackPress", backAction);

        return () => {
            BackHandler.removeEventListener("hardwareBackPress", backAction);
        };
    }, [backAction]);


    {
        return (
            <ImageBackground resizeMode='cover'
                source={Images.Loginbg}
                style={{ flex: 1, alignItems: 'center' }}>

                <GAuthHeader title1="Create Profile" rightItem>
                    <BulletComponent numberof={3} activeIndex={3} />
                </GAuthHeader>

                <ScrollEnable style={{ borderWidth: 0 }} containerStyle={{ alignItems: 'center', flex: 1 }}>

                    <View style={{ marginVertical: 30, borderWidth: 0 }}>
                        <View style={Style.bigO}>
                            <Image
                                source={userImage}
                                style={{ height: 148, width: 148, borderRadius: 74, resizeMode: imagePicked ? "cover" : "contain" }}
                            />
                        </View>

                        <TouchableOpacity onPress={() => onChooseImage()} style={Style.smallO}>
                            <Image
                                source={Images.more}
                                style={{ height: 20, width: 20, resizeMode: "contain", marginHorizontal: 5 }}
                            />
                        </TouchableOpacity>

                    </View>

                    <View style={{ justifyContent: "space-between", flex: 1, marginBottom: 20 }}>
                        <IconInput
                            placeholder="Your Username"
                            style={{ textAlign: 'center', }}
                            rightIconDisable
                            value={userName}
                            onChangeText={(text) => onUserNameEnter(text)}
                            rimagePath={showTick ? isUserNameValid ? Images.ticks : Images.cross_pink : null}
                        />

                        <GButton
                            height={65}
                            disabled={!btnColor}
                            radius={32.5}
                            bText='Next'
                            txtcolor='white'
                            bgColor={btnColor ? Colors.abuttonColor : Colors.btxtColor}
                            onPress={() => { onNext() }}
                        />

                    </View>
                </ScrollEnable>
                <Spinner visible={showSpinner} />
            </ImageBackground>
        )
    }

}
const Style = StyleSheet.create({



    bigO: {
        height: 160, width: 160, borderWidth: 10,
        borderRadius: 80, borderColor: 'rgb(112,105,255)',
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: 'rgb(83,61,213)'

    },
    smallO: {
        position: 'absolute', bottom: 5, right: 5,
        borderWidth: 5, borderColor: 'rgb(112,105,255)',
        backgroundColor: Colors.abuttonColor,
        height: 45, width: 45, borderRadius: 23.5,
        justifyContent: 'center', alignItems: 'center'

    }




})