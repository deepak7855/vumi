import React, { Component } from 'react'
import { View, ImageBackground, StatusBar, Text, TouchableOpacity } from 'react-native'
import {
    GButton, IconInput, TxtW, Images, GAuthHeader,
    Colors, ScrollContainer, ScrollButtonTop,
} from '../common';
import { Actions } from 'react-native-router-flux';
import Helper from '../lib/Helper';
import Toast from 'react-native-root-toast';
import Spinner from 'react-native-loading-spinner-overlay';
import pushToScreen from '../navigation/NavigationActions';


export class Login extends Component {

    constructor(props) {
        super(props);
        this.state = {
            login: false,
            btnColor: true,
            userName: '',
            password: '',
            enableBtn: false,
            showSpinner: false
        };
    }



    onUserNameEnter = (text) => {
        text.length >= 5 && this.state.password.length >= 8 ?
            this.setState({ enableBtn: true, userName: text }) :
            this.setState({ enableBtn: false, userName: text })
    }

    onPasswordInput = (text) => {
        text.length >= 8 && this.state.userName.length >= 5 ?
            this.setState({ enableBtn: true, password: text }) :
            this.setState({ enableBtn: false, password: text })
    }

    onLogin = () => {

        var data = {

            username: this.state.userName,
            password: this.state.password,
            device_type: Helper.device_type,
            device_id: Helper.device_token
        }
        this.setState({ showSpinner: true })
        Helper.makeRequest({ url: "login", method: "POST", data: data }).then((resp) => {

            if (resp.status == 'true') {
                Helper.userInfo = resp.data;
                Helper.saveDataInAsync("userInfo", resp.data);
                Helper.saveDataInAsync("token", resp.token)
                Actions.reset('TabBar')
            } else {
                Toast.show(resp.message)
            }
            this.setState({ showSpinner: false })
        })

    }


    render() {
        return (
            <ImageBackground resizeMode='cover' source={Images.Loginbg} style={{ flex: 1, alignItems: 'center' }}>

                <GAuthHeader bArrow title1="Log In" />

                <ScrollContainer containerStyle={{ alignItems: 'center', justifyContent: 'center', flex: 1 }}>
                    <View style={{}}>
                        <IconInput
                            value={this.state.userName}
                            onChangeText={(text) => this.onUserNameEnter(text)}
                            limagePath={Images.userName}
                            placeholder='Username'
                            textContentType="none"
                        />
                        <IconInput
                            value={this.state.password}
                            limagePath={Images.pass}
                            placeholder='Password'
                            onChangeText={(text) => this.onPasswordInput(text)}
                            secureTextEntry
                            textContentType="none"
                        />
                        <Text style={{ color: 'white', fontSize: 11, marginLeft: 15 ,marginTop:-10}}>*Minimum 8 characters required</Text>


                    </View>


                </ScrollContainer>

                <ScrollButtonTop style={{ alignItems: 'center', }}>
                    <TouchableOpacity style={{ marginVertical: 20 }}
                        onPress={() => { pushToScreen('ForgotPassword') }}>
                        <TxtW style={{ color: Colors.txtColor, fontSize: 16 }}>Forgot Password?</TxtW>
                    </TouchableOpacity>
                    <GButton
                        height={65}
                        radius={32.5}
                        disabled={!this.state.enableBtn}
                        bText='Next'
                        txtcolor='#fff'
                        bgColor={this.state.enableBtn ? Colors.abuttonColor : Colors.btxtColor}
                        onPress={() => { this.onLogin() }}
                    />
                </ScrollButtonTop>

                <Spinner visible={this.state.showSpinner} />
            </ImageBackground>
        )
    }
}
