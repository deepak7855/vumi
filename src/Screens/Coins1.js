import React, { Component } from 'react'
import { Text, View, TouchableOpacity, StyleSheet, FlatList, ScrollView, Image, Animated, StatusBar, SafeAreaView } from 'react-native'
import {
    Images, IconInput, width,
    Fontfamily, GSmallButton, styles,
} from '../common';
import { CoinListRow } from '../components/Coins/CoinListRow';
import { SearchInputBarCompo } from '../components/Coins/SearchInputBarCompo';
import { Actions } from 'react-native-router-flux';
import { SearchConditionsCompo } from '../components/Coins/SearchConditionsCompo';
import fonts from '../common/fonts';
import { CashInNoCoinsModel } from '../components/Coins/CashInNoCoinsModel';
import { WhatIsTrollModelCompo } from '../components/common/WhatIsTrollModelCompo';

const DATA = [0, 1, 2, 3, 4, 5, 6, 0];



export class Coins1 extends Component {

    constructor(props) {
        super(props);
        this.state = {
            fadeAnim: new Animated.Value(300),
            scrollUp: true,
            scrolldown: false
        };


    }

    fadeIn = () => {
        // Will change fadeAnim value to 1 in 5 seconds
        Animated.timing(this.state.fadeAnim, {
            toValue: 300,
            duration: 1000
        }).start();
    };

    fadeOut = () => {
        // Will change fadeAnim value to 0 in 5 seconds
        Animated.timing(this.state.fadeAnim, {
            toValue: 100,
            duration: 1000
        }).start();
    };

    onScroll = (e) => {
        console.log(e.nativeEvent.contentOffset.y)
        if (e.nativeEvent.contentOffset.y > 0 && this.state.scrollUp) {
            this.setState({ scrollUp: false, scrolldown: true }, () => {
                this.fadeOut()
            })
        }
        if (e.nativeEvent.contentOffset.y < 10 && this.state.scrolldown && !this.state.scrollUp) {
            this.setState({ scrolldown: false, scrollUp: true }, () => {
                this.fadeIn()
            })
        }
    }


    render() {

        return (
            <SafeAreaView style={{ flex: 1, backgroundColor: "white" }}>
                <FlatList
                    contentContainerStyle={{ flexGrow: 1 }}
                    data={[1, 2, 3, 4, 5, 6, 7, 8]}
                    ListHeaderComponent={({ item }) => <Animated.View style={[{ backgroundColor: 'lightblue', height: this.state.fadeAnim }]} >
                        {
                            [1, 2, 3, 4, 5, 6, 7, 8].map((item, index) => <View style={{ padding: 5 }}><Text>{index}</Text></View>)
                        }
                    </Animated.View>}
                    ItemSeparatorComponent={({ item }) => <View style={{ height: 10, backgroundColor: 'white' }} />}
                    stickyHeaderIndices={[0]}
                    renderItem={({ item }) => <View style={{ height: 100, backgroundColor: 'wheat' }} ><Text>{item}</Text></View>}
                    keyExtractor={(item, index) => index}
                    onScroll={(e) => this.onScroll(e)}
                />
            </SafeAreaView>
        )
    }
    componentDidMount = () => {

        this.functionevevnt = this.props.navigation.addListener('didFocus', () => {
            StatusBar.setBackgroundColor('white');
            StatusBar.setBarStyle('dark-content')
        });
    };
    componentWillUnmount = () => {
        this.functionevevnt.remove();
    };

}

const Style = StyleSheet.create({

})