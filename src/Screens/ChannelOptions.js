import React, { Component } from 'react';
import { View, Text, FlatList, Image, TouchableOpacity, StyleSheet,ActivityIndicator, RefreshControl, StatusBar } from 'react-native';
import { GHeader, Images } from '../common';
import fonts from '../common/fonts';
import { SavedAndPostRowCompo } from '../components/common/SavedAndPostRowCompo';


import Helper from '../lib/Helper';
import Toast from 'react-native-root-toast';
import Spinner from 'react-native-loading-spinner-overlay';
import FastImage from 'react-native-fast-image';
import Config from '../lib/Config';


export default class ChannelOptions extends Component {
    constructor(props) {
        super(props);
        this.state = {
            showSpinner: false,
            arrChannelOption: [], first_post_id: '', showLoaderPageing: false, showLoaderRefresh: false

        };
    }
    componentDidMount() {
        this.getChannelOptions(true, "")
    }

    getChannelOptions = (spinner, last_id) => {
        this.setState({ showSpinner: spinner })
        Helper.makeRequest({ url: "get-temp-post", method: "POST", data: { last_id: last_id } }).then((resp) => {
            this.state.showLoaderPageing = false
            this.state.showLoaderRefresh = false
            this.state.showSpinner = false
            if (last_id == "") {
                this.state.arrChannelOption = []
            }
            if (resp.status == 'true') {
                if (resp.data && resp.data.data && resp.data.data.length > 0) {
                    this.state.arrChannelOption = [...this.state.arrChannelOption, ...resp.data.data]
                }
                this.state.first_post_id = resp.data.first_id ? resp.data.first_id : ""

            } else {
                Toast.show(resp.message)
            }
            this.setState({})
        })
    }

    onScroll = () => {
        console.log("onScroll-------")
        if (!this.state.showLoaderPageing && this.state.arrChannelOption.length > 0 && this.state.first_post_id != '' && this.state.arrChannelOption[this.state.arrChannelOption.length - 1].id != this.state.first_post_id) {
            this.setState({ showLoaderPageing: true })
            this.getChannelOptions(false, this.state.arrChannelOption[this.state.arrChannelOption.length - 1].id);
        }
    }
    _pullToRefresh = () => {
        this.setState({ showLoaderRefresh: true })
        this.getChannelOptions(false, "");
    }

    render_Activity_footer = () => {
        var footer_View = (
            <View style={{ flex: 1, backgroundColor: '#ffffff', alignItems: 'center', justifyContent: 'center', padding: 15 }}>
                <ActivityIndicator size="large" color="#000000" />
            </View>
        );
        return footer_View;
    };

    render() {
        return (
            <View style={{ flex: 1, backgroundColor: '#f5f6fa', }}>
                <Spinner visible={this.state.showSpinner} />

                <StatusBar backgroundColor="white" />
                <GHeader
                    bArrow
                    greyBackButton
                    color="#606076"
                    backgroundColor="white"
                    title1="Channel Options"
                />
                <View style={{ padding: 30, justifyContent: "center", alignItems: "center", backgroundColor: "#efeff5" }}>
                    <Text style={{ color: "#9797aa", fontSize: 15, fontFamily: fonts.SFProDisplay_Semibold }}>What are Split Views?</Text>
                    <Text style={{ color: "#a1a1ad", fontSize: 14, fontFamily: fonts.SFProRegular, textAlign: "center", marginTop: 10 }}>“Split Views” shows repeat viewers your other posts. Your cashed-in post will always be shown first.</Text>
                </View>

                <View style={{ paddingVertical: 12, justifyContent: "center", alignItems: "center", backgroundColor: "#f5f5f8" }}>
                    <Text style={{ color: "#848484", fontSize: 15, fontFamily: fonts.SFProDisplay_Light }}>Drag posts to set order</Text>
                </View>

                <View style={styles.flatlist}>
                    <FlatList
                        data={this.state.arrChannelOption}
                        ListFooterComponent={this.state.showLoaderPageing && this.render_Activity_footer}
                        onEndReached={this.onScroll}
                        onEndReachedThreshold={0.5}
                        contentContainerStyle={{ flexGrow: 1 }}
                        ItemSeparatorComponent={() => <View style={{ height: 5, backgroundColor: '#f5f6fa' }} />}
                        renderItem={({ item, index }) => <SavedAndPostRowCompo hideRightButton showNumbers index={index} item={item} draggableIcon />}
                        keyExtractor={item => item.id}
                        refreshControl={
                            <RefreshControl
                                refreshing={this.state.showLoaderRefresh}
                                onRefresh={this._pullToRefresh}
                            />
                        }
                        ListEmptyComponent={() => (
                            <View style={styles.emptyContainer}>
                                {!this.state.showSpinner &&
                                    <Text style={styles.emptyText}>No Record Found!</Text>
                                }
                            </View>
                        )}
                    />
                </View>
            </View>
        );
    }
}


const styles = StyleSheet.create({
    flatlist: {
        backgroundColor: '#f5f6fa',
        flex: 1,
    },

    listemptyContainer: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center"
    },

    emptyText: {
        color: "#a2a2b2",
        fontSize: 17,
        fontFamily: fonts.SFProMedium
    },

    emptyImage: {
        height: 120,
        width: 120,
        resizeMode: "contain",
        marginTop: 55,
        marginBottom: 30,
    },

    hiddenContainer: { flexDirection: "row", alignItems: 'center', justifyContent: "space-between", flex: 1, },
    closeView: { width: 45, height: "100%", justifyContent: "center", alignItems: "center", backgroundColor: '#ff5555', },

    emptyContainer: {
        flex: 1,
        backgroundColor: "#edeff5",
        justifyContent: "center",
        alignItems: "center"
    },

});