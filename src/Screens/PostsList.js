import React, { Component } from 'react';
import { View, Text, FlatList, RefreshControl, DeviceEventEmitter, Image, TouchableOpacity, StyleSheet, StatusBar, ActivityIndicator } from 'react-native';
import { GHeader, Images, Colors } from '../common';
import fonts from '../common/fonts';
import { SavedAndPostRowCompo } from '../components/common/SavedAndPostRowCompo';
import { SwipeListView } from 'react-native-swipe-list-view';
import { Actions } from 'react-native-router-flux';
import pushToScreen from '../navigation/NavigationActions';

import Helper from '../lib/Helper';
import Toast from 'react-native-root-toast';
import Spinner from 'react-native-loading-spinner-overlay';
import FastImage from 'react-native-fast-image';
import Config from '../lib/Config';
import ImageLoadView from '../components/ImageLoadView';


export default class PostsList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showSpinner: false,
      arrDraftPostList: [], first_post_id: '', showLoaderPageing: false, showLoaderRefresh: false,
      videoPaused: false,
    };

  }

  componentDidMount() {
    this.functionevevnt = this.props.navigation.addListener('didFocus', () => {
      // StatusBar.setBackgroundColor('#232323');
      // StatusBar.setBarStyle('light-content')
      this.getDraftPostListData(true, "")
    });
    this.getDraftPostListData(true, "")

  }
  componentWillUnmount() {
    this.functionevevnt.remove()
  }
  getDraftPostListData = (spinner, last_id) => {
    this.setState({ showSpinner: spinner })
    Helper.makeRequest({ url: "get-temp-post", method: "POST", data: { last_id: last_id } }).then((resp) => {
      this.state.showLoaderPageing = false
      this.state.showLoaderRefresh = false
      this.state.showSpinner = false
      if (last_id == "") {
        this.state.arrDraftPostList = []
      }
      if (resp.status == 'true') {
        if (resp.data && resp.data.data && resp.data.data.length > 0) {
          this.state.arrDraftPostList = [...this.state.arrDraftPostList, ...resp.data.data]
        }
        this.state.first_post_id = resp.data.first_id ? resp.data.first_id : ""

      } else {
        Toast.show(resp.message)
      }
      this.setState({})
    })
  }
  deleteSavePost(id, index) {
    this.setState({ showSpinner: true })
    let data = {
      post_id: id,
    }
    Helper.makeRequest({ url: "delete-temp-post", method: "POST", data: data }).then((resp) => {
      if (resp.status == 'true') {
        this.state.arrDraftPostList.splice(index, 1)
      } else {
      }
      Toast.show(resp.message)

      this.setState({ showSpinner: false })

    })

  }
  onScroll = () => {
    console.log("onScroll-------")
    if (!this.state.showLoaderPageing && this.state.arrDraftPostList.length > 0 && this.state.first_post_id != '' && this.state.arrDraftPostList[this.state.arrDraftPostList.length - 1].id != this.state.first_post_id) {
      this.setState({ showLoaderPageing: true })
      this.getDraftPostListData(false, this.state.arrDraftPostList[this.state.arrDraftPostList.length - 1].id);
    }
  }
  _pullToRefresh = () => {
    this.setState({ showLoaderRefresh: true })
    this.getDraftPostListData(false, "");
  }
  renderSaveListView = (({ item, index }) => {
    let postDetails = (item && item.post_detail && item.post_detail.length > 0) ? item : ""
    // console.log('postDetails',postDetails)

    if (postDetails == "") {
      return (<View />)
    }
    return (
      <View style={styles.rowContainer} >
        <ImageLoadView
          resizeMode={"cover"}
          style={styles.userImg}
          source={postDetails.post_detail[0].thumbnail_media ? {
            uri: Config.imageUrl + postDetails.post_detail[0].thumbnail_media
          } : Images.User_profile_icon}
        />
        <TouchableOpacity onPress={() => this.methodBtnClick(item)} style={{ width: "70%", marginLeft: 5, padding: 3 }}>
          {postDetails.user && <Text numberOfLines={1} style={styles.userName}>{Helper.userInfo.username}</Text>}
          <View style={styles.userContainer}>
            <Text numberOfLines={1} style={styles.grp}>{postDetails.title}</Text>
          </View>
          <View style={styles.statusView}>
            <Image
              style={{
                height: 25,
                width: 25,
                marginLeft: 5,
                resizeMode: "contain"
              }}
              source={Images.edit}
            />

          </View>
          {/* <View style={styles.statusView}>
            <View style={styles.statusRow}>
              <Text style={styles.views}>{Helper.convertValueBillions(postDetails.total_views)}</Text>
              <Image
                style={styles.ViewIng}
                source={Images.eye_grey_color}
              />
            </View>
            <View style={styles.statusRow}>
              <Text style={styles.views}>{Helper.convertValueBillions(postDetails.total_likes)}</Text>
              <Image
                style={styles.ViewIng}
                source={Images.heart_grey}
              />
            </View>
          </View> */}
        </TouchableOpacity>
        <TouchableOpacity onPress={() => pushToScreen('CashIn', { fromCoins: false })} style={styles.arrowContainer}>
          <Image
            source={Images.right_arrow}
            style={styles.arrowImg}
          />
        </TouchableOpacity>
      </View>
    )
  })
  methodBtnClick = (item) => {
    console.log(item, "itemmmmmm");
    this.setState({ videoPaused: true }, () => {
      let data = {}
      data.Id = item.id
      data.title = item.title.trim()
      data.description = item.description.trim()
      data.display_name = item.display_name.trim()
      data.social_link = item.social_link.trim()
      data.caption = item.caption.trim()

      // data. mediaType = item && item.post_detail && item.post_detail[0].type,
      // data.media = item && item.post_detail && item.post_detail[0].media,
      // dicPostDetails = data
      // console.log(data, "data");
      let media = item && item.post_detail

      let mediaArr = []
      media.map((item, i) => {
        mediaArr.push(Config.imageUrl + item.media)
        console.log(mediaArr, 'mediaArr1');

      }

      )

      console.log(this.state.mediaArr, 'mediaArr');
      // pushToScreen('PreviewPost', { mediaType: item && item.post_detail[0] && item.post_detail[0].type == 'IMAGE' ? 'Picture' : item.post_detail[0].type, media: mediaArr, dicPostDetails: data });
      pushToScreen('CreatePost', { media: mediaArr, mediaType: item && item.post_detail[0] && item.post_detail[0].type == 'IMAGE' ? 'Picture' : item.post_detail[0].type, EditPostList: true, EditData: data });

      // pushToScreen('CreatePost');

    })
  }

  render_Activity_footer = () => {
    var footer_View = (
      <View style={{ flex: 1, backgroundColor: '#ffffff', alignItems: 'center', justifyContent: 'center', padding: 15 }}>
        <ActivityIndicator size="large" color="#000000" />
      </View>
    );
    return footer_View;
  };


  onPostHeaderPress = () => {
    // Actions.UploadPost();
    pushToScreen('UploadPost', { isComeFrom: 'postList' });
  }
  methodCashInClick() {
    if (Number(Helper.userSettings.temp_post_count) > 0) {
      pushToScreen('CashIn', { fromCoins: false })
    }
    else {
      Toast.show("Please create first post.")
    }
  }
  gotoBackMessage = () => {
    Actions.pop();
    DeviceEventEmitter.emit("updatePost", "resp")
  }
  render() {
    return (
      <View style={{ flex: 1, backgroundColor: '#f5f6fa', }}>
        {/* <StatusBar backgroundColor="white" /> */}
        <GHeader
          // showAddPostBtn
          ProfileBack
          greyBackButton
          color="#606076"
          backgroundColor="white"
          title1="Posts"
          gotoBackMessage={() => this.gotoBackMessage()}
          // onPostHeaderPress={() => this.onPostHeaderPress()}
        />
        <TouchableOpacity onPress={() => this.onPostHeaderPress()}
          style={{ backgroundColor: Colors.bluelight, flexDirection: 'row', alignItems: 'center', justifyContent: 'center', height: 55, marginHorizontal: 25, borderRadius: 8, marginTop: 25, }}>
          <View style={{ flex: 1, backgroundColor: Colors.bluedark, flexDirection: 'row', alignItems: 'center', justifyContent: 'center', height: 54, borderRadius: 8, marginBottom: 10 }}>
            <Image style={{ width: 20, height: 20, margin: 6 }} resizeMode={'contain'} source={Images.addpost}></Image>
            <Text style={{ color: Colors.white, fontSize: 13, fontFamily: fonts.Montserrat_SemiBold }}>Upload a Post</Text>
          </View>
        </TouchableOpacity>
        <View style={styles.flatlist}>
          <SwipeListView
            rightOpenValue={-45}
            disableRightSwipe
            contentContainerStyle={{ flexGrow: 1 }}
            refreshControl={
              <RefreshControl
                refreshing={this.state.showLoaderRefresh}
                onRefresh={this._pullToRefresh}
              />
            }
            renderHiddenItem={(rowData, rowMap) => (
              <View style={styles.hiddenContainer}>
                <View style={{ width: "50%", backgroundColor: 'transparent' }} />
                <TouchableOpacity onPress={() => this.deleteSavePost(rowData.item.id, rowData.index)} style={styles.closeView}>
                  <Image
                    source={Images.close}
                    style={{ height: 55, width: 55, resizeMode: "contain", }}
                  />
                </TouchableOpacity>
              </View>
            )}
            data={this.state.arrDraftPostList}
            ItemSeparatorComponent={() => <View style={{ height: 5, backgroundColor: '#f5f6fa' }} />}
            // renderItem={({ item, index }) => <SavedAndPostRowCompo fromSavedList />}
            renderItem={this.renderSaveListView}
            keyExtractor={item => item.id}
            ListFooterComponent={this.state.showLoaderPageing && this.render_Activity_footer}
            onEndReached={this.onScroll}
            onEndReachedThreshold={0.5}
            ListEmptyComponent={() => (
              <View style={styles.listemptyContainer}>
                <Text style={styles.emptyText}>You haven’t saved any posts yet…</Text>
                <Image
                  style={styles.emptyImage}
                  source={Images.star}
                />
                <Text style={[styles.emptyText, { textAlign: "center" }]}>Tap the star button on posts{'\n'} to save them.</Text>

              </View>
            )}
          />
        </View>
        {/* <View style={{ backgroundColor: 'white', flexDirection: "row", alignItems: "center", justifyContent: "space-around", paddingVertical: 10 }}>
          <Image
            source={Images.monitor}
            style={{ height: 25, width: 30, resizeMode: 'contain' }}
          />
          <View style={{}}>
            <Text style={{ color: '#94949a', fontFamily: fonts.SFProDisplay_Light, fontSize: 14 }}>Upload posts online at</Text>
            <Text style={{ color: '#7ca9ff', fontFamily: fonts.SFProMedium, fontSize: 16 }}>vumi.app/upload</Text>
          </View>
          <View style={{ height: 25, width: 30, }} />
        </View> */}

        <Spinner visible={this.state.showSpinner} />

      </View>
    );
  }
}


const styles = StyleSheet.create({
  flatlist: {
    backgroundColor: '#f5f6fa',
    flex: 1,
    marginTop: 25
  },

  listemptyContainer: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center"
  },

  emptyText: {
    color: "#a2a2b2",
    fontSize: 17,
    fontFamily: fonts.SFProMedium
  },

  emptyImage: {
    height: 120,
    width: 120,
    resizeMode: "contain",
    marginTop: 55,
    marginBottom: 30,
  },
  rowContainer: {
    backgroundColor: "white",
    paddingHorizontal: 10,
    paddingVertical: 15,
    flexDirection: "row"
  },

  userImg: {
    height: 60,
    width: 60,
    // borderWidth:0.5
  },

  roundUserImg: {
    height: 40,
    width: 40,
    borderRadius: 20,
    resizeMode: "cover"
  },

  userName: {
    color: "#4c4c55",
    fontSize: 16,
    fontFamily: fonts.SFProDisplay_Semibold
  },
  grp: {
    color: "black",
    fontSize: 20,
    fontFamily: fonts.SFProDisplay_Bold,
    width: "95%",
  },

  statusView: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
    width: "30%",
    paddingRight: 10, marginTop: 5
  },

  statusRow: {
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center"
  },

  views: {
    color: "#919191",
    fontSize: 12,
    fontFamily: fonts.SFProRegular
  },

  ViewIng: {
    height: 10,
    width: 15,
    marginLeft: 5,
    resizeMode: "contain"
  },

  arrowContainer: {
    alignItems: "flex-end",
    justifyContent: "center",
    flex: 1,
    paddingRight: 5
  },

  arrowImg: {
    height: 15,
    width: 8,
    resizeMode: "contain"
  },

  userContainer: {
    flexDirection: "row",
    justifyContent: "space-between"
  },
  index: { marginRight: 10, textAlignVertical: "center", color: "#9d9da5", fontSize: 16, fontFamily: fonts.SFProDisplay_Semibold },
  hiddenContainer: { flexDirection: "row", alignItems: 'center', justifyContent: "space-between", flex: 1, },
  closeView: { width: 45, height: "100%", justifyContent: "center", alignItems: "center", backgroundColor: '#ff5555', }


});