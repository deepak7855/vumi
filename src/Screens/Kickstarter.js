import React, { Component } from 'react';
import { View, Text, FlatList, Image, TouchableOpacity, StyleSheet, StatusBar } from 'react-native';
import { GHeader, Images } from '../common';
import fonts from '../common/fonts';

export default class Kickstarter extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    render() {
        return (
            <View style={{ flex: 1, backgroundColor: 'white', }}>
                <StatusBar backgroundColor="white" />
                <GHeader
                    bArrow
                    greyBackButton
                    color="#606076"
                    backgroundColor="white"
                    title1="Kickstarter"
                />
                <View style={{ flex: 1, backgroundColor: '#efeff5', alignItems: "center", justifyContent: "center" }}>
                    <Text style={{ color: "#c3c3d6", fontSize: 16, fontFamily: fonts.Poppins_Bold }}> IMAGE / GIF </Text>
                </View>
                <View style={{ backgroundColor: '#efeff5', alignItems: "center", justifyContent: "center" }}>
                    <TouchableOpacity style={{ backgroundColor: "#16d3cb", alignItems: "center", justifyContent: "center", width: "80%", paddingVertical: 15, marginVertical: 20, borderRadius: 25 }}>
                        <Text style={{ color: "white", fontSize: 16, fontFamily: fonts.Poppins_Bold }}> Support us! </Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}
