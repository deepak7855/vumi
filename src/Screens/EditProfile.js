import React, { Component } from 'react';
import { View, Text, FlatList, Image, TouchableOpacity, StyleSheet, StatusBar, ScrollView } from 'react-native';
import { GHeader, Images, Colors, Fontfamily, } from '../common';
import fonts from '../common/fonts';
import CameraController from '../common/CameraController';
import Config from '../lib/Config';
import Helper from '../lib/Helper';
import { InputCompoModel } from '../components/common/InputCompoModel';
import Toast from 'react-native-root-toast';
import Spinner from 'react-native-loading-spinner-overlay';
import ModalFilterPicker from '../components/Filterpicker/src/index';
import { Actions } from 'react-native-router-flux';
import ImageLoadView from '../common/ImageLoadView';


export default class EditProfile extends Component {
    constructor(props) {
        super(props);
        this.state = {
            profile_image: Images.User_profile_icon,
            username: "",
            bio: "",
            location: "",
            showInput: false,
            imagePicked: false,
            showCountryModel: false,
            showSpinner: false,
            countryList: [],
            doesSomeThingChange: false
        };
    }


    onChooseImage = () => {
        CameraController.selecteImage((response) => {
            if (response.uri) {
                this.setState({ profile_image: { uri: response.uri }, imagePicked: true, doesSomeThingChange: true })
            }
        });
    }

    onSave = () => {
        let data = new FormData();
        if (this.state.imagePicked) {
            data.append('profile_image', {
                uri: this.state.profile_image.uri,
                name: 'test.jpg',
                type: 'image/jpeg'
            });
        }
        data.append('bio', this.state.bio == "null" ? "" : this.state.bio);
        data.append('location', this.state.location == "null" ? "" : this.state.location);
        this.setState({ showSpinner: true })
        Helper.makeRequest({ url: "update-profile", method: "POST", data: data, isImage: true }).then((resp) => {
            if (resp.status == 'true') {
                Helper.userInfo = resp.data;
                Helper.saveDataInAsync("userInfo", resp.data);
                Actions.pop()
            } else {
                Toast.show(resp.message)
            }
            this.setState({ showSpinner: false })
        })
    }

    onDonePress = (text) => {
        this.setState({ showInput: false, bio: text, doesSomeThingChange: text == this.state.bio ? false : true })
    }

    onSelectCountry = (countryInfo) => {
        // let loc = countryInfo.name + ", " + countryInfo.sortname
        let loc = countryInfo.name

        this.setState({ location: loc, showCountryModel: false, doesSomeThingChange: loc == this.state.location ? false : true })
    }

    render() {
        return (
            <View style={{ flex: 1, backgroundColor: 'white', }}>
                <StatusBar backgroundColor="white" barStyle="dark-content" />
                <GHeader
                    bArrow
                    greyBackButton
                    color="#606076"
                    backgroundColor="white"
                    title1="Edit Profile"
                />
                <ScrollView contentContainerStyle={{ flexGrow: 1, backgroundColor: '#efeff5', alignItems: "center", }}>
                    <View style={{ marginVertical: 50, }}>
                        <View style={{ height: 120, width: 120, borderRadius: 60 }}>
                            {/* <Image
                                source={this.state.profile_image}
                                style={styles.bigO}
                            /> */}
                            <ImageLoadView
                                resizeMode={"cover"}
                                source={this.state.profile_image}
                                style={styles.bigO}
                            />
                        </View>

                        <TouchableOpacity onPress={() => { this.onChooseImage() }} style={styles.smallO}>
                            <Image
                                style={{ height: 25, width: 25, resizeMode: "contain" }}
                                source={Images.edit}
                            />
                        </TouchableOpacity>
                    </View>

                    <View style={styles.row}>
                        <Text style={styles.title}>USERNAME</Text>
                        <Text numberOfLines={1} style={styles.value}>{this.state.username}</Text>
                    </View>

                    <View style={styles.row}>
                        <Text style={styles.title}>LOCATION</Text>
                        <TouchableOpacity onPress={() => this.setState({ showCountryModel: true })} style={{ width: '100%' }}>
                            <Text numberOfLines={1} style={styles.value}>{this.state.location == "null" || this.state.location == '' ? 'Pick Your Country' : this.state.location}</Text>
                            {/* <Text numberOfLines={1} style={styles.value}>{this.state.location  || 'Pick Your Country'}</Text> */}

                        </TouchableOpacity>
                    </View>

                    <View style={styles.row}>
                        <Text style={styles.title}>BIO</Text>
                        <TouchableOpacity style={{ width: '100%' }} onPress={() => this.setState({ showInput: true })}>
                            <Text numberOfLines={1} style={styles.value}>{this.state.bio == 'null' || !this.state.bio ? 'About You!' : this.state.bio}</Text>
                        </TouchableOpacity>
                    </View>

                </ScrollView>
                <TouchableOpacity
                    disabled={!this.state.doesSomeThingChange}
                    onPress={() => this.onSave()} style={[styles.saveBtn, { opacity: this.state.doesSomeThingChange ? 1 : 0.6 }]}>
                    <Text style={{ color: "white", fontSize: 18, fontFamily: fonts.SFProDisplay_Heavy }}>Save</Text>
                </TouchableOpacity>

                <InputCompoModel
                    showInput={this.state.showInput}
                    bio={this.state.bio}
                    onDonePress={(text) => this.onDonePress(text)}
                    hideModel={() => this.setState({ showInput: false })}
                />

                <ModalFilterPicker
                    placeholderText="Search Country..."
                    visible={this.state.showCountryModel}
                    onSelect={(countryInfo) => this.onSelectCountry(countryInfo)}
                    onCancel={() => this.setState({ showCountryModel: false })}
                    options={this.state.countryList}
                />

                <Spinner visible={this.state.showSpinner} />
            </View>
        );
    }
    componentDidMount = async () => {
        const data = await Helper.getDataFromAsync("userInfo");
        console.log('UserInfo-------------  ', data)
        this.setState({
            profile_image: { uri: Config.imageUrl + data.profile_image },
            username: data.username,
            bio: data.bio,
            location: data.country_name
        })


        Helper.makeRequest({ url: "country", method: "POST", data: '' }).then((resp) => {
            if (resp.status == 'true') {
                this.setState({ countryList: resp.data })
            } else {
                Toast.show(resp.message)
            }

        })

    };
}

const styles = StyleSheet.create({
    bigO: {
        height: 120, width: 120,
        borderRadius: 60, resizeMode: "cover"
    },
    smallO: {
        position: 'absolute', bottom: 5, right: 5,
        height: 25, width: 25,
    },
    row: { flexDirection: 'row', height: 60, backgroundColor: 'white', alignItems: 'center', justifyContent: 'space-between', width: '100%', paddingHorizontal: 20, marginVertical: 5 },
    title: { fontFamily: Fontfamily.MSB, color: '#b7b7c3', fontSize: 12, width: '35%' },
    value: { fontFamily: Fontfamily.MB, color: '#73737c', width: '65%', textAlign: "right" },
    saveBtn: { marginVertical: 10, marginBottom: 20, paddingVertical: 10, width: '80%', alignItems: "center", borderRadius: 25, backgroundColor: "#60c2ff", alignSelf: "center" },
});