
import * as React from 'react';
import Config from "./Config";
import Toast from 'react-native-root-toast';
import { AsyncStorage, Alert, Platform, Linking } from 'react-native';
import NetInfo from "@react-native-community/netinfo";
import { Actions } from 'react-native-router-flux';


export default class Helper extends React.Component {

    static toast;
    static userInfo = {};
    static device_type = Platform.OS == 'android' ? 'ANDROID' : 'IOS';
    static device_token = 'simulater';
    static modalData = {};
    static isSplash = true
    static userSettings = {
        user_coins: 0, today_flag: 0,
        temp_post_count: 0,
        total_views_count: 0,
        total_likes_count: 0, admin_post_count: 0
    }

    static coinView = 0
    static getFormData(obj) {
        let formData = new FormData();
        for (let i in obj) {
            formData.append(i, obj[i]);
        }
        return formData;
    }

    static recent_chat_history = [];

    constructor() { }

    static registerToast(toast) {
        Helper.toast = toast;
    }

    static showToast(msg) {
        if (msg) {
            //Toast.show(msg);
            Toast.show(msg, {
                duration: 2000,
                position: Toast.positions.BOTTOM,
                shadow: true,
                animation: true,
                hideOnPress: true,
                delay: 0,
            });
        }
    }

    static alert(alertMessage, cb) {
        Alert.alert(
            Config.app_name,
            alertMessage,
            [
                { text: 'OK', onPress: () => { if (cb) cb(true); console.log('OK Pressed') } },
            ],
            { cancelable: false }
        )
    }


    static confirm(alertMessage, cb) {
        Alert.alert(
            Config.app_name,
            alertMessage,
            [
                { text: 'OK', onPress: () => { if (cb) cb(true); console.log('OK Pressed') } },
                { text: 'Cancel', onPress: () => { if (cb) cb(false); }, style: 'cancel' },
            ],
            { cancelable: false }
        )
    }

    static permissionConfirm(alertMessage, cb) {
        Alert.alert(
            Config.app_name,
            alertMessage,
            [
                { text: 'NOT NOW', onPress: () => { if (cb) cb(false); }, style: 'cancel' },
                { text: 'SETTINGS', onPress: () => { if (cb) cb(true); console.log('OK Pressed') } },
            ],
            { cancelable: false }
        )
    }

    static async saveDataInAsync(key, val) {
        try {
            let tempval = JSON.stringify(val);
            await AsyncStorage.setItem(key, tempval);
        } catch (error) {
            console.error(error, "AsyncStorage")
            // Error setting data 
        }
    }

    static async getDataFromAsync(key) {
        try {
            let value = await AsyncStorage.getItem(key);
            if (value) {
                let newvalue = JSON.parse(value);
                return newvalue;
            } else {
                return value;
            }

        } catch (error) {

            console.error(error, "AsyncStorage")
            // Error retrieving data
        }
    }

    static validate(form, validations) {
        let isValidForm = true;
        let errors = {};

        let message = '';
        if (!validations) {
            validations = form.validators;
        }
        let customvalidator = {
            service: "Service name"
        };

        var reg = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;; //email
        var numberRegex = /^\d+$/; // number
        for (let val in validations) {
            if (!isValidForm) break;
            if (form[val]) {
                for (let i in validations[val]) {
                    var valData = validations[val][i];

                    let tempval = form[val];


                    if (i == "required" && !form[val].trim()) {
                        isValidForm = false;
                        let value = (val.charAt(0).toUpperCase() + val.slice(1)).split("_").join(" ");
                        message = value + " is required";

                    }

                    // else if ((i == "minLength" || i == "minLengthDigit") && form[val].length < valData) {
                    //     isValidForm = false;
                    //     let value = (val.charAt(0).toUpperCase() + val.slice(1)).split("_").join(" ");
                    //     var cStr = i == "minLengthDigit" ? " digit" : " characters";
                    //     message = value + " should be greater than " + valData + cStr;

                    // }
                    else if ((i == "minLength" || i == "minLengthDigit") && (form[val].length < valData + 2) && (val == 'mobile')) {
                        isValidForm = false;
                        let value = (val.charAt(0).toUpperCase() + val.slice(1)).split("_").join(" ");
                        var cStr = i == "minLengthDigit" ? " digit" : " chars";
                        message = value + " should be greater than or equal to " + valData + cStr;

                    }
                    else if ((i == "minLength" || i == "minLengthDigit") && form[val].length < valData) {
                        isValidForm = false;
                        let value = (val.charAt(0).toUpperCase() + val.slice(1)).split("_").join(" ");
                        var cStr = i == "minLengthDigit" ? " digit" : " chars";
                        message = value + " should be greater than or equal to " + valData + cStr;

                    }
                    else if ((i == "maxLength" || i == "maxLengthDigit") && form[val].length > valData) {
                        isValidForm = false;
                        let value = (val.charAt(0).toUpperCase() + val.slice(1)).split("_").join(" ");
                        var cStr = i == "maxLengthDigit" ? " digit" : " characters";
                        message = value + " should be smaller than " + valData + cStr;
                    }
                    else if (i == "matchWith" && form[val] != form[valData]) {
                        isValidForm = false;
                        let value = (val.charAt(0).toUpperCase() + val.slice(1)).split("_").join(" ");
                        let value2 = (valData.charAt(0).toUpperCase() + valData.slice(1)).split("_").join(" ");
                        message = value + " and " + value2 + " should be same";
                    }
                    else if (i == "email" && reg.test(form[val]) == false) {
                        isValidForm = false;
                        message = "Please enter valid email address";
                    }

                    else if (i == "numeric" && numberRegex.test(form[val]) == false) {
                        isValidForm = false;
                        let value = (val.charAt(0).toUpperCase() + val.slice(1)).split("_").join(" ");
                        message = value + " should be number only";
                    }

                    if (message) {
                        this.showToast(message);
                        break;
                    }

                }
            }
            else {
                let tempmsg;

                if (customvalidator[val]) {
                    tempmsg = customvalidator[val];
                } else {
                    tempmsg = (val.charAt(0).toUpperCase() + val.slice(1)).split("_").join(" ");
                }

                this.showToast(tempmsg + " is required");
                isValidForm = false;
                break;
            }
        }
        return isValidForm;
    }



    static async makeRequest({ url, method, data, isImage }) {

        let finalUrl = Config.url + url;
        let token = await this.getDataFromAsync("token");;
        var varheaders = {
            Accept: 'application/json',
            'Content-Type': isImage ? 'multipart/form-data;' : 'application/json',
        }

        if (token) varheaders['Authorization'] = 'Bearer ' + token;

        console.log("Api Header---", JSON.stringify(varheaders))
        console.log('')
        console.log("Api URL*************** ", finalUrl);
        console.log('')
        console.log('')
        console.log("Api Data Send--------  ", JSON.stringify(data));

        let apiData;
        if (isImage) {
            apiData = data
        } else {
            apiData = JSON.stringify(data)
        }
        return NetInfo.fetch().then(state => {
            if (state.isConnected) {
                return fetch(finalUrl, {
                    body: apiData,
                    method: method,
                    headers: varheaders,
                })
                    .then((response) => {

                        return response.json()
                    })
                    .then((responseJson) => {
                        console.log("Api Resp--------- ", finalUrl, "rrrrrrrrrr", JSON.stringify(responseJson))
                        if (responseJson.status == "false" && responseJson.error == 401) {
                            this.gotoLogout();
                            return responseJson;
                        } else if (responseJson.status == "false" && responseJson.force_logout == "true") {
                            this.gotoLogout();
                            return responseJson;
                        } else {
                            return responseJson;
                        }

                    })
                    .catch((error, a) => {
                        this.showToast("Please check your internet connection.");
                        console.log("Error---->  ", error);
                        return false
                    });
            } else {
                Toast.show("No Internet Connection")
                return false
            }
        })
    }
    static getImageUrl(url) {
        return finalUrl = Config.imageUrl + url;
    }

    static convertValueBillions(labelValue) {

        if (!labelValue) {
            return 0
        }
        // Nine Zeroes for Billions
        return Math.abs(Number(labelValue)) >= 1.0e+9

            ? Math.abs(Number(labelValue)) / 1.0e+9 + "B"
            // Six Zeroes for Millions 
            : Math.abs(Number(labelValue)) >= 1.0e+6

                ? Math.abs(Number(labelValue)) / 1.0e+6 + "M"
                // Three Zeroes for Thousands
                : Math.abs(Number(labelValue)) >= 1.0e+3

                    ? Math.abs(Number(labelValue)) / 1.0e+3 + "K"

                    : Math.abs(Number(labelValue));
    }
    static openLinkingUrl = async (url) => {
        let getTUrl = url
        if (!url.includes('http://') && !url.includes('https://')) {
            getTUrl = 'http://' + url
        }
        const supported = await Linking.canOpenURL(getTUrl);
        if (supported) {
            await Linking.openURL(getTUrl).catch(err => console.error('An error occurred', err));
        }
        else {
            getUrl = "http://www.google.com/search?q=" + url
            const newTLink = await Linking.canOpenURL(getUrl);
            if (newTLink) {
                await Linking.openURL(getUrl).catch(err => console.error('An error occurred', err));
            }
        }

        // Linking.canOpenURL(getTUrl).then(supported => {
        //     if (supported) {
        //         Linking.openURL(getTUrl);
        //     } else {
        //         Toast.show('Fail to open Url.');
        //         console.log("Don't know how to open URI: " + getTUrl);
        //     }
        // });

        // const supported = await Linking.canOpenURL(url);
        // if (supported) {
        //     await Linking.openURL(url).catch(err => console.error('An error occurred', err));
        // }
        // else {
        //     if (!url.includes('http://') && !url.includes('https://')) {
        //         let getTUrl = 'http://' + url
        //         const newLink = await Linking.canOpenURL(getTUrl);
        //         if (newLink) {
        //             await Linking.openURL(getTUrl).catch(err => console.error('An error occurred', err));
        //         }
        //     }
        //     else {

        //         let getUrl = "http://www.google.com/search?q=" + url
        //         const newTLink = await Linking.canOpenURL(getUrl);
        //         if (newTLink) {
        //             await Linking.openURL(getUrl).catch(err => console.error('An error occurred', err));
        //         }
        //     }
        // }
    }
    static gotoLogout() {
        console.log("test");
        Helper.userInfo = {};
        Helper.saveDataInAsync("userInfo", "");
        Helper.saveDataInAsync("token", "")
        Helper.saveDataInAsync("watchListContant", "");
        Actions.reset('Auth')
        console.log("test>>>>>>>>>>>>>>>>>>>>>>>>>>");

    }


}

