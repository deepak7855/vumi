import React, { useState, useEffect } from 'react';
import { Animated, Text, View,Image } from 'react-native';
import { TouchableOpacity } from 'react-native-gesture-handler';
import Helper from '../Lib/Helper';
import { EventRegister } from 'react-native-event-listeners';

export default class LocalNotification extends React.Component {
    constructor(props) {
        super(props);
        this.state = {fadeIn: new Animated.Value(0),
                     fadeOut: new Animated.Value(1),
                     localNotiShow:false,
                     localNotiTitle:'',
                     localNotiText:'',
                     NotiAllData:''
                    };

                    
    }


    componentWillReceiveProps(nextprops) {
        //alert(JSON.stringify(nextprops.localNotiShow))
        this.setState({localNotiShow:nextprops.localNotiShow,localNotiTitle:nextprops.NotiAllData.title,localNotiText:nextprops.NotiAllData.message,NotiAllData:nextprops.NotiAllData},() => {
            if(nextprops.localNotiShow == true){
                //this.fadeIn()
            }})
    }
    
    componentDidMount() {
        
        //this.fadeIn()
        
    }
 
    fadeIn() {
      this.state.fadeIn.setValue(0)                  
      Animated.timing(
        this.state.fadeIn,           
        {
          toValue: 1,                   
          duration: 600,              
        }
      ).start(() => this.myFadeOut());                        
   }
 
//    fadeOut() {
//      this.state.fadeOut.setValue(1)
//      Animated.timing(                  
//         this.state.fadeOut,            
//         {
//           toValue: 0,                   
//           duration: 3000,              
//         }
//      ).start();                        
//    }


    myFadeOut(){
        setTimeout(() => {
                //Helper.localNotiShow = false;
                Helper.localNotiShow = false;
                this.setState({localNotiShow:false,fadeIn: new Animated.Value(0)})
            }, 7000);
        
    }

    jumpFromNoti(){
        //alert('ok');
        Helper.localNotiShow = false;
        
        let backdata = {
            refresh: true
        }
        EventRegister.emit('localNotificationHide', backdata);

        if(this.state.NotiAllData.type == 'Quote'){
            Helper.nav.setNavigate('SendQuote',{ 'req_id': this.state.NotiAllData.service_request_id });
        }
        else if(this.state.NotiAllData.type == 'AppointmentDetailerCancel'){
            Helper.nav.setNavigate('AppointmentView',{ 'appo_id': this.state.NotiAllData.service_request_id });
        }
        else if(this.state.NotiAllData.type == 'Complete'){
            Helper.nav.setNavigate('AppointmentView',{ 'appo_id': this.state.NotiAllData.service_request_id });
        }
        // else if(this.state.NotiAllData.type == 'PaymentRelease'){
        //     Helper.nav.setNavigate('AppointmentView',{ 'appo_id': this.state.NotiAllData.service_request_id });
        // }
        
        
    }
 
   render() {
    return (
        <View style={{position:'absolute',top:0,width:'100%'}}>
       {(this.state.localNotiShow == true) && (
           <TouchableOpacity 
        //    onPress={() => this.jumpFromNoti()}
           >
           <Animated.View                 
              style={{height:60,opacity: this.state.fadeIn,padding:8,backgroundColor: 'rgba(0,0,0,0.8)',}}
           >
           <View >
                
                <Text style={{fontSize: 15, color:'#fff',fontWeight:'bold'}}>{this.state.localNotiTitle}</Text>
             
                <Text style={{fontSize: 15, color:'#ededed'}}>{this.state.localNotiText}</Text>
            
                
           </View>
              
           </Animated.View>
           </TouchableOpacity>
       )}
        </View>
    )
    }
       
} 
