export default {
    //LOCAL_SERVER
    // url:             "http://demo2server.com/vumi/api/",
    // imageUrl:        "http://demo2server.com/vumi/",
    // chat_url:        "http://198.74.62.156:9146/",

    //LIVE_SERVER
    url: "http://18.235.166.123/api/",
    imageUrl: "http://18.235.166.123/",
    chat_url: "http://18.235.166.123:8001",
    termsAndConditions: 'http://18.235.166.123/terms-of-use',
    privacyPolicy: 'http://18.235.166.123/privacy-policy',


    webUrl: "",
    app_name: "Vumi"
}