import * as React from 'react';
import SocketIOClient from 'socket.io-client';
import { EventRegister } from 'react-native-event-listeners';

import { NetInfo } from 'react-native'

import Config from './Config';
import Helper from './Helper';
import {Message} from '../screens/Message/Message';
import Chat from '../screens/Message/chat';
import Moment from 'moment'


export default class ChatController extends React.Component {
    isConnected = false;
    constructor() {

    }

    static checkConnection(type, cb) {
        let newtype = type;
        Helper.getData('userdata').then((userdata) => {
            NetInfo.isConnected.fetch().then(isConnected => {
                if (isConnected) {

                    if (ChatController.isConnected) {

                        cb(true);
                        return;
                    } else {
                        Helper.showLoader();
                        ChatController.connectUser(userdata.id, newtype);
                        cb(false);
                    }

                } else {
                    alert('Oops! It seems that you are not connected to the internet. Please check your internet connection and try again.');
                    //this.props.navigation.goBack();

                }

            }).catch((error) => {
                Helper.isNetConnected = false;
            });
        });
    }

    static getbadgescount() {
        if (this.socket && this.socket.connected) {
            console.log("objectobjectobjectobjectobjectobject")
            Helper.getData('userdata').then((data) => {
                if (data) {
                    let tempdate = new Date();
                    let gettime = Moment.utc(tempdate).format("YYYY-MM-DD HH:mm:ss")
                    ChatController.getAppoinmentMessageCount('get_appointment_message_count', {
                        last_id: 0,
                        user_id: data.id,
                        role_id: 1,
                        utc_date_time:gettime
                    });
                }
            })

        }




    }

    static connectUser(userid, type) {

        this.socket = SocketIOClient(Config.chat_url, { forceNew: true, reconnection: true, query: { 'user_id': userid, 'device_token': 'd9e8577425ea6b603ac0bd5d6dc65e63aa0fe955f2563c1f57e8fe5d451f7b7f' } });
        this.reConnect();
        this.socket.on('connect', (data) => {

            this.isConnected = true;
            if (type == "getinbox") {
                Message.getInbox(userid);
            } else if (type == "gethistory") {
                Chat.getChatHistory('after');
            }else 
            {
                this.getbadgescount();
            }
            console.log("connected", type);
            console.log("connected");

            Helper.hideLoader();
        });
        this.socket.on('disconnect', (data) => {
            this.isConnected = false;
            console.log("disconnected");
            //this.reConnect();
            this.getbadgescount();
        });
        this.socket.on('get_message_history_response', (data) => {
            EventRegister.emit('get_message_history_response', data);
        });
        this.socket.on('multi_user', (data) => {
            EventRegister.emit('multi_user', data);
        });
        this.socket.on('receive_message', (data) => {
            EventRegister.emit('receive_message', data);
        });
        this.socket.on('get_inbox_list_response', (data) => {
            EventRegister.emit('get_inbox_list_response', data);
        });
        this.socket.on('get_appointment_message_count_response', (data) => {
            EventRegister.emit('get_appointment_message_count_response', data);
        });
        this.socket.on('check_user_status_response', (data) => {
            EventRegister.emit('check_user_status_response', data);
        });
        this.socket.on('read_message_update_response', (data) => {
            EventRegister.emit('read_message_update_response', data);
        });
        this.socket.on('service_notification_response', (data) => {
            EventRegister.emit('service_notification_response', data);
        });
        
    }

    static reConnect() {
        Helper.getData('userdata').then((userdata) => {
            if (userdata) {
                this.socket.connect();
            }
        })
    }

    static colseConnection() {
        Helper.getData('userdata').then((userdata) => {
            if (userdata) {
                console.log("socket close");
                this.socket.close();
            }
        })
    }


    static disconnectUser() {
        this.socket.disconnect();
    }

    static chatDataEvent(eventname, formdata) {

        console.log(formdata);
        this.socket.emit(eventname, formdata);
    }

    static getAppoinmentMessageCount(eventname, formdata) {
        console.log(eventname, formdata,"eventttttttttt");
        this.socket.emit(eventname, formdata);

    }

}





// import * as React from 'react';
// import SocketIOClient from 'socket.io-client';
// import { EventRegister } from 'react-native-event-listeners';
// import { NetInfo } from 'react-native'
// import Config from './Config';
// import Helper from './Helper';

// var isConnected=false;
// export default class ChatController extends React.Component {
//     isConnectedNew = false;
//     constructor() {
//     }

//     static checkConnection(type, cb) {
//         let newtype = type;
//         Helper.getData('userdata').then((userdata) => {
//             NetInfo.isConnected.fetch().then(isConnected => { 
//                 if (isConnected) {
//                     if (ChatController.isConnected) {
//                         cb(true);
//                         return;
//                     } else { 
//                         Helper.showLoader();
//                         ChatController.connectUser(userdata.id, newtype);
//                         cb(false);
//                     }

//                 } else {
//                     alert('Oops! It seems that you are not connected to the internet. Please check your internet connection and try again.');
//                     //this.props.navigation.goBack();

//                 }

//             }).catch((error) => {
//                 Helper.isNetConnected = false;
//             });
//         });
//     }

//     static connectUser(userid,type) {

//         if(isConnected == true)
//         {
//             return
//         }
//         this.socket = SocketIOClient(Config.chat_url, {
//             forceNew: true, reconnection: true,
//             query: {
//                 'user_id': userid,
//                 user_id_tbl: 'users',
//                 'device_token': 'd9e8577425ea6b603ac0bd5d6dc65e63aa0fe955f2563c1f57e8fe5d451f7b7f'
//             }
//         }
//         );

//         this.reConnect();

//     //     if (this.socket && !this.socket.connected) {
//     //         this.reConnect();
//     // }
//     this.socket.on('connect', (data) => {

//         this.isConnectedNew = true;
//         if (type == "getinbox") {
//             Message.getInbox(userid);
//         } else if (type == "gethistory") {
//             Chat.getChatHistory('after');
//         }
//         console.log("connected", type);


//         console.log("connected");
//         isConnected = true 
//         this.getbadgescount();

//     });
//         this.socket.on('disconnect', (data) => {

//             this.isConnectedNew = false;
//             console.log("disconnected");
//             isConnected = false 
//             this.reConnect();
//         });
//         this.socket.on('get_message_history_response', (data) => {
//             EventRegister.emit('get_message_history_response', data);
//         });
//         this.socket.on('multi_user', (data) => {
//             EventRegister.emit('multi_user', data);
//         });
//         this.socket.on('receive_message', (data) => {
//             EventRegister.emit('receive_message', data);
//         });
//         this.socket.on('get_inbox_list_response', (data) => {
//             EventRegister.emit('get_inbox_list_response', data);
//         });

//         this.socket.on('get_appointment_message_count_response', (data) => {
//             EventRegister.emit('get_appointment_message_count_response', data);
//         });
//         this.socket.on('check_user_status_response', (data) => {
//             EventRegister.emit('check_user_status_response', data);
//         });


//     }

//     static reConnect() {
//         Helper.getData('userdata').then((userdata) => {
//             if (userdata) {
//                 this.socket.connect();
//             }
//         })
//     }

// static readbadges(data){
//     if (this.socket && this.socket.connected) {
//         Helper.getData('userdata').then((data) => {
//             if (data) {
//                 ChatController.getAppoinmentMessageCount('get_appointment_message_count_read', {
//                     last_id: 0,
//                     user_id: data.id,
//                     roll_id:1
//                 });

//             }
//         })

//     }
// }



//     static getbadgescount() {
//         if (this.socket && this.socket.connected) {
//             Helper.getData('userdata').then((data) => {
//                 if (data) {
//                     ChatController.getAppoinmentMessageCount('get_appointment_message_count', {
//                         last_id: 0,
//                         user_id: data.id,
//                         roll_id:1
//                     });

//                 }
//             })

//         }




//     }

//     static disconnectUser() {
//         this.socket.disconnect();
//     }

//     static chatDataEvent(eventname, formdata) {
//         console.log(formdata);
//         this.socket.emit(eventname, formdata);
//     }


//     static getAppoinmentMessageCount(eventname, formdata) {
//         console.log(eventname, formdata, "DSf  Emaiitere");
//         this.socket.emit(eventname, formdata);

//     }

// }