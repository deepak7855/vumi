import React, { Component } from 'react';
import {
  SafeAreaView,
  StyleSheet,
  Text,
  TouchableOpacity,
  Platform,
} from 'react-native';
import RNIap, {
  Product,
  ProductPurchase,
  PurchaseError,
  acknowledgePurchaseAndroid,
  purchaseErrorListener,
  purchaseUpdatedListener,
} from 'react-native-iap';
const itemSkus = Platform.select({
  ios: ['com.vumi.fastpass'],
  android: ['com.vumi.fastpass'],
});
const itemSubs = Platform.select({
  ios: ['com.vumi.fastpass', 'com.vumi.onemonth.nonnewing', 'com.vumi.sixmonth.nonnewing', 'com.vumi.twelvemonth.nonnewing'],
  android: ['com.vumi.fastpass', 'com.vumi.onemonth.nonnewing', 'com.vumi.sixmonth.nonnewing', 'com.vumi.twelvemonth.nonnewing'],
});



let purchaseUpdateSubscription;
let purchaseErrorSubscription;

export default class InappPurchase extends Component {
  constructor(props) {
    super(props);
    this.state = {
      productList: [],
      receipt: '',
      availableItemsMessage: '',
    };
  }
  async componentDidMount(): void {



  }

  componentWillUnmount(): void {
    if (purchaseUpdateSubscription) {
      purchaseUpdateSubscription.remove();
      purchaseUpdateSubscription = null;
    }
    if (purchaseErrorSubscription) {
      purchaseErrorSubscription.remove();
      purchaseErrorSubscription = null;
    }

  }

  static async methodInit() {

    if (purchaseUpdateSubscription) {
      purchaseUpdateSubscription.remove();
      purchaseUpdateSubscription = null;
    }
    if (purchaseErrorSubscription) {
      purchaseErrorSubscription.remove();
      purchaseErrorSubscription = null;
    }

    try {
      const result = await RNIap.initConnection();
      await RNIap.consumeAllItemsAndroid();
    } catch (err) {
      console.log('error in cdm => ', err);
    }



    purchaseUpdateSubscription =  purchaseUpdatedListener(async (purchase: SubscriptionPurchase) =>  {
      console.log('purchaseUpdatedListener', purchase);
      const receipt = purchase.transactionReceipt;
      console.log('receipt', receipt);
      if (receipt) {
        if (Platform.OS === 'ios') {
          await RNIap.finishTransactionIOS(purchase.transactionId);
        } else if (Platform.OS === 'android') {
          // If consumable (can be purchased again)
          await RNIap.consumePurchaseAndroid(purchase.purchaseToken);
          // If not consumable
          await RNIap.acknowledgePurchaseAndroid(purchase.purchaseToken);
        }
      }


      // if (receipt) {
      //   RNIap.deliverOrDownloadFancyInAppPurchase(purchase.transactionReceipt)
      //   .then( async (deliveryResult) => {
      //     if (isSuccess(deliveryResult)) {
      //       // Tell the store that you have delivered what has been paid for.
      //       // Failure to do this will result in the purchase being refunded on Android and
      //       // the purchase event will reappear on every relaunch of the app until you succeed
      //       // in doing the below. It will also be impossible for the user to purchase consumables
      //       // again until you do this.
      //       if (Platform.OS === 'ios') {
      //         await RNIap.finishTransactionIOS(purchase.transactionId);
      //       } else if (Platform.OS === 'android') {
      //         // If consumable (can be purchased again)
      //         await RNIap.consumePurchaseAndroid(purchase.purchaseToken);
      //         // If not consumable
      //         await RNIap.acknowledgePurchaseAndroid(purchase.purchaseToken);
      //       }

      //       // From react-native-iap@4.1.0 you can simplify above `method`. Try to wrap the statement with `try` and `catch` to also grab the `error` message.
      //       // If consumable (can be purchased again)
      //       await RNIap.finishTransaction(purchase, true);
      //       // If not consumable
      //       await RNIap.finishTransaction(purchase, false);
      //     } else {
      //       // Retry / conclude the purchase is fraudulent, etc...
      //     }
      //   });
      // }
    });

    purchaseErrorSubscription = purchaseErrorListener((error: PurchaseError) => {
      console.warn('purchaseErrorListener', error);
    });


    // purchaseUpdateSubscription = purchaseUpdatedListener(
    //   async (purchase: ProductPurchase) => {
    //     if (
    //       purchase.purchaseStateAndroid === 1 &&
    //       !purchase.isAcknowledgedAndroid
    //     ) {
    //       try {
    //         const ackResult = await acknowledgePurchaseAndroid(
    //           purchase.purchaseToken,
    //         );
    //         console.log('ackResult----------------------------- ',ackResult)
    //       } catch (ackErr) {
    //         console.warn('ackErr', ackErr);
    //       }
    //     }
    //     this.purchaseConfirmed();
    //     console.log('***********************  ',purchase)
    //     // this.setState({ receipt: purchase.transactionReceipt });
    //     purchaseErrorSubscription = purchaseErrorListener(
    //       (error: PurchaseError) => {
    //         console.log('purchaseErrorListener', error);
    //         // alert('purchase error', JSON.stringify(error));
    //       },
    //     );
    //   },
    // );

  }

  static getItems = async (): void => {
    try {
      console.log('itemSkus[0]', itemSkus[0]);
      const products: Product[] = await RNIap.getProducts(itemSkus);
      this.requestPurchase(itemSkus[0]);
    } catch (err) {
      console.log('getItems || purchase error => ', err);
    }
  };
  static getSubscriptions = async (): void => {
    try {
      const products = await RNIap.getSubscriptions(itemSubs);
      console.log('Products => ', products);
      // this.setState({ productList: products });
    } catch (err) {
      //alert('alert2', JSON.stringify(err))
      console.log('getSubscriptions error => ', err);
    }
  };

  static getAvailablePurchases = async (): void => {
    try {
      const purchases = await RNIap.getAvailablePurchases();
      console.log('Available purchases => ', purchases);
      // if (purchases && purchases.length > 0) {
      //   this.setState({
      //     availableItemsMessage: `Got ${purchases.length} items.`,
      //     receipt: purchases[0].transactionReceipt,
      //   });
      // }
    } catch (err) {
      console.warn(err.code, err.message);
      console.log('getAvailablePurchases error => ', err);
    }
  };
  static requestPurchase = async (sku): void => {
    try {
      console.log('requestPurchaserequestPurchase', sku)
      RNIap.requestPurchase(sku);
    } catch (err) {
      console.log('requestPurchase error => ', err);
    }
  };
  static requestSubscription = async (sku) => {

    InappPurchase.methodInit()

    try {
      await this.getSubscriptions();
      await RNIap.requestSubscription(sku);
    } catch (err) {
      alert(err.toLocaleString());
    }
  };
  purchaseConfirmed = (conform) => {
    console.log('conformconformconformconform', conform)

    //you can code here for what changes you want to do in db on purchase successfull
  };
  render() {
    return (
      <SafeAreaView style={styles.rootContainer}>
        <TouchableOpacity
          onPress={() => {
            // this.getItems();
          }}
          style={styles.buttonStyle}>
          <Text style={styles.buttonText}>Test IAP</Text>
        </TouchableOpacity>
      </SafeAreaView>
    );
  }
}
const styles = StyleSheet.create({
  rootContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonStyle: {
    width: '70%',
    backgroundColor: '#000',
    borderRadius: 25,
    padding: 10,
    justifyContent: 'center',
    alignItems: 'center',
  },
  buttonText: {
    color: '#fff',
    fontSize: 17,
    fontWeight: 'bold',
  },
});

// getProduct.requestSubscription(item.plan_name)
//  getProduct.getSubscriptions()