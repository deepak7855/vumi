import { createStore, applyMiddleware } from 'redux';
import thunkMiddleware from 'redux-thunk';
import { createLogger } from 'redux-logger';
import AllReducer from '../_reducer';
const loggerMiddleware = createLogger();

export const Store = createStore(
     AllReducer,
    
    applyMiddleware(
       // applyMiddleware(thunk),
        thunkMiddleware,
        loggerMiddleware
    )
);