import {
    EAMIL_CHANGED, PASSWORD_CHANGED,
    LOGIN_USER_SUCCESS,
    LOGIN_USER_FAIL,
    LOGIN_USER,

    FATCHING_DATA,
    SUCCESS_DATA,
    REGISTER_FAIL,
    FATCHING_VEHICLE_DATA,SUCCESS_VEHICLE_DATA,VEHICLE_DATA_FAIL,
    FATCHING_VMAKE_DATA,SUCCESS_VMAKE_DATA,
    FATCHING_VTYPE_DATA,SUCCESS_VTYPE_DATA,
    FATCHING_VCOLOR_DATA,SUCCESS_VCOLOR_DATA,FAIL_VCOLOR_DATA

} from './type';
import Urls from '../../redux/api/Urls'
import Helper from '../../src/Lib/Helper'

// export const emailChange = (text) => {

//     return {
//         type: EAMIL_CHANGED,
//         payload: text
//     }
// }

// export const passwordChanged = (text) => {
//     return {
//         type: PASSWORD_CHANGED,
//         payload: text
//     }
// };

// export const loginUser = ({ email, password }) => {
//     // console.log('loginAction', email, password)
//     return (dispatch) => {
//         dispatch({ type: LOGIN_USER });
//         return Agent.User.Login({ email, password })
//             .then(res => {
//                 return dispatch({ 
//                     type: LOGIN_USER_SUCCESS, payload: res.data,

//                  })
//             }
//             )
//             .catch(error => dispatch({ type: LOGIN_USER_FAIL, payload: error }))
//     };
// }

// export const loginUser = ( emailpassword, props) => {
//     return (dispatch) => {
//         dispatch({ type: LOGIN_USER });
//         return Agent.User.Login(emailpassword)
//             .then(res => {
//                 if (res.data === "Email is Blank" || res.data.status) {
//                     console.log('response', res.data)
//                     loginUserSuccess(dispatch, res.data)
//                 }
//                 else {
//                     if (res.data && res.data.token) {
//                         console.log('response.data.token', res.data.token);
//                         loginUserSuccess(dispatch, res.data);
//                         props.navigation.navigate("verify");
//                     }
//                 }
//             }
//             )
//             .catch(() => loginUserFail(dispatch))
//     };
// }

// const loginUserSuccess = (dispatch, res) => {
//     dispatch({
//         type: LOGIN_USER_SUCCESS,
//         payload: res
//     });
// }

// const loginUserFail = (dispatch) => {
//     dispatch({ type: LOGIN_USER_FAIL });
// }


// export const UserLogin=(data)=>{
//     console.log(data)
//     return (dispatch) =>{
//         alert(data)
//         //  dispatch ({type: REGISTER_USER })
//         //  return Urls.User.loginUser(data)
//         //  .then((res)=>{
//         //      return(dispatch)({type:REGISTER_SUCCESS ,payload:res.data})
//         //  })
//     }
// }



// export const UserRegister =(allvalue,props)=>{
//     //  console.log(" alldatat " ,allvalue)
//     return (dispatch) =>{
//          dispatch ({type: REGISTER_USER })
//          return Urls.User.SignUp(allvalue)
//                .then((res)=>{
//               return(dispatch)({type:REGISTER_SUCCESS ,payload:res.data})
//           })
//     }
// }


export const getLocationList =(allvalue,props)=>{
    //  console.log(" alldatat " ,allvalue)
    return (dispatch) =>{
         dispatch ({type: FATCHING_DATA })
         return Helper.makeRequest({ url: "get-user-locations", method: "POST"})
               .then((res)=>{ 
                // console.log("Get locaiton list "+JSON.stringify(res));
                let arr=[]
              return(dispatch)({type:SUCCESS_DATA ,payload: (res.data && res.data !== undefined)? res.data : arr})
          })




    }
}



  export   const  getVehicelData  =(allvalue,props)=>{
    //  console.log(" alldatat " ,allvalue)
    return (dispatch) =>{
         dispatch ({type: FATCHING_VEHICLE_DATA })
         return  Helper.makeRequest({ url: "get-user-vehicles", method: "POST"})
               .then((res)=>{ 
                let arr=[]
              return(dispatch)({type:SUCCESS_VEHICLE_DATA ,payload: (res.data && res.data !== undefined)? res.data : arr})
          })




    }
}



export const getVehicleMake =(allvalue,props)=>{
    return (dispatch) =>{
         dispatch ({type: FATCHING_VMAKE_DATA })
         return Helper.makeRequest({ url: "get-brands", method: "GET"})
               .then((data)=>{
                var Vehicle = []
                if (data.status == 'true') { 
                    for (let i of data.data) {
                      Vehicle.push({
                        id:i.id,
                        key: i.name.toLowerCase(),
                        label: i.name.toUpperCase(),
                        brand_id: i.id,
                      })
                    }
                  }
              return(dispatch)({type:SUCCESS_VMAKE_DATA ,payload:Vehicle})
          })




    }
}


export const getVehicleType =(allvalue,props)=>{
    return (dispatch) =>{
         dispatch ({type: FATCHING_VTYPE_DATA })
         return Helper.makeRequest({ url: "get-type", method: "GET" })
               .then((data)=>{
                var VehicleType = []
                if (data.status == 'true') { 
                    for (let i of data.data) {
                      VehicleType.push({
                        id:i.id,
                        key: i.type,
                        label: i.type,
                        name: i.type,
                      })
                    }
                  }
              return(dispatch)({type:SUCCESS_VTYPE_DATA ,payload:VehicleType})
          })




    }
}


export const getVehicleColor =(allvalue,props)=>{
    return (dispatch) =>{
         dispatch ({type: FATCHING_VCOLOR_DATA })
         return Helper.makeRequest({ url: "get-colors", method: "GET"})
               .then((data)=>{
                var VehicleColor = []
                if (data.status == 'true') { 
                      for (let i of data.data) {
                        VehicleColor.push({
                          id:i.id,
                          key: i.color_name,
                          label: i.color_name,
                          name: i.color_name,
                        })
                    }
                  }
              return(dispatch)({type:SUCCESS_VCOLOR_DATA ,payload:VehicleColor})
          })
    }
}



//  export const loginUser = ({ email, password }) => {

//     return (dispatch) => {

//         dispatch({type: LOGIN_USER});
//         firebase.auth().signInWithEmailAndPassword(email, password)
//             .then(user => loginUserSuccess(dispatch, user))
//             .catch(() => {
//                 firebase.auth().createUserWithEmailAndPassword(email, password)
//                     .then(user => loginUserSuccess(dispatch, user))
//                     .catch (() => loginUserFail(dispatch))

//             })


//     };
// }
