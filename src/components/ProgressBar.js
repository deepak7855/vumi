import React, { Component } from 'react';
import { View, Text, Animated } from 'react-native';
import fonts from '../common/fonts';

export const ProgressBar = (props) => {
    // alert(JSON.stringify(props))

    return (
        <View>
            {props.watchdemo == true ?
                <View style={{}} >
                    <View style={{ width: "100%", backgroundColor: props.colorUncompleted, height: 20 }} >
                        <View style={{ width: (props.points / props.total_views * 100) + "%", backgroundColor: props.colorCompleted, height: 20 }} >

                        </View>
                    </View>
                </View>
                :
                <View style={{ flex: 1, }} >
                    <View style={{ width: "100%", backgroundColor: props.colorUncompleted, height: 40, borderRadius: 10 }} >
                        <View style={{ width: (props.points / props.total_views * 100) + "%", backgroundColor: props.colorCompleted, height: 40, borderRadius: 10, }} >

                        </View>
                    </View>
                    <View style={{ flexDirection: "row", alignItems: "center", justifyContent: "space-between", paddingHorizontal: 12, zIndex: 1, position: "absolute", flex: 1, width: '100%', paddingVertical: 1, marginTop: 10 }}>
                        {/* <Text style={{ color: "#f2fff3", fontSize: 18, fontFamily: fonts.Poppins_Bold }}>{props.points}</Text> */}
                        <Animated.View
                            style={[
                                {
                                    opacity: props.animationview
                                }
                            ]}
                        >
                            <Text style={{ color: "#f2fff3", fontSize: 11, fontFamily: fonts.Poppins_Bold, }}>Finding viewers...</Text>
                        </Animated.View>

                        <Text style={{ color: "#afb4c4", fontSize: 14, fontFamily: fonts.SFProDisplay_Heavy, }}>{parseFloat(props.points / props.total_views * 100).toFixed(2)}%</Text>
                    </View>
                </View>
            }
        </View>




    )
}