import React from 'react';
import { View, ActivityIndicator, Image, StyleSheet } from 'react-native';
import FastImage from 'react-native-fast-image'

export default class ImageLoadView extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            loading: true,
            error: false,
            defaultImage: this.props.source
        }
    }
    render() {
        return (
            <View style={styles.container}>
                <View style={this.props.style}>
                    <FastImage
                        style={this.props.style}
                        resizeMode={this.props.resizeMode}
                        source={this.props.source}
                        onLoadStart={(e) => this.setState({ loading: true })}
                        onLoadEnd={(e) => this.setState({ loading: false })}
                        resizeMode={this.props.resizeMode}
                    />
                </View >
                {this.state.loading &&
                    <ActivityIndicator style={[this.props.style, styles.activityIndicator]} animating={true} size="small" color={this.props.indicatorColor ? this.props.indicatorColor : '#000'} />
                }
            </View>
        );
    }
}
const styles = StyleSheet.create({
    container: {
        // flex: 1,
    },
    activityIndicator: {
        position: 'absolute', zIndex: 1, backgroundColor: "transparent", left: 0, right: 0, bottom: 0, top: 0
    }
})
