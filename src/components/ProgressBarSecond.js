import React, { Component } from 'react';
import { View, Text } from 'react-native';
import fonts from '../common/fonts';

export const ProgressBarSecond = (props) => {
    return (
        <View style={{ flex: 1, }} >
            <View style={{ width: "100%", backgroundColor: props.colorUncompleted, height: 25, borderRadius: 20 }} >
                <View style={{ width: (props.completed / props.total * 100) + "%", backgroundColor: props.colorCompleted, height: 25, borderRadius: 20, }} >

                </View>
            </View>
            <View style={{ flexDirection: "row", alignItems: "center", justifyContent: "flex-end", paddingHorizontal: 12, zIndex: 1, position: "absolute", flex: 1, width: '100%', paddingVertical: 5 }}>
                <Text style={{ color: "#afb4c4", fontSize: 12, fontFamily: fonts.SFProMedium, }}>{props.actualValue}/{props.total}</Text>
            </View>
        </View>
    )
}