import React, { Component } from 'react'
import { Text, View, StyleSheet, Image, ScrollView, Dimensions, FlatList, TouchableOpacity, Modal, StatusBar, BackHandler, Alert, SafeAreaView, TouchableWithoutFeedback, RefreshControl, Animated } from 'react-native'
import { UserDetailComponent } from '../../components/Watch/UserDetailComponent';
import { VideoDetailCompo } from '../../components/Watch/VideoDetailCompo';
import Video from 'react-native-video';
import { Actions } from 'react-native-router-flux';
import Helper from '../../lib/Helper';
import ProgressBar from "react-native-progress/Bar";
import Toast from 'react-native-root-toast';
import Spinner from 'react-native-loading-spinner-overlay';
import Config from '../../lib/Config';
import ImageViewSwiper from '../../components/Watch/ImageViewSwiper';
import pushToScreen from '../../navigation/NavigationActions';
import FastImage from 'react-native-fast-image';
import Emojiimages from '../../assets/Themes/Images';
import { BlurView } from "@react-native-community/blur";
import fonts from '../../common/fonts';
import { GHeader, Images, Colors } from '../../common';
import ImageLoadView from '../ImageLoadView';

const screenWidth = Dimensions.get('window').width;
const screenHeight = Dimensions.get('window').height;


export default class PreviewPost extends Component {

    constructor(props) {
        super(props);
        this.state = {
            modalVisible: false,
            coins: '',
            watchList: [],
            showSpinner: false,
            overlayShow: false,
            arrReportList: [],
            reportViewOverleyShowIndex: -1,
            current_page: 1,
            emojiViewShowIndex: -1,
            duration: 0,
            currentTime: 0,
            activeIndex: 0,
            videoPaused: false,
            progress: 0,
            lastTap: null,
        };
        this.timeout = null;
        this.didBlurListener = this.props.navigation.addListener(
            'didBlur',
            (obj) => { this.changeState() }
        );
    }

    changeState = () => {
        this.setState({ videoPaused: true, watchList: [], })
    }
    getSettingApi = () => {
        this.setState({ showSpinner: true })
        Helper.makeRequest({ url: 'settings', method: "POST", data: '' }).then((resp) => {
            if (resp.status == 'true') {
                Helper.userSettings = resp.data
                this.setState({})

            } else {
                Helper.userSettings = {}
                this.setState({})

            }
            this.setState({ showSpinner: false })
        })
    }

    getWatchList = () => {
        this.setState({ showSpinner: true })
        Helper.makeRequest({ url: `get-home-feed?page=${this.state.current_page}`, method: "POST", data: '' }).then((resp) => {
            if (resp.status == 'true') {
                let watchList = this.state.watchList.concat(resp.data.data)
                this.setState({ watchList, last_page: resp.data.last_page })
            } else {
                Toast.show(resp.message)
            }
            this.setState({ showSpinner: false })
        })
    }

    onEndReached = () => {
        if (this.state.current_page < this.state.last_page) {
            this.setState({
                current_page: this.state.current_page + 1,
            }, () => {
                this.getWatchList();
            })
        }
    }

    onRefresh = () => {
        this.setState({ selectedIndex: 0, watchList: [], }, () => {
            this.getSettingApi()
            this.getWatchList();
        })
    }


    onPress = (val) => {
        this.setState({ modalVisible: val })
    }

    handleProgressPress = e => {
        const position = e.nativeEvent.locationX;
        const progress = (position / (screenWidth - 8)) * this.state.duration;
        this.player.seek(progress);
    };

    handleProgress = progress => {
        this.setState({
            progress: parseFloat(progress.currentTime / progress.playableDuration)
        });
    };

    handleLoad = (meta) => {
        this.setState({
            progress: 0,
            duration: meta.duration, currentTime: meta.currentTime
        })
    }

    _onViewableItemsChanged = props => {
        const changed = props.changed;
        changed.forEach(item => {
            if (item.isViewable) {
                this.setState({ selectedIndex: item.index, progress: 0 }, () => {
                    clearTimeout(this.timeout);
                    this.timeout = setTimeout(() => {
                        this.updatePostViewStatus()
                    }, 3000);

                })
            }
        });
    };

    updatePostViewStatus = () => {
        // if (this.state.watchList.length > 0) {
        //     if (this.state.watchList[this.state.selectedIndex].is_view == 0) {

        //         let data = {
        //             post_id: this.state.watchList[this.state.selectedIndex].id
        //         }
        //         Helper.makeRequest({ url: "post-view", method: "POST", data: data }).then((resp) => {
        //             if (resp.status == 'true') {
        //                 let list = this.state.watchList
        //                 let listRow = resp.data
        //                 // listRow.is_view = 1;
        //                 // listRow.total_views = listRow.total_views + 1
        //                 list[this.state.selectedIndex] = listRow;
        //                 this.setState({ watchList: list })
        //             } else {
        //                 Toast.show(resp.message)
        //             }
        //         })
        //     }
        // }
    }

    onLikePress = (id, is_like, index) => {
        let data = {
            post_id: id,
            is_like: is_like
        }
        Helper.makeRequest({ url: "post-like", method: "POST", data: data }).then((resp) => {
            if (resp.status == 'true') {
                let list = this.state.watchList
                let listRow = resp.data
                // listRow.is_like = is_like;
                // listRow.total_likes = is_like == 1 ? (listRow.total_likes + 1) : (listRow.total_likes - 1);
                list[index] = listRow;
                this.setState({ watchList: list })
            } else {
                Toast.show(resp.message)
            }
        })
    }

    openMenu = () => {
        this.menu.open();
    };

    methodReportSubmit(item, index) {
        let dicPost = this.state.watchList[this.state.reportViewOverleyShowIndex]
        this.setState({ showSpinner: true })
        Helper.makeRequest({ url: 'flag-post', method: "POST", data: { post_id: dicPost.id, flag_reason_id: item.id } }).then((resp) => {
            if (resp.status == 'true') {
                let dic = dicPost
                dic.is_flag_by_me = 1
                this.state.watchList.splice(index, 1, dic)
                Helper.userSettings.today_flag = Number(Helper.userSettings.today_flag) + 1
            }
            Helper.showToast(resp.message)
            this.setState({ reportViewOverleyShowIndex: -1, showSpinner: false })
        })
    }
    renderReportView = ({ item, index }) => {
        return (
            <TouchableOpacity onPress={() => this.methodReportSubmit(item, index)} style={{ padding: 15, flexDirection: 'row', alignItems: 'center', justifyContent: 'space-between', backgroundColor: 'white', borderBottomColor: 'black', borderBottomWidth: 1.0 }}>
                <Text style={{ fontSize: 15, fontFamily: fonts.SFProDisplay_Semibold, width: '90%' }}>{item.title}</Text>
                <Image resizeMode={'contain'} style={{ width: 15, height: 15, tintColor: Colors.black }} source={Images.right_arrow}></Image>
            </TouchableOpacity>
        )
    }

    renderReportModal() {
        return (
            <Modal
                animationType={"fade"}
                transparent={true}
                visible={true}
                onRequestClose={() => { }}>
                <View style={{ flex: 1, justifyContent: 'flex-end' }}>
                    <View style={{ width: '100%', alignItems: 'center', backgroundColor: Colors.white, paddingVertical: 15, borderBottomColor: Colors.coolGrey, borderBottomWidth: 1.0 }}>
                        <Text style={{ fontFamily: fonts.SFProDisplay_Semibold, fontSize: 20, color: Colors.black, textAlign: 'center' }}>Report</Text>
                    </View>
                    <View style={{ width: '100%', height: '50%', justifyContent: 'flex-end' }}>
                        <FlatList style={{ flex: 1, backgroundColor: Colors.white }}
                            data={this.state.arrReportList}
                            renderItem={this.renderReportView}
                            keyExtractor={(item, index) => index}
                        >
                        </FlatList>
                    </View>
                </View>
                <Spinner visible={this.state.showSpinner} />

            </Modal>


        );
    }
    renderImageSwiper() {
        return (
            <View style={{ flex: 1 }}>
                <FlatList
                    horizontal
                    data={this.props.media}
                    pagingEnabled
                    keyExtractor={(item, index) => index}
                    showsHorizontalScrollIndicator={false}
                    scrollEventThrottle={16}
                    onMomentumScrollEnd={this.handlePageChange}
                    renderItem={({ item }) => {
                        return (
                            <View style={styles.squareView}>
                                <TouchableWithoutFeedback onPress={() => this.onImageVideoPress()}>
                                    {/* <Image
                                        style={styles.squareView}
                                        source={{ uri: item }}
                                    /> */}
                                    <ImageLoadView
                                        style={styles.squareView}
                                        source={{ uri: item }}
                                    />
                                </TouchableWithoutFeedback>
                            </View>
                        )
                    }}
                />
                <View style={{ display: this.props.media.length > 1 ? 'flex' : 'none', flexDirection: "row", marginTop: 5, justifyContent: "center" }}>
                    {
                        this.props.media.map((item, index) => (
                            <View style={{ height: 10, width: 10, borderRadius: 5, marginLeft: 7, backgroundColor: this.state.activeIndex == index ? '#38bfff' : "white" }} />
                        ))
                    }
                </View>
            </View>
        );
    }
    handlePageChange = (e) => {
        var offset = e.nativeEvent.contentOffset;
        if (offset) {
            var page = Math.ceil(offset.x / Dimensions.get('window').width);
            this.setState({ activeIndex: page });
        }
    }

    onImageVideoPress = (params) => {

        const now = Date.now();
        const DOUBLE_PRESS_DELAY = 300;
        if (this.state.lastTap && (now - this.state.lastTap) < DOUBLE_PRESS_DELAY) {
            this.setState({ overlayShow: 1 })
        } else {
            if (this.state.overlayShow) {
                this.setState({ overlayShow: 0 })
            }
            this.setState({ videoPaused: !this.state.videoPaused })
            this.state.lastTap = now;
        }
    }


    renderPreviewView() {
        return (
            <View style={{ height: screenHeight - screenHeight / 4.6 }}>
                <View style={styles.subContainer}>
                    {/* <UserDetailComponent item={Helper.userInfo} /> */}
                    <TouchableOpacity disabled onPress={() => this.setState({ overlayShow: 0 })} style={styles.squareView}>

                        {
                            this.props.mediaType == 'Picture' ?
                                <View style={styles.squareView}>
                                    {this.renderImageSwiper()}
                                </View> :
                                <View style={{ flex: 1, alignItems: "center" }}>

                                    <View style={styles.squareView}>
                                        <TouchableWithoutFeedback onPress={() => this.onImageVideoPress()}>
                                            <Video
                                                source={{ uri: this.props.media[0] }}
                                                ref={(ref) => {
                                                    this.player = ref
                                                }}
                                                onProgress={this.handleProgress}
                                                onLoad={this.handleLoad}
                                                repeat
                                                resizeMode="cover"
                                                style={styles.video}
                                                paused={this.state.videoPaused}
                                            />
                                        </TouchableWithoutFeedback>
                                    </View>
                                    {this.state.progress ? <ProgressBar
                                        progress={this.state.progress}
                                        color="#5856d6"
                                        unfilledColor="#282828"
                                        borderColor="transparent"
                                        width={screenWidth - 8}
                                        borderRadius={0}
                                        height={7}
                                    /> : <View style={{ width: screenWidth - 8, height: 7, backgroundColor: "#282828" }} />}
                                </View>}

                        {this.state.overlayShow == 1 && this.methodRenderOverlayView()}

                    </TouchableOpacity>
                    {this.state.emojiViewShowIndex == 0 && this.renderEmojiView()}
                    {this.renderLikeViewBack()}
                    {/* <PostLikeView /> */}
                    {/* <VideoDetailCompo
                        isEmojiShow={this.state.emojiViewShowIndex == index ? false : true}
                        likeImage={arrEmaoji[0].image}
                        EmojiViewShow={(index) => this.methodEmojiViewShow(index)}
                        onLikePress={(post_id, is_like, index) => this.onLikePress(post_id, is_like, index)}
                        item={item}
                        index={index}
                        onChatPress={(post_id) => pushToScreen('Comments', { post_id })}
                    /> */}
                </View>


            </View>
        )
    }
    renderUserPreview() {

        return (
            <View style={styles.userContainer}>
                <View style={styles.subContainerLeft}>
                    <Image
                        resizeMode="cover"
                        source={Helper.userInfo.profile_image ? { uri: Config.imageUrl + Helper.userInfo.profile_image } : Images.User_profile_icon}
                        style={styles.user}
                    />
                    <View style={{ paddingHorizontal: 5 }}>
                        <Text style={styles.userName}>{this.props.dicPostDetails && this.props.dicPostDetails.display_name ? this.props.dicPostDetails.display_name : Helper.userInfo.username}</Text>
                        <View style={styles.subContainerLeft}>
                            <Image
                                source={Images.inactive_overview}
                                style={styles.earth}
                            />
                            <Text style={styles.country}>{Helper.userInfo.location ? Helper.userInfo.location : "Earth"}</Text>
                        </View>
                    </View>
                </View>
                <View style={styles.subContainerRight}>
                    <Text style={styles.userName}>{"0"}</Text>
                    <Text style={styles.views}>Views</Text>
                </View>
            </View>
        )
    }
    renderLikeViewBack() {
        return (
            <View style={styles.userContainer}>
                <View style={styles.heartView}>
                    <Text style={styles.name}>{this.props.dicPostDetails.title}</Text>
                </View>
                <View style={styles.heartView}>
                    <View style={styles.heartView}>
                        <Text style={styles.like}>{0}</Text>
                        <TouchableOpacity>
                            <Image
                                // source={this.props.dicPostDetails.is_like == 1 ? Images.heart : Images.heart_grey}
                                source={Images.heart_grey}
                                style={styles.heart}
                            />
                        </TouchableOpacity>
                    </View>
                    {/* <Text style={[styles.like, { marginLeft: 15 }]}>{"" || this.props.dicPostDetails.total_likes}</Text> */}
                    <Text style={[styles.like, { marginLeft: 15 }]}>0</Text>

                    <TouchableOpacity>
                        <Image
                            source={Images.chats}
                            style={styles.heart}
                        />
                    </TouchableOpacity>
                </View>
            </View>
        )
    }
    render() {
        console.log(Helper.userInfo, "");
        console.log(this.props.dicPostDetails, "this.props.dicPostDetails");
        return (
            <SafeAreaView style={styles.container}>
                <GHeader
                    bArrow
                    greyBackButton
                    color="white"
                    backgroundColor={Colors.headerPurple}
                    title1={"Preview"}
                    //showPhotoBtn
                    title1FontSize={20}
                    photoBtnText=""
                    photoBtnDisabled={true}
                // onJoinHeaderPress={this.onJoinHeaderPress}
                // onPhotoButtonPress={() => this.onNextPress()}
                />
                {this.renderUserPreview()}
                {this.renderPreviewView()}
                <Spinner visible={this.state.showSpinner} />
            </SafeAreaView>
        )
    }
    methodRenderOverlayView() {

        return (
            <TouchableOpacity onPress={() => this.setState({ overlayShow: -1 })} style={{
                flex: 1, position: 'absolute', zIndex: 1, height: '100%'
                , width: '100%'
            }}>
                <BlurView
                    style={styles.absolute}
                    blurType="dark"
                    blurAmount={10}
                    reducedTransparencyFallbackColor="black"
                />
                <View style={{
                    padding: 10, alignSelf: 'center', alignItems: 'center', justifyContent: 'center', flex: 1, position: 'absolute', zIndex: 1, height: '100%'
                    , width: '100%'
                }}>
                    <Text style={{ fontFamily: fonts.SFProMedium, fontSize: 17, color: 'white' }}>{this.props.dicPostDetails.description}</Text>
                    <Text style={{ fontFamily: fonts.SFProMedium, fontSize: 17, color: 'white', marginTop: 10 }}>{this.props.dicPostDetails.social_link}</Text>
                    {/* <View style={{ width: 200, height: 40, borderRadius: 20, marginTop: 10, backgroundColor: Colors.orange, alignItems: 'center', justifyContent: 'center' }}>
                        <Text style={{ fontFamily: fonts.SFProDisplay_Bold, fontSize: 17, color: 'white' }}>{"Soundcloud"}</Text>
                    </View> */}

                </View>
                <View style={{ flexDirection: 'row', zIndex: 1, position: 'absolute', bottom: 15, left: 15 }}>
                    <TouchableOpacity style={{ marginRight: 15 }}>
                        <Image style={{ width: 40, height: 40 }} source={Images.flag} />
                    </TouchableOpacity>
                    <TouchableOpacity>
                        <Image style={{ width: 40, height: 40 }} source={Images.starGrey}></Image>
                    </TouchableOpacity>
                </View>
            </TouchableOpacity>
        )
    }
    startSeekVideo = () => {

    }
    methodFavourite(item, index) {
        this.setState({ showSpinner: true })
        Helper.makeRequest({ url: 'post-save', method: "POST", data: { post_id: item.id } }).then((resp) => {
            if (resp.status == 'true') {
                let dic = item
                dic.is_save = item.is_save == 0 ? 1 : 0
                this.state.watchList.splice(index, 1, dic)
            }
            Helper.showToast(resp.message)
            this.setState({ showSpinner: false })
        })
    }
    getReportList() {
        this.setState({ showSpinner: true })
        Helper.makeRequest({ url: 'get-flag-reason', method: "POST", data: '' }).then((resp) => {
            if (resp.status == 'true') {
                this.state.arrReportList = resp.data
            }
            else {
                this.state.arrReportList = []
            }
            this.setState({ showSpinner: false })
        })
    }
    endSeekVideo = (time) => {

    }
    renderEmojiView() {

        return (
            <Animated.View style={{ flex: 1, position: 'absolute', flexDirection: 'row', zIndex: 1, backgroundColor: '#fff', borderRadius: 23, height: 46, justifyContent: 'center', alignItems: 'center', elevation: 2, shadowColor: 'rgba(0,0,0,0.5)', shadowOpacity: 1.0, paddingHorizontal: 5, bottom: 80, right: 5 }}>
                {arrEmaoji.map((item, index) => {
                    return (
                        this.renderEmoji(item, index)
                    )
                })}
            </Animated.View>
        )
    }
    renderEmoji(item, index) {
        return (
            <TouchableOpacity onPress={() => this.methodEmojiClick(item.id)}>
                <FastImage
                    style={{ width: 36, height: 36, marginLeft: index == 0 ? 0 : 5, }}
                    source={item.gif}
                // source={{
                //     uri: index == 0 ? item.url : item.url,
                // }}

                />
            </TouchableOpacity>
        )
    }
    methodEmojiViewShow(index) {
        this.setState({ emojiViewShowIndex: index })
    }
    methodEmojiClick(id) {
        let dic = this.state.watchList[this.state.emojiViewShowIndex]
        dic.is_like = id
        this.state.watchList.splice(this.state.emojiViewShowIndex, 1, dic)
        let post_id = dic.id
        let is_like = id
        let index = this.state.emojiViewShowIndex
        this.setState({ emojiViewShowIndex: -1 })
    }



    updateStatusBar = () => {
        StatusBar.setBackgroundColor('black');
        StatusBar.setHidden(false);
        StatusBar.setBarStyle('light-content');
    }

    async componentDidMount() {

        let userInfo = await Helper.getDataFromAsync('userInfo');
        if (userInfo) {
            Helper.userInfo = userInfo;
            this.setState({ coins: userInfo.coins })
        }

        this.functionevevnt = this.props.navigation.addListener('didFocus', () => {
            this.updateStatusBar();
            this.setState({ videoPaused: false })
        });


        this.updateStatusBar();
    }

    componentWillUnmount() {

        this.functionevevnt.remove();
    }

}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: Colors.lightBlack,
    },
    absolute: {
        position: "absolute",
        top: 0,
        left: 0,
        bottom: 0,
        right: 0
    },
    subContainer: {
        paddingVertical: 20
    },

    video: {
        width: screenWidth,
        aspectRatio: 1,
        flex: 1
    },
    squareView: {
        height: screenWidth,
        width: screenWidth,
        justifyContent: "center"
    },
    userContainer: {
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "space-between",
        paddingHorizontal: 10,
        paddingVertical: 8,
        height: 60,
    },
    subContainerLeft: {
        flexDirection: "row",
        alignItems: "center", marginTop: 5
    },
    userName: {
        color: Colors.cloudyBlue,
        fontSize: 15,
        fontFamily: fonts.SFProRegular
    },
    earth: {
        height: 12,
        width: 12,
        borderRadius: 6,
        marginRight: 5
    },
    user: {
        height: 35,
        width: 35,
        borderRadius: 17.5,
        marginRight: 5
    },
    country: {
        color: Colors.purple,
        fontSize: 12,
        fontFamily: fonts.SFProRegular
    },
    subContainerRight: {
        alignItems: "flex-end"
    },
    views: {
        color: Colors.purplishGrey,
        fontSize: 12,
        fontFamily: fonts.SFProRegular
    },
    name: {
        color: '#c0bed1',
        fontFamily: fonts.SFProMedium,
        fontSize: 15, width: '75%',
    },
    like: {
        color: '#c0bed1',
        fontFamily: fonts.SFProRegular,
        fontSize: 15
    },
    heart: {
        height: 20,
        width: 20,
        marginLeft: 5,
        resizeMode: "contain"
    },
    heartView: {
        flexDirection: "row",
        alignItems: "center"
    },

});

const arrEmaoji = [{ id: 1, url: 'https://raw.githubusercontent.com/duytq94/facebook-reaction-animation2/master/Images/like.gif', image: Emojiimages.like_static_fill, gif: Emojiimages.like_gif }, { id: 2, url: 'https://raw.githubusercontent.com/duytq94/facebook-reaction-animation2/master/Images/love.gif', image: Emojiimages.love_gif, gif: Emojiimages.love_gif }, { id: 3, url: 'https://raw.githubusercontent.com/duytq94/facebook-reaction-animation2/master/Images/haha.gif', image: Emojiimages.haha_static, gif: Emojiimages.haha_static }, { id: 4, url: 'https://raw.githubusercontent.com/duytq94/facebook-reaction-animation2/master/Images/wow.gif', image: Emojiimages.wow_static, gif: Emojiimages.wow_static }, { id: 5, url: 'https://raw.githubusercontent.com/duytq94/facebook-reaction-animation2/master/Images/sad.gif', image: Emojiimages.sad_static, gif: Emojiimages.sad_gif }, { id: 6, url: 'https://raw.githubusercontent.com/duytq94/facebook-reaction-animation2/master/Images/angry.gif', image: Emojiimages.angry_static, gif: Emojiimages.angry_gif }]
