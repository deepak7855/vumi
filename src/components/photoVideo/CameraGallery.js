import React, { Component } from 'react';
import {
    Dimensions,
    StyleSheet,
    Text,
    ScrollView,
    StatusBar,
    Platform,
    TouchableOpacity,
    View,
    Image,
    AppState,FlatList
} from 'react-native';
//  import CameraRollPicker from 'react-native-camera-roll-picker';
import CameraRollPicker from '../photoVideo/CameraRollPicker/react-native-camera-roll-picker/index';
 import RNConvertPhAsset from 'react-native-convert-ph-asset';

import { GHeader, Images } from '../../common';
import { PERMISSIONS, check, request, openSettings } from 'react-native-permissions'
import Config from '../../lib/Config';
import Toast from 'react-native-root-toast';
import Helper from '../../lib/Helper';
import { Actions } from 'react-native-router-flux';
import pushToScreen from '../../navigation/NavigationActions';
import Spinner from 'react-native-loading-spinner-overlay';
import GalleryMediaPicker from '@around25/react-native-gallery-media-picker'

const screenWidth = Dimensions.get('window').width

export default class CameraGallery extends Component {
    constructor(props) {
        super(props);

        this.state = {
            selected: [],
            activepage: 0,
            selectedImages: [],
            havePermissions: null,
            selectedVideos: [],
            showSettingButton: false,
            flag: false,showSpinner:false
        };

        this.getSelectedImages = this.getSelectedImages.bind(this);
    }
    handlePageChange = (e) => {
        var offset = e.nativeEvent.contentOffset;
        if (offset) {
            var page = Math.ceil(offset.x / Dimensions.get('window').width);
            this.setState({ activepage: page });
        }
    }
    getSelectedFiles(files, current) {
        // this.setState({ totalFiles: files.length, selected: files });
        // console.log('Current: ',current);
        console.log("current---------------------",current);

        console.log("files---------------------",files);
      }
     
    getSelectedImages(images, current) {
        console.log("getSelectedImages----------->>>>>>>>>>>>>>>>>>------",images)
        if (this.props.galleryType == 'Photos') {
            var selectedImages = images.map((item) => {
                return item.uri
            })



            this.setState({ selected: images, selectedImages });
        } else {

            // this.setState({ showSpinner: true })

            var selectedVideos = images.map((item) => {
                return item.uri
            })
            // console.log("-------------",selectedVideos[0])

            this.setState({ selected: images, selectedVideos,showSpinner: false });

           

        }

    }

    onNextPress = () => {
        if(Platform.OS == 'ios' && this.props.galleryType != 'Photos')
        {
            this.setState({ showSpinner: true })
            RNConvertPhAsset.convertVideoFromUrl({
                url: this.state.selectedVideos[0],
                convertTo: 'mov',
                quality: 'medium'
            }).then((response) => {
                console.log("RNConvertPhAsset---------",response);
               // this.setState({ selected: images, selectedVideos : [response.path],showSpinner: false });
               this.setState({ showSpinner: false })

                pushToScreen('CreatePost', { media: [response.path], mediaType: this.props.cameraType,isComeFrom:this.props.isComeFrom });

            }).catch((err) => {
               console.log(err)
               this.setState({ showSpinner: false })
               alert("Something wrong...")
            });
        }
        else{
            pushToScreen('CreatePost', { media: this.props.galleryType == 'Photos' ? this.state.selectedImages : this.state.selectedVideos, mediaType: this.props.cameraType,isComeFrom:this.props.isComeFrom });
        }


        return
     
    }

    openSetting = () => {
        this.setState({ flag: true }, () => {
            openSettings().catch(() => console.warn('cannot open settings'));
        })
    }

    render() {
        // alert('j')
        return (
            <View style={styles.container}>
                <GHeader
                    bArrow
                    greyBackButton
                    color="white"
                    backgroundColor="#232323"
                    title1={this.props.galleryType == 'Photos' ? "Upload up to 4 Photos" : 'Upload Video'}
                    showPhotoBtn
                    title1FontSize={14}
                    photoBtnText="Next"
                    photoBtnDisabled={this.state.selected.length > 0 ? false : true}
                    onJoinHeaderPress={this.onJoinHeaderPress}
                    onPhotoButtonPress={() => this.onNextPress()}
                />
                {
                    <View style={{ display: this.state.selectedImages.length > 0 ? 'flex' : 'none', backgroundColor: "#232323" }}>
                        {this.state.selectedImages.length > 1 && <View style={{ flexDirection: "row", justifyContent: "center", backgroundColor: "#232323", paddingBottom: 15 }}>
                            {
                                this.state.selectedImages.map((item, index) => (
                                    <View style={{ height: 10, width: 10, borderRadius: 5, marginLeft: 5, backgroundColor: this.state.activepage == index ? '#b9b7d9' : "#707070" }} />
                                ))
                            }
                        </View>}

                        <FlatList
                            extraData={this.state.selectedImages}
                            data={this.state.selectedImages}
                            horizontal
                            pagingEnabled
                            onMomentumScrollEnd={this.handlePageChange}
                            scrollEventThrottle={16}
                            keyExtractor={(item, index) => index}
                            renderItem={(item, index) => {
                                return (
                                    <View style={{ width: screenWidth, height: screenWidth }}>
                                        <Image
                                            source={{ uri: item.item }}
                                            style={{ height: '100%', width: '100%', resizeMode: "contain" }}
                                        />
                                    </View>
                                )
                            }}
                        />
                    </View>
                }
                {
                    this.state.havePermissions !== null ?

                        <View style={{ flex: 1, }}>
                            {
                                this.state.havePermissions ?
                                    <View style={{ flex: 1, }}>
                                        <CameraRollPicker
                                            maximum={this.props.galleryType == 'Photos' ? 4 : 1}
                                            selected={this.state.selected}
                                            assetType={this.props.galleryType}
                                            imagesPerRow={3}
                                            imageMargin={5}
                                            groupTypes={'All'}
                                            callback={this.getSelectedImages} />
{/* 
                                            <GalleryMediaPicker
                                                 groupTypes='All'
                                                 assetType='Videos'
                                                //  markIcon={marker}
                                                //  customSelectMarker={this.renderSelectMarker()}
                                                 batchSize={1}
                                                 emptyGalleryText={'There are no photos or video'}
                                                 maximumSelectedFiles={3}
                                                 selected={this.state.selected}
                                                 itemsPerRow={3}
                                                 imageMargin={3}
                                                 //customLoader={this.renderLoader()}
                                                 callback={this.getSelectedFiles.bind(this)}
                                            /> */}



                                    </View>
                                    :
                                    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', backgroundColor: '#232323' }}>

                                        <Image style={{ height: 70, width: 70, resizeMode: 'contain', marginBottom: 50 }} source={Images.warning} />

                                        <Text style={{ color: 'white' }}>You need permissions to</Text>
                                        <Text style={{ color: 'white' }}>access Gallery.</Text>

                                        {
                                            this.state.showSettingButton ?
                                                <TouchableOpacity onPress={() => this.openSetting()} style={{ paddingHorizontal: 15, paddingVertical: 10, borderRadius: 5, backgroundColor: 'white', marginTop: 50 }}>
                                                    <Text style={{ fontSize: 18, fontFamily: fonts.SFProDisplay_Bold }}>Open Settings</Text>
                                                </TouchableOpacity>
                                                :
                                                <TouchableOpacity onPress={() => this.requestForPermission()} style={{ paddingHorizontal: 15, paddingVertical: 10, borderRadius: 5, backgroundColor: 'white', marginTop: 50 }}>
                                                    <Text style={{ fontSize: 18, fontFamily: fonts.SFProDisplay_Bold }}>Request</Text>
                                                </TouchableOpacity>
                                        }

                                    </View>
                            }
                        </View>
                        :
                        <View style={{ flex: 1, backgroundColor: '#232323' }} />
                }
                <Spinner visible={this.state.showSpinner} />

            </View>
        );
    }

    requestForPermission = () => {
        request(
            Platform.select({
                android: PERMISSIONS.ANDROID.READ_EXTERNAL_STORAGE,
                ios: PERMISSIONS.IOS.PHOTO_LIBRARY,
            })
        ).then((result) => {
            if (result === 'granted') {
                this.setState({ havePermissions: true })
            } else if (result == 'blocked' || result === "unavailable") {
                this.setState({ showSettingButton: true, havePermissions: false })
            } else {
                this.setState({ havePermissions: false })
            }

        })
    }

    checkGalleryPermission = async () => {
        this.setState({ flag: false })
        if (Platform.OS == 'android') {
            const res = await check(PERMISSIONS.ANDROID.READ_EXTERNAL_STORAGE);
            if (res === 'granted') {
                this.setState({ havePermissions: true })
            } else if (res === 'denied') {
                this.requestForPermission();
            } else if (res === 'blocked') {
                this.setState({ showSettingButton: true })
            }
        } else {
            const res = await check(PERMISSIONS.IOS.PHOTO_LIBRARY);
           // alert(res)
            if (res === 'granted') {
                this.setState({ havePermissions: true })
            }  else if (res === 'blocked'|| res === "unavailable") {
                this.setState({ showSettingButton: true, havePermissions: false  })
            }
            else {
                this.requestForPermission();
            }
        }
    }

    componentDidMount = () => {
        this.checkGalleryPermission()
//asdf
        this.functionevevnt = this.props.navigation.addListener('didFocus', () => {
            
            // StatusBar.setBackgroundColor('#232323');
            // StatusBar.setBarStyle('light-content')
            StatusBar.setHidden(true)
            this.setState({selected:[]})
        });
        AppState.addEventListener('change', this._handleAppStateChange);
    };

    _handleAppStateChange = (nextAppState) => {
        if (this.state.flag && nextAppState === 'active') {
            // Do something here on app active foreground mode.
            this.checkGalleryPermission();
            console.log("App is in Active Foreground Mode.")
        }
    };


    componentWillUnmount() {
        AppState.removeEventListener('change', this._handleAppStateChange);
        this.functionevevnt.remove();
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },

});