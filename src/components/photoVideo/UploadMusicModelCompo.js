import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from "react-native";
import fonts from '../../common/fonts';
import Modal from 'react-native-modal';


export const UploadMusicModelCompo = (props) => {

    return (
        <Modal
            backdropOpacity={0.9}
            isVisible={props.showMusicModel}>
            <View style={styles.container}>
                <View style={{ backgroundColor: "white", padding: 15, width: "80%", borderRadius: 15, alignItems: "center", }}>
                    <Text style={{ fontSize: 18, fontFamily: fonts.SFProDisplay_Semibold, color: '#383841' }}>Upload Music</Text>

                    <View style={{ marginVertical: 80 }}>
                        <Text style={{ fontSize: 18, fontFamily: fonts.SFProDisplay_Semibold, color: '#383841', opacity: 0.5 }}>Upload music from</Text>
                        <Text style={{ fontSize: 18, fontFamily: fonts.SFProDisplay_Heavy, color: '#383841' }}>vumi.app/upload</Text>
                    </View>

                    <TouchableOpacity onPress={() => { props.hideModel() }} style={[styles.btn]}>
                        <Text style={[styles.btnTxt, { color: 'white' }]}>Ok</Text>
                    </TouchableOpacity>

                </View>
            </View>
        </Modal>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: "center",
        justifyContent: "center",
    },

    btnTxt: { fontSize: 18, fontFamily: fonts.SFProDisplay_Heavy },
    btn: { backgroundColor: "#bd64ff", width: '90%', alignItems: "center", justifyContent: "center", paddingVertical: 15, borderRadius: 25 },

});