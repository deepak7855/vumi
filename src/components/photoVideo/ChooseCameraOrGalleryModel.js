import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from "react-native";
import fonts from '../../common/fonts';
import Modal from 'react-native-modal';


export const ChooseCameraOrGalleryModel = (props) => {

    return (
        <Modal
            backdropOpacity={0.9}
            isVisible={props.showPhotoModel}>
            <View style={styles.container}>

                <TouchableOpacity onPress={() => props.openCameraGallery()} style={[styles.btn, styles.cameraBtn]}>
                    <Text style={[styles.btnTxt, { color: '#555560' }]}>Camera Roll</Text>
                </TouchableOpacity>

                <TouchableOpacity onPress={() => props.openCamera()} style={[styles.btn, styles.takeBtn]}>
                    <Text style={[styles.btnTxt, { color: '#555560' }]}>Take Photo</Text>
                </TouchableOpacity>

                <TouchableOpacity onPress={() => props.hideModel()} style={[styles.btn, { borderRadius: 10, }]}>
                    <Text style={[styles.btnTxt, { color: '#fa3f78' }]}>Cancel</Text>
                </TouchableOpacity>

            </View>
        </Modal>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: "center",
        justifyContent: "flex-end",
        paddingHorizontal: '10%',
    },
    btnTxt: { fontSize: 18, fontFamily: fonts.Montserrat_SemiBold },
    btn: { backgroundColor: "white", width: '100%', alignItems: "center", justifyContent: "center", paddingVertical: 15 },
    takeBtn: { borderBottomRightRadius: 10, borderBottomLeftRadius: 10, marginBottom: 15, borderTopColor: '#F5F5F5', borderTopWidth: 1 },
    cameraBtn: { borderTopRightRadius: 10, borderTopLeftRadius: 10 }
});