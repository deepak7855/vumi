import React, { Component } from 'react';
import {
    Dimensions,
    StyleSheet,
    Text,
    ScrollView,
    StatusBar,
    Platform,
    TouchableOpacity,
    View,
    Image,
    AppState, FlatList,
    TextInput,
    TouchableWithoutFeedback
} from 'react-native';

import { GHeader, Images, Colors } from '../../common';
import { PERMISSIONS, check, request, openSettings } from 'react-native-permissions'
import Config from '../../lib/Config';
import Toast from 'react-native-root-toast';
import Helper from '../../lib/Helper';
import { Actions } from 'react-native-router-flux';
import pushToScreen from '../../navigation/NavigationActions';
import Spinner from 'react-native-loading-spinner-overlay';
import { KeyboardAwareScrollView } from 'react-native-keyboard-aware-scroll-view'
import ImagePicker from 'react-native-image-crop-picker';
import Video from 'react-native-video';
import ProgressBar from "react-native-progress/Bar";
import fonts from '../../common/fonts';

const screenWidth = Dimensions.get('window').width

export default class CreatePost extends Component {
    constructor(props) {
        super(props);
        this.state = {
            selected: [],
            activepage: 0,
            havePermissions: null,
            selectedVideos: [],
            showSettingButton: false,
            flag: false, showSpinner: false,
            mediaType: this.props.mediaType ? this.props.mediaType : 'Picture',
            selectedImages: [],
            title: this.props.EditData && this.props.EditData.title ? this.props.EditData.title : "",
            caption: this.props.EditData && this.props.EditData.description ? this.props.EditData.description : "",
            duration: 0,
            currentTime: 0,
            activeIndex: 0,
            videoPaused: false,
            progress: 0,
            displayName: this.props.EditData && this.props.EditData.display_name ? this.props.EditData.display_name : Helper.userInfo.username,
            socialLink: this.props.EditData && this.props.EditData.social_link ? this.props.EditData.social_link : "",
            arrIcons: [{ id: 1, image: Images.UploadPost.close }, { id: 2, image: Images.UploadPost.plus }, { id: 3, image: Images.UploadPost.crop }, { id: 4, image: Images.UploadPost.eye }],

        };

        let temp = JSON.stringify(this.props.media)
        this.state.selectedImages = JSON.parse(temp)

        console.log("temp", temp)


        this.getSelectedImages = this.getSelectedImages.bind(this);
    }
    handlePageChange = (e) => {
        var offset = e.nativeEvent.contentOffset;
        if (offset) {
            var page = Math.ceil(offset.x / Dimensions.get('window').width);
            this.setState({ activepage: page });
        }
    }

    getSelectedImages(images, current) {
        if (this.props.galleryType == 'Photos') {
            var selectedImages = images.map((item) => {
                return item.uri
            })
            this.setState({ selected: images, selectedImages });
        } else {
            var selectedVideos = images.map((item) => {
                return item.uri
            })
            this.setState({ selected: images, selectedVideos });
        }
    }

    onNextPress() {
        if (this.props.EditPostList == true) {
            const data = new FormData()
            data.append('post_id', this.props.EditData && this.props.EditData.Id)
            data.append('title', this.state.title.trim())
            data.append('description', this.state.caption.trim())
            data.append('display_name', this.state.displayName.trim())
            data.append('social_link', this.state.socialLink.trim())

            console.log('data----------', data)

            this.setState({ showSpinner: true })
            Helper.makeRequest({ url: "edit-post", method: "POST", data: data, isImage: true }).then((resp) => {
                this.setState({ showSpinner: false })
                if (resp.status == 'true') {
                    Toast.show(resp.message);
                    Actions.pop()
                } else {
                    Toast.show(resp.message)
                }
            })

        } else {
            let mediaType = this.props.mediaType
            let media = this.state.selectedImages
            if (media.length > 0) {
                const data = new FormData()
                // if (this.state.selected_pack) {
                //     data.append('points', String(this.state.selected_pack))
                // } else {
                //     Toast.show('Please select View Pack')
                //     return false
                // }
                if (mediaType == 'Picture') {
                    media.forEach((element, i) => {
                        data.append(`media${i + 1}`, {
                            uri: element,
                            name: (new Date().getMilliseconds() + 1) + '.jpg',
                            type: 'image/jpeg'
                        });
                    });
                    data.append('image_count', media.length)
                    data.append('video_count', 0)
                } else {
                    media.forEach((element, i) => {
                        data.append(`video${i + 1}`, {
                            uri: Platform.OS == 'ios' ? element.replace('file://', "") : element,
                            name: (new Date().getMilliseconds() + 1) + '.mov',
                            type: 'video/mov'
                        });
                    });
                    data.append('image_count', 0)
                    data.append('video_count', media.length)

                }
                data.append('title', this.state.title.trim())
                data.append('description', this.state.caption.trim())
                data.append('display_name', this.state.displayName.trim())
                data.append('social_link', this.state.socialLink.trim())
                console.log('data----------', data)
                this.setState({ showSpinner: true })
                Helper.makeRequest({ url: "add-post", method: "POST", data: data, isImage: true }).then((resp) => {
                    if (resp.status == 'true') {
                        this.setState({ showSpinner: false })
                        // Actions.OverView();
                        if (this.props.isComeFrom == "overView") {
                            pushToScreen('OverView');
                            pushToScreen('PostsList');
                        }
                        else {
                            // pushToScreen('Profile');
                            // pushToScreen('PostsList');
                            Actions.popTo('PostsList')

                        }
                        Toast.show(resp.message);
                    } else {
                        Toast.show(resp.message)
                    }
                    this.setState({ showSpinner: false })
                })
            }
        }
    }

    openSetting = () => {
        this.setState({ flag: true }, () => {
            openSettings().catch(() => console.warn('cannot open settings'));
        })
    }
    onPress = (val) => {
        this.setState({ modalVisible: val })
    }

    handleProgressPress = e => {
        const position = e.nativeEvent.locationX;
        const progress = (position / (screenWidth - 8)) * this.state.duration;
        this.player.seek(progress);
    };

    handleProgress = progress => {
        // console.log("handleProgress-------------",progress)
        this.setState({
            progress: parseFloat(progress.currentTime / progress.playableDuration)
        });
    };

    handleLoad = (meta) => {
        this.setState({
            progress: 0,
            duration: meta.duration, currentTime: meta.currentTime
        })
    }

    _onViewableItemsChanged = props => {
        const changed = props.changed;
        changed.forEach(item => {
            if (item.isViewable) {
                this.setState({ selectedIndex: item.index, progress: 0 }, () => {
                    clearTimeout(this.timeout);
                    this.timeout = setTimeout(() => {
                        this.updatePostViewStatus()
                    }, 3000);

                })
            }
        });
    };
    methodBtnClick(id) {

        // alert(JSON.stringify(this.state.selectedImages))

        switch (id) {
            case 1:
                this.state.selectedImages.splice(this.state.activepage, 1)
                break;
            case 2:
                Actions.pop()
                break;
            case 3:
                ImagePicker.openCropper({
                    path: this.state.selectedImages[this.state.activepage],
                }).then(images => {
                    // alert(JSON.stringify(images))
                    console.log("images---------", images)
                    this.state.selectedImages.splice(this.state.activepage, 1, images.path)
                    this.setState({})

                });
                // pushToScreen('CropImage', { image: this.state.selectedImages[this.state.activepage] });
                break;
            case 4:
                this.setState({ videoPaused: true }, () => {
                    let data = {}
                    data.title = this.state.title.trim()
                    data.description = this.state.caption.trim()
                    data.display_name = this.state.displayName.trim()
                    data.social_link = this.state.socialLink.trim()
                    pushToScreen('PreviewPost', { mediaType: this.props.mediaType, media: this.props.media, dicPostDetails: data });
                    console.log(this.props.mediaType, "this.props.mediaType");
                    console.log(data, " datadata");
                    console.log(this.props.media, "this.props.media");

                })
                break;
            default:
                break;
        }


        this.setState({})
    }

    render() {
        return (
            <View style={styles.container}>
                <GHeader
                    bArrow
                    greyBackButton
                    color="white"
                    backgroundColor={Colors.headerPurple}
                    title1={this.props.EditPostList == true ? (this.props.mediaType == 'Picture' ? "Edit Post" : 'Edit Post') : (this.props.mediaType == 'Picture' ? "Upload Photos" : 'Upload Video')}
                    //showPhotoBtn
                    title1FontSize={14}
                    photoBtnText="Save"
                    photoBtnDisabled={this.state.selected.length > 0 ? false : true}
                    onJoinHeaderPress={this.onJoinHeaderPress}
                // onPhotoButtonPress={() => this.onNextPress()}
                />

                <KeyboardAwareScrollView style={{ flex: 1 }} bounce={false} showsHorizontalScrollIndicator={false} showsVerticalScrollIndicator={false} >
                    <View style={{ flex: 1 }}>
                        {
                            <View style={{ display: this.state.selectedImages.length > 0 ? 'flex' : 'none', backgroundColor: "#232323" }}>
                                {this.props.mediaType == 'Picture' ?
                                    <View style={{ flex: 1 }}>
                                        <FlatList
                                            extraData={this.state}
                                            data={this.state.selectedImages}
                                            horizontal
                                            pagingEnabled
                                            onMomentumScrollEnd={this.handlePageChange}
                                            scrollEventThrottle={16}
                                            keyExtractor={(item, index) => index}
                                            renderItem={(item, index) => {
                                                return (
                                                    <View style={{ width: screenWidth, height: screenWidth, backgroundColor: Colors.lightBackgroundColor }}>
                                                        <Image
                                                            source={{ uri: item.item }}
                                                            resizeMode={"contain"}
                                                            style={{ height: '100%', width: '100%', }}
                                                        />
                                                    </View>
                                                )
                                            }}
                                        />
                                        {this.state.selectedImages.length > 1 && <View style={{ flexDirection: "row", justifyContent: "center", position: 'absolute', alignSelf: 'center', zIndex: 1, bottom: 20, paddingBottom: 15 }}>
                                            {
                                                this.state.selectedImages.map((item, index) => (
                                                    <View style={{ height: 10, width: 10, borderRadius: 5, marginLeft: 5, backgroundColor: this.state.activepage == index ? Colors.headerPurple : Colors.white }} />
                                                ))
                                            }
                                        </View>}
                                        {this.props.EditPostList == true ?
                                            null
                                            :
                                            <View style={{ position: 'absolute', right: 15, zIndex: 1, justifyContent: 'center', height: '100%' }}>

                                                <View style={{}}>
                                                    {
                                                        this.state.arrIcons.map((item, index) => {
                                                            if (this.state.selectedImages.length == 1 && item.id == 1) {
                                                                return (
                                                                    <View />
                                                                )
                                                            }
                                                            return (
                                                                <TouchableOpacity onPress={() => this.methodBtnClick(item.id)} style={{ height: 50, width: 50, marginTop: 15, alignItems: 'center', justifyContent: 'center', }}>
                                                                    <Image resizeMode={'contain'} style={{ height: 50, width: 50 }} source={item.image}></Image>
                                                                </TouchableOpacity>
                                                            )
                                                        })
                                                    }
                                                </View>
                                            </View>
                                        }

                                    </View>
                                    :
                                    <View style={{ flex: 1, alignItems: "center" }}>
                                        <View style={styles.squareView}>
                                            <TouchableWithoutFeedback onPress={() => this.setState({ videoPaused: !this.state.videoPaused })}>
                                                <Video
                                                    source={{ uri: this.state.selectedImages[0] }}
                                                    ref={(ref) => {
                                                        this.player = ref
                                                    }}
                                                    onProgress={this.handleProgress}
                                                    onLoad={this.handleLoad}
                                                    repeat
                                                    resizeMode="cover"
                                                    style={styles.video}
                                                    paused={this.state.videoPaused}
                                                />
                                            </TouchableWithoutFeedback>
                                            {this.props.EditPostList == true ?
                                                null
                                                :
                                                <View style={{ position: 'absolute', right: 15, zIndex: 1, justifyContent: 'center', height: '100%' }}>
                                                    <View style={{}}>
                                                        {
                                                            this.state.arrIcons.map((item, index) => {
                                                                if (this.state.selectedImages.length == 1 && item.id == 1 || (this.props.mediaType != "Picture" && item.id == 3)) {
                                                                    return (
                                                                        <View />
                                                                    )
                                                                }
                                                                return (
                                                                    <TouchableOpacity onPress={() => this.methodBtnClick(item.id)} style={{ height: 50, width: 50, marginTop: 15, alignItems: 'center', justifyContent: 'center', }}>
                                                                        <Image resizeMode={'contain'} style={{ height: 50, width: 50 }} source={item.image}></Image>
                                                                    </TouchableOpacity>
                                                                )
                                                            })
                                                        }
                                                    </View>
                                                </View>
                                            }
                                        </View>
                                        {this.state.progress ? <ProgressBar
                                            progress={this.state.progress}
                                            color="#5856d6"
                                            unfilledColor="#282828"
                                            borderColor="transparent"
                                            width={screenWidth - 8}
                                            borderRadius={0}
                                            height={7}
                                        /> : <View style={{ width: screenWidth - 8, height: 7, backgroundColor: "#282828" }} />}
                                    </View>}

                                <View style={{ flex: 1, backgroundColor: Colors.white, }}>
                                    <View style={{ paddingHorizontal: 15, backgroundColor: Colors.white, alignItems: "center", width: '100%', borderBottomColor: Colors.lineBorderColor, borderBottomWidth: 1.0 }}>
                                        <TextInput
                                            value={this.state.title}
                                            onChangeText={(text) => this.setState({ title: text })}
                                            placeholder="Title"
                                            placeholderTextColor="#cacadd"
                                            maxLength={80}
                                            style={{ width: "100%", fontFamily: fonts.Poppins_Medium, fontSize: 20, color: "#6e6e8b", paddingVertical: 0, height: 70, }}
                                            onSubmitEditing={() => this.captionRef.focus()}
                                            returnKeyType={'next'}
                                        />
                                    </View>
                                    <View style={{ paddingHorizontal: 15, backgroundColor: Colors.white, height: 100, justifyContent: 'center', width: '100%', borderBottomColor: Colors.lineBorderColor, borderBottomWidth: 1.0 }}>
                                        <TextInput
                                            ref={ref => this.captionRef = ref}
                                            value={this.state.caption}
                                            onChangeText={(text) => this.setState({ caption: text })}
                                            placeholder="Caption"
                                            placeholderTextColor="#cacadd"
                                            maxLength={250}
                                            multiline
                                            style={{ width: "100%", fontFamily: fonts.Poppins_Medium, fontSize: 20, color: "#6e6e8b", paddingVertical: 0 }}
                                            onSubmitEditing={() => this.captionRef.focus()}
                                            returnKeyType={'default'}
                                        />
                                    </View>
                                    <View style={{ paddingHorizontal: 15, backgroundColor: Colors.white, alignItems: "center", width: '100%', borderBottomColor: Colors.lineBorderColor, borderBottomWidth: 1.0 }}>
                                        <TextInput
                                            value={this.state.displayName}
                                            onChangeText={(text) => this.setState({ displayName: text })}
                                            placeholder="Display Name"
                                            placeholderTextColor="#cacadd"
                                            maxLength={80}
                                            style={{ width: "100%", fontFamily: fonts.Poppins_Medium, fontSize: 20, color: "#6e6e8b", paddingVertical: 0, height: 70, }}
                                            returnKeyType={'default'}
                                        />
                                    </View>

                                    <View style={{ paddingHorizontal: 15, height: 70, flexDirection: 'row', justifyContent: 'space-between', backgroundColor: Colors.white, alignItems: "center", width: '100%', borderBottomColor: Colors.lineBorderColor, borderBottomWidth: 1.0 }}>
                                        <View style={{ flexDirection: 'row', alignItems: 'center', width: '40%' }}>
                                            <TextInput
                                                onChangeText={(text) => this.setState({ displayName: text })}
                                                placeholder="Social Link"
                                                placeholderTextColor="#cacadd"
                                                style={{ fontFamily: fonts.Poppins_Medium, fontSize: 20, color: "#6e6e8b", paddingVertical: 0, }}
                                                returnKeyType={'default'}
                                                editable={false}
                                            />

                                            <View style={{ width: 25, height: 25, marginLeft: 10 }}>
                                                <Image style={{ width: 25, height: 25 }} resizeMode={'contain'} source={Images.profile.info}></Image>
                                            </View>

                                        </View>

                                        <View style={{ marginLeft: 10, height: 40, borderRadius: 20, width: '50%', backgroundColor: Colors.lightBackgroundColor, alignItems: 'center', justifyContent: 'center' }}>
                                            <TextInput
                                                value={this.state.socialLink}
                                                onChangeText={(text) => this.setState({ socialLink: text })}
                                                placeholder="Paste URL here"
                                                placeholderTextColor="#cacadd"
                                                style={{ fontFamily: fonts.Poppins_Medium, fontSize: 18, color: "#6e6e8b", paddingVertical: 0, }}
                                                returnKeyType={'default'}
                                            />
                                        </View>

                                    </View>
                                    {this.props.EditPostList == true ?
                                        <TouchableOpacity onPress={() => this.methodUploadMedia()} style={{ marginHorizontal: 20, marginVertical: 30, backgroundColor: Colors.appGreen, alignItems: 'center', justifyContent: 'center', height: 60, borderRadius: 30 }}>
                                            <Text style={{ fontFamily: fonts.Montserrat_Bold, fontSize: 20, color: Colors.white, paddingVertical: 0, }}>SAVE</Text>
                                        </TouchableOpacity>
                                        :
                                        <TouchableOpacity onPress={() => this.methodUploadMedia()} style={{ marginHorizontal: 20, marginVertical: 30, backgroundColor: Colors.appGreen, alignItems: 'center', justifyContent: 'center', height: 60, borderRadius: 30 }}>
                                            <Text style={{ fontFamily: fonts.Montserrat_SemiBold, fontSize: 20, color: Colors.white, paddingVertical: 0, }}>Upload Post</Text>
                                        </TouchableOpacity>
                                    }

                                </View>

                            </View>
                        }

                    </View>
                </KeyboardAwareScrollView>

                <Spinner visible={this.state.showSpinner} />

            </View>
        );
    }
    methodUploadMedia() {
        if (this.state.title.trim().length == 0) {
            Toast.show('Please enter title.', { position: 0 })
        }
        else if (this.state.caption.trim().length == 0) {
            Toast.show('Please enter caption.', { position: 0 })
        }
        else if (this.state.displayName.trim().length == 0) {
            Toast.show('Please enter display name.', { position: 0 })
        }
        else {
            this.onNextPress()
        }
    }

    requestForPermission = () => {
        request(
            Platform.select({
                android: PERMISSIONS.ANDROID.READ_EXTERNAL_STORAGE,
                ios: PERMISSIONS.IOS.PHOTO_LIBRARY,
            })
        ).then((result) => {
            if (result === 'granted') {
                this.setState({ havePermissions: true })
            } else if (result == 'blocked') {
                this.setState({ showSettingButton: true, havePermissions: false })
            } else {
                this.setState({ havePermissions: false })
            }

        })
    }

    checkGalleryPermission = async () => {
        this.setState({ flag: false })
        if (Platform.OS == 'android') {
            const res = await check(PERMISSIONS.ANDROID.READ_EXTERNAL_STORAGE);
            if (res === 'granted') {
                this.setState({ havePermissions: true })
            } else if (res === 'denied') {
                this.requestForPermission();
            } else if (res === 'blocked') {
                this.setState({ showSettingButton: true })
            }
        } else {
            const res = await check(PERMISSIONS.IOS.PHOTO_LIBRARY);
            if (res === 'granted') {
                this.setState({ havePermissions: true })
            } else if (res === 'denied') {
                this.requestForPermission();
            } else if (res === 'blocked') {
                this.setState({ showSettingButton: true })
            }
        }
    }

    componentDidMount = () => {

        // this.checkGalleryPermission()
        this.functionevevnt = this.props.navigation.addListener('didFocus', () => {
            // StatusBar.setBackgroundColor('#232323');
            // StatusBar.setBarStyle('light-content')
            StatusBar.setHidden(true)
        });
        // AppState.addEventListener('change', this._handleAppStateChange);
    };

    _handleAppStateChange = (nextAppState) => {
        if (this.state.flag && nextAppState === 'active') {
            // Do something here on app active foreground mode.
            this.checkGalleryPermission();
            console.log("App is in Active Foreground Mode.")
        }
    };


    componentWillUnmount() {
        AppState.removeEventListener('change', this._handleAppStateChange);
        this.functionevevnt.remove();
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
    },
    squareView: {
        height: screenWidth,
        width: screenWidth,
        justifyContent: "center"
    },
    video: {
        width: screenWidth,
        aspectRatio: 1,
        flex: 1
    },

});