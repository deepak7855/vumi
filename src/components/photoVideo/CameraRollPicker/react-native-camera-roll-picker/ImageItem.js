import React, { Component } from 'react';
import {
  Image,
  StyleSheet,
  Dimensions,
  TouchableOpacity,
  Text,
  View
} from 'react-native';
import PropTypes from 'prop-types';

const checkIcon = require('./circle-check.png');

const styles = StyleSheet.create({
  marker: {
    position: 'absolute',
    top: 5,
    right: 5,
    backgroundColor: 'transparent',

  },
});

class ImageItem extends Component {
  componentWillMount() {
    let { width } = Dimensions.get('window');
    const { imageMargin, imagesPerRow, containerWidth } = this.props;

    if (typeof containerWidth !== 'undefined') {
      width = containerWidth;
    }
    this.imageSize = (width - (imagesPerRow + 1) * imageMargin) / imagesPerRow;
  }

  handleClick(item) {
    this.props.onClick(item);
  }
  getCount = (obj, objArray) => {
    // console.log('obj',obj.node.image.filename );
    console.log('objArray',objArray  );

    let index = 0;
    index = objArray.map(e => e.filename).indexOf(obj.node.image.filename);
    console.log('index',index+1  );

    return index + 1


  }

  render() {
    const {
      item, selected, selectedMarker, imageMargin, selectedArray
    } = this.props;

    const marker = selectedMarker ||
      // (<Image
      //   style={[styles.marker, { width: 25, height: 25 }]}
      //   source={}
      // />);
      (<View style={{
        alignItems: 'center', justifyContent: 'center',
        position: 'absolute',
        top: 5, left: 5,
        right: 5, width: 25, height: 25, borderRadius: 25 / 2,
        backgroundColor: 'rgb(44, 146, 247)',
      }}>
        <Text style={{ width: 25, height: 25, color: '#fff', borderRadius: 25 / 2, textAlign: 'center', fontSize: 15, padding: 2 }}>{this.getCount(item, selectedArray)}</Text>
      </View>);

    const { image } = item.node;
    const marker1 = selectedMarker ||
      (<Text style={[styles.marker, { left: 5, width: 25, height: 25, color: '#fff', borderColor: '#fff', borderRadius: 25 / 2, borderWidth: 1, textAlign: 'center', fontSize: 15, padding: 2 }]}></Text>)

   
 
    console.log(selectedArray, 'selectedArray');
    console.log(selectedArray.length, 'selectedArraylength');
    return (

      <View>
        <TouchableOpacity
          style={{ marginBottom: imageMargin, marginRight: imageMargin }}
          onPress={() => selectedArray.length > 0 ? this.handleClick(image) : {}}
          onLongPress={() => selectedArray.length == 0 ? this.handleClick(image) : {}}
        >
          <Image
            source={{ uri: image.uri }}
            style={{ height: this.imageSize, width: this.imageSize }}
          />
          {(selected) ? marker : selectedArray.length > 0 ? marker1 : null}
        </TouchableOpacity>

      </View>

    );
  }
}

ImageItem.defaultProps = {
  item: {},
  selected: false,
  selectedArray: []
};

ImageItem.propTypes = {
  item: PropTypes.object,
  selected: PropTypes.bool,
  selectedMarker: PropTypes.element,
  imageMargin: PropTypes.number,
  imagesPerRow: PropTypes.number,
  onClick: PropTypes.func,
  selectedArray: PropTypes.array,
};

export default ImageItem;
