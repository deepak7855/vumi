import React, { Component } from 'react';
import {
  Dimensions,
  StyleSheet,
  Text,
  ScrollView,
  StatusBar,
  Platform,
  TouchableOpacity,
  View,
  Image,
  AppState, FlatList,
  TextInput
} from 'react-native';

import { CropView } from 'react-native-image-crop-tools';
import { GHeader, Images, Colors } from '../../common';

const screenWidth = Dimensions.get('window').width

export default class CropImage extends Component {
  constructor(props) {
    super(props);

    this.state = {
      image: props.image,
      crop: { x: 0, y: 0 },
      zoom: 1,
      aspect: 4 / 3,
    }
  }


  render() {
    return (
      <View style={{ flex: 1 }}>
        <GHeader
          bArrow
          greyBackButton
          color="white"
          backgroundColor="#232323"
          title1={"Crop Image"}
          title1FontSize={14}
 
        />
        <CropView
          sourceUrl={this.state.image}
          style={styles.container}
          // ref={cropViewRef}
          onImageCrop={(res) => console.log(res)}
          keepAspectRatio
          // aspectRatio={{ width: 16, height: 9 }}
        />
      </View>
    )
  }

}

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },

});