import React, { Component } from 'react';
import { View, Text, FlatList, Image, StyleSheet, Dimensions, TouchableWithoutFeedback } from 'react-native';
import { color } from 'react-native-reanimated';
import { Colors } from '../../common/Colors';
import ImageLoadView from '../../common/ImageLoadView';
import Config from '../../lib/Config';

const screenWidth = Dimensions.get('window').width;

export default class ImageViewSwiper extends Component {
    constructor(props) {
        super(props);
        this.state = {
            activeIndex: 0
        };
    }


    handlePageChange = (e) => {
        var offset = e.nativeEvent.contentOffset;
        if (offset) {
            var page = Math.ceil(offset.x / Dimensions.get('window').width);
            this.setState({ activeIndex: page });
        }
    }


    render() {
        return (
            <View style={{ flex: 1 }}>
                <FlatList
                    horizontal
                    data={this.props.data}
                    pagingEnabled
                    keyExtractor={(item, index) => index}
                    showsHorizontalScrollIndicator={false}
                    scrollEventThrottle={16}
                    onMomentumScrollEnd={this.handlePageChange}
                    renderItem={({ item }) => {
                        return (
                            <View style={styles.squareView}>
                                <TouchableWithoutFeedback onPress={() => this.props.onImagePress(item, this.props.index)} >
                                    {/* <Image
                                        style={styles.squareView}
                                        source={{ uri: Config.imageUrl + item.media }}
                                    /> */}
                                    <ImageLoadView
                                        style={styles.squareView}
                                        source={{ uri: Config.imageUrl + item.media }}
                                    />
                                    {/* <ImageLoadView
                                        style={styles.user_img}
                                        indicatorColor={Colors.white}
                                        source={{ uri: Config.imageUrl + item.media }}
                                        resizeMode={'cover'}
                                    /> */}
                                </TouchableWithoutFeedback>
                            </View>
                        )
                    }}
                />
                <View style={{ display: this.props.data.length > 1 ? 'flex' : 'none', flexDirection: "row", marginTop: 5, justifyContent: "center" }}>
                    {
                        this.props.data.map((item, index) => (
                            <View style={{ height: 10, width: 10, borderRadius: 5, marginLeft: 7, backgroundColor: this.state.activeIndex == index ? '#38bfff' : "white" }} />
                        ))
                    }
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({

    squareView: {
        height: screenWidth,
        width: screenWidth,
        justifyContent: "center"
    }
});