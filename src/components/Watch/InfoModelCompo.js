import React from 'react';
import { View, Text, Image, StyleSheet, TouchableOpacity, Alert } from "react-native";
import { Images } from '../../common/Images';
import fonts from '../../common/fonts';
import Modal from 'react-native-modal';
import Config from '../../lib/Config';
import Helper from '../../lib/Helper';
import ImageLoadView from '../../common/ImageLoadView';




export const InfoModelCompo = (props) => {
    console.log(props, "propspropspropspropsprops");
    return (
        <Modal
            isVisible={props.showInfoModel}>
            <View style={styles.container}>
                {
                    Object.keys(props.selectedRowDetail).length > 0 &&
                    <View style={styles.subContainer}>
                        {
                            Helper.userInfo.id != props.selectedRowDetail.user.id &&
                            <View style={{ flexDirection: "row", paddingHorizontal: 10, justifyContent: "space-between" }}>

                                <TouchableOpacity onPress={() => props.onShowBlockModel()}>
                                    <Image
                                        style={styles.top_icon}
                                        source={Images.bloak_usr}
                                    />
                                </TouchableOpacity>



                                <TouchableOpacity onPress={() => props.goToChat(props.selectedRowDetail)}>
                                    <Image
                                        style={styles.top_icon}
                                        source={Images.brdr_msg}
                                    />
                                </TouchableOpacity>
                            </View>
                        }
                        <View style={{ alignItems: "center" }}>
                            {/* <Image
                                style={styles.user}
                                source={props.selectedRowDetail.user.profile_image ? { uri: Helper.getImageUrl(props.selectedRowDetail.user.profile_image) } : Images.User_profile_icon}
                            /> */}
                            <ImageLoadView
                                resizeMode="cover"
                                style={styles.user}
                                source={props.selectedRowDetail.user.profile_image ? { uri: Helper.getImageUrl(props.selectedRowDetail.user.profile_image) } : Images.User_profile_icon}
                            />
                            <Text numberOfLines={1} style={[styles.userName, { textAlign: 'center' }]}>{Helper.userInfo.id == props.selectedRowDetail.user.id ? 'You' : props.selectedRowDetail.user.username}</Text>
                            <Text numberOfLines={1} style={styles.country}>{props.selectedRowDetail?.user?.country_name || props.selectedRowDetail?.user?.location || ''}</Text>

                            <View style={styles.separater} />

                            <Text style={styles.comment}>{props.selectedRowDetail.user.bio != 'null' && props.selectedRowDetail.user.bio ? props.selectedRowDetail.user.bio : ''}</Text>

                            {/* <TouchableOpacity style={styles.instaBtn}>
                                <Text style={styles.InstaText}>Instagram</Text>
                            </TouchableOpacity> */}

                        </View>
                    </View>
                }

                <TouchableOpacity onPress={() => props.hideInfoModel(false)} style={styles.closeBtn}>
                    <Image
                        style={styles.closeIcon}
                        source={Images.close}
                    />
                </TouchableOpacity>
            </View>
        </Modal>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: "center",
        justifyContent: "center",
        paddingHorizontal: 10,
    },
    subContainer: {
        backgroundColor: 'white',
        borderRadius: 25,
        width: '100%',
        paddingTop: 10,
    },
    user: {
        height: 100,
        width: 100,
        borderRadius: 50,
        resizeMode: "cover",
    },

    userName: {
        fontFamily: fonts.Montserrat_SemiBold,
        fontSize: 18,
        color: "#4b4b4b"
    },

    country: {
        fontFamily: fonts.SFProRegular,
        fontSize: 14,
        color: "#575757"
    },

    top_icon: {
        height: 36,
        width: 36,
        resizeMode: "contain"
    },

    separater: {
        height: 1,
        backgroundColor: "lightgrey",
        width: "65%",
        marginVertical: 15
    },

    comment: {
        fontFamily: fonts.SFProRegular,
        fontSize: 14,
        color: "#5f5f5f",
        width: "70%",
        textAlign: "center"
    },

    instaBtn: {
        backgroundColor: "#5851db",
        width: '75%',
        paddingVertical: 10,
        marginTop: 30,
        marginBottom: 20,
        alignItems: "center",
        justifyContent: "center",
        borderRadius: 20
    },

    InstaText: {
        color: 'white',
        fontSize: 16,
        fontFamily: fonts.Montserrat_SemiBold
    },

    closeBtn: {
        height: 60,
        width: 60,
        borderRadius: 30,
        justifyContent: "center",
        alignItems: "center",
        alignSelf: "center",
        backgroundColor: "white",
        marginTop: 30
    },

    closeIcon: {
        height: 60,
        width: 60,
        resizeMode: "contain"
    },

});