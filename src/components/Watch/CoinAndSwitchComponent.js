import React, { useCallback, useEffect, useState } from 'react';
import { View, Text, Image, StyleSheet, TouchableOpacity, DeviceEventEmitter } from "react-native";
import { Images } from '../../common/Images';
import { Colors } from '../../common/Colors';

import Helper from '../../lib/Helper';
import { ProgressBar } from '../ProgressBar';

export const CoinAndSwitchComponent = (props) => {

    return (
        <View style={styles.container}>
            <View style={[styles.subContainer, { width: "80%" }]}>
                <Image
                    source={Images.coin}
                    style={styles.coin}
                />
                <Text style={styles.coinText}>{props.coin}</Text>
                <Text style={styles.coinText1}>{props.viewgetcoins > 0 ? "+" + props.viewgetcoins : ''}</Text>

            </View>
            <View style={[styles.subContainer, { width: "30%" }]}>
                <Image
                    source={Images.lockicon}
                    style={styles.lock}
                />
                <View style={{ width: "50%" }}>
                    <ProgressBar watchdemo={true} total_views={props.admintotalView} points={props.currentshowView} completed={44} colorCompleted={"#41FFAf"} colorUncompleted={"#202127"}

                    />
                </View>
                {/* <TouchableOpacity style={[styles.switchSmall, { backgroundColor: props.toggleValue ? Colors.appGreen : Colors.lightBlack }]} />
                <TouchableOpacity style={[styles.switchLarge, { backgroundColor: !props.toggleValue ? Colors.appGreen : Colors.lightBlack }]} />
             */}
            </View>
        </View>
    )

}

const styles = StyleSheet.create({
    container: {
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "space-between",
        paddingHorizontal: 10,
        paddingVertical: 8,
        height: 60,
        backgroundColor: "black"
    },
    subContainer: {
        flexDirection: "row",
        alignItems: "center"
    },
    coin: {
        height: 22,
        width: 22,
        resizeMode: "contain",
        marginRight: 10
    },
    switchSmall: {
        height: 25,
        width: 18
    },
    switchLarge: {
        height: 25,
        width: 40
    },
    coinText: {
        color: "#E7E7F0",
        fontSize: 13,

    },
    coinText1: {
        marginLeft: 5,
        color: "#1FEAA6",
        fontSize: 13,

    },
    lock: {
        height: 15,
        width: 15,
        resizeMode: "contain",
        marginRight: 10
    }
});