import React from 'react';
import { View, Text, Image, StyleSheet, Dimensions } from "react-native";
import { Images } from '../../common/Images';
import fonts from '../../common/fonts';
import Modal from 'react-native-modal';
import { Colors } from '../../common/Colors';

const screenWidth = Dimensions.get('window').width;

export const GetMoreCoinsModelCompo = (props) => {
    return (
        <Modal
            onBackdropPress={() => props.toggleModel(false)}
            isVisible={props.modalVisible}>
            <View style={styles.container}>
                <Text style={{ color: '#c4c2e6', fontSize: 22, fontFamily: fonts.SFProRegular }}> Swipe up to </Text>
                <Text style={{ color: '#c4c2e6', fontSize: 22, fontFamily: fonts.SFProRegular }}> get more coins! </Text>

                <View style={{ backgroundColor: "#3d3e5f", width: '62%', height: '55%', borderRadius: 25, borderWidth: 6, borderColor: "#9b93c3", marginTop: 40, paddingTop: 20, paddingHorizontal: 15, alignItems: "center" ,justifyContent:"space-between"}}>
                    <View style={{ backgroundColor: "#494c6f", width: '100%', height: '55%', borderRadius: 25, }} />
                    <View style={{ backgroundColor: "#343755", width: '100%', height: '35%', borderTopRightRadius: 25, borderTopLeftRadius: 25 }} />

                </View>
            </View>
        </Modal>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: "center",
        justifyContent: "center",
    },

});