import React from 'react';
import { View, Text, StyleSheet, TouchableOpacity } from "react-native";
import fonts from '../../common/fonts';
import Modal from 'react-native-modal';


export const BlockModelCompo = (props) => {
console.log(props.selectedRowDetail.user.is_blocked)
    return (
        <Modal
            style={{ flex: 1 }}
            backdropOpacity={0.9}
            isVisible={props.showBlockModel}>
            <View style={styles.container}>
                <View style={styles.subContainer}>
                    <View style={{ alignItems: "center", padding: 30 }}>
                        <Text style={styles.confirmText}>Are you sure you want to {props.selectedRowDetail.user.is_blocked == 1 ? 'Unblock' : 'block'}</Text>
                        <Text numberOfLines={1}style={[styles.userName,{textAlign:'center'}]}>{props.selectedRowDetail.user.username}?</Text>
                    </View>
                </View>

                <View style={styles.btnView}>
                    <TouchableOpacity onPress={() => props.hideBlockModel()} style={styles.d_block_btn}>
                        <Text style={styles.blk}>Cancel</Text>
                    </TouchableOpacity>

                    <TouchableOpacity onPress={() => props.onBlock(props.selectedRowDetail)} style={styles.b_btn}>
                        <Text style={styles.blk}>{props.selectedRowDetail.user.is_blocked == 1 ? 'Unblock' : 'Block'}</Text>
                    </TouchableOpacity>
                </View>


            </View>
        </Modal>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: "center",
        justifyContent: "center",
        paddingHorizontal: 10,
    },
    subContainer: {
        backgroundColor: 'white',
        borderRadius: 25,
        width: '100%'
    },
    confirmText: {
        fontFamily: fonts.SFProRegular,
        fontSize: 14,
        color: "#575757"
    },

    userName: {
        fontFamily: fonts.Montserrat_SemiBold,
        fontSize: 18,
        color: "#4b4b4b"
    },

    btnView: {
        flexDirection: "row",
        marginTop: 50,
        justifyContent: "space-between",
        width: "100%"
    },

    d_block_btn: {
        width: '50%',
        alignItems: "center",
        padding: 13,
        justifyContent: "center",
        borderRadius: 30,
        borderWidth: 1,
        borderColor: "#4e4f58"
    },

    b_btn: {
        width: '40%',
        alignItems: "center",
        padding: 13,
        justifyContent: "center",
        borderRadius: 30,
        backgroundColor: "#ff50a1"
    },

    blk: {
        color: 'white',
        fontSize: 16,
        fontFamily: fonts.Montserrat_SemiBold
    },


});