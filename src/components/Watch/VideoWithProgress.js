import React, { Component } from 'react';
import {
    View,
    StyleSheet,
    Dimensions,
    TouchableWithoutFeedback
} from 'react-native';
import ProgressBar from 'react-native-progress/Bar';
import Video from 'react-native-video';



const screenWidth = Dimensions.get('window').width;

export default class VideoWithProgress extends Component {
    constructor(props) {
        super(props);
        this.state = {
            duration: 0,
            currentTime: 0,
            progress: 0
        };

    }

    handleProgress = progress => {
        // if (progress.currentTime) {
        //     console.log( progress.playableDuration)
        //     this.setState({
        //         progress: parseFloat(progress.currentTime / this.state.duration),
        //     });
        // }



        // if (this.state.duration != 6) {
        //     let time = setInterval(() => {
        //         if (this.state.duration <= 6) {
        //             let duration = this.state.duration + 1
        //             this.setState({
        //                 duration,
        //                 progress: duration / 6,
        //             }, () => {
        //                 console.log(this.state.progress, '   ', duration)
        //             });
        //         } else if (this.state.progress > 6) {
        //             this.setState({ progress: 6 })
        //             clearTimeout(time);
        //         }
        //     }, 1000)
        // }


    };

    handleLoad = meta => {
        // this.setState({
        //     progress: 0,
        //     duration: 0,
        //     // duration: meta.duration,
        //     currentTime: meta.currentTime,
        // },()=>{
        //     if (this.state.duration != 6) {
        //         let time = setInterval(() => {
        //             if (this.state.duration <= 6) {
        //                 let duration = this.state.duration + 1
        //                 this.setState({
        //                     duration,
        //                     progress: duration / 6,
        //                 }, () => {
        //                     console.log(this.state.progress, '   ', duration)
        //                 });
        //             } else if (this.state.progress > 6) {
        //                 this.setState({ progress: 6 })
        //                 clearTimeout(time);
        //             }
        //         }, 1000)
        //     }
        // });
    };


    render() {

        return (
            <View style={{ flex: 1, alignItems: 'center' }}>
                <View style={styles.squareView}>
                    <TouchableWithoutFeedback
                        onPress={() => this.props.methodVideoPause(this.props.index)}>
                        <Video
                      
                            source={{
                                uri:
                                    this.props.uri
                            }}
                            ref={ref => {
                                this.player = ref;
                            }}
                            onProgress={this.handleProgress}
                            onLoad={this.handleLoad}
                            repeat
                            resizeMode="cover"
                            style={styles.video}
                            paused={this.props.paused}
                            controls={true}
                        />
                    </TouchableWithoutFeedback>
                </View>
                {/* {this.state.progress ? (
                    <View style={{ flexDirection: 'row' }}>
                        <ProgressBar
                            progress={this.state.progress}
                            color="#5856d6"
                            unfilledColor="#282828"
                            borderColor="transparent"
                            width={screenWidth - 15}
                            borderRadius={0}
                            height={7}
                        />
                        <View style={{ height: 8, width: 15, backgroundColor: '#e1d500' }} />
                    </View>
                ) : (
                        <View
                            style={{
                                width: screenWidth - 8,
                                height: 7,
                                backgroundColor: '#282828',
                            }}
                        />
                    )} */}
            </View>
        )
    }
    componentDidMount = () => {
        this.setState({
            index: this.props.index
        })
    };

}

const styles = StyleSheet.create({

    video: {
        width: screenWidth,
        aspectRatio: 1,
        flex: 1,
    },
    squareView: {
        height: screenWidth,
        width: screenWidth,
        justifyContent: 'center',
    },
});