import React from 'react';
import { View, Text, Image, StyleSheet, TouchableOpacity } from "react-native";
import { Images } from '../../common/Images';
import { Colors } from '../../common/Colors';
import fonts from '../../common/fonts';
import Config from '../../lib/Config';
import ImageLoadView from '../ImageLoadView';


export const UserDetailComponent = (props) => {
    return (
        <View style={styles.container}>
            <TouchableOpacity disabled={props.item.user_id == 0 ? true : false} onPress={() => props.onUserPress(props.item)} style={[styles.subContainerLeft, { flex: 1 }]}>
                {/* <Image
                    resizeMode="cover"
                    source={props.item.profile ? { uri: Config.imageUrl + props.item.profile } : Images.User_profile_icon}
                    style={styles.user}
                /> */}
                <ImageLoadView
                    resizeMode="cover"
                    source={props.item.profile ? { uri: Config.imageUrl + props.item.profile } : Images.User_profile_icon}
                    style={styles.user}
                />
                <View style={{ paddingHorizontal: 5 }}>
                    <Text numberOfLines={1} style={styles.userName}>{"" || props.item.display_name}</Text>
                    <View style={styles.subContainerLeft}>
                        <Image
                            source={Images.inactive_overview}
                            style={styles.earth}
                        />
                        <Text numberOfLines={1} style={styles.country}>{props.item.country == "null" || props.item.country == '' ? "Earth" : props.item.country}</Text>
                    </View>
                </View>

            </TouchableOpacity>

            <TouchableOpacity style={[styles.subContainerRight, { flex: 0.5, alignItems: 'flex-end', marginLeft: 30 }]} onPress = {() =>props.onViewPress(props.item)}>
                <Text numberOfLines={1} style={styles.totalview}>{"" || props.item.total_views}</Text>
                <Text numberOfLines={1} style={styles.views}>Views</Text>
            </TouchableOpacity>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "space-between",
        paddingHorizontal: 10,
        paddingVertical: 8,
        height: 60,
    },
    subContainerLeft: {
        flexDirection: "row",
        alignItems: "center", marginTop: 5
    },
    userName: {
        color: '#E7E7F0',
        fontSize: 15,
        fontFamily: fonts.SFProRegular
    },
    totalview: {
        color: "#E7E7F0",
        fontSize: 15,
        fontFamily: fonts.SFProRegular
    },
    earth: {
        height: 12,
        width: 12,
        borderRadius: 6,
        marginRight: 5
    },
    user: {
        height: 35,
        width: 35,
        borderRadius: 17.5,
        marginRight: 5
    },
    country: {
        color: '#4E5BFD',
        fontSize: 12,
        fontFamily: fonts.SFProRegular
    },
    subContainerRight: {
        alignItems: "flex-end"
    },
    views: {
        color: '#555569',
        fontSize: 12,
        fontFamily: fonts.SFProRegular
    }
});