import React from 'react';
import { View, Text, Image, StyleSheet, TouchableOpacity } from "react-native";
import { Images } from '../../common/Images';
import { Colors } from '../../common/Colors';
import fonts from '../../common/fonts';
import Config from '../../lib/Config';


export const UserDetailShowAds = (props) => {

    return (
        <View style={styles.container}>

            <View  style={styles.subContainerLeft}>
                <Image
                    resizeMode="cover"
                    source={Images.User_profile_icon}
                    style={styles.user}
                />
                <View style={{ paddingHorizontal: 5 }}>
                    <Text style={styles.userName}>Dummy</Text>
                    <View style={styles.subContainerLeft}>
                        <Image
                            source={Images.inactive_overview}
                            style={styles.earth}
                        />
                        <Text style={styles.country}>"Earth"</Text>
                    </View>
                </View>

            </View>

            <View style={styles.subContainerRight}>
                <Text style={styles.userName}>0</Text>
                <Text style={styles.views}>Views</Text>
            </View>
        </View>
        
    )
}

const styles = StyleSheet.create({
    container: {
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "space-between",
        paddingHorizontal: 10,
        paddingVertical: 8,
        height: 60,
    },
    subContainerLeft: {
        flexDirection: "row",
        alignItems: "center", marginTop: 5
    },
    userName: {
        color: Colors.cloudyBlue,   
        fontSize: 15,
        fontFamily: fonts.SFProRegular
    },
    earth: {
        height: 12,
        width: 12,
        borderRadius: 6,
        marginRight: 5
    },
    user: {
        height: 35,
        width: 35,
        borderRadius: 17.5,
        marginRight: 5
    },
    country: {
        color: Colors.purple,
        fontSize: 12,
        fontFamily: fonts.SFProRegular
    },
    subContainerRight: {
        alignItems: "flex-end"
    },
    views: {
        color: Colors.purplishGrey,
        fontSize: 12,
        fontFamily: fonts.SFProRegular
    }
});