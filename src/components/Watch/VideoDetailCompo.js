import React from 'react';
import { View, Text, Image, StyleSheet, TouchableOpacity, TouchableWithoutFeedback } from "react-native";
import { Images } from '../../common/Images';
import { Colors } from '../../common/Colors';
import fonts from '../../common/fonts';
import { MenuContext, Menu, MenuOptions, MenuOption, MenuTrigger } from 'react-native-popup-menu';
import Emojiimages from '../../assets/Themes/Images';
import { color } from 'react-native-reanimated';


export const VideoDetailCompo1 = (props) => {
    // openMenu(){
    //     alert('k')
    //     // this.menu.open();
    //   };

    return (
        <View style={styles.container}>
            <View style={styles.subContainer}>

                <Text style={styles.name}>{props.item.title}</Text>
            </View>
            <View style={styles.subContainer}>
                <Text style={styles.like}>{"" || props.item.total_likes}</Text>

                <MenuContext>
                    {/* <TouchableWithoutFeedback onLongPress={{}}>
                       
                               </TouchableWithoutFeedback> */}

                    <Menu ref={c => ({})}>
                        <MenuTrigger>
                            <Image
                                source={props.item.is_like == 1 ? Images.heart : Images.heart_grey}
                                style={styles.heart}
                            />
                        </MenuTrigger>
                        <MenuOptions>
                            <MenuOption onSelect={() => alert(`Save`)} text="Save" />
                            <MenuOption onSelect={() => alert(`Delete`)}>
                                <Text style={{ color: 'red' }}>Delete</Text>
                            </MenuOption>
                            <MenuOption
                                onSelect={() => alert(`Not called`)}
                                disabled={true}
                                text="Disabled"
                            />
                        </MenuOptions>
                    </Menu>

                </MenuContext>


                {/* <Text style={[styles.like, { marginLeft: 15 }]}>{"" || props.item.total_likes}</Text> */}
                <Text style={[styles.like, { marginLeft: 15 }]}>{props.item.comment_count}</Text>

                <TouchableOpacity onPress={() => props.onChatPress(props.item.id)}>
                    <Image
                        source={Images.chats}
                        style={styles.heart}
                    />
                </TouchableOpacity>
            </View>
        </View>
    )
}

export const VideoDetailCompo = (props) => {
    return (
        <View style={styles.container}>
            <View style={styles.subContainer}>

                <Text numberOfLines={1} style={styles.name}>{props.item.title}</Text>
            </View>
            <View style={styles.subContainer}>
                {props.isEmojiShow &&
                    <View style={styles.subContainer}>
                        <Text style={styles.like}>{"" || props.item.total_likes && props.item.total_likes > 0 ? props.item.total_likes : 0}</Text>
                        <TouchableOpacity onLongPress={() => props.EmojiViewShow(props.index)}
                            onPress={() => props.onLikePress(props.item.id, props.item.is_like > 0 ? 0 : 1, props.index)}>
                            <Image
                                // source={props.item.is_like == 1 ? Images.heart : Images.heart_grey}
                                source={props.likeImage ? props.likeImage : Images.heart_grey}
                                style={styles.heart}
                            />
                        </TouchableOpacity>
                    </View>}

                {/* <Text style={[styles.like, { marginLeft: 15 }]}>{"" || props.item.total_likes}</Text> */}
                <Text style={[styles.like, { marginLeft: 15 }]}>{props.item.comment_count}</Text>

                <TouchableOpacity
                    onPress={() => props.onChatPress(props.item.id)}>
                    <Image
                        source={Images.chats}
                        style={[styles.heart, { tintColor: props.item.is_commented ? '#43B7FF' : '#2A3033' }]}
                    />
                </TouchableOpacity>
            </View>
        </View>
    )
}


export const VideoDetailCompo2 = (props) => {
    return (
        <View style={styles.container}>
            <View style={styles.subContainer}>

                <Text style={styles.name}>HeyRocco - Elsewhere</Text>
            </View>
            <View style={styles.subContainer}>
                <Text style={styles.like}>{"" || props.item.total_likes}</Text>
                <TouchableOpacity onLongPress={() => alert('hi')}
                    onPress={() => props.onLikePress(props.item.id, props.item.is_like == 1 ? 0 : 1, props.index)}>
                    <Image
                        source={props.likeImage}
                        style={styles.heart}
                    />
                </TouchableOpacity>

                {/* <Text style={[styles.like, { marginLeft: 15 }]}>{"" || props.item.total_likes}</Text> */}
                <Text style={[styles.like, { marginLeft: 15 }]}>{props.item.comment_count}</Text>

                <TouchableOpacity
                    onPress={() => props.onChatPress(props.item.id)}>
                    <Image
                        source={Images.chats}
                        style={styles.heart}
                    />
                </TouchableOpacity>
            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "space-between",
        paddingHorizontal: 10,
        paddingVertical: 8,
        height: 60,
        marginHorizontal: 5
    },
    subContainer: {
        flexDirection: "row",
        alignItems: "center"
    },
    name: {
        color: '#E7E7F0',
        fontFamily: fonts.SFProMedium,
        fontSize: 15, width: '75%',
    },
    like: {
        color: '#E7E7F0',
        fontFamily: fonts.SFProRegular,
        fontSize: 15
    },
    heart: {
        height: 20,
        width: 20,
        marginLeft: 5,
        resizeMode: "contain"
    }
});
const arrEmaoji = [{ id: 1, url: 'https://raw.githubusercontent.com/duytq94/facebook-reaction-animation2/master/Images/like.gif', image: Emojiimages.like_static_fill, gif: Emojiimages.like_gif }, { id: 2, url: 'https://raw.githubusercontent.com/duytq94/facebook-reaction-animation2/master/Images/love.gif', image: Emojiimages.love_gif, gif: Emojiimages.love_gif }, { id: 3, url: 'https://raw.githubusercontent.com/duytq94/facebook-reaction-animation2/master/Images/haha.gif', image: Emojiimages.haha_static, gif: Emojiimages.haha_static }, { id: 4, url: 'https://raw.githubusercontent.com/duytq94/facebook-reaction-animation2/master/Images/wow.gif', image: Emojiimages.wow_static, gif: Emojiimages.wow_static }, { id: 5, url: 'https://raw.githubusercontent.com/duytq94/facebook-reaction-animation2/master/Images/sad.gif', image: Emojiimages.sad_static, gif: Emojiimages.sad_gif }, { id: 6, url: 'https://raw.githubusercontent.com/duytq94/facebook-reaction-animation2/master/Images/angry.gif', image: Emojiimages.angry_static, gif: Emojiimages.angry_gif }]
