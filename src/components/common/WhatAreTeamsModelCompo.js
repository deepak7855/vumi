import React, { useState } from 'react';
import { View, Text, StyleSheet, Dimensions, TouchableOpacity, FlatList, Image } from "react-native";
import fonts from '../../common/fonts';
import Modal from 'react-native-modal';
import { Images } from '../../common';

const ScreenWidth = Dimensions.get('window').width

const data = [{ id: 1, value: 'Get friends to enter your username upon sign-up to earn tons of bonus coins!', img: Images.two, heading: "Earn More Coins" },
{ id: 2, value: 'You’ll earn coins for every post they view, over the next two weeks.', img: Images.three, heading: "Teammates Help You Earn" },
{ id: 3, value: 'There’s no limit to how many members you can add! VIP users get even bigger bonuses.', img: Images.one, heading: "Unlimited Mates!" }]

export const WhatAreTeamsModelCompo = (props) => {


    const [activepage, setActivePage] = useState(0)

    const handlePageChange = (e) => {
        var offset = e.nativeEvent.contentOffset;
        if (offset) {
            var page = Math.ceil(offset.x / Dimensions.get('window').width);
            setActivePage(page)
        }
    }

    return (
        <Modal
            backdropOpacity={0.9}
            isVisible={props.showTeamsModel}>
            <View style={styles.container}>
                <View style={styles.blueView}>
                    <View style={styles.titltView}>
                        <Text style={styles.title}>What are teams?</Text>
                        <View style={styles.dotView}>
                            <Text style={styles.textOne}>?</Text>
                        </View>
                    </View>


                    <FlatList
                        horizontal
                        data={data}
                        pagingEnabled
                        showsHorizontalScrollIndicator={false}
                        scrollEventThrottle={16}
                        onMomentumScrollEnd={handlePageChange}
                        renderItem={({ item, index }) => (
                            <View style={{ width: ScreenWidth - 50, alignItems: "center", backgroundColor: "#31298e" }}>

                                <Text style={{ color: 'white', fontFamily: fonts.Poppins_Bold, fontSize: 30, marginVertical: 20, textAlign: "center", width: ScreenWidth - 100 }}>{item.heading}</Text>
                                <Image
                                    style={{ height: 120, width: 120, resizeMode: "contain", }}
                                    source={item.img}
                                />
                                <Text style={{ color: 'white', fontFamily: fonts.SFProDisplay_Semibold, fontSize: 18, marginVertical: 20, textAlign: "center", width: ScreenWidth - 100, }}>{item.value}</Text>

                            </View>
                        )}
                    />
                    <View style={{ flexDirection: "row", justifyContent: "center", paddingVertical: 10, backgroundColor: "#31298e" }}>
                        {
                            data.map((item, index) => (
                                <View style={{ height: 10, width: 10, borderRadius: 5, marginLeft: 5, backgroundColor: activepage == index ? '#b9b7d9' : "#03022b" }} />
                            ))
                        }
                    </View>
                    <TouchableOpacity onPress={() => props.hideTeamsModel()} style={styles.btn}>
                        <Text style={styles.btnTxt}>Cool, I get it</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </Modal>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: "center",
        justifyContent: "center",
    },
    dotView: {
        backgroundColor: "#725cff",
        height: 30,
        width: 30,
        borderRadius: 15,
        justifyContent: "center",
        alignItems: "center",
        marginRight: 10
    },

    textOne: {
        color: "white",
        fontSize: 16,
        fontFamily: fonts.SFProRegular
    },

    blueView: {
        width: ScreenWidth - 50,
        backgroundColor: "#4036b4",
        borderRadius: 25
    },

    titltView: {
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "space-between",
        padding: 15
    },

    title: {
        fontFamily: fonts.Poppins_Bold,
        fontSize: 23,
        color: "white"
    },

    middleBox: {
        height: 150,
        backgroundColor: "#31298e"
    },

    listRow: {
        flexDirection: "row",
        paddingHorizontal: 15,
        paddingVertical: 7
    },

    listText: {
        fontFamily: fonts.Poppins_Medium,
        fontSize: 14,
        color: "white",
        width: '88%'
    },

    btn: {
        backgroundColor: "#9300ff",
        borderBottomLeftRadius: 25,
        borderBottomRightRadius: 25,
        justifyContent: "center",
        alignItems: "center",
        paddingVertical: 15
    },

    btnTxt: {
        color: 'white',
        fontSize: 17,
        fontFamily: fonts.SFProDisplay_Heavy
    }



});