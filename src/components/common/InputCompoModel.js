import React, { useState, useEffect } from 'react';
import { View, Text, TouchableOpacity, Image, TextInput } from "react-native";
import fonts from '../../common/fonts';
import Modal from 'react-native-modal';
import { Images } from '../../common';

export const InputCompoModel = (props) => {

    const [username, setUserName] = useState('')
    const [colorChange, setColorChange] = useState(false)

    const onUserNameInput = (text) => {
        setUserName(text)
        username.length > 5 ? setColorChange(true) : null
    }
    useEffect(() => {
        props.bio ? setUserName(props.bio) : ""
    }, [props]);

    return (
        <Modal
            backdropOpacity={0.9}
            isVisible={props.showInput}>
            <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', }}>
                <View style={{ width: '90%', backgroundColor: 'white', borderRadius: 25, }}>
                    <View style={{ flexDirection: 'row', alignItems: 'center', padding: 10, width: '100%', justifyContent: 'space-between', }}>
                        <View style={{ height: 50, width: 50, }} />
                        <Text style={{ color: "#383841", fontFamily: fonts.Montserrat_SemiBold, fontSize: 18, }}>Enter Bio</Text>
                        <TouchableOpacity onPress={() => { props.hideModel() }}>
                            <Image
                                source={Images.close}
                                style={{ height: 50, width: 50, resizeMode: "contain", }}
                            />
                        </TouchableOpacity>
                    </View>


                    <View style={{ backgroundColor: "#f9f8fa", paddingVertical: 70, alignItems: "center" }}>
                        <TextInput
                            value={username}
                            onChangeText={(text) => onUserNameInput(text)}
                            placeholder="About You"
                            placeholderTextColor="#cacadd"
                            maxLength={256}
                            style={{ borderBottomColor: "lightgrey", borderBottomWidth: 1, width: "80%", fontFamily: fonts.Montserrat_SemiBold, fontSize: 20, color: "#6e6e8b" }}
                        />
                    </View>

                    <View style={{ backgroundColor: "white", padding: 15, alignItems: "center", borderBottomLeftRadius: 25, borderBottomRightRadius: 25 }}>
                        <TouchableOpacity onPress={() => props.onDonePress(username)} disabled={!colorChange} style={{ width: "100%", backgroundColor: colorChange ? '#bd64ff' : "#d6d4dd", justifyContent: "center", alignItems: "center", borderRadius: 25, paddingVertical: 10 }}>
                            <Text style={{ color: "white", fontSize: 18, fontFamily: fonts.SFProDisplay_Heavy }}>Done</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        </Modal>
    )
}

