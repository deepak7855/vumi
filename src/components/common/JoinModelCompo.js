import React, { useState } from 'react';
import { View, Text, TouchableOpacity, Image, TextInput } from "react-native";
import fonts from '../../common/fonts';
import Modal from 'react-native-modal';
import { Images } from '../../common';

export const JoinModelCompo = (props) => {

    const [username, setUserName] = useState('')
    const [colorChange, setColorChange] = useState(false)

    const onUserNameInput = (text) => {
        setUserName(text)
        username.length >= 4 ? setColorChange(true) : setColorChange(false)
    }

    return (
        <Modal
            backdropOpacity={0.9}
            isVisible={props.showJoinModel}>
            <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', }}>
                <View style={{ width: '90%', backgroundColor: 'white', borderRadius: 25, }}>
                    <View style={{ flexDirection: 'row', alignItems: 'center', padding: 10, width: '100%', justifyContent: 'space-between', }}>
                        <View style={{ height: 50, width: 50, }} />
                        <Text style={{ color: "#383841", fontFamily: fonts.Montserrat_SemiBold, fontSize: 18, }}>Join a Team</Text>
                        <TouchableOpacity onPress={() => props.NotJoinTeam()}>
                            <Image
                                source={Images.close}
                                style={{ height: 50, width: 50, resizeMode: "contain", }}
                            />
                        </TouchableOpacity>

                    </View>


                    <View style={{ backgroundColor: "#f9f8fa", paddingVertical: 70, alignItems: "center" }}>
                        <TextInput
                            // value={username}
                            onChangeText={(text) => onUserNameInput(text)}
                            placeholder="Enter a Username"
                            placeholderTextColor="#cacadd"
                            style={{ borderBottomColor: "lightgrey", borderBottomWidth: 1, width: "80%", fontFamily: fonts.Montserrat_SemiBold, fontSize: 20, color: "#6e6e8b" }}
                        />
                    </View>

                    <View style={{ backgroundColor: "white", padding: 15, alignItems: "center", borderBottomLeftRadius: 25, borderBottomRightRadius: 25 }}>
                        <TouchableOpacity onPress={() => props.onJoin(username)} disabled={!colorChange} style={{ width: "100%", backgroundColor: colorChange ? '#bd64ff' : "#d6d4dd", justifyContent: "center", alignItems: "center", borderRadius: 25, paddingVertical: 10 }}>
                            <Text style={{ color: "white", fontSize: 18, fontFamily: fonts.SFProDisplay_Heavy }}>Join</Text>
                        </TouchableOpacity>
                    </View>
                </View>
            </View>
        </Modal>
    )
}

