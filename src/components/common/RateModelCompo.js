import React, { useState } from 'react';
import { View, Text, TouchableOpacity, Image, TextInput } from "react-native";
import fonts from '../../common/fonts';
import Modal from 'react-native-modal';
import { Images } from '../../common';

export const RateModelCompo = (props) => {

    const [rateValue, setRateValue] = useState(0)

    const onRatePress = (value) => {
        props.setShowLRateModel(rateValue)
    }


    return (
        <Modal
            backdropOpacity={0.9}
            isVisible={props.showRateModel}>
            <View style={{ flex: 1, }}>
                <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', }}>
                    <View style={{ width: '90%', backgroundColor: 'white', borderRadius: 25, justifyContent: "space-between", alignItems: "center", paddingVertical: 25 }}>
                        <Text style={{ color: "#383841", fontFamily: fonts.SFProDisplay_Semibold, fontSize: 18, }}>Thanks for using Vumi! </Text>

                        <View style={{ marginVertical: 40, alignItems: "center" }}>
                            <Text style={{ color: "#c0c0d3", fontFamily: fonts.SFProMedium, fontSize: 15, }}>How are we doing?</Text>
                            <View style={{ flexDirection: "row", alignItems: "center" }}>
                                {
                                    [1, 2, 3, 4, 5].map((item) => (<>
                                        <TouchableOpacity style={{ margin: 5 }} onPress={() => setRateValue(item)}>

                                            <Image
                                                source={item <= rateValue ? Images.yellow_star : Images.star_grey}
                                                style={{ height: 25, width: 25, borderRadius: 12.5, resizeMode: "contain", }}
                                            />
                                        </TouchableOpacity>
                                    </>))
                                }
                            </View>


                        </View>
                        <TouchableOpacity onPress={() => onRatePress()} style={{ width: "90%", backgroundColor: "#ff64ff", justifyContent: "center", alignItems: "center", borderRadius: 15, paddingVertical: 10 }}>
                            <Text style={{ color: "white", fontSize: 18, fontFamily: fonts.SFProDisplay_Heavy }}>Rate</Text>
                        </TouchableOpacity>
                    </View>
                    <TouchableOpacity onPress={() => props.setShowLRateModel(false)}>
                        <Image
                            source={Images.grey_close}
                            style={{ height: 50, width: 50, borderRadius: 25, resizeMode: "contain", marginTop: 20 }}
                        />
                    </TouchableOpacity>
                </View>
            </View>
        </Modal >
    )
}

