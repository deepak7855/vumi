import React from 'react';
import { View, Text, StyleSheet, Image, TouchableOpacity } from "react-native";
import fonts from '../../common/fonts';
import Modal from 'react-native-modal';
import { Fontfamily, Images } from '../../common'


export const ResendOtpModelCompo = (props) => {
    return (
        <Modal
            backdropOpacity={0.9}
            isVisible={props.resendOtpModel}>

            <View style={Style.modalContainer}>

                <View style={Style.modalContainer1}>

                    <View style={Style.popUpTop}>
                        <Text style={{ color: 'rgb(90,86,103)', fontFamily: Fontfamily.MB, fontSize: 20 }}>Sent!</Text>
                    </View>

                    <View style={[Style.popUpTop, { height: 200, backgroundColor: 'rgb(242,240,247)', }]}>
                        <Text style={{ fontFamily: Fontfamily.MSB, color: 'rgb(134,131,149)', fontSize: 14 }}>Tap the link send to</Text>
                        <Text style={{ fontFamily: Fontfamily.MSB, color: 'rgb(84,79,100)', fontSize: 25, marginVertical: 8 }}>+{props.country_code} {props.phone}</Text>
                    </View>

                    <TouchableOpacity onPress={() => props.onResend()} style={[Style.popUpTop, { borderBottomRightRadius: 40, borderBottomLeftRadius: 40, backgroundColor: 'rgb(30,206,199)' }]}>
                        <Text style={Style.reSent}>Resend Link</Text>
                    </TouchableOpacity>
                </View>

                <TouchableOpacity style={Style.crosView}
                    onPress={() => { props.onCloseModel(false) }}>
                    <Image
                        source={Images.grey_close}
                        style={{ height: 60, width: 60, resizeMode: "contain" }}
                    />
                </TouchableOpacity>
            </View>

        </Modal>
    )
}

const Style = StyleSheet.create({

    popUpTop: {
        height: 60,
        alignItems: 'center',
        justifyContent: 'center'
    },
    crosView: {
        marginTop: 30, 
    },
    modalContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },

    modalContainer1: {
        borderWidth: 1,
        backgroundColor: 'white',
        width: 280, borderRadius: 40
    },
    reSent: {
        fontFamily: Fontfamily.MEB,
        color: 'rgb(250,248,255)', fontSize: 18
    }


});