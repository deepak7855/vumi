import React, { useState } from 'react';
import { View, Text, TouchableOpacity, Image, TextInput, StyleSheet } from "react-native";
import fonts from '../../common/fonts';
import { Images } from '../../common';
import FastImage from 'react-native-fast-image';
import Config from '../../lib/Config';
import Helper from '../../lib/Helper';
import ImageLoadView from '../../common/ImageLoadView';

export const SavedAndPostRowCompo = (props) => {



    return (
        <View style={styles.rowContainer}>
            <Text style={styles.index}>{props.index}</Text>

            {/* <Image
                source={Images.watch_video}
                style={styles.roundUserImg}
            />

            <Image
                source={Images.watch_video}
                style={styles.userImg}
            /> */}


            <ImageLoadView
                resizeMode={"cover"}
                style={styles.userImg}
                source={props.item.post_detail[0].thumbnail_media ? {
                    uri: Config.imageUrl + props.item.post_detail[0].thumbnail_media,
                    priority: FastImage.priority.normal,
                } : Images.User_profile_icon}
                resizeMode={FastImage.resizeMode.stretch}
            />

            <View style={{ width: "70%", marginLeft: 5, padding: 3 }}>
                <Text numberOfLines={1} style={styles.userName}>{props.item.title}</Text>
                <View style={styles.userContainer}>
                    {/* {
                        props.fromSavedList && <Text style={styles.grp}>Davontae Ray</Text>
                    } */}


                    <View style={styles.statusView}>
                        <View style={styles.statusRow}>
                            <Text style={styles.views}>{Helper.convertValueBillions(props.item.total_views)}</Text>

                            {/* <Text style={styles.views}>30.1K</Text> */}
                            <Image
                                style={styles.ViewIng}
                                source={Images.eye_grey_color}
                            />
                        </View>

                        <View style={styles.statusRow}>
                            <Text style={styles.views}>{Helper.convertValueBillions(props.item.total_likes)}</Text>

                            {/* <Text style={styles.views}>101.7K</Text> */}
                            <Image
                                style={styles.ViewIng}
                                source={Images.heart_grey}
                            />
                        </View>
                    </View>
                </View>
            </View>
            {
                !props.hideRightButton &&
                <TouchableOpacity style={styles.arrowContainer}>
                    <Image
                        source={Images.right_arrow}
                        style={styles.arrowImg}
                    />
                </TouchableOpacity>
            }
            {
                props.draggableIcon &&
                <View style={styles.arrowContainer}>
                    <Image source={Images.dashed} style={{ height: 25, width: 25, resizeMode: "contain" }} />
                </View>
            }

        </View>
    )
}


const styles = StyleSheet.create({
    rowContainer: {
        backgroundColor: "white",
        paddingHorizontal: 10,
        paddingVertical: 15,
        flexDirection: "row"
    },

    userImg: {
        height: 60,
        width: 60,
        resizeMode: "contain"
    },

    roundUserImg: {
        height: 40,
        width: 40,
        borderRadius: 20,
        resizeMode: "cover"
    },

    userName: {
        color: "#4c4c55",
        fontSize: 17,
        fontFamily: fonts.SFProDisplay_Semibold
    },

    grp: {
        color: "#919191",
        fontSize: 12,
        fontFamily: fonts.SFProRegular,
        width: "45%"
    },

    statusView: {
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center",
        width: "55%",
        paddingRight: 10
    },

    statusRow: {
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center"
    },

    views: {
        color: "#919191",
        fontSize: 12,
        fontFamily: fonts.SFProRegular
    },

    ViewIng: {
        height: 10,
        width: 15,
        marginLeft: 5,
        resizeMode: "contain"
    },

    arrowContainer: {
        alignItems: "flex-end",
        justifyContent: "center",
        flex: 1,
        paddingRight: 5
    },

    arrowImg: {
        height: 15,
        width: 8,
        resizeMode: "contain"
    },

    userContainer: {
        flexDirection: "row",
        justifyContent: "space-between"
    },
    index: { marginRight: 10, textAlignVertical: "center", color: "#9d9da5", fontSize: 16, fontFamily: fonts.SFProDisplay_Semibold },
});
