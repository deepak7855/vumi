import React, { useState } from 'react';
import { View, Text, TouchableOpacity, Image, ScrollView } from "react-native";
import fonts from '../../common/fonts';
import Modal from 'react-native-modal';
import { colors, Images } from '../../common';

export const QuestionMarkModel = (props) => {

    return (
        <Modal
            backdropOpacity={0.9}
            isVisible={props.showQuestionModel}>

            <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', }}>
                <View style={{ flex: .8, width: '90%', backgroundColor: 'white', borderRadius: 25, justifyContent: "space-between", alignItems: "center", padding: 25 }}>
                    <Text style={{ color: colors.dblue, fontFamily: fonts.SFProDisplay_Semibold, fontSize: 18, textAlign: 'center', }}>{props.heading}</Text>
                    <ScrollView contentContainerStyle={{ flexGrow: 1,paddingTop: 15, }}>
                        <Text style={{ color: "lightgery", fontFamily: fonts.SFProMedium, fontSize: 15,  }}>{props.content}</Text>
                    </ScrollView>
                </View>
                <TouchableOpacity onPress={() => props.closeQuestionModel()}>
                    <Image
                        source={Images.grey_close}
                        style={{ height: 50, width: 50, borderRadius: 25, resizeMode: "contain", marginTop: 20 }}
                    />
                </TouchableOpacity>
            </View>
        </Modal>
    )
}

