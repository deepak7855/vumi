import React, { useState } from 'react';
import { View, Text, TouchableOpacity, Image, TextInput, ActivityIndicator } from "react-native";
import fonts from '../../common/fonts';
import Modal from 'react-native-modal';
import { Images } from '../../common';
import Config from '../../lib/Config';
import ImageLoadView from '../../common/ImageLoadView';

export const LogoutModelCompo = (props) => {

    return (
        <Modal
            backdropOpacity={0.9}
            isVisible={props.showLogoutModel}>
            <View style={{ flex: 1, }}>
                <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', }}>
                    <View style={{ width: '90%', backgroundColor: 'white', borderRadius: 25, justifyContent: "space-between", alignItems: "center", paddingVertical: 25 }}>
                        <Text style={{ color: "#383841", fontFamily: fonts.Poppins_Medium, fontSize: 15, }}>Are you sure you want to logout?</Text>

                        <View style={{ marginVertical: 40, alignItems: "center" }}>
                            {/* <Image
                                source={props.userData ? { uri: Config.imageUrl + props.userData.user_image } : null}
                                style={{ height: 70, width: 70, borderRadius: 35, resizeMode: "cover" }}
                            /> */}


                            <ImageLoadView
                                resizeMode={'cover'}
                                source={props.userData ? { uri: Config.imageUrl + props.userData.user_image } : null}
                                style={{ height: 70, width: 70, borderRadius: 35, resizeMode: "cover" }}

                            />
                            <Text style={{ color: "#383841", fontFamily: fonts.SFProDisplay_Heavy, fontSize: 22, }}>{props.userData ? props.userData.username : null}</Text>

                        </View>
                        <TouchableOpacity onPress={() => props.setShowLogoutModel(true)} style={{ width: "90%", backgroundColor: "#ff64ff", justifyContent: "center", alignItems: "center", borderRadius: 15, paddingVertical: 10 }}>
                            {props.Loader ? <ActivityIndicator color={'#fff'} size={'small'} /> :
                                <Text style={{ color: "white", fontSize: 18, fontFamily: fonts.SFProDisplay_Heavy }}>Logout</Text>
                            }
                        </TouchableOpacity>
                    </View>
                    <TouchableOpacity onPress={() => props.setShowLogoutModel(false)}>
                        <Image
                            source={Images.grey_close}
                            style={{ height: 50, width: 50, borderRadius: 25, resizeMode: "contain", marginTop: 20 }}
                        />
                    </TouchableOpacity>
                </View>
            </View>
        </Modal >
    )
}

