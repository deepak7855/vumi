import React from 'react';
import { View, Text, StyleSheet, Dimensions, TouchableOpacity } from "react-native";
import fonts from '../../common/fonts';
import Modal from 'react-native-modal';


export const WhatIsTrollModelCompo = (props) => {
    const data = [{ id: 1, value: 'All users are required to pay the community toll upon cashing-in.' },
    { id: 2, value: 'All users are required to pay the community toll upon cashing-in.' },
    { id: 3, value: 'If the toll is high, just continue viewing posts and wait for it to drop back down.' }]
    return (
        <Modal
            backdropOpacity={0.9}

            isVisible={props.trollModel}>
            <View style={styles.container}>
                <View style={styles.blueView}>
                    <View style={styles.titltView}>
                        <Text style={styles.title}>What’s the toll?</Text>
                        <View style={styles.dotView}>
                            <Text style={styles.textOne}>?</Text>
                        </View>
                    </View>

                    <View style={styles.middleBox} />
                    <View style={{ width: '100%', paddingVertical: 10 }}>
                        {
                            data.map((item) => (
                                <View style={styles.listRow}>
                                    <View style={styles.dotView}>
                                        <Text style={styles.textOne}>{item.id}</Text>
                                    </View>
                                    <Text style={styles.listText}>{item.value}</Text>
                                </View>
                            ))
                        }
                    </View>

                    <TouchableOpacity onPress={() => props.hideTrollModel()} style={styles.btn}>
                        <Text style={styles.btnTxt}>Cool, I get it</Text>
                    </TouchableOpacity>
                </View>
            </View>
        </Modal>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: "center",
        justifyContent: "center",
    },
    dotView: {
        backgroundColor: "#725cff",
        height: 30,
        width: 30,
        borderRadius: 15,
        justifyContent: "center",
        alignItems: "center",
        marginRight: 10
    },

    textOne: {
        color: "white",
        fontSize: 16,
        fontFamily: fonts.SFProRegular
    },

    blueView: {
        width: "90%",
        backgroundColor: "#4036b4",
        borderRadius: 25
    },

    titltView: {
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "space-between",
        padding: 15
    },

    title: {
        fontFamily: fonts.Poppins_Bold,
        fontSize: 26,
        color: "white"
    },

    middleBox: {
        height: 150,
        backgroundColor: "#31298e"
    },

    listRow: {
        flexDirection: "row",
        paddingHorizontal: 15,
        paddingVertical: 7
    },

    listText: {
        fontFamily: fonts.Poppins_Medium,
        fontSize: 14,
        color: "white",
        width: '88%'
    },

    btn: {
        backgroundColor: "#9300ff",
        borderBottomLeftRadius: 25,
        borderBottomRightRadius: 25,
        justifyContent: "center",
        alignItems: "center",
        paddingVertical: 15
    },

    btnTxt: {
        color: 'white',
        fontSize: 17,
        fontFamily: fonts.SFProDisplay_Heavy
    }



});