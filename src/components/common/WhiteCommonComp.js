import React, { useState } from 'react';
import { View, Text, TouchableOpacity, Image, TextInput, StatusBar } from "react-native";
import fonts from '../../common/fonts';
import { Images } from '../../common';
import { ProgressBarThird } from '../ProgressBarThird';

export const WhiteCommonComp = (props) => {


    return (

        <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', backgroundColor: "#f0f0f5" }}>
            <StatusBar hidden />
            <Image
                style={{ height: 220, width: 220, resizeMode: "contain" }}
                source={props.img}
            />
            <Text style={{ color: "#c6c6e0", fontSize: 16, fontFamily: fonts.Montserrat_Bold }}>Insert (1) view coin to play</Text>
            <View style={{ width: "80%", paddingVertical: 10, paddingHorizontal: 20 }}>
                <ProgressBarThird
                    height={15}
                    borderRadius={15}
                    total={10000}
                    completed={3750}
                    colorCompleted={'#a0ffe5'}
                    colorUncompleted={'white'}
                />
            </View>
        </View>
    )
}

