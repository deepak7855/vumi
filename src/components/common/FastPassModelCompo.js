import React, { useState } from 'react';
import { View, Text, TouchableOpacity, Image, TextInput } from "react-native";
import fonts from '../../common/fonts';
import Modal from 'react-native-modal';
import { Images } from '../../common';

export const FastPassModelCompo = (props) => {



    return (
        <Modal
            backdropOpacity={0.9}
            isVisible={props.showFastPassModel}>
            <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center', }}>
                <Image
                    style={{ width: 280, height: undefined, aspectRatio: 1, resizeMode: "contain" }}
                    source={Images.fast_pass}
                />
                <TouchableOpacity onPress={() => props.hideFastPassModel('buy')}  style={{ backgroundColor: "#0bd5cc", height: 70, width: 263, justifyContent: "center", alignItems: "center", borderBottomLeftRadius: 25, borderBottomRightRadius: 25, }}>
                    <Text style={{ color: "white", fontSize: 24, fontFamily: fonts.SFProDisplay_Heavy }}>Buy Now</Text>
                    <Text style={{ color: "white", fontSize: 17, fontFamily: fonts.SFProDisplay_Bold }}>$0.99</Text>
                </TouchableOpacity>

                <TouchableOpacity onPress={() => props.hideFastPassModel('cross')}>
                    <Image
                        style={{ height: 50, width: 50, resizeMode: "contain", marginTop: 30 }}
                        source={Images.round_close}
                    />
                </TouchableOpacity>
            </View>
        </Modal>
    )
}

