import React, { Component } from 'react';
import { Text, View, TouchableOpacity, StyleSheet, Image, SafeAreaView } from 'react-native';
// import { AppStyles } from '../../Common/AppStyles'


export class BulletComponent extends Component {
    render() {
        var myloop = [];

        for (let i = 1; i <= this.props.numberof; i++) {
            myloop.push(
                <Text 
                style={(i == this.props.activeIndex) ? 
                    AppStyles.activeBullet : 
                    AppStyles.Bullet}>&#8226;</Text>
            );
        }
        return (
            <View style={{ flexDirection: 'row',justifyContent:'flex-end' }}>
                {myloop}
            </View>



        )
    }
}

const AppStyles = StyleSheet.create({
    activeBullet: {
        fontSize: 28,
        color: 'white',
      
        marginHorizontal: 2
    },
    Bullet: {
        fontSize: 28,
        color: '#9180f4',
     

        // textAlign:'',
        marginHorizontal: 2
    },
})