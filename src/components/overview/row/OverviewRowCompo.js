import React from 'react';
import { View, Text, StyleSheet, Image, TouchableOpacity, FlatList } from "react-native";
import fonts from '../../../common/fonts';
import { Images } from '../../../common';
import Helper from '../../../lib/Helper';
import Config from '../../../lib/Config';
import ImageLoadView from '../../../components/ImageLoadView';
import { Colors } from 'react-native/Libraries/NewAppScreen';


export const OverviewRowCompo = (props) => {
    let item = props.data.data[0]
    // console.log('pro----------- ', item)
    const ImgCompo = ({ img }) => {
        return (
            // <Image
            //     source={img.profile_image ? { uri: Config.imageUrl + img.profile_image } : Images.User_profile_icon}
            //     style={{ height: 25, width: 25, borderRadius: 15, marginHorizontal: 5, resizeMode: 'cover' }}
            // />
            <View style={{ marginHorizontal: 5, alignItems: 'center', justifyContent: 'center' }}>
                <ImageLoadView
                    resizeMode='cover'
                    source={img.profile_image ? { uri: Config.imageUrl + img.profile_image } : Images.User_profile_icon}
                    style={{ height: 25, width: 25, borderRadius: 25 / 2, resizeMode: 'cover', }}

                />
            </View>

        );
    }

    return (
        <>
            <View>
                <TouchableOpacity onPress={() => props.goToDetail(item.id, item.total_likes, item.total_views, item.display_name)}
                    style={{
                        flex: 1,
                        paddingVertical: 15,
                        paddingLeft: 10,
                        backgroundColor: 'white',
                        marginTop: 6,
                        alignItems: 'center'
                    }}>

                    <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center' }}>

                        <View style={{ flex: 1, alignItems: 'center', }}>
                            <ImageLoadView
                                source={
                                    item &&
                                        item.post_detail.length > 0 &&
                                        item.post_detail[0].thumbnail_media ? { uri: Config.imageUrl + item.post_detail[0].thumbnail_media }
                                        :
                                        Images.User_profile_icon
                                }
                                style={{ height: 80, width: 80, resizeMode: "cover", }}

                            />

                        </View>

                        <View style={{ flex: 3, marginRight: 10, alignItems: 'center', }}>
                            <View style={{
                                justifyContent: 'space-between',
                                flex: 1,
                                flexDirection: 'row',
                                alignItems: 'center',

                            }}>

                                <View style={{ flex: 1, flexDirection: 'row', alignItems: 'center', }}>
                                    <View style={{ flex: 5 }}>
                                        <View style={{ flex: 1, flexDirection: 'row', justifyContent: 'space-between' }}>
                                            <View style={{ flex: 1, }} >
                                                <Text numberOfLines={1} style={{ color: "#4c4c55", fontFamily: fonts.Montserrat_SemiBold, fontSize: 17, paddingLeft: 5 }}>{item.display_name}</Text>
                                            </View>

                                        </View>


                                        <View style={{ flexDirection: 'row', alignItems: 'center', paddingLeft: 5 }}>
                                            <Image
                                                resizeMode='contain'
                                                source={item.post_status == 'Active' ? Images.blew_eye : Images.overView.eye_grey}
                                                // source={Images.overView.eye_grey}

                                                style={{ height: 18, width: 18, marginRight: 3 }}
                                            />
                                            <Text style={{ color: "#7987a2", fontSize: 13 }}>{"" || item.total_views}</Text>

                                            <View style={{ marginHorizontal: 15, flexDirection: 'row', padding: 3, alignItems: 'center' }}>
                                                <Image
                                                    resizeMode='contain'
                                                    source={item.post_status == 'Active' ? Images.heart : Images.overView.heart_grey}
                                                    // source={Images.overView.heart_grey}

                                                    style={{ height: 15, width: 15, marginHorizontal: 3 }}
                                                />
                                                <Text style={{ color: "#7987a2", fontSize: 13 }}>{"" || item.total_likes}</Text>
                                            </View>
                                            <View style={{ flexDirection: 'row', padding: 3, alignItems: 'center' }}>
                                                {/* <Image
                                                    resizeMode='contain'
                                                    source={item.post_status == 'Active' ? Images.chats : Images.chats}
                                                    // source={Images.overView.heart_grey}
                                                    style={{ height: 15, width: 15, marginHorizontal: 3 }}
                                                /> */}
                                                <Image
                                                    resizeMode='contain'
                                                    source={Images.chats}
                                                    style={{ height: 15, width: 15, marginHorizontal: 3, tintColor: item.post_status == 'Active' ? '#43B7FF' : '#2A3033' }}
                                                />
                                                <Text style={{ color: "#7987a2", fontSize: 13 }}>{"" || item.comments_count}</Text>
                                            </View>
                                        </View>

                                    </View>

                                    {/* <TouchableOpacity onPress={() => props.goToDetail(item.id, item.total_likes, item.total_views,item.display_name)} style={{ flex: 1, alignItems: 'flex-end' }}>
                                    <Image
                                        source={Images.right_arrow}
                                        style={{ height: 15, width: 15, resizeMode: "contain", marginHorizontal: 5 }}
                                    />
                                </TouchableOpacity> */}

                                    <View
                                        // onPress={() => props.goToDetail(item.id, item.total_likes, item.total_views, item.display_name)}
                                        style={{ flex: 1, alignItems: 'flex-end', marginTop: 30 }}>
                                        <Image
                                            source={Images.right_arrow}
                                            style={{ height: 15, width: 15, resizeMode: "contain", marginHorizontal: 5 }}
                                        />
                                    </View>
                                    {item.total_views != item.views ?
                                        <TouchableOpacity
                                            onPress={() => props.gotoRefunt(item.id)}
                                            style={{ alignItems: 'center', justifyContent: 'center', top: 1, right: 1, position: 'absolute', zIndex: 1, backgroundColor: '#edeff5', width: 68, height: 23, borderRadius: 6, borderColor: 'rgb(234,245,254),', borderWidth: 1.5 }}>
                                            <Text style={{ fontSize: 10, color: 'rgb(179,192,216)', fontFamily: fonts.Poppins_Medium }}>REFUND</Text>
                                        </TouchableOpacity>
                                        :
                                        null
                                    }
                                </View>
                            </View>

                            <View style={{ flexDirection: 'row', alignItems: "center", paddingBottom: 1 }}>
                                <View style={{ width: '88%' }}>
                                    <FlatList
                                        horizontal
                                        // scrollEnabled={false}
                                        showsHorizontalScrollIndicator={false}
                                        data={item.recent_views}
                                        renderItem={({ item }) => <ImgCompo img={item} />}
                                        keyExtractor={item => item.id}
                                    />
                                </View>

                                {/* <TouchableOpacity  onPress={() => props.onShowPackageInfo(props.index)} style={{ flex: 1, alignItems: 'flex-end' }}>
                                <Image
                                    source={props.selecetdIndex == props.index ? Images.up_grey : Images.down_grey}
                                    style={{ height: 15, width: 15, resizeMode: "contain", marginHorizontal: 5 }}
                                />
                            </TouchableOpacity> */}
                            </View>
                        </View>
                    </View>
                </TouchableOpacity>
            </View>

        </>
    )

}

const styles = StyleSheet.create({


});

