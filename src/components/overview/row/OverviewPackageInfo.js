import React, { Component } from 'react';
import { View, Text, Image, ImageBackground } from 'react-native';
import { ProgressBar } from '../../ProgressBar';
import { Images } from '../../../common';
import Helper from '../../../lib/Helper';
import fonts from '../../../common/fonts';


class OverviewPackageInfo extends Component {
    constructor(props) {
        super(props);
        this.state = {
        };
    }

    render() {
         return (
            <View style={{ paddingVertical: 10, justifyContent: "space-around", borderTopColor: '#edeff5', flexDirection: "row", backgroundColor: 'white', alignItems: 'center', paddingHorizontal: 5 }}>
                {/* <ImageBackground source={Images.packs[this.props.index % Images.packs.length]} style={{ height: 48, width: 55, resizeMode: "contain", justifyContent: 'center', alignItems: 'center' }}
                imageStyle={{borderRadius:10}}>
                    <Text style={{ fontSize: 18, fontFamily: fonts.Montserrat_Bold, color: 'white' }}>{Helper.convertValueBillions(this.props.item.views)}</Text>
                </ImageBackground> */}
                <View
                    style={{ height: 48, width: 55, resizeMode: "contain", justifyContent: 'center', alignItems: 'center', backgroundColor: this.props.item && this.props.item.post_package ? this.props.item.post_package.color_code : '#41FFAf', borderRadius: 10 }}
                >
                    <Text style={{ fontSize: 18, fontFamily: fonts.Montserrat_Bold, color: 'white' }}>{Helper.convertValueBillions(this.props.item.views)}</Text>
                </View>
                <View style={{ width: "80%" }}>
                    <ProgressBar animationview={this.props.animationview} total_views={this.props.item.views} points={this.props.item.total_views} completed={44} colorCompleted={this.props.item && this.props.item.post_package ? this.props.item.post_package.color_code : '#41FFAf'} colorUncompleted={'#eaedf4'} />
                </View>
            </View>
        );
    }
}

export default OverviewPackageInfo;
