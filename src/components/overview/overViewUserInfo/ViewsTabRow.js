import React from 'react';
import { View, Text, StyleSheet, Image, TouchableOpacity } from "react-native";
import moment from 'moment';
import fonts from '../../../common/fonts';
import { Images } from '../../../common';
import Config from '../../../lib/Config';
import ImageLoadView from '../../ImageLoadView';


export const ViewsTabRow = (props) => {

    // console.log("this props------------- ", props.item)
    // moment.utc(item.created_at).local().fromNow()

    return (
        <View style={{ padding: 10, paddingVertical: 15, flexDirection: "row" }}>
            <TouchableOpacity onPress={() => props.onUserImagePress(props.item)} style={{ flexDirection: "row", alignItems: "flex-end" }}>
             
                 <ImageLoadView
                    resizeMode='cover'
                    source={props.item ? { uri: Config.imageUrl + props.item.user.profile_image } : Images.dummy_user}
                    style={styles.userImage}
                />
                {
                    props.fromViews && props.item?.user?.view_count > 0 &&
                    <View style={styles.badge}>
                        <Text style={{ fontSize: 7, color: "white" }}>{props.item?.user?.view_count}</Text>
                    </View>
                }
            </TouchableOpacity>
            <View style={{ marginLeft: 8, width: "50%" }}>
                <Text style={styles.userName}>{props.item ? props.item.user.username : 'username*'}</Text>
                <View style={{ flexDirection: "row", alignItems: "center" }}>
                    <Text style={styles.time}>{moment.utc(props.item.created_at).local().fromNow()}</Text>
                    {/* <View style={styles.dot} />
                    <Text style={styles.sec}>0:27</Text> */}
                </View>
            </View>
            <View style={{ width: "30%", flexDirection: "row", flexWrap: "wrap", justifyContent: "flex-end" }}>
                {
                    props.item.user.favorite_count > 0 &&
                    <Image
                        source={Images.yellow_star}
                        style={styles.smallImg}
                    />
                }
                {
                    props.item.user.comment_count > 0 &&
                    <TouchableOpacity onPress={() => props.onCommentIconPress(props.item.post_id)}>
                        <Image
                            source={Images.chats}
                            style={styles.smallImg}
                        />
                    </TouchableOpacity>
                }
                {
                    props.item.user.like_count > 0 &&
                    <Image
                        source={Images.heart}
                        style={styles.smallImg}
                    />
                }

                {
                    props.item.user.views > 0 &&
                    <Image
                        source={Images.blew_eye}
                        style={styles.smallImg}
                    />
                }

                {/* <Image
                    source={Images.emails}
                    style={styles.smallImg}
                />
                <Image
                    source={Images.eye}
                    style={styles.smallImg}
                />

                <Image
                    source={Images.ticks}
                    style={styles.smallImg}
                /> */}

            </View>
        </View>
    )
}

const styles = StyleSheet.create({


    userImage: { height: 54, width: 54, borderRadius: 54/2, resizeMode: "cover",borderWidth: 0.5  },
    badge: { height: 18, width: 18, borderRadius: 9, backgroundColor: "#2c70ed", borderColor: 'white', borderWidth: 2, zIndex: 1, marginLeft: -13, justifyContent: "center", alignItems: "center" },
    userName: { color: "#62626f", fontSize: 17, fontFamily: fonts.Poppins_Bold },
    time: { color: "#4c4c55", fontSize: 12, fontFamily: fonts.Poppins_Medium, opacity: 0.5 },
    dot: { height: 3, width: 3, borderRadius: 1.5, backgroundColor: "#4c4c55", marginHorizontal: 5, opacity: 0.5 },
    sec: { color: "#4c4c55", fontSize: 12, fontFamily: fonts.Poppins_Medium, opacity: 0.5 },
    smallImg: { height: 17, width: 17, resizeMode: "contain", margin: 3.5 },
});