import React from 'react';
import { View, Text, StyleSheet, Image, TouchableOpacity } from "react-native";

import fonts from '../../../common/fonts';
import { Images } from '../../../common';
import Helper from '../../../lib/Helper';
import Config from '../../../lib/Config';


export const CommentTabRow = (props) => {

    return (
        <View style={{ padding: 10, paddingVertical: 15, flexDirection: "row", alignItems: "flex-start" }}>
            <TouchableOpacity onPress={() => props.onUserImagePress(props.item)} style={{ flexDirection: "row", alignItems: "flex-end" }}>
                <Image
                    source={props.item ? { uri: Config.imageUrl + props.item.user.profile_image } : Images.dummy_user}
                    resizeMode={'cover'}
                    style={styles.userImage}
                />
                {
                    props.item?.user?.comment_count > 0 &&
                    <View style={styles.badge}>
                        <Text style={{ fontSize: 7, color: "white" }}> {props.item?.user?.comment_count}</Text>
                    </View>
                }
            </TouchableOpacity>
            <View style={{ marginLeft: 8, width: "50%" }}>
                <Text style={styles.userName}>{props.item.user_name}</Text>
                <Text style={styles.comment}>{props.item.comment}</Text>
            </View>
            <View style={{ width: "30%", flexDirection: "row", flexWrap: "wrap", justifyContent: "flex-end" }}>
                {
                    props.item.user.favorite_count > 0 &&
                    <Image
                        source={Images.yellow_star}
                        style={styles.smallImg}
                    />
                }
                {
                    props.item.user.comment_count > 0 &&
                    <TouchableOpacity onPress={() => props.onCommentIconPress(props.item.post_id)}>
                        <Image
                            source={Images.chats}
                            style={styles.smallImg}
                        />
                    </TouchableOpacity>
                }
                {
                    props.item.user.like_count > 0 &&
                    <Image
                        source={Images.heart}
                        style={styles.smallImg}
                    />
                }

                {
                    props.item.user.views > 0 &&
                    <Image
                        source={Images.blew_eye}
                        style={styles.smallImg}
                    />
                }
            </View>
        </View>
    )
}

const styles = StyleSheet.create({


    userImage: { height: 54, width: 54, borderRadius: 27, },
    badge: { height: 18, width: 18, borderRadius: 9, backgroundColor: "#2c70ed", borderColor: 'white', borderWidth: 2, zIndex: 1, marginLeft: -13, justifyContent: "center", alignItems: "center" },
    userName: { color: "#62626f", fontSize: 17, fontFamily: fonts.Poppins_Bold },
    time: { color: "#4c4c55", fontSize: 12, fontFamily: fonts.Poppins_Medium, opacity: 0.5 },
    dot: { height: 3, width: 3, borderRadius: 1.5, backgroundColor: "#4c4c55", marginHorizontal: 5, opacity: 0.5 },
    sec: { color: "#4c4c55", fontSize: 12, fontFamily: fonts.Poppins_Medium, opacity: 0.5 },
    smallImg: { height: 17, width: 17, resizeMode: "contain", margin: 3.5 },
    comment: { color: "#96989f", fontSize: 14, fontFamily: fonts.Poppins_Medium },
});