import React, { Component } from 'react';
import { View, Text } from 'react-native';
import fonts from '../common/fonts';

export const ProgressBarThird = (props) => {
    return (
        <View style={{ flex: 1, }} >
            <View style={{ width: "100%", backgroundColor: props.colorUncompleted, height: props.height || 25, borderRadius: props.borderRadius || 0 }} >
                <View style={{ width: (props.completed / props.total * 100) + "%", backgroundColor: props.colorCompleted, height: props.height || 25, borderRadius: props.borderRadius || 0 }} >

                </View>
            </View>

        </View>
    )
}