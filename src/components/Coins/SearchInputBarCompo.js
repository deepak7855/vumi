import React from 'react';
import { View, Text, Image, StyleSheet } from 'react-native';
import { Images, IconInput } from '../../common';


export const SearchInputBarCompo = (props) => {
    return (
        <View >
            <IconInput
                limagePath={Images.coins1.search}
                style={{ height: 40, fontSize: 12, }}
                placeholder={props.placeholder ? props.placeholder : ""}
                placeholderTextColor="#dcdce2"
                onChangeText={props.onChangeText}
                width={props.width ? props.width : 320}
                value={props.value}
            />

        </View>
    )
}

const styles = StyleSheet.create({
    container: {

    }
});