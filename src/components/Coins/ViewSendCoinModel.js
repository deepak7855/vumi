import React from 'react';
import { View, Text, Image, StyleSheet, Dimensions, TouchableOpacity } from "react-native";
import { Images } from '../../common/Images';
import fonts from '../../common/fonts';
import Modal from 'react-native-modal';

export const ViewSendCoinModel = (props) => {
    return (
        <Modal
            backdropOpacity={0.9}
            isVisible={props.showViewSendModel}>
            <View style={styles.container}>
                <Text style={{ color: "white", fontFamily: fonts.SFProDisplay_Bold, fontSize: 20, }}> View Coin sent! </Text>

                <TouchableOpacity style={styles.crosView}
                    onPress={() => { props.hideViewModel() }}>
                    <Image
                        source={Images.grey_close}
                        style={{ height: 60, width: 60, resizeMode: "contain" }}
                    />
                </TouchableOpacity>
            </View>
        </Modal>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: "center",
        justifyContent: "center",
    },
    crosView: {
        marginTop: 30,
    },
});