import React, { useState } from 'react';
import { View, Text, Image, StyleSheet, TouchableOpacity } from 'react-native';
import { Images, IconInput, Fontfamily } from '../../common';



export const SearchConditionsCompo = (props) => {

    const [selectedTab, setSelectedTab] = useState('')

    const onItemClick = (type) => {
        setSelectedTab(type)
        props.onSearchTypeChange(type)
    }

    return (
        <View style={{ flexDirection: 'row', width: "100%", backgroundColor: "white",paddingVertical: 14,shadowOpacity:1,shadowRadius:1 ,shadowColor:"#fff" }}>
            <View style={{ width: '30%', marginHorizontal: 20 }}>
                <Text style={[styles.tabtxtstyle, { color: 'rgb(82,82,90)' }]}>Transfers</Text>
            </View>

            <View style={{ width: '50%', justifyContent: 'space-between', flexDirection: 'row', marginHorizontal: 20, paddingLeft: props.fromCoins ? 20 : '20%', }}>
                <TouchableOpacity onPress={() => onItemClick("")}><Text style={[styles.tabtxtstyle, { color: props.selectedType == '' ? 'rgb(96,139,255)' : 'rgb(216,217,219)' }]}>ALL</Text></TouchableOpacity>
                <TouchableOpacity onPress={() => onItemClick("IN")}><Text style={[styles.tabtxtstyle, { color: props.selectedType == 'IN' ? 'rgb(96,139,255)' : 'rgb(216,217,219)' }]}>IN</Text></TouchableOpacity>
                <TouchableOpacity onPress={() => onItemClick("OUT")}><Text style={[styles.tabtxtstyle, { color: props.selectedType == 'OUT' ? 'rgb(96,139,255)' : 'rgb(216,217,219)' }]}>OUT</Text></TouchableOpacity>
                {
                    props.fromCoins && <TouchableOpacity onPress={() => onItemClick("MIND")}><Text style={[styles.tabtxtstyle, { color: props.selectedType == 'MIND' ? 'rgb(96,139,255)' : 'rgb(216,217,219)' }]}>MINED</Text></TouchableOpacity>
                }

            </View>
        </View>
    )
}

const styles = StyleSheet.create({
    tabtxtstyle: {
        fontSize: 15,
        color: 'rgb(216,217,219)',
        fontFamily: Fontfamily.PB

    },
});