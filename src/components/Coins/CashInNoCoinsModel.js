import React from 'react';
import { View, Text, Image, StyleSheet, Dimensions, TouchableOpacity } from "react-native";
import { Images } from '../../common/Images';
import fonts from '../../common/fonts';
import Modal from 'react-native-modal';
import { Colors } from '../../common/Colors';

const screenWidth = Dimensions.get('window').width;

export const CashInNoCoinsModel = (props) => {
    return (
        <Modal
            backdropOpacity={0.9}
            onBackdropPress={() => props.showNoCoinsModel(false)}
            isVisible={props.modelVisible}>
            <View style={styles.container}>
                <View style={{ backgroundColor: "#212439", height: 140, width: 140, borderRadius: 20, alignItems: "center", justifyContent: "center", marginBottom: 20 }}>
                    <Image
                        style={{ height: 42, width: 65, resizeMode: "contain" }}
                        source={Images.eye_grey}
                    />
                </View>
                <Text style={{ color: '#a1a1b2', fontSize: 22, fontFamily: fonts.SFProRegular }}>You need at least <Text style={{ color: "#dedee8", fontFamily: fonts.Montserrat_SemiBold }}> 500 coins </Text> </Text>
                <Text style={{ color: '#a1a1b2', fontSize: 22, fontFamily: fonts.SFProRegular }}> to redeem your views. </Text>




            </View>
            <TouchableOpacity style={{ backgroundColor: "#07b2ab", paddingVertical: 10, justifyContent: "center", alignItems: "center", borderRadius: 25, marginTop: 20, width: "100%" }}>
                <Text style={{ color: "#ededed", fontSize: 20, fontFamily: fonts.Montserrat_ExtraBold }}>Watch & Earn Coins</Text>
            </TouchableOpacity>
        </Modal>
    )
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: "center",
        justifyContent: "center",
    },

});