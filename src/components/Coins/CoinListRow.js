import React from 'react';
import { View, Text, Image, StyleSheet, TouchableOpacity } from 'react-native';
import { Images, colors } from '../../common';
import fonts from '../../common/fonts';
import Helper from '../../lib/Helper';
import moment from 'moment';
import ImageLoadView from '../../components/ImageLoadView';


// export const CoinListRow1 = (props) => {
//     return (
//         <View style={styles.container}>
//             <View style={styles.redDot} />
//             <Image
//                 resizeMode='contain'
//                 source={Images.dummy_user}
//                 style={styles.user}
//             />
//             <View style={{ marginHorizontal: 9 }}>
//                 <View style={{ flexDirection: "row" }}>
//                     <Text style={styles.userName}>You Sent Ariana 106</Text>
//                     <Text style={styles.vc}>vc</Text>
//                 </View>
//                 <View style={{ flexDirection: "row", alignItems: "center" }}>
//                     <Text style={styles.subTitle}>4h ago</Text>
//                     <View style={styles.greyDot} />
//                     <Text style={styles.subTitle}>Thnx for ur support</Text>
//                 </View>
//             </View>
//         </View>
//     )
// }


export const CoinListRow = (props) => {
    if (props.item) {
        let dicUser = {}
        let showLabel = ""
        if (props.item.user_id == 0) {
            dicUser = props.item.admin_detail
            showLabel = "You Receive"
        } else {
            if (props.item.user.id == Helper.userInfo.id) {
                dicUser = props.item.otheruser
                showLabel = "You Sent"
            }
            else {
                dicUser = props.item.user
                showLabel = "You Receive"
            }
        }


        return (
            <TouchableOpacity disabled={props.item.user_id == 0 ? true : false} onPress={() => props.onRowPress()} style={[styles.container, { backgroundColor: 'white' }]}>
               { props.item.is_read != Number(1) ?   <View style={styles.redDot} />:null}
                { props.item.user_id == 0 ?
                    <TouchableOpacity style={{
                        height: 50,
                        width: 50,
                        borderRadius: 50 / 2,
                        resizeMode: 'cover',
                    }} >
                        <ImageLoadView
                            resizeMode='cover'
                            source={dicUser.profile_picture ? { uri: Helper.getImageUrl(dicUser.profile_picture) } : Images.User_profile_icon}
                            style={styles.user}
                        />
                    </TouchableOpacity>
                    :
                    <TouchableOpacity style={{
                        height: 50,
                        width: 50,
                        borderRadius: 50 / 2,
                        resizeMode: 'cover',
                    }} onPress={() => props.onUserImagePress(props.item)}>
                        <ImageLoadView
                            resizeMode='cover'
                            source={dicUser.profile_image ? { uri: Helper.getImageUrl(dicUser.profile_image) } : Images.User_profile_icon}
                            style={styles.user}
                        />
                    </TouchableOpacity>
                }
                <TouchableOpacity disabled={props.item.user_id == 0 ? true : false} onPress={() => props.onRowPress()} style={{ flex: 1, marginHorizontal: 9, }}>
                    <View style={{ flexDirection: "row" }}>
                        {props.item.user_id == 0 ?
                            < Text style={styles.adminuserName}>{showLabel + " " + dicUser.name + " " + props.item.coins}</Text>
                            :
                            < Text style={styles.userName}>{showLabel + " " + dicUser.username + " " + props.item.coins}</Text>
                        }
                        <Text style={styles.vc}>vc</Text>
                    </View>
                    <View style={{ flex: 1, flexDirection: "row", alignItems: "center", }}>

                        <Text style={props.item.user_id == 0 ? styles.subTitle1 : styles.subTitle}>{moment.utc(props.item.created_at).local().fromNow()}</Text>
                        <View style={styles.greyDot} />
                        <Text numberOfLines={1} style={[styles.subBlackTitle, { flex: 1 }]}>{props.item.note}</Text>
                    </View>
                    {/* <Text numberOfLines={1} style={styles.subBlackTitle}>{props.item.note}</Text> */}
                </TouchableOpacity>
            </TouchableOpacity >
        )
    }

}




export const RecentCoinListRow = (props) => {
    // console.log(props,"RecentCoinListRow")
    const item = props.item
    return (
        <View>
            {props.item && <TouchableOpacity
                onPress={() => props.onRowPress()}
                style={styles.container}>
                <View style={styles.redDot} />
                {props.item &&
                    <View style={{
                        height: 50,
                        width: 50,
                        borderRadius: 50 / 2,
                        resizeMode: 'cover',
                    }}
                    // onPress={() => props.onUserImagePress(props.item)}>
                    >
                        <ImageLoadView
                            resizeMode='cover'
                            source={{ uri: Helper.getImageUrl(item.profile_image) }}
                            style={styles.user}
                        />
                    </View>
                }
                {props.item && <View
                    // onPress={() => props.onRowPress()}
                    style={{ marginHorizontal: 9, }}>
                    <View style={{ flexDirection: "row" }}>
                        <Text style={styles.userName}>{item.username}</Text>
                        {/* <Text style={styles.vc}>vc</Text> */}
                    </View>
                    {props.item && <View style={{ flexDirection: "row", alignItems: "center" }}>
                        <Text style={styles.subTitle}>{moment.utc(item.send_date).local().fromNow()}</Text>
                        {props.item && <View style={styles.greyDot} />}
                    </View>}
                    {props.item && <Text numberOfLines={2} style={styles.subBlackTitle}>{item.note}</Text>}

                </View>}
            </TouchableOpacity>
            }
        </View>
    )
}

const styles = StyleSheet.create({
    container: {
        flexDirection: 'row',
        flex: 1,
        alignItems: 'center',
        paddingVertical: 7,
        paddingLeft: 20
    },
    subTitle: {
        // color: "#4c4c55",
        color: colors.blackgrey,
        fontSize: 12,
        fontFamily: fonts.SFProRegular

    },
    subTitle1: {
        // color: "#4c4c55",
        color: colors.dblue,
        fontSize: 12,
        fontFamily: fonts.SFProRegular

    },
    subBlackTitle: {
        color: colors.blackgrey,
        fontSize: 12,
        fontFamily: fonts.SFProRegular

        // width: '65%'
    },
    redDot: {
        height: 9,
        width: 9,
        borderRadius: 4.5,
        backgroundColor: "#ff526c",
        marginRight: -3,
        zIndex: 1,
        borderColor: 'white',
        borderWidth: 1
    },

    user: {
        height: 50,
        width: 50,
        borderRadius: 50 / 2,
        resizeMode: 'cover'
    },

    userName: {
        color: "#4c4c55",
        fontSize: 15,
        fontFamily: fonts.SFProMedium
    },
    adminuserName: {
        color: "#4c4c55",
        fontSize: 15,
        fontFamily: fonts.Montserrat_SemiBold
    },
    vc: {
        fontSize: 9,
        color: "#4c4c55"
    },

    greyDot: {
        height: 3,
        width: 3,
        borderRadius: 1.5,
        backgroundColor: "#4c4c55",
        opacity: 0.3,
        marginHorizontal: 5
    }

});