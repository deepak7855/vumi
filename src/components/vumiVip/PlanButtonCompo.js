import React, { useState } from 'react';
import { View, Text, TouchableOpacity, Image, TextInput, StyleSheet } from "react-native";
import fonts from '../../common/fonts';
import Modal from 'react-native-modal';
import { Images } from '../../common';

export const PlanButtonCompo = (props) => {

    // const [plan, setplan] = useState(props.selectedPlan)


    const onPlanSelect = (type, name) => {
        // setplan(name)
        props.onPlanSelect(type, name)
    }

    return (

        <View style={{ width: "99%", marginHorizontal: 1 }}>
            <View style={[styles.bestDealView, { display: props.packageId == props.selectedPlan ? "flex" : 'none', backgroundColor: props.planColor }]}>
                <Text style={styles.bestDealText}>{props.planName}</Text>
            </View>
            <TouchableOpacity onPress={() => onPlanSelect(props.packageId)} style={[styles.planView, { backgroundColor: props.packageId == props.selectedPlan ? props.activeBgColor : "#f8f8f8", }]}>
                <Text style={[styles.mnth, { color: props.packageId == props.selectedPlan ? "white" : '#a7a6a9', }]}>{props.month} Months</Text>
                <Text style={[styles.planAmt, { color: props.packageId == props.selectedPlan ? "#e1d6ff" : '#a7a6a9', }]}>${props.price}/mo</Text>
            </TouchableOpacity>
        </View>
    )
}

const styles = StyleSheet.create({
    bestDealView: { height: 30, justifyContent: "center", alignItems: "center", borderTopRightRadius: 5, borderTopLeftRadius: 5, marginTop: -30, zIndex: 1 },
    bestDealText: { color: "white", fontSize: 11, fontFamily: fonts.Poppins_Bold, },
    planView: { width: "100%", justifyContent: "center", alignItems: "center", height: 100 },
    mnth: { fontSize: 18, fontFamily: fonts.Poppins_Bold },
    planAmt: { fontSize: 11, fontFamily: fonts.Poppins_Medium },
});