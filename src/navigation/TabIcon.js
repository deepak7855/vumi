import React, { useEffect, useState } from 'react';
import PropTypes from 'prop-types';
import { Text, Image } from 'react-native';
import { Images } from '../common';
import Helper from '../lib/Helper';
import Config from '../lib/Config';
import { View } from 'react-native';


const propTypes = {
    focused: PropTypes.bool,
    title: PropTypes.string,
    imag: PropTypes.string
};

const defaultProps = {
    focused: false,
    title: '',
};

const TabIcon = props => {

    const [profile_image, set_user_image] = useState(null)

    useEffect(() => {
        Helper.getDataFromAsync('userInfo').then((userInfo) => {
            set_user_image(userInfo.profile_image)
        })
    });

    return (
        <>
            {
                props.navigation.state.key == 'Me' ?
                    props.focused ?
                        <Image
                            resizeMode={'cover'}
                            source={{ uri: Config.imageUrl + profile_image }}
                            style={{ height: 24, width: 24, marginTop: 15, borderRadius: 14 }}
                        />
                        :
                        <Image
                        
                            blurRadius={2}
                            resizeMode={'cover'}
                            source={{ uri: Config.imageUrl + profile_image }}
                            style={{ height: 24, width: 24, marginTop: 15, borderRadius: 14 }}
                        />


                    :
                    <Image
                        resizeMode={'contain'}
                        source={props.focused ? props.Selcted : props.UnSelcted}
                        style={props.styl ? props.styl : { height: 24, width: 24, marginTop: 15 }}
                    />
            }

            <Text style={{}}>{props.title}</Text>
        </>
    )
}


TabIcon.propTypes = propTypes;
TabIcon.defaultProps = defaultProps;

export default TabIcon;