import {
    SignUpStep3,
    Verification,
    Password,
    SignUp,
    ForgotPassword,
    Login,
    GetStarted,
    Referral,
    Profile,
    Coins1,
    OverView,
    Notification,
    NotificationList,
    Watch,
    Game,
    Setting, AddCards, UploadPost,
    ChangePassword
} from '../Screens';
import Splash from '../Screens/Splash';
import React from 'react';
import { Platform, StyleSheet, } from 'react-native';

import {
    Scene,
    Router,
    Overlay,
    ActionConst,
    Tabs,
    Stack,
} from 'react-native-router-flux';

import TabIcon from './TabIcon'
import { Images } from '../common';
import Comments from '../Screens/Comments';
import RecentSendCoins from '../Screens/RecentSendCoins';
import CashIn from '../Screens/CashIn';
import SendCoin from '../Screens/SendCoin';
import { YourBonus } from '../Screens/YourBonus';
import OverviewUserInfo from '../Screens/OverviewUserInfo';
import Teams from '../Screens/Teams';
import SavedList from '../Screens/SavedList';
import PostsList from '../Screens/PostsList';
import Saved from '../Screens/Saved';
import ChannelOptions from '../Screens/ChannelOptions';
import VumiVip from '../Screens/VumiVip';
import Kickstarter from '../Screens/Kickstarter';
import BonusBox from '../Screens/BonusBox';
import BlockedUsers from '../Screens/BlockedUsers';
import HowToGetViews from '../Screens/HowToGetViews';
import Faq from '../Screens/Faq';
import Messages from '../Screens/Messages';
import MessageSearchList from '../Screens/MessageSearchList';

import WhatIsVumiVip from '../Screens/WhatIsVumiVip';
import EditProfile from '../Screens/EditProfile';
import Chat from '../Screens/Chat';
import CameraGallery from '../components/photoVideo/CameraGallery';
import CreatePost from '../components/photoVideo/CreatePost';
import CropImage from '../components/photoVideo/CropImage';
import PreviewPost from '../components/photoVideo/PreviewPost';



import camera from '../Screens/camera';
import { CreatePassword } from '../Screens/CreatePassword';
import TestScreen from '../Screens/TestScreen';
// import VideoTrimmer from '../Screens/VideoTrimmer';
import Coin from '../Screens/Coin';
import LikesList from '../Screens/LikesList';
import ViewsList from '../Screens/ViewsList';
import CoinOld from '../Screens/CoinOld';
import Helper from '../lib/Helper';




const stateHandler = (prevState, newState, action) => {

};
const prefix = Platform.OS === 'android' ? 'mychat://mychat/' : 'mychat://';


export const Routes = () => (
    <Router
        onStateChange={stateHandler}
        uriPrefix={prefix}>
        <Overlay key="overlay">


            <Scene hideNavBar panHandlers={null}>
                <Stack key="Auth" title="Auth" hideNavBar>
                    <Scene key="Splash" component={Splash} />
                    <Scene key="GetStarted" component={GetStarted} />
                    <Scene key="Login" component={Login} />
                    <Scene key="SignUp" component={SignUp} />
                    <Scene key="Notification" component={Notification} />
                    <Scene key="Password" component={Password} />
                    <Scene key="Referral" component={Referral} />
                    <Scene key="SignUpStep3" component={SignUpStep3} />
                    <Scene key="ForgotPassword" component={ForgotPassword} />
                    <Scene key="Verification" component={Verification} />
                    <Scene key="YourBonus" component={YourBonus} />
                    <Scene key="CreatePassword" component={CreatePassword} />

                </Stack>

                <Tabs
                    key="TabBar"
                    routeName="TabBar"
                    hideNavBar
                    showLabel
                    activeTintColor="#ffffff"
                    inactiveTintColor="#4a41bf"

                    // activeBackgroundColor=''
                    // inactiveBackgroundColor='#00022'
                    tabBarStyle={styles.tabBarStyle}>

                    <Stack
                        initial
                        key="Watch"
                        title=""
                        hideNavBar
                        // navigationBarStyle={{}}
                        icon={TabIcon}
                        Selcted={Images.active_watch}
                        UnSelcted={Images.tv}
                    >

                        <Scene key="Watch" component={Watch} initial />
                        <Scene key="Comments" component={Comments} hideNavBar hideTabBar />
                        <Scene key="Chat" component={Chat} hideNavBar hideTabBar />
                        <Scene key="ViewsList" component={ViewsList} hideNavBar hideTabBar />

                    </Stack>


                    <Stack key="Coins"
                        hideNavBar
                        inactiveBackgroundColor="#FFF"
                        activeBackgroundColor="#DDD"
                        icon={TabIcon}
                        Selcted={Images.active_coin}
                        UnSelcted={Images.inactive_coin}
                    >
                        {/* <Scene key="Coin" component={Coin} title="Coins" initial/> */}
                        <Scene key="CoinOld" component={CoinOld} title="CoinOld" />
                        <Scene key="RecentSendCoins" component={RecentSendCoins} title="RecentSendCoins" />
                        <Scene key="SendCoin" component={SendCoin} title="SendCoin" />
                        <Scene key="CashIn" component={CashIn} title="CashIn" hideNavBar hideTabBar />
                        <Scene key="Chat" component={Chat} hideNavBar hideTabBar />
                    </Stack>


                    <Stack
                        key="Overview"
                        title=""
                        hideNavBar
                        inactiveBackgroundColor="#FFF"
                        activeBackgroundColor="#DDD"
                        icon={TabIcon}
                        Selcted={Images.active_overview}
                        UnSelcted={Images.inactive_overview}
                    >

                        <Scene key="OverView" component={OverView} title="OverView" type={ActionConst.RESET} />
                        <Scene key="OverviewUserInfo" component={OverviewUserInfo} hideNavBar hideTabBar />
                        <Scene key="CashIn" component={CashIn} title="CashIn" hideNavBar hideTabBar />
                        <Scene key="UploadPost" component={UploadPost} hideNavBar hideTabBar />
                        <Scene key="CameraGallery" component={CameraGallery} hideNavBar hideTabBar />
                        <Scene key="CreatePost" component={CreatePost} hideNavBar hideTabBar />
                        <Scene key="CropImage" component={CropImage} hideNavBar hideTabBar />
                        <Scene key="PreviewPost" component={PreviewPost} hideNavBar hideTabBar />
                        <Scene key="Comments" component={Comments} hideNavBar hideTabBar />
                        <Scene key="Chat" component={Chat} hideNavBar hideTabBar />
                        <Scene key="camera" component={camera} hideNavBar hideTabBar />
                        {/* <Scene key="VideoTrimmer" component={VideoTrimmer} /> */}

                    </Stack>




                    <Stack
                        key="Games"
                        title=""
                        hideNavBar
                        icon={TabIcon}
                        Selcted={Images.active_games}
                        UnSelcted={Images.games}

                    >
                        <Scene key="Game" component={Game} />
                    </Stack>


                    <Stack
                        key="Me"
                        title=""
                        hideNavBar
                        icon={TabIcon}
                        Selcted={Images.dummy_user}
                        UnSelcted={Images.dummy_user}

                    >

                        <Scene key="Profile" component={Profile} />
                        <Scene key="Setting" component={Setting} hideNavBar hideTabBar />
                        <Scene key="Messages" component={Messages} hideNavBar hideTabBar />
                        <Scene key="Chat" component={Chat} hideNavBar hideTabBar />
                        <Scene key="NotificationList" component={NotificationList} hideNavBar hideTabBar />
                        <Scene key="Saved" component={Saved} />
                        <Scene key="SavedList" component={SavedList} hideNavBar hideTabBar />
                        <Scene key="PostsList" component={PostsList} hideNavBar hideTabBar />
                        <Scene key="CashIn" component={CashIn} title="CashIn" hideNavBar hideTabBar />

                        <Scene key="ChannelOptions" component={ChannelOptions} hideNavBar hideTabBar />
                        <Scene key="VumiVip" component={VumiVip} hideNavBar hideTabBar />
                        <Scene key="Kickstarter" component={Kickstarter} hideNavBar hideTabBar />
                        <Scene key="BonusBox" component={BonusBox} hideNavBar hideTabBar />
                        <Scene key="BlockedUsers" component={BlockedUsers} hideNavBar hideTabBar />
                        <Scene key="HowToGetViews" component={HowToGetViews} hideNavBar hideTabBar />
                        <Scene key="Faq" component={Faq} hideNavBar hideTabBar />
                        <Scene key="Teams" component={Teams} hideNavBar hideTabBar />
                        <Scene key="WhatIsVumiVip" component={WhatIsVumiVip} hideNavBar hideTabBar />
                        <Scene key="EditProfile" component={EditProfile} hideNavBar hideTabBar />
                        <Scene key="CameraGallery" component={CameraGallery} hideNavBar hideTabBar />
                        <Scene key="CreatePost" component={CreatePost} hideNavBar hideTabBar />
                        <Scene key="CropImage" component={CropImage} hideNavBar hideTabBar />
                        <Scene key="PreviewPost" component={PreviewPost} hideNavBar hideTabBar />

                        <Scene key="camera" component={camera} hideNavBar hideTabBar />
                        {/* <Scene key="VideoTrimmer" component={VideoTrimmer} /> */}
                        <Scene key="ChangePassword" component={ChangePassword} hideNavBar hideTabBar />


                        <Scene key="UploadPost" component={UploadPost} hideNavBar hideTabBar />
                        <Scene key="AddCards" component={AddCards} hideNavBar hideTabBar />
                        <Scene key="MessageSearchList" component={MessageSearchList} hideNavBar hideTabBar />
                        <Scene key="LikesList" component={LikesList} title="Likes" hideNavBar hideTabBar />
                        <Scene key="ViewsList" component={ViewsList} hideNavBar hideTabBar />

                    </Stack>
                </Tabs>
            </Scene>

        </Overlay>
    </Router>
)
const styles = StyleSheet.create({

    tabBarStyle: {
        backgroundColor: '#5e5ce6',
        height: 50,

    },
});
