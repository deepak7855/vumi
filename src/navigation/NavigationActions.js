import { Actions } from 'react-native-router-flux';


const pushToScreen = (destinationScene, props) => {
    // console.log('push', destinationScene, Actions.currentScene, props);
    if (Actions.currentScene === destinationScene) {
        return;
    }
    return Actions[destinationScene](props);
};

export default pushToScreen;